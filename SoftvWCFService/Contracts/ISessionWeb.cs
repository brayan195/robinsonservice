﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Softv.Entities;
using SoftvWCFService.Entities;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISessionWeb
    {

        #region inserts
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddVendedor(string nombre, string domicilio, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddespacioDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddespacioDepartamento(string nombre, bool? suma, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddAmenitie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddAmenitie(string desc, bool? activo);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTorre", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTorre(torre obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTorre", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTorre(torre obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTorreById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        torre GetTorreById(int? clv_torre);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddVista", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddVista(string desc, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Addusuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Addusuario(UsuarioEntity usuario, VendedorEntity vendedor);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDepartamento(DepartamentoEntity obj, List<DepartamentoEspacioEntity> espacios);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddComprador", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddComprador(string nombre, string domicilio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEstado(string nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddColonia(ColoniaEntity colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCalle(ColoniaEntity calle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddLocalidad(localidad obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddProyecto(ProyectoEntity proyecto, List<TorreEntity> torres, List<InfoMobiliti> amenities, List<ServiciosEntity> servicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEstatusDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEstatusDepartamento(int? piso, string tipoDeDepartamento, int? numeroDepartamento, string fechaEntrega, string estatus, int? clv_vendedor, int? clv_comprador, float? precioListaVenta
            , float? precioActual, float? precioVendido, string fechaApartado, float? importeApartado, string fechaVenta, float? importeEnganche, float? porcentaje,
            int? numeroDePagosEnganche, string fechaEscrituracion, string notario, int? clv_torre, bool activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCliente(ClienteEntity obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClientesActivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? UpdateClientesActivo(ClienteEntity obj);
        #endregion

        #region gets
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedorN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVendedorN(string nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVista", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVista();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVendedor();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UsuarioEntity> GetUsuarios();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuariosnN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoUser> GetUsuariosnN(string nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVistaN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVistaN(string nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSpaceDept", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoDepto> GetSpaceDept();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSpaceDeptN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoDepto> GetSpaceDeptN(string nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAmenitie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetAmenitie();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedorC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVendedorC(int? clv_vendedor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuariosC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UsuarioEntity GetUsuariosC(int? clv_usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVistaC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVistaC(int? clv_vista);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAmenitieC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetAmenitieC(int? clv_amenitie);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSpaceDeptC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoDepto> GetSpaceDeptC(int? clv_espacio_departamento);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAmenitieN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetAmenitieN(string nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDepartamentos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DepartamentoEntity> GetDepartamentos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTorre", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<torre> GetTorre();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTorreC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<torre> GetTorreC(int? clv_torre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCompradores", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoComprador> GetCompradores();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoPais> GetEstado();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoPais> GetColonia();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaPaisCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoPais> GetColoniaPaisCiudad(int? clv_pais, int? clv_ciudad);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalleColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoPais> GetCalleColonia(int? clv_colonia);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCalle(ColoniaEntity calle);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCallesById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<InfoPais> GetCallesById(int? clv_calle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoPais> GetCalles();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<localidad> GetLocalidad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadoC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoPais> GetEstadoC(int? clv_estado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColonioaC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ColoniaEntity> GetColonioaC(int? clv_colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<localidad> GetLocalidadById(int? clv_localidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDepartamentoC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DepartamentoEntity> GetDepartamentoC(int? clv_departamento);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEspaciosDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DepartamentoEspacioEntity> GetEspaciosDepartamento(int? clv_departamento);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoProyecto> GetProyecto();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProyectoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProyectoEntity GetProyectoById(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoClientes GetClienteById(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstatusDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EstatusDepartamentoEntity GetEstatusDepartamento(int? IdDetalle);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //List<InfoClientes> GetCliente(int? usuario);


        #endregion

        #region Update

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateVendedor(string nombre, int? clv_vendedor, string domicilio, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateVista", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateVista(string nombre, int? clv_vista, string descripcion, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateAmenitie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateAmenitie(string nombre, int? clv_amenitie, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateUsuarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateUsuarios(UsuarioEntity usuario, VendedorEntity vendedor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateSDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateSDepartamento(string nombre, int? clv_espacio, bool? suma, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Autenticacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoUser Autenticacion();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateComprador", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoComprador UpdateComprador(string nombre, int? clv_comprador, string domicilio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEstado(string nombre, int? clv_estado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateColonia(ColoniaEntity colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateLocalidad(localidad obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDepartamentos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDepartamentos(DepartamentoEntity obj, List<DepartamentoEspacioEntity> espacios);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "UpdateProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? UpdateProyecto(ProyectoEntity proyecto, List<InfoMobiliti> amenities);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCliente(ClienteEntity obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEstatusDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEstatusDepartamento(EstatusDepartamentoEntity obj, List<InfoMobiliti> vistas, List<DepartamentoEnganchePagoEntity> pagos, List<NivelesEntity> nivel);
        #endregion

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTiposUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<tipoUsuario> GetTiposUsuario();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetStatusById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<tipoUsuario> GetStatusById(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetStatus", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<tipoUsuario> GetStatus();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTiposUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTiposUsuario(tipoUsuario obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeletePerfil", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string DeletePerfil(tipoUsuario obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        string DeleteCiudad(tipoUsuario obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Addstatus", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Addstatus(tipoUsuario obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTiposUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTiposUsuario(tipoUsuario obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateStatus", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? UpdateStatus(tipoUsuario obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddModelo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddModelo(tipoUsuario obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateModelo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? UpdateModelo(tipoUsuario obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModeloById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<tipoUsuario> GetModeloById(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModelosByMarca", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<tipoUsuario> GetModelosByMarca(int? clv_marca);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModelo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<tipoUsuario> GetModelo();



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ObtenModulosUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Modulo> ObtenModulosUsuario(int? tipoUsuario, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoUsuarioById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        tipoUsuario GetTipoUsuarioById(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMunicipios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<municipios> GetMunicipios();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMunicipio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMunicipio(municipios obj);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMarca", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? AddMarca(Marcas obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Addfirmware", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? Addfirmware(Marcas obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMarcaModelo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Marcas> GetMarcaModelo(int? clv_modelo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFirmwareMarcaModelo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Infomodelo> GetFirmwareMarcaModelo(int? clv_firmware);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMarca", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<Marcas> GetMarca();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFirmware", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<Marcas> GetFirmware();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCiudad(municipios obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMunicipiosById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<municipios> GetMunicipiosById(int? clv_municipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFirmwareById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Marcas> GetFirmwareById(int? clv_firmware);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMarcaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Marcas> GetMarcaById(int? clv_marca);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMunicipio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateMunicipio(municipios obj);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMarcaModelo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? UpdateMarcaModelo(Marcas obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateFirmMarcaModelo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateFirmMarcaModelo(Marcas obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMunicipiosByEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<municipios> GetMunicipiosByEstado(int? clv_estado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadesByMunicipio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<localidad> GetLocalidadesByMunicipio(int? clv_municipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniasByLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ColoniaEntity> GetColoniasByLocalidad(int? clv_localidad, int? clv_municipio);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ObtenModulos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Modulo> ObtenModulos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GuardaPermisos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GuardaPermisos(List<Modulo> modulos, int? tipoUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstatusDepartamentoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EstatusDepartamentoEntity> GetEstatusDepartamentoList(int? clv_vendedor, int? clv_proyecto, string torre, int? piso, int? estatus, string NumDepartamento, int? clv_vista);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTorresProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<string> GetTorresProyecto(int? clv_proyecto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPisosTorre", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<int> GetPisosTorre(string torre, int? clv_proyecto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstatusVentaDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EstatusVentaDepartamentoEntity> GetEstatusVentaDepartamento();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ServiciosEntity> GetServicios();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddServicio(string nombre_Servicio, string descripcion, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServicioC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ServiciosEntity> GetServicioC(int? clv_servicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateServicio(int? clv_servicio, string nombre_Servicio, string descripcion, bool? activo);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServicioProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ServiciosEntity> GetServicioProyecto(int? clv_proyecto);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCotizacion(

             int? Clv_cliente,
            int? Clv_vendedor,
             decimal? Cetes,
             int? Multiplicador,
             decimal? Reservacion,
             decimal? Escrituracion,
             int? IdDetalle,
            Double? EngancheInicial,
            Double? EngancheAdicional,
            Double? EngancheDiferido,
            Double? Escrituras,
            int? MesesDiferidos,
            int? Op


            );




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CotizacionEntity> GetCotizacion(int? clv_cotizacion, string fechaCotizacion, string fechaExpiracion, string cliente, int? op, long? clv_usuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddProspecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ProspectoEntity> AddProspecto(string Nombre, string Direccion, string Correo, string Telefono);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateProspecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateProspecto(int? Clv_prospecto, string Nombre, string Direccion, string Correo, string Telefono);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProspecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ProspectoEntity> GetProspecto();



        [WebInvoke(Method = "*", UriTemplate = "GetProspectoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ProspectoEntity> GetProspectoById(int? Clv_Prospecto);

        [WebInvoke(Method = "*", UriTemplate = "AddConfiguracionSistema", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddConfiguracionSistema(float Cetes, int? MultiplicadorDescuentos, int? MultiplicadorFinanciamiento,
        int? Expiracion, string Folio, string Serie, float IVA, float InteresAtraso, float Escrituracion, float Reservacion, float DescuentoEnganche);

        [WebInvoke(Method = "*", UriTemplate = "GetConfiguracionSistema", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ConfiguracionEntity> GetConfiguracionSistema();

        [WebInvoke(Method = "*", UriTemplate = "GetConfiguracionSistemaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ConfiguracionEntity> GetConfiguracionSistemaById(int? @id);

        [WebInvoke(Method = "*", UriTemplate = "UpdateConfiguracionSistema", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateConfiguracionSistema(int? id, float? Cetes, int? MultiplicadorDescuentos, int? MultiplicadorFinanciamiento,
        int? Expiracion, string Folio, string Serie, float IVA, float InteresAtraso, float Escrituracion, float Reservacion, float DescuentoEnganche);

        [WebInvoke(Method = "*", UriTemplate = "AddPlanPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPlanPago(float? engancheInicial, float? engancheDiferido, float? escrituras);

        [WebInvoke(Method = "*", UriTemplate = "GetPlanPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PlanpagoEntity> GetPlanPago();

        [WebInvoke(Method = "*", UriTemplate = "GetPlanPagoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PlanpagoEntity> GetPlanPagoById(int? id);

        [WebInvoke(Method = "*", UriTemplate = "UpdatePlanPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePlanPago(int? id, float? engancheInicial, float? engancheDiferido, float? escrituras);

        //[WebInvoke(Method = "*", UriTemplate = "ReporteCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //string ReporteCotizacion(CotizacionEntity ObjReporte);


        [WebInvoke(Method = "*", UriTemplate = "AddEnganche", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? AddEnganche(decimal? Precio_inicial, decimal? Monto_enganche_Inicial_sobre_precio, decimal? Monto_enganche_diferido, decimal? Monto_enganche_Total,
        decimal? Monto_Financiamiento_sobre_engachediferido, decimal? Monto_enganche_diferido_mas_financiomiento, decimal? Monto_liq_a_la_escrituracion,
        decimal? Precio_total_del_departamento, decimal? Precio_por_M2, int? id_cotizacion);

        [WebInvoke(Method = "*", UriTemplate = "UpdateEnganche", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEnganche(int? Id_Enganche_Diferido, decimal? Precio_inicial, decimal? Monto_enganche_Inicial_sobre_precio, decimal? Monto_enganche_diferido, decimal? Monto_enganche_Total,
        decimal? Monto_Financiamiento_sobre_engachediferido, decimal? Monto_enganche_diferido_mas_financiomiento, decimal? Monto_liq_a_la_escrituracion,
        decimal? Precio_total_del_departamento, decimal? Precio_por_M2, int? id_cotizacion);

        [WebInvoke(Method = "*", UriTemplate = "DeleteRelDepartamentoCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelDepartamentoCliente(int? clv_comprador, int? Clv_departamento);

        [WebInvoke(Method = "*", UriTemplate = "GetTipoVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipoVendedorEntity> GetTipoVendedor();        


        [WebInvoke(Method = "*", UriTemplate = "GetrelDepClienteById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EstatusDepartamentoEntity> GetrelDepClienteById(int? clv_comprador);

        [WebInvoke(Method = "*", UriTemplate = "AddRelacionUsuarioUD", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelacionUsuarioUD(List<int> departamentos, int? IdUsuario);


        [WebInvoke(Method = "*", UriTemplate = "DeleteRelacionUD", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelacionUD(int? Id);

        [WebInvoke(Method = "*", UriTemplate = "GetRelacionUD", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<relUsuarioDepartamentoEntity> GetRelacionUD(int? IdUsuario);





        [WebInvoke(Method = "*", UriTemplate = "GetClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ClientesEntity> GetClientes(int? clv_usuario, int? clv_estatus, string nombre, string celular, string pais);

        [WebInvoke(Method = "*", UriTemplate = "AddMotivosCancelacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMotivosCancelacion(Motivos_de_cancelacionEntity motivos);

        [WebInvoke(Method = "*", UriTemplate = "EditMotivosCancelacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? EditMotivosCancelacion(Motivos_de_cancelacionEntity motivos);

        [WebInvoke(Method = "*", UriTemplate = "GetMotivosCancelacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Motivos_de_cancelacionEntity> GetMotivosCancelacion();

        [WebInvoke(Method = "*", UriTemplate = "GetMotivosCancelacionById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Motivos_de_cancelacionEntity GetMotivosCancelacionById(int? Id);



        [WebInvoke(Method = "*", UriTemplate = "addConceptoDePago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? addConceptoDePago(string Clv_txt, string Descripcion, bool Activo, bool CobroAutomatico, Decimal Monto);

        [WebInvoke(Method = "*", UriTemplate = "updateConceptoDePago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? updateConceptoDePago(int Id, string Clv_txt, string Descripcion, bool Activo, bool CobroAutomatico, Decimal Monto);

        [WebInvoke(Method = "*", UriTemplate = "getConceptosDePago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<conceptosDePagoEntity> getConceptosDePago();

        [WebInvoke(Method = "*", UriTemplate = "getConceptosDePagoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        conceptosDePagoEntity getConceptosDePagoById(int id);


        [WebInvoke(Method = "*", UriTemplate = "GetBancos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<bancoEntity> GetBancos();

        [WebInvoke(Method = "*", UriTemplate = "addBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? addBanco(string nombre, bool? activo);

        [WebInvoke(Method = "*", UriTemplate = "updateBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? updateBanco(int? id, string nombre, bool activo);

        [WebInvoke(Method = "*", UriTemplate = "GetBancoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bancoEntity GetBancoById(int? id);


        [WebInvoke(Method = "*", UriTemplate = "GetCuentasBancarias", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<cuentaBancariaEntity> GetCuentasBancarias();

        [WebInvoke(Method = "*", UriTemplate = "AddCuentaBancaria", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCuentaBancaria(int banco, string cuenta, string CLABE, bool? activo);

        [WebInvoke(Method = "*", UriTemplate = "getCuentaBancariaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        cuentaBancariaEntity getCuentaBancariaById(int? id);

        [WebInvoke(Method = "*", UriTemplate = "updateCuentaBancaria", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? updateCuentaBancaria(int? id, int banco, string cuenta, string CLABE, bool? activo);


        [WebInvoke(Method = "*", UriTemplate = "GetCotizacionDescuento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CotizacionEntity GetCotizacionDescuento(
              decimal? EngancheInicial,
            decimal? EngancheDiferido,
            decimal? EngancheAdicional,
            decimal? LiquidacionEscrituracion,
            Decimal? PrecioMetro,
            Decimal? MetrosDepartamento,
            String FechaEntrega,
            int? Op
            );


        [WebInvoke(Method = "*", UriTemplate = "GetCotizacionDiferido", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CotizacionEntity GetCotizacionDiferido(
            decimal? EngancheInicial,
            decimal? EngancheDiferido,
            decimal? LiquidacionEscrituracion,
            String FechaEntrega,
            decimal? PrecioMetro,
            decimal? MetrosDepartamento
            );

        [WebInvoke(Method = "*", UriTemplate = "GetCotizacionById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CotizacionEntity GetCotizacionById(int? clv_cotizacion);

        [WebInvoke(Method = "*", UriTemplate = "UpdateCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCotizacion(
             int? clv_cotizacion,
            decimal? Reservacion,
            int? Escrituracion,
            int? EngancheInicial,
            int? EngancheDiferido,
            int? Escrituras,
            int? MesesDiferidos
            );

        [WebInvoke(Method = "*", UriTemplate = "AddContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddContrato(int Clv_cotizacion, string bodega, string cajones, string FechaReservacion, string FechaEscrituracion, decimal? MontoContrato);

        [WebInvoke(Method = "*", UriTemplate = "GetContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ContratoEntity> GetContrato(long? contrato, string nombre, string segundoNombre, string ApellidoPaterno, string ApellidoMaterno, int? clv_usuario, string Estatus);


        [WebInvoke(Method = "*", UriTemplate = "ReporteContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ReporteContrato(int? Opexportacion, int? contrato);

        [WebInvoke(Method = "*", UriTemplate = "ReporteAceptacionDeCompra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ReporteAceptacionDeCompra(int? Opexportacion, int? cotizacion, int? Op1);


        [WebInvoke(Method = "*", UriTemplate = "GetReporteCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteCotizacion(int? Op, int? id, string url);

        [WebInvoke(Method = "*", UriTemplate = "GetContratoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratoEntity GetContratoById(long? contrato);

        [WebInvoke(Method = "*", UriTemplate = "cancelarCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? cancelarCotizacion(int id, string Estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstatusDepartamentoListByIdVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EstatusDepartamentoEntity> GetEstatusDepartamentoListByIdVendedor(int? Clv_vendedor, int? clv_proyecto, string torre, int? piso, int? estatus);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstatusDepartamentoListaDisponible", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EstatusDepartamentoEntity> GetEstatusDepartamentoListaDisponible(int? clv_proyecto, string torre, int? piso, int? estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesWEB", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ClientesEntity> GetClientesWEB();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddClientesWEB", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddClientesWEB(int id, int clv_vendedor, int clv_usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddAgenda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddAgenda(string Tipo, string Fecha, int? Clv_vendedor, string Observaciones, int? Clv_cliente, int? accion, string ObservacionesConCliente);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMotivoClienteDescartado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMotivoClienteDescartado(string IdMotivo, string Observaciones, string Fecha, int? IdVendedor, int? IdCliente);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelUsuarioProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelUsuarioProyecto(int idUsuario, int idProyecto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelUsuarioProyectoDisponibles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<relUsuarioProyectoEntity> GetRelUsuarioProyectoDisponibles(int idUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelUsuarioProyectoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<relUsuarioProyectoEntity> GetRelUsuarioProyectoById(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelUsuarioProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelUsuarioProyecto(int id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAllProyectos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<relUsuarioProyectoEntity> GetAllProyectos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddBodegaCajon", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddBodegaCajon(List<BodegaCajonEntity> obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCajon", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCajon(List<BodegaCajonEntity> obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBodegasCajonesByIdDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<BodegaCajonEntity> GetBodegasCajonesByIdDepartamento(int? IdDetalle, string Nivel, int Tipo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCajonesByIdDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<BodegaCajonEntity> GetCajonesByIdDepartamento(int? IdDetalle, string Nivel, int Tipo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPalnosTipo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PlanosBodega> GetPalnosTipo(int? clv_proyecto, string Nivel, int Tipo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AsignacionAutomatica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AsignacionAutomatica();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAgenda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<AgendaEntity> GetAgenda(int idUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAgendaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        AgendaEntity GetAgendaById(int id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateAgenda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateAgenda(int id, string Tipo, string Fecha, int? Clv_vendedor, string Observaciones, int? Clv_cliente, int accion, string ObservacionesConCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "CancelarAgenda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? CancelarAgenda(int id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoImagen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipoImagen> GetTipoImagen();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTipoVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTipoVendedor(int? Id, string Descripcion, bool? Activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTipoVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTipoVendedor(string Descripcion, bool? Activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAgendasByUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<AgendaEntity> GetAgendasByUsuario(int? clv_usuario, String Fecha1, String Fecha2, int? folio, string tipo, string status);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImagenesDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> GetImagenesDepartamento();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetHistorialObs", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<AgendaEntity> GetHistorialObs(int clv_usuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ReporteCartaAgradecimientoNocompra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ReporteCartaAgradecimientoNocompra(int? clv_cliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEstatusDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteEstatusDepartamento(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteResumenEstatusDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteResumenEstatusDepartamento(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDepartamentosApartadosVendidos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteDepartamentosApartadosVendidos(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus, string fechaInicio, string fechaFin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteListadoClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteListadoClientes(int? Opexportacion, List<UsuarioEntity> usuarios, string fecha_inicio, string fecha_fin, int? clv_estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteListadoClientesResumen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteListadoClientesResumen(int? Opexportacion, List<UsuarioEntity> usuarios, int? clv_estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAgendasByCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<AgendaEntity> GetAgendasByCliente(int clv_cliente, String Fecha1, String Fecha2);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstatusCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Estatus_Cliente> GetEstatusCliente();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteLlamadasVisitasPendientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteLlamadasVisitasPendientes(int? Opexportacion, string fechaInicio, string fechaFin, List<UsuarioEntity> usuarios, int tipoAgenda);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteLlamadasVisitasEjecutadas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteLlamadasVisitasEjecutadas(int? Opexportacion, string fechaInicio, string fechaFin, List<UsuarioEntity> usuarios, int tipoAgenda);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateFechaExpiracion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateFechaExpiracion(int id, string fecha);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCotizacionByCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CotizacionEntity> GetCotizacionByCliente(int? Clv_cliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetrelProyectoAmenitie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelAmenitieEntity> GetrelProyectoAmenitie(int clv_proyecto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteAmenitieProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteAmenitieProyecto(int id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateProyecto(ProyectoEntity proyecto, List<TorreEntity> torres, List<InfoMobiliti> amenities, List<ServiciosEntity> servicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetrelProyectoServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelServicioEntity> GetrelProyectoServicio(int clv_proyecto);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteServicioProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteServicioProyecto(int id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteTorreProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteTorreProyecto(int clv_proyecto, string Torre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMotivoCacelacionPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MotivosCancelacionFacturas> GetMotivoCacelacionPago();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNivel", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<NivelesEntity> GetNivel();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNivelbyDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<relNivelesDepartamentoEntity> GetNivelbyDepartamento(long id_Departamento);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addRecepcionDePago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? addRecepcionDePago(int clv_cliente, int clv_usuario, string serie, int folio, bool pagoDolares, decimal total);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addRecepcionDePagoDet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? addRecepcionDePagoDet(int clv_factura, int clv_concepto, decimal precio_unitario, string descripcion, int contrato, String Periodo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addRecepcionDePagoImpuestos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? addRecepcionDePagoImpuestos(int idDetalle, float IVA, float IEPS, float subTotal, float total);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "updateRecepcionDePago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? updateRecepcionDePago(int? id, string banco, string cuenta, string concepto, DateTime fecha, float monto, string formaDePago, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRecepcionDePagos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<recepcionDePagosEntity> GetRecepcionDePagos();

        [WebInvoke(Method = "*", UriTemplate = "GetRecepcionDePagoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        recepcionDePagosEntity GetRecepcionDePagoById(int id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Searchclientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ClientesEntity> Searchclientes(int? Op, string nombre, string otro_nombre, string primer_apellido, string segundo_apellido);
    
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMotivoCancelacionPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMotivoCancelacionPago(string Descripcion, bool? Activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMotivoCancelacionPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateMotivoCancelacionPago(int? Id, string Descripcion, bool? Activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMotivoCacelacionPagoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MotivosCancelacionFacturas GetMotivoCacelacionPagoById(int Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTorresProyecto1", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelTorresEntity> GetTorresProyecto1(int clv_proyecto, string torre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AceptaPropuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AceptaPropuesta(int? clv_cotizacion);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlanosBodega", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PlanosBodegaEntity GetPlanosBodega(string Nivel, int clv_proyecto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedoresByUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UsuarioEntity> GetVendedoresByUsuario(int? clv_usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "getCuentasBancariasByBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<cuentaBancariaEntity> getCuentasBancariasByBanco(int? idBanco);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "selectPagoEnganche", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? selectPagoEnganche(int? clv_cotizacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "selectPagoApartado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? selectPagoApartado(int? clv_cotizacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPagos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<pagoEntity> GetPagos(int? Factura, int? Cotizacion, string FechaInicio, string FechaFinn, int Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "CancelarPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? CancelarPago(int id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "cotizacionPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<cotizacionPago> cotizacionPago();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addRelacionPagosContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? addRelacionPagosContrato(int clv_concepto, string descripcion, string fechaPago, decimal monto, int contrato, int clv_factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelacionPagosContratoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<relPagosContratoEntity> GetRelacionPagosContratoById(int contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetContratoByCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetContratoByCotizacion(int clv_cotizacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "SearchCotizacionByCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<cotizacionPago> SearchCotizacionByCliente(int Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetContratoByCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<contratoByclienteEntity> GetContratoByCliente(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelCancelacionFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? AddRelCancelacionFactura(int? clv_Factura, int? id_Usuario, int? Motivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ObtenMenuUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Modulo> ObtenMenuUsuario(int? tipoUsuario, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFechaReservacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        fechaReservacionEntity GetFechaReservacion(int clv_cotizacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteIngresos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteIngresos(int? Opexportacion, string fechaInicio, string fechaFin, int usuarios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteClientesVencidos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteClientesVencidos(int? Opexportacion, string fechaInicio, string fechaFin);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteClientesPorVencer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteClientesPorVencer(int? Opexportacion, string fechaInicio, string fechaFin);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEstadoCuentaEnviado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteEstadoCuentaEnviado(int? Opexportacion, string fechaInicio, string fechaFin);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addMetodoPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? addMetodoPago(int Clv_factura, int Clv_tipoPago, decimal Monto, int banco, long cuenta , int numeroCheque, string cuentaTrans);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteVendedorDepApartadosVendidos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteVendedorDepApartadosVendidos(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus, string fechaInicio, string fechaFin, List<UsuarioEntity> usuarios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ValidarCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? ValidarCotizacion(int? clv_cotizacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "getFechasEA", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        fechaEngancheApartado getFechasEA(int? clv_cotizacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ValidaContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? ValidaContrato(int? contrato);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteIngresosPorCobrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteIngresosPorCobrar(int? Opexportacion, string fechaInicio, string fechaFin, List<conceptosDePagoEntity> concepto);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteGeneralIngresos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteGeneralIngresos(int? Opexportacion, string fechaInicio, string fechaFin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTicket", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetTicket(int? Opexportacion, int? IdDetalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQrPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetQrPago(int? Opexportacion, String CadenaQr);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ActivarCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? ActivarCotizacion(int? Id, string FechaInicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVistaByProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<VistaEntity> GetVistaByProyecto(int? clv_vendedor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMediosDifusion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Medios_de_comunicacionEntity> GetMediosDifusion();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteTarea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteTarea(int? Opexportacion,int? clv_comprador, string fechaInicio, string fechaFin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMediosDifusionById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Medios_de_comunicacionEntity GetMediosDifusionById(int? clv_medio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMedio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMedio(string nombre, bool estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMedio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateMedio(int? clv_medio, string nombre, bool estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMedioContacto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MedioContacto> GetMedioContacto();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMedioContactoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MedioContacto GetMedioContactoById(int? clv_medioContacto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMedioContacto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMedioContacto(string nombre, bool estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMedioContacto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateMedioContacto(int? clv_medioContacto, string nombre, bool estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDatosCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDatosCliente(int clv_comprador, string estado, string municipio, string localidad, string colonia, string calle, string numero, string numeroInterior,
        int? codigoPostal, string rfc, string curp, string folio);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "getDatosDepCotizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatosDepCotizacionEntity getDatosDepCotizacion(int? IdDetalle);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAcepataCompra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetAcepataCompra(int? Op, int? cotizacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addOLT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? addOLT(OLTEntity olt);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "EnviarCorreoMasivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? EnviarCorreoMasivo(CorreoMensajeEntity correo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addMonto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? addMonto(MontoEntity monto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFaturaSerie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<SerieFacturaEntity> GetFaturaSerie();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuarioAlegra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<UsuarioAlegraEntity> GetUsuarioAlegra();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEpayco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<EpaycoEntity> GetEpayco();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadosDecuentaEnviar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EstadosCuentaEntity> GetEstadosDecuentaEnviar();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "EnviaSms", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? EnviaSms(String Celular1);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ConGeneralCorreo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<GneralCorreoEntity> ConGeneralCorreo();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addGeneralcorreo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? addGeneralcorreo(GneralCorreoEntity correo);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addEpayco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? addEpayco(EpaycoEntity Epayco);






        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addUsuarioAlegra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int?  addUsuarioAlegra(UsuarioAlegraEntity Usuario);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addSerie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? addSerie(SerieFacturaEntity serie);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "addMensaje", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? addMensaje(MensajeEntity mensaje);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMensaje", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MensajeEntity> GetMensaje();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFirmwareModelo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<FirmwareModeloEntity> GetFirmwareModelo(int? clv_modelo);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GenraFacturaAlegra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GenraFacturaAlegra(Pagoentity Pago);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "updateOLT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? updateOLT(OLTEntity olt);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteOLT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? DeleteOLT(OLTEntity olt);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetOLTById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        OLTEntity GetOLTById(int? id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetOLTByIdCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        List<OLTEntity> GetOLTByIdCliente(int? id);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetOLTByIdClienteRecepcionPagos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<OLTEntity> GetOLTByIdClienteRecepcionPagos(int? id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetOLTList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<OLTEntity> GetOLTList(int? clienteId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMonto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MontoEntity> GetMonto();



    }
}

