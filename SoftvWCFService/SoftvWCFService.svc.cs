﻿
using Microsoft.IdentityModel.Tokens;
using SoftvWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using Softv.Entities;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SoftvWCFService.Controllers;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;


namespace SoftvWCFService
{
    [ScriptService]
    public partial class SoftvWCFService : ISessionWeb
    {

        UsuarioController UsuarioController = new UsuarioController();

        MotivosCancelacionController MotivosCancelacionController = new MotivosCancelacionController();

        #region inserts


        public int? AddVendedor(string nombre, string domicilio, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_vendedor @nombre ,@domicilio,@activo", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@domicilio", domicilio);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_vendedor " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public torre GetTorreById(int? clv_torre)
        {


            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    torre torre = new torre();
                    IDataReader rd = null;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_torre_clv  @clv_torre", connection);
                        command.Parameters.AddWithValue("@clv_torre", clv_torre);
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                torre.clv_torre = int.Parse(rd[0].ToString());
                                torre.clv_proyecto = int.Parse(rd[1].ToString());
                                torre.pisos = int.Parse(rd[2].ToString());
                                torre.activo = bool.Parse(rd[3].ToString());
                                torre.nombre = rd[4].ToString();
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_torre " + ex.Message, ex);
                        }

                        return torre;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }




















        public int? UpdateTorre(torre obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        SqlCommand strSQL = new SqlCommand("update_torre");
                        strSQL.Connection = connection;
                        strSQL.CommandType = CommandType.StoredProcedure;
                        strSQL.CommandTimeout = 0;

                        SqlParameter par1 = new SqlParameter("@clv_torre", SqlDbType.Int);
                        par1.Value = obj.clv_torre;
                        par1.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par1);

                        SqlParameter par2 = new SqlParameter("@clv_proyecto", SqlDbType.Int);
                        par2.Value = obj.clv_proyecto;
                        par2.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par2);

                        SqlParameter par3 = new SqlParameter("@pisos", SqlDbType.Int);
                        par3.Value = obj.pisos;
                        par3.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par3);

                        SqlParameter par4 = new SqlParameter("@nombre", SqlDbType.VarChar);
                        par4.Value = obj.nombre;
                        par4.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@activo", SqlDbType.Bit);
                        par5.Value = obj.activo;
                        par5.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par5);


                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            strSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_torre " + ex.Message, ex);
                        }

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? AddTorre(torre obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_torre  @clv_proyecto,@pisos,@nombre, @activo", connection);
                        command.Parameters.AddWithValue("@clv_proyecto", obj.clv_proyecto);
                        command.Parameters.AddWithValue("@pisos", obj.pisos);
                        command.Parameters.AddWithValue("@nombre", obj.nombre);
                        command.Parameters.AddWithValue("@activo", obj.activo);
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_torre " + ex.Message, ex);
                        }

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? AddespacioDepartamento(string nombre, bool? suma, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_espacio_departamento @nombre ,@suma,@activo", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@suma", suma);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error AddDepartamento " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? AddAmenitie(string desc, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_amenitie @descripcion,@activo", connection);
                        //command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", desc);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error AddAmenitie " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? AddVista(string desc, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_vista  @descripcion,@activo", connection);
                        //command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", desc);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro insert_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? Addusuario(UsuarioEntity usuario, VendedorEntity vendedor)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            { try
                {
                    return UsuarioController.AddUsuario(usuario, vendedor);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public int? AddDepartamento(DepartamentoEntity obj, List<DepartamentoEspacioEntity> espacios)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                int result = 0;
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {

                    try
                    {

                        SqlCommand command = new SqlCommand("insert_departamentos", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@TipoDepartamento", obj.TipoDepartamento);
                        command.Parameters.AddWithValue("@NumeroNiveles", obj.NumeroNiveles);
                        command.Parameters.AddWithValue("@MetrosCuadradosTotales", obj.MetrosTotales);
                        command.Parameters.AddWithValue("@CostoDepartamento ", obj.CostoDepartamento);
                        command.Parameters.AddWithValue("@Activo", obj.Activo);
                        command.Parameters.AddWithValue("@Lado", obj.Lado);

                        SqlParameter par2 = new SqlParameter("@clv_departamento", SqlDbType.Int);
                        par2.Direction = ParameterDirection.Output;
                        command.Parameters.Add(par2);
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                        result = int.Parse(par2.SqlValue.ToString());
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Erro insert_vista " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                    }


                    if (result > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<espacios>");
                        espacios.ForEach(y =>
                        {
                            sb.Append("<espacio>");
                            sb.Append("<clvespacio>" + y.clv_espacio_departamento + "</clvespacio>");
                            sb.Append("<metros>" + y.metros + "</metros>");
                            sb.Append("<descripcion>" + y.descripcion + "</descripcion>");
                            sb.Append("</espacio>");
                        });
                        sb.Append("</espacios>");


                        try
                        {

                            SqlCommand command = new SqlCommand("GeneraEspaciosDepartamento", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@clv_departamento", result);
                            command.Parameters.AddWithValue("@xml", sb.ToString());


                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro GeneraEspaciosDepartamento " + ex.Message, ex);
                        }

                    }


                    return result;
                }

            }

        }
        public int? AddComprador(string nombre, string domicilio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_comprador  @nombre ,@domicilio", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@domicilio", domicilio);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insertando el  Comprador " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? AddEstado(string nombre)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_pais @nombre ", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_estado " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public int? AddColonia(ColoniaEntity colonia)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                int result = 0;
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("insert_colonia", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@nombre", colonia.nombre);
                    command.Parameters.AddWithValue("@activo", colonia.activo);
                    command.Parameters.AddWithValue("@clv_pais", colonia.clv_pais);
                    command.Parameters.AddWithValue("@clv_ciudad", colonia.clv_ciudad);
                    command.Parameters.AddWithValue("@cp", colonia.cp);
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (connection != null)
                            connection.Close();
                        throw new Exception("Error insert_colonia " + ex.Message, ex);
                    }

                    return result;
                }

            }

        }


        public int? AddCalle(ColoniaEntity calle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                int result = 0;
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("insert_calle", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@nombre", calle.nombre);
                    command.Parameters.AddWithValue("@activo", calle.activo);
                    command.Parameters.AddWithValue("@clv_colonia", calle.clv_colonia);
                

                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (connection != null)
                            connection.Close();
                        throw new Exception("Error insert_colonia " + ex.Message, ex);
                    }

                    return result;
                }

            }

        }




        public int? UpdateCalle(ColoniaEntity calle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                int result = 0;
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("UpdateCalles", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@nombre", calle.nombre);
                    command.Parameters.AddWithValue("@clv_calle", calle.clv_calle);
                    command.Parameters.AddWithValue("@clv_colonia", calle.clv_colonia);


                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (connection != null)
                            connection.Close();
                        throw new Exception("Error insert_colonia " + ex.Message, ex);
                    }

                    return result;
                }

            }

        }





        public int? AddLocalidad(localidad obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                int result = 0;
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {

                    IDataReader rd = null;
                    try
                    {


                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        SqlCommand strSQL = new SqlCommand("insert_localidad");
                        strSQL.Connection = connection;
                        strSQL.CommandType = CommandType.StoredProcedure;
                        strSQL.CommandTimeout = 0;

                        SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                        par1.Value = obj.nombre;
                        par1.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par1);

                        SqlParameter par2 = new SqlParameter("@clv_localidad", SqlDbType.Int);
                        par2.Direction = ParameterDirection.Output;
                        strSQL.Parameters.Add(par2);
                        strSQL.ExecuteNonQuery();

                        result = int.Parse(par2.SqlValue.ToString());

                    }
                    catch (Exception ex)
                    {
                        if (connection != null)
                            connection.Close();
                        throw new Exception("Error insert_localidad " + ex.Message, ex);
                    }

                    obj.municipios.ForEach(x =>
                    {

                        try
                        {
                            SqlCommand command = new SqlCommand("insert_localidadesMunicipios  @clv_municipio,@clv_localidad", connection);
                            command.Parameters.AddWithValue("@clv_municipio", x.clv_municipio);
                            command.Parameters.AddWithValue("@clv_localidad", result);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_EstadoMunicipios " + ex.Message, ex);
                        }

                    });

                    return result;
                }
            }

        }
        public int? AddProyecto(ProyectoEntity proyecto, List<TorreEntity> torres, List<InfoMobiliti> amenities, List<ServiciosEntity> servicio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    MessageEntity m = new MessageEntity();
                    int? clave = 0;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        SqlCommand cmd = new SqlCommand("insert_proyecto", connection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = proyecto.nombre;
                        cmd.Parameters.Add("@numeroExterior", SqlDbType.VarChar).Value = proyecto.numeroExterior;
                        cmd.Parameters.Add("@costoMetroCuadradoInicial", SqlDbType.Money).Value = proyecto.costoMetroCuadradoInicial;
                        cmd.Parameters.Add("@precioVentaActual", SqlDbType.Money).Value = proyecto.precioVentaActual;
                        cmd.Parameters.Add("@clv_estado", SqlDbType.Int).Value = proyecto.clv_estado;
                        cmd.Parameters.Add("@clv_municipio", SqlDbType.Int).Value = proyecto.clv_municipio;
                        cmd.Parameters.Add("@clv_localidad", SqlDbType.Int).Value = proyecto.clv_localidad;
                        cmd.Parameters.Add("@clv_colonia", SqlDbType.Int).Value = proyecto.clv_colonia;
                        cmd.Parameters.Add("@activo", SqlDbType.Bit).Value = proyecto.activo;
                        cmd.Parameters.Add("@latitud", SqlDbType.VarChar).Value = proyecto.latitud;
                        cmd.Parameters.Add("@longuitud", SqlDbType.VarChar).Value = proyecto.longuitud;
                        cmd.Parameters.Add("@factorIncremento", SqlDbType.Int).Value = proyecto.factorIncremento;
                        cmd.Parameters.Add("@cambioEtapaPorVenta", SqlDbType.Int).Value = proyecto.cambioEtapaPorVenta;
                        cmd.Parameters.Add("@direccion", SqlDbType.VarChar).Value = proyecto.direccion;
                        cmd.Parameters.Add("@numeroEtapas", SqlDbType.VarChar).Value = proyecto.numeroEtapas;
                        cmd.Parameters.Add("@frasePublicitaria", SqlDbType.VarChar).Value = proyecto.frasePublicitaria;

                        var returnParameter = cmd.Parameters.Add("@clv_proyecto", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();
                        clave = int.Parse(returnParameter.Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        connection.Close();
                        throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                    }


                    if (clave > 0)
                    {
                        StringBuilder xml = new StringBuilder();
                        xml.Append("<proyecto>");

                        torres.ForEach(torre_ =>
                        {


                            torre_.Pisos.ForEach(piso_ =>
                            {

                                piso_.Departamentos.ForEach(departamento_ =>
                                {

                                    for (var a = 0; a < departamento_.cantidad; a++)
                                    {
                                        xml.Append("<torre>");
                                        xml.Append("<proyecto>" + clave + "</proyecto>");
                                        xml.Append("<torre>" + torre_.Descripcion + "</torre>");
                                        xml.Append("<piso>" + piso_.Piso + "</piso>");
                                        xml.Append("<departamento>" + departamento_.Clv_departamento + "</departamento>");
                                        xml.Append("<fechaentrega>" + piso_.FechaEntrega + "</fechaentrega>");
                                        xml.Append("<etapa>" + piso_.Etapa + "</etapa>");
                                        xml.Append("<Lado>" + departamento_.Lado + "</Lado>");
                                        xml.Append("</torre>");

                                    }


                                });


                            });


                        });
                        xml.Append("</proyecto>");





                        try
                        {

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand cmd = new SqlCommand("GeneraTorrePisoDepartamento", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@xml", SqlDbType.VarChar).Value = xml.ToString();


                            var returnParameter1 = cmd.Parameters.Add("@mensaje", SqlDbType.Int);
                            returnParameter1.Direction = ParameterDirection.Output;

                            var returnParameter2 = cmd.Parameters.Add("@error", SqlDbType.Int);
                            returnParameter2.Direction = ParameterDirection.Output;

                            cmd.ExecuteNonQuery();


                            m.error = int.Parse(returnParameter2.Value.ToString());
                            m.mensaje = returnParameter1.Value.ToString();



                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }



                        try
                        {

                            StringBuilder str = new StringBuilder();
                            str.Append("<amenities>");

                            amenities.ForEach(x =>
                            {

                                str.Append("<ameninitie>");
                                str.Append(" <clv>" + x.clv_amenitie + "</clv>");
                                str.Append(" <observacion>" + x.observacion + "</observacion>");
                                str.Append("</ameninitie>");
                            });

                            str.Append("</amenities>");

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand cmd = new SqlCommand("GeneraAmenitiesProyecto", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@xml", SqlDbType.VarChar).Value = str.ToString();
                            cmd.Parameters.Add("@clv_proyecto", SqlDbType.Int).Value = clave;
                            cmd.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }

                        try
                        {

                            StringBuilder str = new StringBuilder();
                            str.Append("<servicio>");

                            servicio.ForEach(x =>
                            {

                                str.Append("<servicio>");
                                str.Append(" <clv>" + x.clv_servicio + "</clv>");
                                str.Append(" <descripcion>" + x.descripcion + "</descripcion>");
                                str.Append("</servicio>");
                            });

                            str.Append("</servicio>");

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand cmd = new SqlCommand("GeneraServiciosProyecto", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@xml", SqlDbType.VarChar).Value = str.ToString();
                            cmd.Parameters.Add("@clv_proyecto", SqlDbType.Int).Value = clave;
                            cmd.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }

                    }

                    return clave;

                }
            }

        }


        public int? AddEstatusDepartamento(int? piso, string tipoDeDepartamento, int? numeroDepartamento, string fechaEntrega, string estatus, int? clv_vendedor, int? clv_comprador, float? precioListaVenta
            , float? precioActual, float? precioVendido, string fechaApartado, float? importeApartado, string fechaVenta, float? importeEnganche, float? porcentaje,
            int? numeroDePagosEnganche, string fechaEscrituracion, string notario, int? clv_torre, bool activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_estatusDepartamento @piso, @tipoDeDepartamento, @numeroDepartamento, @fechaEntrega , @estatus, @clv_vendedor,@clv_comprador, @precioListaVenta,@precioActual,@precioVendido,@fechaApartado,@importeApartado,@fechaVenta,@importeEnganche,@porcentaje,@numeroDePagosEnganche,@fechaEscrituracion,@notario,@clv_torre, @activo ", connection);

                        command.Parameters.AddWithValue("@piso", piso);
                        command.Parameters.AddWithValue("@tipoDeDepartamento", tipoDeDepartamento);
                        command.Parameters.AddWithValue("@numeroDepartamento", numeroDepartamento);
                        command.Parameters.AddWithValue("@fechaEntrega", fechaEntrega);
                        command.Parameters.AddWithValue("@estatus", estatus);
                        command.Parameters.AddWithValue("@clv_vendedor", clv_vendedor);
                        command.Parameters.AddWithValue("@clv_comprador", clv_comprador);
                        command.Parameters.AddWithValue("@precioListaVenta", precioListaVenta);
                        command.Parameters.AddWithValue("@precioActual", precioActual);
                        command.Parameters.AddWithValue("@precioVendido", precioVendido);
                        command.Parameters.AddWithValue("@bodega_activa", activo);
                        command.Parameters.AddWithValue("@fechaApartado", fechaApartado);
                        command.Parameters.AddWithValue("@importeApartado", importeApartado);
                        command.Parameters.AddWithValue("@fechaVenta", fechaVenta);
                        command.Parameters.AddWithValue("@importeEnganche", importeEnganche);
                        command.Parameters.AddWithValue("@porcentaje", porcentaje);
                        command.Parameters.AddWithValue("@numeroDePagosEnganche", numeroDePagosEnganche);
                        command.Parameters.AddWithValue("@fechaEscrituracion", fechaEscrituracion);
                        command.Parameters.AddWithValue("@notario", notario);
                        command.Parameters.AddWithValue("@clv_torre", clv_torre);
                        command.Parameters.AddWithValue("@activo", activo);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro insert_estatusDepartamentos " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public int? AddCliente(ClienteEntity obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))


                    {
                        SqlCommand command = new SqlCommand("AddClientes", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@nombre", SqlDbType.VarChar).Value = obj.nombre;
                        command.Parameters.Add("@nit", SqlDbType.VarChar).Value = obj.nit;
                        command.Parameters.Add("@pais", SqlDbType.VarChar).Value = obj.clv_pais;
                        command.Parameters.Add("@ciudad", SqlDbType.VarChar).Value = obj.clv_ciudad;
                        command.Parameters.Add("@telefono", SqlDbType.VarChar).Value = obj.telefono;
                        command.Parameters.Add("@celular", SqlDbType.VarChar).Value = obj.celular;
                        command.Parameters.Add("@correo", SqlDbType.VarChar).Value = obj.correo;
                        command.Parameters.Add("@cedula_rep_legal", SqlDbType.VarChar).Value = obj.cedulaR;
                        command.Parameters.Add("@rut", SqlDbType.VarChar).Value = obj.rut;
                        command.Parameters.Add("@smart_hostname", SqlDbType.VarChar).Value = obj.Dominio;
                        command.Parameters.Add("@smart_password", SqlDbType.VarChar).Value = obj.password;
                        command.Parameters.Add("@colonia", SqlDbType.VarChar).Value = obj.colonia;
                        command.Parameters.Add("@calle", SqlDbType.VarChar).Value = obj.calle;
                  
                        command.Parameters.Add("@Usuario", SqlDbType.VarChar).Value = obj.usuario;

                        command.Parameters.Add("@created_at", SqlDbType.VarChar).Value = obj.Fechacreacion;
                        command.Parameters.Add("@nombreR", SqlDbType.VarChar).Value = obj.nombreR;
                        command.Parameters.Add("@nombrecontratoA", SqlDbType.VarChar).Value = obj.nombrecontrato;
                        command.Parameters.Add("@requiereFac", SqlDbType.VarChar).Value = obj.activo;
                        command.Parameters.Add("@correo1", SqlDbType.VarChar).Value = obj.correo1;
                        command.Parameters.Add("@smart_password1", SqlDbType.VarChar).Value = obj.password1;
                        command.Parameters.Add("@telefonoCA", SqlDbType.VarChar).Value = obj.telefonoCA;
                        command.Parameters.Add("@CorreoCadmin", SqlDbType.VarChar).Value = obj.CorreoCadmin;
                        command.Parameters.Add("@tipocliente", SqlDbType.VarChar).Value = obj.tipocliente;










                        var returnParameter = command.Parameters.Add("@id", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.Output;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();

                            result = int.Parse(returnParameter.Value.ToString());

                            if (obj.clv_pais == 45 && obj.tipocliente == 0)
                            {
                               GeneraClientes(result);
                            }
                          

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        try
                        {

                            //StringBuilder str = new StringBuilder();
                            //str.Append("<departamentos>");

                            //departamentos.ForEach(x =>
                            //{

                            //    str.Append("<departamento>");
                            //    str.Append(" <clv>" + x.Clv_departamento + "</clv>");
                            //    str.Append("</departamento>");
                            //});

                            //str.Append("</departamentos>");

                            //if (connection.State == ConnectionState.Closed)
                            //    connection.Open();
                            //SqlCommand cmd = new SqlCommand("GeneraDepartamentosCliente", connection);
                            //cmd.CommandType = CommandType.StoredProcedure;
                            //cmd.Parameters.Add("@xml", SqlDbType.VarChar).Value = str.ToString();
                            //cmd.Parameters.Add("@clv_comprador", SqlDbType.Int).Value = result;
                            //cmd.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }
                        try
                        {

                            //List<UsuarioEntity> gerentes = GetUsuarios().Where(x => x.clv_tipoVendedor == 1 && x.activo == true).ToList();

                            //gerentes.ForEach(xx =>
                            //{
                            //    InfoClientes solicitudobjc = GetClienteById(result);
                            //    SendEmail(
                            //     xx.clv_usuario,
                            //     solicitudobjc.nombre.ToString(),
                            //     solicitudobjc.otro_nombre.ToString(),
                            //     solicitudobjc.primer_apellido.ToString(),
                            //     solicitudobjc.segundo_apellido.ToString(),
                            //     solicitudobjc.telefono.ToString(),
                            //     solicitudobjc.telefono_casa.ToString(),
                            //     solicitudobjc.celular.ToString(),
                            //     solicitudobjc.correo.ToString()
                            //  );

                            //});
                        }
                        catch (Exception ex)
                        {

                        }
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }



        public int? UpdateClientesActivo(ClienteEntity obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))


                    {
                        SqlCommand command = new SqlCommand("UpdateClienteActivo", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Id_cliente", SqlDbType.VarChar).Value = obj.id;
                        command.Parameters.Add("@activo", SqlDbType.VarChar).Value = obj.activo;

              
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();

                         



                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        try
                        {

               

                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }
                        try
                        {

                            
                        }
                        catch (Exception ex)
                        {

                        }
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }





        public static void GeneraClientes(int Id)
        {
            SoftvWCFService soft = new SoftvWCFService();
            List<UsuarioAlegraEntity> solicitudobjc = soft.GetUsuarioAlegra();

            string UsuarioAlegra = solicitudobjc[0].Usuario;
            string ContraseñaAlegra = solicitudobjc[0].Token;

            dbHelper db = new dbHelper();
            db.agregarParametro("@Id", SqlDbType.Int, Id);
            SqlDataReader reader = db.consultaReader("ObtieneClientesCrearAlegra");
            while (reader.Read())
            {
                string jsonRequest = "";
                if (reader.GetValue(2).ToString() == "LEGAL_ENTITY")
                {
                    jsonRequest = "{" +
                                              "\"name\": \"" + reader.GetValue(0).ToString() + "\"," +
                                              "\"email\": \"" + reader.GetValue(1).ToString() + "\"," +
                                              "\"kindOfPerson\": \"" + reader.GetValue(2).ToString() + "\"," +
                                              "\"type\": \"client\"," +
                                              "\"identificationObject\": {" +
                                                                "\"type\": \"" + reader.GetValue(3).ToString() + "\"," +
                                                    "\"number\": \"" + reader.GetValue(4).ToString() + "\"" +
                                                "}," +
                                              "\"address\": {" +
                                                                "\"address\": \"" + reader.GetValue(5).ToString() + "\"," +
                                                    "\"city\": \"" + reader.GetValue(6).ToString() + "\"," +
                                                    "\"department\": \"" + reader.GetValue(7).ToString() + "\"," +
                                                    "\"country\": \"" + reader.GetValue(8).ToString() + "\"," +
                                                    "\"zipCode\": \"" + reader.GetValue(9).ToString() + "\"" +
                                                "}," +
                                                "\"settings\": {" +
                                                    "\"sendElectronicDocuments\": true" +
                                                "}" +
                                             "}";
                }
                else
                {
                    jsonRequest = "{" +
                                              "\"name\": {" +
                                                    "\"firstName\": \"" + reader.GetValue(0).ToString() + "\"," +
                                                  "\"secondName\": \"" + reader.GetValue(12).ToString() + "\"," +
                                                  "\"lastName\": \"" + reader.GetValue(13).ToString() + "\"" +
                                              "}," +
                                              "\"email\": \"" + reader.GetValue(1).ToString() + "\"," +
                                              "\"kindOfPerson\": \"" + reader.GetValue(2).ToString() + "\"," +
                                              "\"type\": \"client\"," +
                                              "\"identificationObject\": {" +
                                                                "\"type\": \"" + reader.GetValue(3).ToString() + "\"," +
                                                    "\"number\": \"" + reader.GetValue(4).ToString() + "\"" +
                                                "}," +
                                              "\"address\": {" +
                                                                "\"address\": \"" + reader.GetValue(5).ToString() + "\"," +
                                                    "\"city\": \"" + reader.GetValue(6).ToString() + "\"," +
                                                    "\"department\": \"" + reader.GetValue(7).ToString() + "\"," +
                                                    "\"country\": \"" + reader.GetValue(8).ToString() + "\"," +
                                                    "\"zipCode\": \"" + reader.GetValue(9).ToString() + "\"" +
                                                "}," +
                                                "\"settings\": {" +
                                                    "\"sendElectronicDocuments\": true" +
                                                "}" +
                                             "}";
                }
                //Formamos la petición al proveedor
                var httpClient = new HttpClient();
                var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

                byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(UsuarioAlegra + ":" + ContraseñaAlegra);
                string auth = Convert.ToBase64String(toEncodeAsBytes.ToArray());

                //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };//Produccion
                httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);
                httpClient.Timeout = TimeSpan.FromMilliseconds(600000000);



                DateTime time1 = DateTime.Now;

                Task<HttpResponseMessage> response = httpClient.PostAsync("https://api.alegra.com/api/v1/contacts", httpContent);
                var responseAux = response.Result.Content.ReadAsStringAsync();
                string jsonResponse = responseAux.Result.ToString();

                JObject objCliente = JObject.Parse(jsonResponse);

                string id = "";
                foreach (JProperty p in objCliente.Properties())
                {
                    try
                    {
                        if (p.Name == "id")
                        {
                            id = p.Value.ToString();
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                //Vamos a guardar los datos en la base
                try
                {
                    dbHelper db1 = new dbHelper();
                    db1.agregarParametro("@id", SqlDbType.Int, id);
                    db1.agregarParametro("@Contrato", SqlDbType.BigInt, Id);


                    db1.consultaSinRetorno("GuardaRelacionClienteAlegra");
                    db1.conexion.Close();

                    Console.WriteLine("Contrato " + reader.GetInt64(11).ToString() + " Id " + id.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + Environment.NewLine + jsonResponse);
                }
            }
        }





        public void SendEmail(int? clv_usuario, string reemplazanombre, string remplazaotronombre, string remplazaprimerapellido, string remplazasegundoapellido,
        string reemplazatelefonooficina, string reemplazatelefonocasa, string reemplazacelular, string reemplazacorreo)
        {
            SoftvWCFService us = new SoftvWCFService();
            UsuarioEntity user = us.GetUsuariosC((int.Parse(clv_usuario.ToString())));

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential("ohlive.noreply@gmail.com", "0601x-2L");

            string output = null;
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(user.correo));
            email.From = new MailAddress("ohlive.noreply@gmail.com");
            email.Subject = "Nuevo Cliente";

            email.Body = emailBody()
            .Replace("reemplazanombre", reemplazanombre)
            .Replace("remplazaotronombre", remplazaotronombre)
            .Replace("remplazaprimerapellido", remplazaprimerapellido)
            .Replace("remplazasegundoapellido", remplazasegundoapellido)
            .Replace("reemplazatelefonooficina", reemplazatelefonooficina)
            .Replace("reemplazatelefonocasa", reemplazatelefonocasa)
            .Replace("reemplazacelular", reemplazacelular)
            .Replace("reemplazacorreo", reemplazacorreo);

            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            try
            {
                smtp.Send(email);
                email.Dispose();
                output = "Correo electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
        }
        public static string emailBody()
        {
            string Body = "";
            try
            {

                Body = System.IO.File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplate/email.htm");
            }
            catch (Exception e)
            {

            }


            return Body;
        }






















        #endregion

        #region gets
        public List<InfoMobiliti> GetVendedorN(string nombre)
        {
            List<InfoMobiliti> Vendedoreslist = new List<InfoMobiliti>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vendedor_nombre  @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_vendedor = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vendedoreslist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_vendedor_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vendedoreslist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoMobiliti> GetVendedorC(int? clv_vendedor)
        {
            List<InfoMobiliti> Vendedoreslist = new List<InfoMobiliti>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vendedor_clv  @clv_vendedor", connection);
                        command.Parameters.AddWithValue("@clv_vendedor", clv_vendedor);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_vendedor = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.domicilio = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vendedoreslist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro buscar clave " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vendedoreslist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public UsuarioEntity GetUsuariosC(int? clv_usuario)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try {

                    return UsuarioController.GetUsuarioById(clv_usuario);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<InfoMobiliti> GetVistaC(int? clv_vista)
        {

            List<InfoMobiliti> Vlist = new List<InfoMobiliti>();


            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vista_clv  @clv_vista", connection);
                        command.Parameters.AddWithValue("@clv_vista", clv_vista);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_vista = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vlist.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Buscar Clave Vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoDepto> GetSpaceDeptC(int? clv_espacio_departamento)
        {
            List<InfoDepto> Infolist = new List<InfoDepto>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_espacio_departamento_clv   @clv_espacio_departamento", connection);
                        command.Parameters.AddWithValue("@clv_espacio_departamento", clv_espacio_departamento);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoDepto j = new InfoDepto();
                                j.clv_espacio_departamento = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.activo = bool.Parse(rd[3].ToString());
                                j.suma = bool.Parse(rd[4].ToString());
                                Infolist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Buscar Clave deparatamento " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Infolist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoMobiliti> GetAmenitieC(int? clv_amenitie)
        {
            List<InfoMobiliti> V1list = new List<InfoMobiliti>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_amenitie_clv  @clv_amenitie", connection);
                        command.Parameters.AddWithValue("@clv_amenitie", clv_amenitie);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_amenitie = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                V1list.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Buscar Clave Amenitie " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return V1list;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<torre> GetTorreC(int? clv_torre)
        {
            List<torre> V1list = new List<torre>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_torre_clv  @clv_torre", connection);
                        command.Parameters.AddWithValue("@clv_torre", clv_torre);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                torre i = new torre();
                                i.clv_torre = Int32.Parse(rd[0].ToString());
                                i.clv_proyecto = int.Parse(rd[1].ToString());
                                i.pisos = int.Parse(rd[2].ToString());
                                i.activo = bool.Parse(rd[3].ToString());
                                V1list.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Buscar Clave torre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return V1list;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoMobiliti> GetVista()
        {
            List<InfoMobiliti> Vistalist = new List<InfoMobiliti>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vista  ", connection);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_vista = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoMobiliti> GetVendedor() {


            List<InfoMobiliti> Vendedorlist = new List<InfoMobiliti>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vendedor  ", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti j = new InfoMobiliti();
                                j.clv_usuario = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                Vendedorlist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vendedorlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<UsuarioEntity> GetUsuarios()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return UsuarioController.GetUsuarios();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoComprador> GetCompradores()
        {
            List<InfoComprador> Vistalist = new List<InfoComprador>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_comprador  ", connection);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoComprador i = new InfoComprador();
                                i.clv_comprador = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.domicilio = rd[2].ToString();
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Compradores " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoPais> GetEstado()

        {
            List<InfoPais> Vlist = new List<InfoPais>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_pais", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoPais j = new InfoPais();
                                j.clv_pais = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.codigoArea = rd[2].ToString();
                                Vlist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoPais> GetColonia()

        {
            List<InfoPais> Vlist = new List<InfoPais>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_colonia", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoPais j = new InfoPais();
                                j.clv_colonia = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.codigoPostal = Int32.Parse(rd[4].ToString());
                         
                                Vlist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_colonia " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<InfoPais> GetCalles()

        {
            List<InfoPais> Vlist = new List<InfoPais>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetCalles", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoPais j = new InfoPais();
                                j.clv_calle = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                             
                                Vlist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_colonia " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }





        public List<InfoPais> GetCallesById(int? clv_calle)

        {
            List<InfoPais> Vlist = new List<InfoPais>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetCallesById @clv_calle", connection);

                        command.Parameters.AddWithValue("@clv_calle", clv_calle);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoPais j = new InfoPais();
                                j.clv_calle = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.clv_colonia = Int32.Parse(rd[2].ToString());

                                Vlist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_colonia " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }












        public List<localidad> GetLocalidad()

        {
            List<localidad> Vlist = new List<localidad>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_localidad  ", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                localidad j = new localidad();
                                j.clv_localidad = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                Vlist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_localidad " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoPais> GetEstadoC(int? clv_estado)

        {
            List<InfoPais> V1list = new List<InfoPais>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_pais_clv  @clv_pais", connection);
                        command.Parameters.AddWithValue("@clv_pais", clv_estado);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoPais i = new InfoPais();
                                i.clv_pais = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                V1list.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Buscar Clave estado " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return V1list;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<ColoniaEntity> GetColonioaC(int? clv_colonia)
        {
            List<ColoniaEntity> V1list = new List<ColoniaEntity>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_colonia_clv  @clv_colonia", connection);
                        command.Parameters.AddWithValue("@clv_colonia", clv_colonia);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ColoniaEntity i = new ColoniaEntity();
                                i.clv_colonia = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.clv_ciudad = int.Parse(rd[2].ToString());
                                i.clv_pais = int.Parse(rd[3].ToString());
                                
                                i.cp = int.Parse(rd[4].ToString());
                                V1list.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_colonia_clv " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return V1list;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<localidad> GetLocalidadById(int? clv_localidad)
        {
            List<localidad> localidades = new List<localidad>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        SqlCommand strSQL = new SqlCommand("select_localidad_clv");
                        strSQL.Connection = connection;
                        strSQL.CommandType = CommandType.StoredProcedure;
                        strSQL.CommandTimeout = 0;

                        SqlParameter par1 = new SqlParameter("clv_localidad", SqlDbType.Int);
                        par1.Value = clv_localidad;
                        par1.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par1);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = strSQL.ExecuteReader();
                            while (rd.Read())
                            {
                                localidad i = new localidad();
                                i.clv_localidad = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                localidades.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Buscar Clave municipio " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }


                        localidades.ForEach(x =>
                        {


                            x.municipios = GetLocalidadesMunicipio(x.clv_localidad);

                        });


                        return localidades;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoUser> GetUsuariosnN(string nombre)
        {
            List<InfoUser> usuarioslist = new List<InfoUser>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_usuario_nombre  @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoUser i = new InfoUser();
                                i.clv_usuario = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.correo = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                usuarioslist.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_usuario_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return usuarioslist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public InfoUser GetusuarioByUserAndPass(string usuario, string contrasena)
        {

            InfoUser objusuario = new InfoUser();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_usuario_password  @correo,@contrasenia ", connection);
                        command.Parameters.AddWithValue("@correo", usuario);
                        command.Parameters.AddWithValue("@contrasenia", contrasena);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                objusuario.clv_usuario = Int32.Parse(rd[0].ToString());
                                objusuario.correo = rd[1].ToString();
                                objusuario.contrasenia = rd[2].ToString();
                                objusuario.activo = bool.Parse(rd[4].ToString());
                                objusuario.tipoUsuario = Int32.Parse(rd[5].ToString());
                                objusuario.Public_Key = rd[7].ToString();
                                objusuario.Cliente_Id = Int32.Parse(rd[8].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_usuario_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return objusuario;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";
        private const string BasicAuth = "Basic";
        public static string GenerateToken(string username, int expireMinutes = 20000)
        {
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                        new Claim(ClaimTypes.Name, username)
                    }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }
        private static string GetEncodedCredentialsFromHeader()
        {
            WebOperationContext ctx = WebOperationContext.Current;

            // credentials are in the Authorization Header
            string credsHeader = ctx.IncomingRequest.Headers[HttpRequestHeader.Authorization];
            if (credsHeader != null)
            {
                // make sure that we have 'Basic' auth header. Anything else can't be handled
                string creds = null;
                int credsPosition = credsHeader.IndexOf(BasicAuth, StringComparison.OrdinalIgnoreCase);
                if (credsPosition != -1)
                {
                    // 'Basic' creds were found
                    credsPosition += BasicAuth.Length + 1;
                    if (credsPosition < credsHeader.Length - 1)
                    {
                        creds = credsHeader.Substring(credsPosition, credsHeader.Length - credsPosition);
                        return creds;
                    }
                    return null;
                }
                else
                {
                    // we did not find Basic auth header but some other type of auth. We can't handle it. Return null.
                    return null;
                }
            }

            // no auth header was found
            return null;
        }
        public InfoUser Autenticacion()
        {

            InfoUser objsession = new InfoUser();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            if (WebOperationContext.Current.IncomingRequest.Headers["Authorization"] == null)
            {
                //WebOperationContext.Current.OutgoingResponse.Headers.Add("WWW-Authenticate: Basic realm=\"myrealm\"");
                throw new WebFaultException<string>("Acceso no autorizado, favor de validar autenticación", HttpStatusCode.Unauthorized);
            }
            else // Decode the header, check password
            {
                string encodedUnamePwd = GetEncodedCredentialsFromHeader();
                if (!string.IsNullOrEmpty(encodedUnamePwd))
                {
                    byte[] decodedBytes = null;
                    try
                    {
                        decodedBytes = Convert.FromBase64String(encodedUnamePwd);
                    }
                    catch (FormatException)
                    { }

                    string credentials = ASCIIEncoding.ASCII.GetString(decodedBytes);
                    string[] authParts = credentials.Split(':');
                    InfoUser objUsr = GetusuarioByUserAndPass(authParts[0], authParts[1]);
                    if (objUsr.clv_usuario != null)
                    {
                        objsession = objUsr;
                        objsession.token = GenerateToken(authParts[0]);

                        if (objUsr.activo == false)
                        {
                            throw new WebFaultException<string>("Acceso no autorizado, el usuario esta inactivo", HttpStatusCode.Unauthorized);
                        }
                        else
                        {
                            return objsession;
                        }
                    }
                    else
                    {
                        // WebOperationContext.Current.OutgoingResponse.Headers.Add("WWW-Authenticate: Basic realm=\"myrealm\"");
                        throw new WebFaultException<string>("Acceso no autorizado, credenciales no validas", HttpStatusCode.Unauthorized);
                    }
                }
            }

            return objsession;
        }
        public List<InfoMobiliti> GetVistaN(string nombre)
        {

            List<InfoMobiliti> Vlist = new List<InfoMobiliti>();


            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vista_nombre  @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_vista = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vlist.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_vista_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoDepto> GetSpaceDept()
        {
            List<InfoDepto> Infolist = new List<InfoDepto>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_espacio_departamento", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoDepto j = new InfoDepto();
                                j.clv_espacio_departamento = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.activo = bool.Parse(rd[3].ToString());
                                j.suma = bool.Parse(rd[4].ToString());
                                Infolist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Infolist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoDepto> GetSpaceDeptN(string nombre)
        {
            List<InfoDepto> Infolist = new List<InfoDepto>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_espacio_departamento_nombre @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoDepto j = new InfoDepto();
                                j.clv_espacio_departamento = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.metros_cuadrados = float.Parse(rd[2].ToString());
                                j.activo = bool.Parse(rd[3].ToString());
                                Infolist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Infolist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoMobiliti> GetAmenitie()
        {
            List<InfoMobiliti> Vlist = new List<InfoMobiliti>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_amenitie", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti j = new InfoMobiliti();
                                j.clv_amenitie = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.descripcion = rd[2].ToString();
                                j.activo = bool.Parse(rd[3].ToString());
                                Vlist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Amenities " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<InfoMobiliti> GetAmenitieN(string nombre)
        {
            List<InfoMobiliti> V1list = new List<InfoMobiliti>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_amenitie_nombre  @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_amenitie = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                V1list.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_vista_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return V1list;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<DepartamentoEntity> GetDepartamentos()
        {
            List<DepartamentoEntity> Vlist = new List<DepartamentoEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_departamentos", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                DepartamentoEntity j = new DepartamentoEntity();
                                j.Clv_departamento = Int32.Parse(rd[0].ToString());
                                j.TipoDepartamento = rd[1].ToString();
                                j.NumeroNiveles = Int32.Parse(rd[2].ToString());
                                j.MetrosTotales = float.Parse(rd[3].ToString());
                                j.Activo = bool.Parse(rd[5].ToString());
                                j.Lado = rd[6].ToString();
                                Vlist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_departamentos " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<torre> GetTorre()
        {
            List<torre> Vlist = new List<torre>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_torre", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                torre j = new torre();
                                j.clv_torre = Int32.Parse(rd[0].ToString());
                                j.clv_proyecto = int.Parse(rd[1].ToString());
                                j.pisos = int.Parse(rd[2].ToString());
                                j.activo = bool.Parse(rd[3].ToString());
                                j.nombre = rd[4].ToString();
                                Vlist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Amenities " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<DepartamentoEspacioEntity> GetEspaciosDepartamento(int? clv_departamento)
        {

            List<DepartamentoEspacioEntity> Vlist = new List<DepartamentoEspacioEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {


                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("GetDepartamentoEspacios @clv_departamento", connection);
                    command.Parameters.AddWithValue("@clv_departamento", clv_departamento);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            DepartamentoEspacioEntity j = new DepartamentoEspacioEntity();
                            j.clv_espacio_departamento = Int32.Parse(rd[0].ToString());
                            j.nombre = rd[1].ToString();
                            j.metros = float.Parse(rd[2].ToString());
                            j.descripcion = rd[3].ToString();
                            j.suma = bool.Parse(rd[4].ToString());

                            Vlist.Add(j);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }




                }
                return Vlist;
            }

        }

        public List<DepartamentoEntity> GetDepartamentoC(int? clv_departamento)
        {

            List<DepartamentoEntity> Vlist = new List<DepartamentoEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {


                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("select_departamento_clave @clv_departamento", connection);
                    command.Parameters.AddWithValue("@clv_departamento", clv_departamento);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            DepartamentoEntity j = new DepartamentoEntity();
                            j.Clv_departamento = Int32.Parse(rd[0].ToString());
                            j.TipoDepartamento = rd[1].ToString();
                            j.NumeroNiveles = Int32.Parse(rd[2].ToString());
                            j.MetrosTotales = float.Parse(rd[3].ToString());
                            j.CostoDepartamento = decimal.Parse(rd[4].ToString());
                            j.Activo = bool.Parse(rd[5].ToString());
                            j.Lado = rd[6].ToString();
                            j.imagen = rd[7].ToString();
                            j.espacios = new List<DepartamentoEspacioEntity>();
                            j.espacios = GetEspaciosDepartamento(j.Clv_departamento);
                            Vlist.Add(j);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error select_departamentos " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }




                }
                return Vlist;
            }

        }


        public List<InfoMobiliti> GetAmenitiesProyecto(int? clv_proyecto)
        {

            List<InfoMobiliti> Vlist = new List<InfoMobiliti>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {


                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("obtenAmenitiesProyecto @clv_proyecto", connection);
                    command.Parameters.AddWithValue("@clv_proyecto", clv_proyecto);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            InfoMobiliti j = new InfoMobiliti();
                            j.clv_amenitie = Int32.Parse(rd[0].ToString());
                            j.descripcion = rd[2].ToString();
                            j.observacion = rd[4].ToString();
                            Vlist.Add(j);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error obtenAmenitiesProyecto " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }




                }
                return Vlist;
            }

        }




        public List<ServiciosEntity> GetServicioProyecto(int? clv_proyecto)
        {

            List<ServiciosEntity> Vlist = new List<ServiciosEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {


                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("obtenServiciosProyecto @clv_proyecto", connection);
                    command.Parameters.AddWithValue("@clv_proyecto", clv_proyecto);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            ServiciosEntity j = new ServiciosEntity();
                            j.clv_servicio = Int32.Parse(rd[0].ToString());
                            j.nombre_Servicio = rd[1].ToString();
                            j.descripcion = rd[2].ToString();

                            Vlist.Add(j);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error obtenServiciosProyecto " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }




                }
                return Vlist;
            }

        }



        public ProyectoEntity GetProyectoById(int? id)
        {
            ProyectoEntity j = new ProyectoEntity();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_proyecto_clave @clv_proyecto ", connection);
                        command.Parameters.AddWithValue("@clv_proyecto", id);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                                j.clv_proyecto = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.numeroExterior = rd[2].ToString();
                                j.costoMetroCuadradoInicial = decimal.Parse(rd[3].ToString());
                                j.precioVentaActual = decimal.Parse(rd[4].ToString());
                                j.clv_estado = Int32.Parse(rd[5].ToString());
                                j.clv_municipio = Int32.Parse(rd[6].ToString());
                                j.clv_localidad = Int32.Parse(rd[7].ToString());
                                j.clv_colonia = Int32.Parse(rd[8].ToString());
                                j.activo = bool.Parse(rd[9].ToString());
                                j.latitud = rd[10].ToString();
                                j.longuitud = rd[11].ToString();
                                j.factorIncremento = int.Parse(rd[12].ToString());
                                j.cambioEtapaPorVenta = int.Parse(rd[13].ToString());
                                j.direccion = rd[14].ToString();
                                j.numeroEtapas = int.Parse(rd[15].ToString());
                                j.frasePublicitaria = rd[16].ToString();
                                j.Estado = rd[17].ToString();
                                j.Municipio = rd[18].ToString();
                                j.Localidad = rd[19].ToString();
                                j.Colonia = rd[20].ToString();




                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_proyecto_clave " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        j.amenities = GetAmenitiesProyecto(j.clv_proyecto);
                        j.servicio = GetServicioProyecto(j.clv_proyecto);

                        j.Torres = new List<TorreEntity>();
                        List<string> torres = GetTorresProyecto(j.clv_proyecto);
                        torres.ForEach(s =>
                        {
                           
                            TorreEntity t = new TorreEntity();
                            t.Descripcion = s;
                            t.Estatus = int.Parse(t.Estatus.ToString());
                           
                           
                          
                            t.Pisos = new List<PisoEntity>();

                            List<int> pisos = GetPisosTorre(s, j.clv_proyecto);
                            pisos.ForEach(h =>
                            {
                                PisoEntity p = new PisoEntity();
                                p.Piso = h;
                                t.Pisos.Add(p);


                                p.Departamentos = ResumenDepartamentos(p.Piso, j.clv_proyecto, t.Descripcion);

                            });

                            j.Torres.Add(t);

                        });


                        return j;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<DepartamentoEntity> ResumenDepartamentos(int? piso, int? clv_proyecto, string torre)
        {
            try
            {
                List<DepartamentoEntity> Departamentos = new List<DepartamentoEntity>();
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("resumenDepartamentosPiso  @piso,@clv_proyecto,@torre ", connection);
                    command.Parameters.AddWithValue("@piso", piso);
                    command.Parameters.AddWithValue("@clv_proyecto", clv_proyecto);
                    command.Parameters.AddWithValue("@torre", torre);

                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            DepartamentoEntity de = new DepartamentoEntity();
                            de.Clv_departamento = int.Parse(rd[0].ToString());
                            de.TipoDepartamento = rd[1].ToString();
                            de.Lado = rd[2].ToString();
                            de.cantidad = int.Parse(rd[3].ToString());
                            de.Estatus = int.Parse(rd[4].ToString());

                            Departamentos.Add(de);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error resumenDepartamentosPiso " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }



                    return Departamentos;
                }
            }

            catch (Exception ex)
            {

                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public List<DepartamentoEntity> GetDepartamentoCliente(int? clv_comprador)
        {

            List<DepartamentoEntity> Vlist = new List<DepartamentoEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {


                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("obtenDepartamentosCliente @clv_comprador", connection);
                    command.Parameters.AddWithValue("@clv_comprador", clv_comprador);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            DepartamentoEntity j = new DepartamentoEntity();
                            j.Clv_departamento = Int32.Parse(rd[0].ToString());
                            j.TipoDepartamento = rd[1].ToString();

                            Vlist.Add(j);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error obtenAmenitiesProyecto " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }




                }
                return Vlist;
            }

        }

        public InfoClientes GetClienteById(int? id)
        {
            InfoClientes j = new InfoClientes();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetClientById  @id ", connection);
                        command.Parameters.AddWithValue("@id", id);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                                j.id = Int32.Parse(rd[0].ToString());
                                j.nombre_cliente = rd[2].ToString();
                                j.nit = rd[3].ToString();
                                j.clv_pais =Int32.Parse( rd[5].ToString());
                                j.clv_ciudad = Int32.Parse(rd[6].ToString());
                                j.telefono = rd[7].ToString();
                                j.celular = rd[8].ToString();
                                j.correo = rd[9].ToString();
                                j.folio = rd[10].ToString();
                                j.rut = rd[11].ToString();
                                j.Dominio = rd[12].ToString();
                                j.password = rd[13].ToString();
                                j.Fecha = DateTime.Parse(rd[15].ToString());
                                j.Fecha2 = j.Fecha.ToShortDateString();
                                j.colonia = rd[18].ToString();
                                j.calle = rd[19].ToString();
                                j.usuario = rd[20].ToString();
                                j.NombreR = rd[21].ToString();
                                j.nombrecontrato = rd[22].ToString();
                                j.activo = bool.Parse(rd[23].ToString());
                                j.telefonoCA = rd[24].ToString();
                                j.CorreoContactoAdministrativo = rd[25].ToString();
                                j.correo1 = rd[26].ToString();
                                j.password1 = rd[27].ToString();
                                j.tipocliente = bool.Parse(rd[28].ToString());



                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error cliente id " + ex.Message, ex);
                        }


                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return j;
                    }
                }

                catch (Exception ex)
                {

                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<InfoProyecto> GetProyecto()
        {
            List<InfoProyecto> Vlist = new List<InfoProyecto>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_proyecto", connection);



                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoProyecto j = new InfoProyecto();
                                j.clv_proyecto = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.numero_ext = Int32.Parse(rd[2].ToString());
                                j.costoMetroCuadrado = float.Parse(rd[3].ToString());
                                j.precioVentaActual = float.Parse(rd[4].ToString());
                                j.clv_estado = Int32.Parse(rd[5].ToString());
                                j.clv_municipio = Int32.Parse(rd[6].ToString());
                                j.clv_localidad = Int32.Parse(rd[7].ToString());
                                j.clv_colonia = Int32.Parse(rd[8].ToString());
                                j.activo = bool.Parse(rd[9].ToString());
                                j.latitud = float.Parse(rd[10].ToString());
                                j.longitud = float.Parse(rd[11].ToString());
                                j.factorIncremento = Int32.Parse(rd[12].ToString());
                                j.cambioEtapaPorVenta = Int32.Parse(rd[13].ToString());
                                j.direccion = rd[14].ToString();
                                j.numeroEtapas = Int32.Parse(rd[15].ToString());


                                Vlist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Amenities " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        //public List<InfoD>GetEstatusDepartamento()
        //{
        //    List<InfoD> Vlist = new List<InfoD>();

        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        try
        //        {

        //            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        //            using (SqlConnection connection = new SqlConnection(ConnectionString))
        //            {
        //                SqlCommand command = new SqlCommand("select_estatusDepartamento", connection);



        //                IDataReader rd = null;
        //                try
        //                {
        //                    if (connection.State == ConnectionState.Closed)
        //                        connection.Open();
        //                    rd = command.ExecuteReader();
        //                    while (rd.Read())
        //                    {
        //                        InfoD j = new InfoD();
        //                        j.clv_estatusDepartamento = Int32.Parse(rd[0].ToString());
        //                        j.clv_torre = rd[1].ToString();
        //                        j.piso = Int32.Parse(rd[2].ToString());
        //                        j.tipo_departamento = rd[3].ToString();
        //                        j.numero_departamento = Int32.Parse(rd[4].ToString());
        //                        j.fecha_entrega = rd[5].ToString();
        //                        j.estatus = rd[6].ToString();
        //                        j.clv_vendedor = rd[7].ToString();
        //                        j.clv_comprador = rd[8].ToString();
        //                        j.precio_lista_venta = float.Parse(rd[9].ToString());
        //                        j.precio_actual = float.Parse(rd[10].ToString());
        //                        j.precio_vendido = float.Parse(rd[11].ToString());
        //                        j.fecha_apartado = rd[12].ToString();
        //                        j.importe_apartado = float.Parse(rd[13].ToString());
        //                        j.fecha_venta = rd[14].ToString();
        //                        j.importe_enganche = float.Parse(rd[15].ToString());
        //                        j.porcentaje = float.Parse(rd[16].ToString());
        //                        j.numero_de_pago_enganche = Int32.Parse(rd[17].ToString());
        //                        j.fecha_escrituracion = rd[18].ToString();
        //                        j.notario = rd[19].ToString();
        //                        j.activo = bool.Parse(rd[20].ToString());


        //                        Vlist.Add(j);
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    throw new Exception("Error Estatus Departamento " + ex.Message, ex);
        //                }
        //                finally
        //                {
        //                    if (connection != null)
        //                        connection.Close();
        //                    if (rd != null)
        //                        rd.Close();
        //                }



        //                return Vlist;
        //            }
        //        }

        //        catch (Exception ex)
        //        {
        //            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
        //        }
        //    }

        //}
        //public List<InfoClientes> GetCliente(int? usuario)
        //{
        //    List<InfoClientes> Vlist = new List<InfoClientes>();

        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        try
        //        {

        //            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        //            using (SqlConnection connection = new SqlConnection(ConnectionString))
        //            {
        //                SqlCommand command = new SqlCommand("select_comprador", connection);
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.AddWithValue("@usuario", usuario);



        //                IDataReader rd = null;
        //                try
        //                {
        //                    if (connection.State == ConnectionState.Closed)
        //                        connection.Open();
        //                    rd = command.ExecuteReader();
        //                    while (rd.Read())
        //                    {
        //                        InfoClientes j = new InfoClientes();
        //                        j.clv_comprador = Int32.Parse(rd[0].ToString());
        //                        j.nombre = rd[1].ToString();
        //                        j.estado = rd[2].ToString();
        //                        j.municipio = rd[3].ToString();
        //                        j.localidad = rd[4].ToString();
        //                        j.colonia = rd[5].ToString();
        //                        j.numero = rd[6].ToString();
        //                        j.codigo_postal = Int32.Parse(rd[7].ToString());
        //                        j.telefono = rd[8].ToString();
        //                        j.celular = rd[9].ToString();
        //                        j.correo = rd[10].ToString();
        //                        j.activo = bool.Parse(rd[11].ToString());
        //                        j.primer_apellido = rd[12].ToString();
        //                        j.segundo_apellido = rd[13].ToString();
        //                        j.otro_nombre = rd[14].ToString();
        //                        j.calle = rd[15].ToString();
        //                        j.numeroInterior = rd[16].ToString();
        //                        j.usuario = rd[20].ToString();
        //                        j.estatus_cliente = rd[24].ToString();
        //                        Vlist.Add(j);
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    throw new Exception("Error Estatus Departamento " + ex.Message, ex);
        //                }
        //                finally
        //                {
        //                    if (connection != null)
        //                        connection.Close();
        //                    if (rd != null)
        //                        rd.Close();
        //                }

        //                return Vlist;
        //            }
        //        }

        //        catch (Exception ex)
        //        {
        //            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
        //        }
        //    }

        //}





        #endregion

        #region update
        public InfoMobiliti UpdateVendedor(string nombre, int? clv_vendedor, string domicilio, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_vendedor @clv_vendedor,@nombre,@domicilio,@activo", connection);

                        command.Parameters.AddWithValue("@clv_vendedor", clv_vendedor);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@domicilio", domicilio);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateVendedor " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public InfoMobiliti UpdateVista(string nombre, int? clv_vista, string descripcion, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_vista   @clv_vista , @nombre , @descripcion ,@activo", connection);

                        command.Parameters.AddWithValue("@clv_vista", clv_vista);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", descripcion);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateVista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public InfoMobiliti UpdateAmenitie(string nombre, int? clv_amenitie, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_amenitie  @clv_amenitie , @nombre,@descripcion ,@activo", connection);

                        command.Parameters.AddWithValue("@clv_amenitie", clv_amenitie);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", nombre);
                        command.Parameters.AddWithValue("@activo", activo);


                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateAmenitie " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public InfoMobiliti UpdateSDepartamento(string nombre, int? clv_espacio, bool? suma, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_espacio_departamento  @clv_espacio , @nombre ,@activo, @suma ", connection);

                        command.Parameters.AddWithValue("@clv_espacio", clv_espacio);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@activo", activo);
                        command.Parameters.AddWithValue("@suma", suma);

                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateVendedor " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? UpdateUsuarios(UsuarioEntity usuario, VendedorEntity vendedor)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try {

                    return UsuarioController.EditUsuario(usuario, vendedor);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public InfoComprador UpdateComprador(string nombre, int? clv_comprador, string domicilio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_comprador @clv_comprador,@nombre,@domicilio", connection);

                        command.Parameters.AddWithValue("@clv_comprador", clv_comprador);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@domicilio", domicilio);


                        InfoComprador i = new InfoComprador();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateVendedor " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? UpdateEstado(string nombre, int? clv_estado)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_pais @clv_pais,@nombre", connection);

                        command.Parameters.AddWithValue("@clv_pais", clv_estado);
                        command.Parameters.AddWithValue("@nombre", nombre);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateEstado " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? UpdateColonia(ColoniaEntity colonia)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_colonia  @nombre,@activo,@clv_pais,@clv_ciudad,@cp,@clv_colonia", connection);

                        command.Parameters.AddWithValue("@nombre", colonia.nombre);
                        command.Parameters.AddWithValue("@activo", colonia.activo);
                        command.Parameters.AddWithValue("@clv_pais", colonia.clv_pais);
                        command.Parameters.AddWithValue("@clv_ciudad", colonia.clv_ciudad);
                        command.Parameters.AddWithValue("@cp", colonia.cp);
                        command.Parameters.AddWithValue("@clv_colonia", colonia.clv_colonia);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateColonia " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        private int? DeleteRelLocalidadesMunicipio(int? clv_localidad)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                int result = 0;
                IDataReader rd = null;
                try
                {


                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand strSQL = new SqlCommand("elimina_relLocalidadesMunicipio");
                    strSQL.Connection = connection;
                    strSQL.CommandType = CommandType.StoredProcedure;
                    strSQL.CommandTimeout = 0;

                    SqlParameter par2 = new SqlParameter("@clv_localidad", SqlDbType.Int);
                    par2.Value = clv_localidad;
                    par2.Direction = ParameterDirection.Input;
                    strSQL.Parameters.Add(par2);

                    strSQL.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    if (connection != null)
                        connection.Close();
                    throw new Exception("Error elimina_relLocalidadesMunicipio " + ex.Message, ex);
                }

                return result;
            }
        }


        public int? UpdateLocalidad(localidad obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                int result = 0;
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {

                    IDataReader rd = null;
                    try
                    {


                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        SqlCommand strSQL = new SqlCommand("update_localidad");
                        strSQL.Connection = connection;
                        strSQL.CommandType = CommandType.StoredProcedure;
                        strSQL.CommandTimeout = 0;

                        SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                        par1.Value = obj.nombre;
                        par1.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par1);

                        SqlParameter par2 = new SqlParameter("@clv_localidad", SqlDbType.Int);
                        par2.Value = obj.clv_localidad;
                        par2.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par2);

                        strSQL.ExecuteNonQuery();

                    }
                    catch (Exception ex)
                    {
                        if (connection != null)
                            connection.Close();
                        throw new Exception("Error update_localidad " + ex.Message, ex);
                    }


                    DeleteRelLocalidadesMunicipio(obj.clv_localidad);

                    obj.municipios.ForEach(x =>
                    {

                        try
                        {
                            SqlCommand command = new SqlCommand("insert_localidadesMunicipios  @clv_municipio,@clv_localidad", connection);
                            command.Parameters.AddWithValue("@clv_municipio", x.clv_municipio);
                            command.Parameters.AddWithValue("@clv_localidad", obj.clv_localidad);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_EstadoMunicipios " + ex.Message, ex);
                        }

                    });

                    return result;
                }
            }

        }
        public int? UpdateDepartamentos(DepartamentoEntity obj, List<DepartamentoEspacioEntity> espacios)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {


                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {


                    IDataReader rd = null;
                    try
                    {
                        SqlCommand command = new SqlCommand("update_departamentos", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Clv_departamento", obj.Clv_departamento);
                        command.Parameters.AddWithValue("@TipoDepartamento", obj.TipoDepartamento);
                        command.Parameters.AddWithValue("@NumeroNiveles", obj.NumeroNiveles);
                        command.Parameters.AddWithValue("@MetrosCuadradosTotales", obj.MetrosTotales);
                        command.Parameters.AddWithValue("@CostoDepartamento ", obj.CostoDepartamento);
                        command.Parameters.AddWithValue("@Activo", obj.Activo);
                        command.Parameters.AddWithValue("@Lado", obj.Lado);
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Erro insert_departamentos " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }


                    if (obj.Clv_departamento > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<espacios>");
                        espacios.ForEach(y =>
                        {
                            sb.Append("<espacio>");
                            sb.Append("<clvespacio>" + y.clv_espacio_departamento + "</clvespacio>");
                            sb.Append("<metros>" + y.metros + "</metros>");
                            sb.Append("<descripcion>" + y.descripcion + "</descripcion>");
                            sb.Append("</espacio>");
                        });
                        sb.Append("</espacios>");


                        try
                        {

                            SqlCommand command = new SqlCommand("GeneraEspaciosDepartamento", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@clv_departamento", obj.Clv_departamento);
                            command.Parameters.AddWithValue("@xml", sb.ToString());


                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro GeneraEspaciosDepartamento " + ex.Message, ex);
                        }

                    }



                    return obj.Clv_departamento;
                }

            }

        }

        public int? UpdateProyecto(ProyectoEntity proyecto, List<TorreEntity> torres, List<InfoMobiliti> amenities, List<ServiciosEntity> servicio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    MessageEntity m = new MessageEntity();
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        SqlCommand cmd = new SqlCommand("update_proyecto", connection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = proyecto.nombre;
                        cmd.Parameters.Add("@numeroExterior", SqlDbType.VarChar).Value = proyecto.numeroExterior;
                        cmd.Parameters.Add("@costoMetroCuadradoInicial", SqlDbType.Money).Value = proyecto.costoMetroCuadradoInicial;
                        cmd.Parameters.Add("@precioVentaActual", SqlDbType.Money).Value = proyecto.precioVentaActual;
                        cmd.Parameters.Add("@clv_estado", SqlDbType.Int).Value = proyecto.clv_estado;
                        cmd.Parameters.Add("@clv_municipio", SqlDbType.Int).Value = proyecto.clv_municipio;
                        cmd.Parameters.Add("@clv_localidad", SqlDbType.Int).Value = proyecto.clv_localidad;
                        cmd.Parameters.Add("@clv_colonia", SqlDbType.Int).Value = proyecto.clv_colonia;
                        cmd.Parameters.Add("@activo", SqlDbType.Bit).Value = proyecto.activo;
                        cmd.Parameters.Add("@latitud", SqlDbType.VarChar).Value = proyecto.latitud;
                        cmd.Parameters.Add("@longuitud", SqlDbType.VarChar).Value = proyecto.longuitud;
                        cmd.Parameters.Add("@factorIncremento", SqlDbType.Int).Value = proyecto.factorIncremento;
                        cmd.Parameters.Add("@cambioEtapaPorVenta", SqlDbType.Int).Value = proyecto.cambioEtapaPorVenta;
                        cmd.Parameters.Add("@direccion", SqlDbType.VarChar).Value = proyecto.direccion;
                        cmd.Parameters.Add("@numeroEtapas", SqlDbType.VarChar).Value = proyecto.numeroEtapas;
                        cmd.Parameters.Add("@frasePublicitaria", SqlDbType.VarChar).Value = proyecto.frasePublicitaria;
                        cmd.Parameters.Add("@clv_proyecto", SqlDbType.Int).Value = proyecto.clv_proyecto;
                        cmd.ExecuteNonQuery();

                    }
                    catch (Exception ex)
                    {
                        connection.Close();
                        throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                    }



                    if (proyecto.clv_proyecto > 0)
                    {
                        StringBuilder xml = new StringBuilder();
                        xml.Append("<proyecto>");

                        torres.ForEach(torre_ =>
                        {


                            torre_.Pisos.ForEach(piso_ =>
                            {

                                piso_.Departamentos.ForEach(departamento_ =>
                                {

                                    for (var a = 0; a < departamento_.cantidad; a++)
                                    {
                                        xml.Append("<torre>");
                                        xml.Append("<proyecto>" + proyecto.clv_proyecto + "</proyecto>");
                                        xml.Append("<torre>" + torre_.Descripcion + "</torre>");
                                        xml.Append("<piso>" + piso_.Piso + "</piso>");
                                        xml.Append("<departamento>" + departamento_.Clv_departamento + "</departamento>");
                                        xml.Append("<fechaentrega>" + piso_.FechaEntrega + "</fechaentrega>");
                                        xml.Append("<etapa>" + piso_.Etapa + "</etapa>");
                                        xml.Append("<Lado>" + departamento_.Lado + "</Lado>");
                                        xml.Append("</torre>");

                                    }


                                });


                            });


                        });
                        xml.Append("</proyecto>");

                        try
                        {

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand cmd = new SqlCommand("GeneraTorrePisoDepartamento", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@xml", SqlDbType.VarChar).Value = xml.ToString();


                            var returnParameter1 = cmd.Parameters.Add("@mensaje", SqlDbType.Int);
                            returnParameter1.Direction = ParameterDirection.Output;

                            var returnParameter2 = cmd.Parameters.Add("@error", SqlDbType.Int);
                            returnParameter2.Direction = ParameterDirection.Output;

                            cmd.ExecuteNonQuery();


                            m.error = int.Parse(returnParameter2.Value.ToString());
                            m.mensaje = returnParameter1.Value.ToString();



                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }



                        try
                        {

                            StringBuilder str = new StringBuilder();
                            str.Append("<amenities>");

                            amenities.ForEach(x =>
                            {

                                str.Append("<ameninitie>");
                                str.Append(" <clv>" + x.clv_amenitie + "</clv>");
                                str.Append(" <observacion>" + x.observacion + "</observacion>");
                                str.Append("</ameninitie>");
                            });

                            str.Append("</amenities>");

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand cmd = new SqlCommand("GeneraAmenitiesProyecto", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@xml", SqlDbType.VarChar).Value = str.ToString();
                            cmd.Parameters.Add("@clv_proyecto", SqlDbType.Int).Value = proyecto.clv_proyecto;
                            cmd.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }
                        try
                        {

                            StringBuilder str = new StringBuilder();
                            str.Append("<servicio>");

                            servicio.ForEach(x =>
                            {

                                str.Append("<servicio>");
                                str.Append(" <clv>" + x.clv_servicio + "</clv>");
                                str.Append(" <descripcion>" + x.descripcion + "</descripcion>");
                                str.Append("</servicio>");
                            });

                            str.Append("</servicio>");

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand cmd = new SqlCommand("GeneraServiciosProyecto", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@xml", SqlDbType.VarChar).Value = str.ToString();
                            cmd.Parameters.Add("@clv_proyecto", SqlDbType.Int).Value = proyecto.clv_proyecto;
                            cmd.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }

                    }
                }
            

                return 1;
            }

        }


        public void EliminaDepartamentosCliente(int? clv_cliente)
        {
            
                int result = 0;
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("EliminaDepartamentosCliente", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@clv_cliente", clv_cliente);
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                }
            
         }
        
        public int? UpdateCliente(ClienteEntity obj)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("UpdateCliente", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@id", SqlDbType.VarChar).Value = obj.id;
                        command.Parameters.Add("@nombre", SqlDbType.VarChar).Value = obj.nombre;
                        command.Parameters.Add("@nit", SqlDbType.VarChar).Value = obj.nit;
                        command.Parameters.Add("@pais", SqlDbType.VarChar).Value = obj.clv_pais;
                        command.Parameters.Add("@ciudad", SqlDbType.VarChar).Value = obj.clv_ciudad;
                        command.Parameters.Add("@telefono", SqlDbType.VarChar).Value = obj.telefono;
                        command.Parameters.Add("@celular", SqlDbType.VarChar).Value = obj.celular;
                        command.Parameters.Add("@correo", SqlDbType.VarChar).Value = obj.correo;
                        command.Parameters.Add("@cedula_rep_legal", SqlDbType.VarChar).Value = obj.cedulaR;
                        command.Parameters.Add("@rut", SqlDbType.VarChar).Value = obj.rut;
                        command.Parameters.Add("@smart_hostname", SqlDbType.VarChar).Value = obj.Dominio;
                        command.Parameters.Add("@smart_password", SqlDbType.VarChar).Value = obj.password;
                        command.Parameters.Add("@colonia", SqlDbType.VarChar).Value = obj.colonia;
                        command.Parameters.Add("@calle", SqlDbType.VarChar).Value = obj.calle;
                        command.Parameters.Add("@Usuario", SqlDbType.VarChar).Value = obj.usuario;
                        command.Parameters.Add("@created_at", SqlDbType.VarChar).Value = obj.Fechacreacion;
                        command.Parameters.Add("@nombreR", SqlDbType.VarChar).Value = obj.nombreR;
                        command.Parameters.Add("@nombrecontratoA", SqlDbType.VarChar).Value = obj.nombrecontrato;
                        command.Parameters.Add("@requiereFac", SqlDbType.VarChar).Value = obj.activo;
                        command.Parameters.Add("@correo1", SqlDbType.VarChar).Value = obj.correo1;
                        command.Parameters.Add("@smart_password1", SqlDbType.VarChar).Value = obj.password1;
                        command.Parameters.Add("@telefonoCA", SqlDbType.VarChar).Value = obj.telefonoCA;
                        command.Parameters.Add("@CorreoCadmin", SqlDbType.VarChar).Value = obj.CorreoCadmin;
           










                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }

                   
                        try
                        {
                            

                        }
                        catch (Exception ex)
                        {
                            connection.Close();
                            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                        }
                        

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



            public List<EstatusDepartamentoEntity> GetEstatusDepartamentoList(int? clv_vendedor, int? clv_proyecto, string torre, int? piso, int? estatus, string NumDepartamento, int? clv_vista)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<EstatusDepartamentoEntity> lista = new List<EstatusDepartamentoEntity>();

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand command = new SqlCommand("select_estatusDepartamento", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@clv_vendedor", clv_vendedor);
                            command.Parameters.AddWithValue("@clv_proyecto", clv_proyecto);
                            command.Parameters.AddWithValue("@torre", torre);
                            command.Parameters.AddWithValue("@piso", piso); 
                            command.Parameters.AddWithValue("@estatus", estatus);
                            command.Parameters.AddWithValue("@NumDepartamento", NumDepartamento);
                            command.Parameters.AddWithValue("@clv_vista", clv_vista);
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                                EstatusDepartamentoEntity ed = new EstatusDepartamentoEntity();

                                ed.IdDetalle = int.Parse(rd[0].ToString());
                                ed.NumDepartamento = rd[1].ToString();
                                ed.FechaEntrega = "";
                                try
                                {
                                    ed.FechaEntrega = DateTime.Parse(rd[2].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.Estatus = int.Parse(rd[3].ToString());

                                ed.Clv_vendedor = 0;
                                try
                                {
                                    ed.Clv_vendedor = int.Parse(rd[4].ToString());
                                }
                                catch { }

                                ed.Clv_comprador = 0;
                                try
                                {
                                    ed.Clv_comprador = int.Parse(rd[5].ToString());
                                }
                                catch { }



                                ed.PrecioLista = 0;
                                try
                                {
                                    ed.PrecioLista = decimal.Parse(rd[6].ToString());
                                }
                                catch
                                { }

                                ed.PrecioActual = 0;
                                try
                                {
                                    ed.PrecioActual = decimal.Parse(rd[7].ToString());
                                }
                                catch
                                { }

                                ed.PrecioVendio = 0;
                                try
                                {
                                    ed.PrecioVendio = decimal.Parse(rd[8].ToString());
                                }
                                catch
                                { }

                                ed.FechaApartado = "";
                                try
                                {
                                    ed.FechaApartado = DateTime.Parse(rd[9].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.ImporteApartado = 0;
                                try
                                {
                                    ed.ImporteApartado = decimal.Parse(rd[10].ToString());
                                }
                                catch { }

                                ed.FechaVenta = "";
                                try
                                {
                                    ed.FechaVenta = DateTime.Parse(rd[11].ToString()).ToShortDateString();
                                }
                                catch { }


                                ed.FechaEscrituracion = "";
                                try
                                {
                                    ed.FechaEscrituracion = DateTime.Parse(rd[12].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.Notario = rd[13].ToString();

                                ed.Clv_Etapa = int.Parse(rd[14].ToString());

                                ed.Lado = rd[15].ToString();


                                //ed.Nivel = "";
                                //try
                                //{
                                //    ed.Nivel = rd[16].ToString();
                                //}
                                //catch { }



                                ed.IdDetalle = int.Parse(rd[16].ToString());



                                ed.ImporteEnganche = 0;
                                try
                                {
                                    ed.ImporteEnganche = decimal.Parse(rd[17].ToString());
                                }
                                catch { }


                                ed.PorcentajeEncganche = 0;
                                try
                                {
                                    ed.PorcentajeEncganche = int.Parse(rd[18].ToString());
                                }
                                catch { }


                                ed.NumeroPagosEnganche = 0;
                                try
                                {
                                    ed.NumeroPagosEnganche = int.Parse(rd[19].ToString());
                                }
                                catch { }



                                ed.clv_proyecto = int.Parse(rd[20].ToString());

                                ed.Torre = rd[21].ToString();

                                ed.Piso = int.Parse(rd[22].ToString());

                                ed.clv_Departamento = int.Parse(rd[23].ToString());

                                ed.proyecto = rd[24].ToString();

                                ed.departamento = rd[25].ToString();

                                ed.vistas = new List<InfoMobiliti>();

                                ed.vistas = GetVistasEstatusDepartamento(ed.IdDetalle);

                                lista.Add(ed);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_estatusDepartamento_clave " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return lista;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        private List<DepartamentoEnganchePagoEntity> GetPagosEstatusDep(long? IdDetalle)
        {
            List<DepartamentoEnganchePagoEntity> lista = new List<DepartamentoEnganchePagoEntity>();
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("GetPagosEstatusDepartamento @IdDetalle", connection);
                command.Parameters.AddWithValue("@IdDetalle", IdDetalle);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = command.ExecuteReader();
                    while (rd.Read())
                    {
                        DepartamentoEnganchePagoEntity obj = new DepartamentoEnganchePagoEntity();
                        obj.NoPago = int.Parse(rd[1].ToString());
                        obj.Fecha = DateTime.Parse(rd[2].ToString()).ToShortDateString();
                        obj.Importe = decimal.Parse(rd[3].ToString());
                        lista.Add(obj);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error GetVistasEstatusDepartamento " + ex.Message, ex);
                }
            }

            return lista;
        }


        public EstatusDepartamentoEntity GetEstatusDepartamento(int? IdDetalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    EstatusDepartamentoEntity ed = new EstatusDepartamentoEntity();
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand command = new SqlCommand("select_estatusDepartamento_clave", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@IdDetalle", IdDetalle);


                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {



                                ed.IdDetalle = int.Parse(rd[0].ToString());
                                ed.NumDepartamento = rd[1].ToString();
                                ed.FechaEntrega = "";
                                try
                                {
                                    ed.FechaEntrega = DateTime.Parse(rd[2].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.Estatus = int.Parse(rd[3].ToString());

                                ed.Clv_vendedor = 0;
                                try
                                {
                                    ed.Clv_vendedor = int.Parse(rd[4].ToString());
                                }
                                catch { }

                                ed.Clv_comprador = 0;
                                try
                                {
                                    ed.Clv_comprador = int.Parse(rd[5].ToString());
                                }
                                catch { }




                       

                                ed.PrecioVendio = 0;
                                try
                                {
                                    ed.PrecioVendio = decimal.Parse(rd[8].ToString());
                                }
                                catch
                                { }

                                ed.FechaApartado = "";
                                try
                                {
                                    ed.FechaApartado = DateTime.Parse(rd[9].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.ImporteApartado = 0;
                                try
                                {
                                    ed.ImporteApartado = decimal.Parse(rd[10].ToString());
                                }
                                catch { }

                                ed.FechaVenta = "";
                                try
                                {
                                    ed.FechaVenta = DateTime.Parse(rd[11].ToString()).ToShortDateString();
                                }
                                catch { }


                                ed.FechaEscrituracion = "";
                                try
                                {
                                    ed.FechaEscrituracion = DateTime.Parse(rd[12].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.Notario = rd[13].ToString();

                                ed.Clv_Etapa = int.Parse(rd[14].ToString());

                                ed.Lado = rd[15].ToString();

                                //ed.Nivel = "";
                                //try
                                //{
                                //    ed.Nivel = rd[16].ToString();
                                //}
                                //catch { }

                                ed.IdDetalle = int.Parse(rd[16].ToString());



                                ed.ImporteEnganche = 0;
                                try
                                {
                                    ed.ImporteEnganche = decimal.Parse(rd[17].ToString());
                                }
                                catch { }


                                ed.PorcentajeEncganche = 0;
                                try
                                {
                                    ed.PorcentajeEncganche = int.Parse(rd[18].ToString());
                                }
                                catch { }


                                ed.NumeroPagosEnganche = 0;
                                try
                                {
                                    ed.NumeroPagosEnganche = int.Parse(rd[19].ToString());
                                }
                                catch { }



                                ed.clv_proyecto = int.Parse(rd[20].ToString());

                                ed.Torre = rd[21].ToString();

                                ed.Piso = int.Parse(rd[22].ToString());

                                ed.clv_Departamento = int.Parse(rd[23].ToString());

                                ed.proyecto = rd[24].ToString();

                                ed.departamento = rd[25].ToString();

                                ed.preciolista = 0;
                                try
                                {
                                    ed.preciolista = int.Parse(rd[26].ToString());
                                }
                                catch
                                { }
                                ed.precioActual = 0;
                                try
                                {
                                    ed.precioActual = decimal.Parse(rd[27].ToString());
                                }
                                catch
                                { }


                                ed.vistas = new List<InfoMobiliti>();

                                ed.vistas = GetVistasEstatusDepartamento(ed.IdDetalle);

                                ed.pagos = GetPagosEstatusDep(ed.IdDetalle);

                                ed.nivel = new List<relNivelesDepartamentoEntity>();

                                ed.nivel = GetNivelbyDepartamento(ed.IdDetalle);

                                DateTime fechaEntrega = DateTime.Parse(rd[2].ToString());

                                ed.mesesDiferencia = ((fechaEntrega.Year - DateTime.Now.Year ) * 12) + fechaEntrega.Month  - DateTime.Now.Month;

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_estatusDepartamento_clave " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return ed;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        private List<InfoMobiliti> GetVistasEstatusDepartamento(long? IdDetalle)
        {
            List<InfoMobiliti> lista = new List<InfoMobiliti>();
            int result = 0;
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("GetVistasEstatusDepartamento @IdDetalle", connection);
                command.Parameters.AddWithValue("@IdDetalle", IdDetalle);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = command.ExecuteReader();
                    while (rd.Read())
                    {
                        InfoMobiliti obj = new InfoMobiliti();
                        obj.clv_vista = int.Parse(rd[0].ToString());
                        obj.descripcion = rd[2].ToString();
                        lista.Add(obj);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error GetVistasEstatusDepartamento " + ex.Message, ex);
                }
            }

            return lista;
        }

        public void EliminaNivelesDepartamento(long id_Departamento )
        {

            int result = 0;
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("ELiminaNivelDepartamentos", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id_Departamento", id_Departamento);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    throw new Exception("Error " + ex.Message, ex);
                }
            }

        }


        public int? UpdateEstatusDepartamento(EstatusDepartamentoEntity obj, List<InfoMobiliti> vistas, List<DepartamentoEnganchePagoEntity> pagos, List<NivelesEntity> nivel)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand command = new SqlCommand("update_estatusDepartamento", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@IdDetalle", obj.IdDetalle);
                            command.Parameters.AddWithValue("@NumDepartamento", obj.NumDepartamento);
                            //command.Parameters.AddWithValue("@FechaEntrega", obj.FechaEntrega);
                            //command.Parameters.AddWithValue("@Estatus ", obj.Estatus);
                            //command.Parameters.AddWithValue("@Clv_vendedor", obj.Clv_vendedor);
                            //command.Parameters.AddWithValue("@Clv_comprador", obj.Clv_comprador);
                            //command.Parameters.AddWithValue("@PrecioLista", obj.PrecioLista);
                            //command.Parameters.AddWithValue("@PrecioActual", obj.PrecioActual);
                            //command.Parameters.AddWithValue("@PrecioVendio", obj.PrecioVendio);
                            //command.Parameters.AddWithValue("@FechaApartado", obj.FechaApartado);
                            //command.Parameters.AddWithValue("@ImporteApartado", obj.ImporteApartado);
                            //command.Parameters.AddWithValue("@FechaVenta", obj.FechaVenta);
                            //command.Parameters.AddWithValue("@FechaEscrituracion", obj.FechaEscrituracion);
                            command.Parameters.AddWithValue("@Notario", obj.Notario);
                            command.Parameters.AddWithValue("@Lado", obj.Lado);
                            //command.Parameters.AddWithValue("@Clv_Etapa", obj.Clv_Etapa);
                            //command.Parameters.AddWithValue("@ImporteEnganche", obj.ImporteEnganche);
                            //command.Parameters.AddWithValue("@PorcentajeEncganche", obj.PorcentajeEncganche);
                            //command.Parameters.AddWithValue("@NumeroPagosEnganche", obj.NumeroPagosEnganche);
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro update_estatusDepartamento " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        StringBuilder xml = new StringBuilder();
                        xml.Append("<vistas>");
                        vistas.ForEach(x =>
                        {
                            xml.Append("<vista>");
                            xml.Append("<clvvista>" + x.clv_vista + "</clvvista>");
                            xml.Append("</vista>");
                        });
                        xml.Append("</vistas>");

                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand command = new SqlCommand("GeneraVistasEstatusDepartamento", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@IdDetalle", obj.IdDetalle);
                            command.Parameters.AddWithValue("@xml", xml.ToString());
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro GeneraVistasEstatusDepartamento " + ex.Message, ex);
                        }


                        StringBuilder xmlpagos = new StringBuilder();
                        xmlpagos.Append("<pagos>");
                        pagos.ForEach(x1 =>
                        {
                            xmlpagos.Append("<pago>");
                            xmlpagos.Append("<nopago>" + x1.NoPago + "</nopago>");
                            xmlpagos.Append("<fecha>" + x1.Fecha + "</fecha>");
                            xmlpagos.Append("<importe>" + x1.Importe + "</importe>");
                            xmlpagos.Append("</pago>");

                        });
                        xmlpagos.Append("</pagos>");


                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand command = new SqlCommand("GeneraPagosDepartamento", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@IdDetalle", obj.IdDetalle);
                            command.Parameters.AddWithValue("@xml", xmlpagos.ToString());
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro GeneraPagosDepartamento " + ex.Message, ex);
                        }

                        EliminaNivelesDepartamento(obj.IdDetalle);

                        StringBuilder xmlnivel = new StringBuilder();
                        xmlnivel.Append("<Niveles>");
                        nivel.ForEach(x =>
                        {
                            xmlnivel.Append("<Nivel>");
                            xmlnivel.Append("<Nvl>" + x.Nivel + "</Nvl>");
                            xmlnivel.Append("</Nivel>");
                        });
                        xmlnivel.Append("</Niveles>");

                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand command = new SqlCommand("GeneraNivelDepartamento", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@clv_Departamento", obj.IdDetalle);
                            command.Parameters.AddWithValue("@xml", xmlnivel.ToString());
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro GeneraNivelDepartamento " + ex.Message, ex);
                        }


                        return 1;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }













        #endregion





        public List<tipoUsuario> GetTiposUsuario()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_tipoUsuario", connection);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                tipoUsuario tu = new tipoUsuario();
                                tu.Id = Int32.Parse(rd[0].ToString());
                                tu.Nombre = rd[1].ToString();
                                tu.Activo = bool.Parse(rd[2].ToString());
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public List<tipoUsuario> GetStatus()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetStatus", connection);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                tipoUsuario tu = new tipoUsuario();
                                tu.Id = Int32.Parse(rd[0].ToString());
                                tu.Nombre = rd[1].ToString();
                                //tu.Activo = bool.Parse(rd[2].ToString());
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public List<tipoUsuario> GetStatusById(int? Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetStatusById @Id", connection);
                        command.Parameters.AddWithValue("@Id", Id);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                tipoUsuario tu = new tipoUsuario();
                                tu.Id = Int32.Parse(rd[0].ToString());
                                tu.Nombre = rd[1].ToString();
                                //tu.Activo = bool.Parse(rd[2].ToString());
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }




        public int? AddTiposUsuario(tipoUsuario obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_tipoUsuario @nombre,@activo", connection);
                        command.Parameters.AddWithValue("@nombre", obj.Nombre);
                        command.Parameters.AddWithValue("@activo", obj.Activo);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                            result = 1;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public string DeletePerfil(tipoUsuario obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                       
                        SqlCommand command = new SqlCommand("eliminaPerfil", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Id", obj.Id);
                        var returnParameter = command.Parameters.Add("@MSG", SqlDbType.VarChar,250);
                        returnParameter.Direction = ParameterDirection.Output;

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                            result = returnParameter.Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public string DeleteCiudad(tipoUsuario obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        SqlCommand command = new SqlCommand("EliminaCiudad", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Id", obj.Id);
                        var returnParameter = command.Parameters.Add("@MSG", SqlDbType.VarChar, 250);
                        returnParameter.Direction = ParameterDirection.Output;

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                            result = returnParameter.Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }





        public int? Addstatus(tipoUsuario obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("AddStatus @Descripcion", connection);
                        command.Parameters.AddWithValue("@Descripcion", obj.Nombre);
                        //command.Parameters.AddWithValue("@activo", obj.Activo);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                            result = 1;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }





        public List<Modulo> ObtenModulos()
        {
            List<Modulo> modulos = new List<Modulo>();
            List<Modulo> modulosOrdenados = new List<Modulo>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetModulosPermisos", connection);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                Modulo tu = new Modulo();
                                tu.Id = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                tu.componente = rd[2].ToString();
                                tu.activo = bool.Parse(rd[3].ToString());
                                tu.Icono = rd[4].ToString();
                                tu.Principal = bool.Parse(rd[5].ToString());
                                tu.ModuloPadre = int.Parse(rd[6].ToString());
                                tu.Orden = int.Parse(rd[7].ToString());
                                tu.Add = bool.Parse(rd[8].ToString());
                                tu.Edit = bool.Parse(rd[9].ToString());
                                tu.Delete = bool.Parse(rd[10].ToString());
                                tu.Access = bool.Parse(rd[11].ToString());
                                modulos.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error update_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }




                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }


                List<Modulo> principales = modulos.Where(x=> x.Principal == true).OrderBy(x=>x.Orden).ToList();
                principales.ForEach(x=> {
                    
                    x.Tipo = 1;
                    modulosOrdenados.Add(x);
                    List<Modulo> hijos = modulos.Where(s=> s.ModuloPadre == x.Id).OrderBy(s => s.Orden).ToList();

                    hijos.ForEach(t=> {
                        List<Modulo> nietos = modulos.Where(u => u.ModuloPadre == t.Id).OrderBy(u => u.Orden).ToList();
                        t.Tipo = (nietos.Count > 0) ? 4 : 2;
                        modulosOrdenados.Add(t);

                        nietos.ForEach(a =>
                        {

                            a.Tipo = 3;
                            modulosOrdenados.Add(a);
                        });

                    });
                });
                return modulosOrdenados;
            }
        }

        public List<Modulo> ObtenModulosUsuario(int? tipoUsuario, int? op)
        {
            List<Modulo> modulos = new List<Modulo>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {


                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetModulosPrincipales @tipoUsuario", connection);
                        command.Parameters.AddWithValue("@tipoUsuario", tipoUsuario);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                Modulo tu = new Modulo();
                                tu.Id = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                tu.componente = rd[2].ToString();
                                tu.activo = bool.Parse(rd[3].ToString());
                                tu.Icono = rd[4].ToString();
                                tu.Principal = bool.Parse(rd[5].ToString());
                                tu.ModuloPadre = int.Parse(rd[6].ToString());
                                tu.Add = bool.Parse(rd[8].ToString());
                                tu.Edit = bool.Parse(rd[9].ToString());
                                tu.Delete = bool.Parse(rd[10].ToString());
                                tu.Access = bool.Parse(rd[11].ToString());
                                modulos.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error update_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }




                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }               

                return modulos;
            }



        }


        public List<Modulo> ObtenMenuUsuario(int? tipoUsuario, int? op)
        {
            List<Modulo> modulos = new List<Modulo>();
            List<Modulo> modulosOrdenados = new List<Modulo>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {


                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetModulosPrincipales @tipoUsuario", connection);
                        command.Parameters.AddWithValue("@tipoUsuario", tipoUsuario);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                Modulo tu = new Modulo();
                                tu.Id = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                tu.componente = rd[2].ToString();
                                tu.activo = bool.Parse(rd[3].ToString());
                                tu.Icono = rd[4].ToString();
                                tu.Principal = bool.Parse(rd[5].ToString());
                                tu.ModuloPadre = int.Parse(rd[6].ToString());
                                tu.Orden = int.Parse(rd[7].ToString());
                                tu.Add = bool.Parse(rd[8].ToString());
                                tu.Edit = bool.Parse(rd[9].ToString());
                                tu.Delete = bool.Parse(rd[10].ToString());
                                tu.Access = bool.Parse(rd[11].ToString());
                                modulos.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error update_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

                List<Modulo> principales = modulos.Where(x => x.Principal == true).OrderBy(x => x.Orden).ToList();
                principales.ForEach(x => {
                    x.SubModulos = new List<Modulo>();
                    List<Modulo> hijos = modulos.Where(s => s.ModuloPadre == x.Id).OrderBy(s => s.Orden).ToList();
                    x.SubModulos.AddRange( hijos);

                    x.SubModulos.ForEach(t => {
                        t.SubModulos = new List<Modulo>();
                        List<Modulo> nietos = modulos.Where(u => u.ModuloPadre == t.Id).OrderBy(u => u.Orden).ToList();
                        t.SubModulos.AddRange(nietos);

                    });
                });

                return principales;
            }
        }

        public int? GuardaPermisos(List<Modulo> modulos, int? tipoUsuario)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {


                int result = 0;
                StringBuilder sb = new StringBuilder();
                sb.Append("<permisos>");
                modulos.ForEach(sub =>
                {
                    sb.Append("<permiso>");
                    sb.Append("<tipousuario>" + tipoUsuario + "</tipousuario>");
                    sb.Append("<modulo>" + sub.Id + "</modulo>");
                    sb.Append("<add>" + sub.Add + "</add>");
                    sb.Append("<edit>" + sub.Edit + "</edit>");
                    sb.Append("<delete>" + sub.Delete + "</delete>");
                    sb.Append("<access>" + sub.Access + "</access>");
                    sb.Append("</permiso>");


                    //sub.SubModulos.ForEach(x =>
                    //{
                    //    sb.Append("<permiso>");
                    //    sb.Append("<tipousuario>" + tipoUsuario + "</tipousuario>");
                    //    sb.Append("<modulo>" + x.Id + "</modulo>");
                    //    sb.Append("<add>" + x.Add + "</add>");
                    //    sb.Append("<edit>" + x.Edit + "</edit>");
                    //    sb.Append("<delete>" + x.Delete + "</delete>");
                    //    sb.Append("<access>" + x.Access + "</access>");
                    //    sb.Append("</permiso>");
                    //});
                });
                sb.Append("</permisos>");


                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("GuardaPermisos @xml,@tipoUsuario", connection);
                    command.Parameters.AddWithValue("@xml", sb.ToString());
                    command.Parameters.AddWithValue("@tipoUsuario", tipoUsuario);

                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                        result = 1;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error GuardaPermisos " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                    }

                }
                return result;

            }
        }

        public int? UpdateTiposUsuario(tipoUsuario obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_tipoUsuario @id,@nombre,@activo", connection);
                        command.Parameters.AddWithValue("@id", obj.Id);
                        command.Parameters.AddWithValue("@nombre", obj.Nombre);
                        command.Parameters.AddWithValue("@activo", obj.Activo);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                            result = 1;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error update_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public int? UpdateStatus(tipoUsuario obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("UpdateStatus @Descripcion, @Id", connection);
                        command.Parameters.AddWithValue("@Id", obj.Id);
                        command.Parameters.AddWithValue("@Descripcion", obj.Nombre);
                    
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                            result = 1;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error update_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }





        public int? AddModelo(tipoUsuario obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        //SqlCommand command = new SqlCommand("AddModelo @Descripcion", connection);
                        //command.Parameters.AddWithValue("@Descripcion", obj.Nombre);

                        SqlCommand strSQL = new SqlCommand("AddModelo");
                        strSQL.Connection = connection;
                        strSQL.CommandType = CommandType.StoredProcedure;
                        strSQL.CommandTimeout = 0;

                        SqlParameter par1 = new SqlParameter("@Descripcion", SqlDbType.VarChar);
                        par1.Value = obj.Nombre;
                        par1.Direction = ParameterDirection.Input;
                        strSQL.Parameters.Add(par1);

                        SqlParameter par2 = new SqlParameter("@clv_modelo", SqlDbType.Int);
                        par2.Direction = ParameterDirection.Output;
                        strSQL.Parameters.Add(par2);
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        strSQL.ExecuteNonQuery();

                        result = int.Parse(par2.SqlValue.ToString());


                        //command.Parameters.AddWithValue("@activo", obj.Activo);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            //command.ExecuteNonQuery();
                           
                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                            throw new Exception("Error insert_tipoUsuario " + ex.Message, ex);
                        }

                        obj.Marca.ForEach(x =>
                        {

                            try
                            {
                                SqlCommand command = new SqlCommand("AddMarcaModelo @clv_marca,@clv_modelo", connection);
                                command.Parameters.AddWithValue("@clv_modelo", result);
                                command.Parameters.AddWithValue("@clv_marca",x.clv_marca );
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection != null)
                                    connection.Close();
                                throw new Exception("Error insert_PaisCiudades " + ex.Message, ex);
                            }

                        });





                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }




        public int? UpdateModelo(tipoUsuario obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("UpdateModelo @Descripcion, @Id", connection);
                        command.Parameters.AddWithValue("@Id", obj.Id);
                        command.Parameters.AddWithValue("@Descripcion", obj.Nombre);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            command.ExecuteNonQuery();
                            result = 1;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error update_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }


                        DeleteRelMarcaModelo(obj.Id);


                        obj.Marca.ForEach(x =>
                        {

                            try
                            {
                                SqlCommand command1 = new SqlCommand("AddMarcaModelo @clv_marca,@clv_modelo", connection);
                                command1.Parameters.AddWithValue("@clv_marca", x.clv_marca);
                                command1.Parameters.AddWithValue("@clv_modelo", obj.Id);
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                command1.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection != null)
                                    connection.Close();
                                throw new Exception("Error insert_EstadoMunicipios " + ex.Message, ex);
                            }

                        });


                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<tipoUsuario> GetModelo()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetModelo", connection);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                tipoUsuario tu = new tipoUsuario();
                                tu.clv_modelo = Int32.Parse(rd[0].ToString());
                                tu.Nombre = rd[1].ToString();
                                //tu.Activo = bool.Parse(rd[2].ToString());
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        

             public List<tipoUsuario> GetModelosByMarca(int? clv_marca)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetModelosByMarca @clv_marca", connection);
                        command.Parameters.AddWithValue("@clv_marca", clv_marca);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                tipoUsuario tu = new tipoUsuario();
                                tu.clv_modelo = Int32.Parse(rd[0].ToString());
                                tu.Nombre = rd[1].ToString();
                                //tu.Activo = bool.Parse(rd[2].ToString());
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        lista.ForEach(x =>
                        {

                            x.Marca = GetMarcaModelo(x.clv_modelo);
                        });



                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }




        public List<tipoUsuario> GetModeloById(int? Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<tipoUsuario> lista = new List<tipoUsuario>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetModeloById @Id", connection);
                        command.Parameters.AddWithValue("@Id", Id);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                tipoUsuario tu = new tipoUsuario();
                                tu.Id = Int32.Parse(rd[0].ToString());
                                tu.Nombre = rd[1].ToString();
                                //tu.Activo = bool.Parse(rd[2].ToString());
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        lista.ForEach(x =>
                        {

                            x.Marca = GetMarcaModelo(x.Id);
                        });


                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }






        public tipoUsuario GetTipoUsuarioById(int? Id)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    tipoUsuario tu = new tipoUsuario();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_tipoUsuario_clave @id", connection);
                        command.Parameters.AddWithValue("@id", Id);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                                tu.Id = Int32.Parse(rd[0].ToString());
                                tu.Nombre = rd[1].ToString();
                                tu.Activo = bool.Parse(rd[2].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_tipoUsuario_clave " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return tu;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



        public List<municipios> GetMunicipios()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<municipios> lista = new List<municipios>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("select_ciudad", connection);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                municipios tu = new municipios();
                                tu.clv_ciudad = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_tipoUsuario " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        lista.ForEach(x =>
                        {

                            x.estados = GetEstadosMunicipios(x.clv_ciudad);
                        });


                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        private List<municipios> GetLocalidadesMunicipio(int? clv_localidad)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                IDataReader rd = null;
                SqlCommand command = new SqlCommand("GetLocalidadesMunicipio @clv_localidad", connection);
                command.Parameters.AddWithValue("@clv_localidad", clv_localidad);
                List<municipios> municipios = new List<municipios>();
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = command.ExecuteReader();
                    while (rd.Read())
                    {
                        municipios tu = new municipios();
                        tu.clv_municipio = Int32.Parse(rd[0].ToString());
                        tu.nombre = rd[1].ToString();
                        municipios.Add(tu);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error GetLocalidadesMunicipio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }


                return municipios;
            }
        }


        public List<InfoPais> GetEstadosMunicipios(int? clv_municipio)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                IDataReader rd = null;
                SqlCommand command = new SqlCommand("GetPaisesCiudades @clv_ciudad", connection);
                command.Parameters.AddWithValue("@clv_ciudad", clv_municipio);
                List<InfoPais> estados = new List<InfoPais>();
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = command.ExecuteReader();
                    while (rd.Read())
                    {
                        InfoPais tu = new InfoPais();
                        tu.clv_pais = Int32.Parse(rd[0].ToString());
                        tu.nombre = rd[1].ToString();
                        estados.Add(tu);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error GetEstadosMunicipios " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }


                return estados;
            }
        }


        public List<Marcas> GetMarcaModelo(int? clv_modelo)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                IDataReader rd = null;
                SqlCommand command = new SqlCommand("GetMarcaModelo @clv_marca", connection);
                command.Parameters.AddWithValue("@clv_marca", clv_modelo);
                List<Marcas> estados = new List<Marcas>();
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = command.ExecuteReader();
                    while (rd.Read())
                    {
                        Marcas tu = new Marcas();
                        tu.clv_marca = Int32.Parse(rd[0].ToString());
                        tu.nombre = rd[1].ToString();
                        estados.Add(tu);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error GetEstadosMunicipios " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }


                return estados;
            }
        }


        public List<Infomodelo> GetFirmwareMarcaModelo(int? clv_firmware)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                IDataReader rd = null;
                SqlCommand command = new SqlCommand("GetfirmwareMarcaModelo @clv_firmware", connection);
                command.Parameters.AddWithValue("@clv_firmware", clv_firmware);
                List<Infomodelo> estados = new List<Infomodelo>();
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = command.ExecuteReader();
                    while (rd.Read())
                    {
                        Infomodelo tu = new Infomodelo();
                        tu.clv_modelo = Int32.Parse(rd[0].ToString());
                        tu.Nombre = rd[1].ToString();
                        tu.clv_marca = Int32.Parse(rd[2].ToString());
                        tu.nombre = rd[3].ToString();
                        estados.Add(tu);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error GetEstadosMunicipios " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }


                return estados;
            }
        }


        public List<InfoPais> GetColoniaPaisCiudad(int? clv_pais, int? clv_ciudad)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    IDataReader rd = null;
                    SqlCommand command = new SqlCommand("GetColoniaPaisCiudad @clv_pais,@clv_ciudad", connection);
                    command.Parameters.AddWithValue("@clv_pais", clv_pais);
                    command.Parameters.AddWithValue("@clv_ciudad", clv_ciudad);
                    List<InfoPais> estados = new List<InfoPais>();
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            InfoPais tu = new InfoPais();
                            tu.clv_colonia = Int32.Parse(rd[0].ToString());
                            tu.nombre = rd[1].ToString();
                            estados.Add(tu);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error GetEstadosMunicipios " + ex.Message, ex);
                    }

                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }


                    return estados;
                }
            }
        }


        public List<InfoPais> GetCalleColonia(int? clv_colonia)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    IDataReader rd = null;
                    SqlCommand command = new SqlCommand("GetCalleColonia @clv_colonia", connection);
                    command.Parameters.AddWithValue("@clv_colonia", clv_colonia);

                    List<InfoPais> estados = new List<InfoPais>();
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            InfoPais tu = new InfoPais();
                            tu.clv_calle = Int32.Parse(rd[0].ToString());
                            tu.nombre = rd[1].ToString();
                            estados.Add(tu);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error GetEstadosMunicipios " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }


                    return estados;
                }
            }
        }





        public List<municipios> GetMunicipiosById(int? clv_municipio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<municipios> lista = new List<municipios>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("select_ciudad_clv @clv_ciudad ", connection);
                            command.Parameters.AddWithValue("@clv_ciudad", clv_municipio);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                municipios tu = new municipios();
                                tu.clv_ciudad = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_municipios " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        lista.ForEach(x =>
                        {

                            x.estados = GetEstadosMunicipios(x.clv_ciudad);
                        });

                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<Marcas> GetMarcaById(int? clv_marca)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<Marcas> lista = new List<Marcas>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("GetMarcaById @clv_marca ", connection);
                            command.Parameters.AddWithValue("@clv_marca", clv_marca);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                Marcas tu = new Marcas();
                                tu.clv_marca = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_municipios " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        //lista.ForEach(x =>
                        //{

                        //    x.modelos = GetMarcaModelo(x.clv_marca);
                        //});

                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }




        public List<Marcas> GetFirmwareById(int? clv_firmware)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<Marcas> lista = new List<Marcas>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("GetfirmwareById @clv_firmware ", connection);
                            command.Parameters.AddWithValue("@clv_firmware", clv_firmware);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                Marcas tu = new Marcas();
                                tu.clv_firmware = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_municipios " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        lista.ForEach(x =>
                        {

                            x.modelos = GetFirmwareMarcaModelo(x.clv_firmware);
                        });

                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



        private int DeleteRelEstadosMunicipios(int? clv_municipio)
        {
            int result = 0;
            try
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand("DeleteRelPaisesCiudades @clv_ciudad", connection);
                        command.Parameters.AddWithValue("@clv_ciudad", clv_municipio);
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (connection != null)
                            connection.Close();
                        throw new Exception("Error DeleteRelEstadosMunicipios " + ex.Message, ex);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        private int DeleteRelMarcaModelo(int? clv_marca)
        {
            int result = 0;
            try
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand("DeleteRelMarcaModelo @clv_marca", connection);
                        command.Parameters.AddWithValue("@clv_marca", clv_marca);
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (connection != null)
                            connection.Close();
                        throw new Exception("Error DeleteRelEstadosMunicipios " + ex.Message, ex);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        private int DeleteRelFirmMarcaModelo(int? clv_firmware)
        {
            int result = 0;
            try
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand("DeleteRelFirmMarcaModelo @clv_firmware", connection);
                        command.Parameters.AddWithValue("@clv_firmware", clv_firmware);
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (connection != null)
                            connection.Close();
                        throw new Exception("Error DeleteRelFirmMarcaModelo " + ex.Message, ex);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public int? AddMunicipio(municipios obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                int result = 0;
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        try
                        {

                            SqlCommand strSQL = new SqlCommand("insert_ciudades");
                            strSQL.Connection = connection;
                            strSQL.CommandType = CommandType.StoredProcedure;
                            strSQL.CommandTimeout = 0;

                            SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                            par1.Value = obj.nombre;
                            par1.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par1);

                            SqlParameter par2 = new SqlParameter("@clv_ciudades", SqlDbType.Int);
                            par2.Direction = ParameterDirection.Output;
                            strSQL.Parameters.Add(par2);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            strSQL.ExecuteNonQuery();

                            result = int.Parse(par2.SqlValue.ToString());

                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_municipios " + ex.Message, ex);
                        }
                        obj.estados.ForEach(x =>
                        {

                            try
                            {
                                SqlCommand command = new SqlCommand("insert_PaisCiudades @clv_pais,@clv_ciudad", connection);
                                command.Parameters.AddWithValue("@clv_pais", x.clv_pais);
                                command.Parameters.AddWithValue("@clv_ciudad", result);
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection != null)
                                    connection.Close();
                                throw new Exception("Error insert_PaisCiudades " + ex.Message, ex);
                            }

                        });

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? UpdateMarcaModelo(Marcas obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                int result = 0;
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        try
                        {

                            SqlCommand strSQL = new SqlCommand("updateMarca");
                            strSQL.Connection = connection;
                            strSQL.CommandType = CommandType.StoredProcedure;
                            strSQL.CommandTimeout = 0;

                            SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                            par1.Value = obj.nombre;
                            par1.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par1);

                            SqlParameter par2 = new SqlParameter("@clv_marca", SqlDbType.VarChar);
                            par2.Value = obj.clv_marca;
                            par2.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par2);

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            strSQL.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_municipios " + ex.Message, ex);
                        }

                        //DeleteRelMarcaModelo(obj.clv_marca);


                        //obj.modelos.ForEach(x =>
                        //{

                        //    try
                        //    {
                        //        SqlCommand command = new SqlCommand("AddMarcaModelo @clv_marca,@clv_modelo", connection);
                        //        command.Parameters.AddWithValue("@clv_marca", obj.clv_marca);
                        //        command.Parameters.AddWithValue("@clv_modelo", x.clv_modelo);
                        //        if (connection.State == ConnectionState.Closed)
                        //            connection.Open();
                        //        command.ExecuteNonQuery();
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        if (connection != null)
                        //            connection.Close();
                        //        throw new Exception("Error insert_EstadoMunicipios " + ex.Message, ex);
                        //    }

                        //});

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }




        public int? AddMarca(Marcas obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                int result = 0;
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        try
                        {

                            SqlCommand strSQL = new SqlCommand("AddMarca");
                            strSQL.Connection = connection;
                            strSQL.CommandType = CommandType.StoredProcedure;
                            strSQL.CommandTimeout = 0;

                            SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                            par1.Value = obj.nombre;
                            par1.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par1);

                            SqlParameter par2 = new SqlParameter("@clv_marca", SqlDbType.Int);
                            par2.Direction = ParameterDirection.Output;
                            strSQL.Parameters.Add(par2);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            strSQL.ExecuteNonQuery();

                            result = int.Parse(par2.SqlValue.ToString());

                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_municipios " + ex.Message, ex);
                        }
                        obj.modelos.ForEach(x =>
                        {

                            try
                            {
                                SqlCommand command = new SqlCommand("AddMarcaModelo @clv_marca,@clv_modelo", connection);
                                command.Parameters.AddWithValue("@clv_modelo", x.clv_modelo);
                                command.Parameters.AddWithValue("@clv_marca", result);
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection != null)
                                    connection.Close();
                                throw new Exception("Error insert_PaisCiudades " + ex.Message, ex);
                            }

                        });

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? Addfirmware(Marcas obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                int result = 0;
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        try
                        {

                            SqlCommand strSQL = new SqlCommand("Addfirmware");
                            strSQL.Connection = connection;
                            strSQL.CommandType = CommandType.StoredProcedure;
                            strSQL.CommandTimeout = 0;

                            SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                            par1.Value = obj.nombre;
                            par1.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par1);

                            SqlParameter par2 = new SqlParameter("@clv_firmware", SqlDbType.Int);
                            par2.Direction = ParameterDirection.Output;
                            strSQL.Parameters.Add(par2);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            strSQL.ExecuteNonQuery();

                            result = int.Parse(par2.SqlValue.ToString());

                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_municipios " + ex.Message, ex);
                        }
                        obj.modelos.ForEach(x =>
                        {

                            try
                            {
                                SqlCommand command = new SqlCommand("AddfirmMarcaModelo @clv_firmware,@clv_marca,@clv_modelo", connection);
                                command.Parameters.AddWithValue("@clv_modelo", x.clv_modelo);            
                                command.Parameters.AddWithValue("@clv_marca", x.clv_marca);
                                command.Parameters.AddWithValue("@clv_firmware", result);
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection != null)
                                    connection.Close();
                                throw new Exception("Error insert_PaisCiudades " + ex.Message, ex);
                            }

                        });

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public int? UpdateFirmMarcaModelo(Marcas obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                int result = 0;
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        try
                        {

                            SqlCommand strSQL = new SqlCommand("updatefirmware");
                            strSQL.Connection = connection;
                            strSQL.CommandType = CommandType.StoredProcedure;
                            strSQL.CommandTimeout = 0;

                            SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                            par1.Value = obj.nombre;
                            par1.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par1);

                            SqlParameter par2 = new SqlParameter("@clv_firmware", SqlDbType.VarChar);
                            par2.Value = obj.clv_firmware;
                            par2.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par2);

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            strSQL.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_municipios " + ex.Message, ex);
                        }

                        DeleteRelFirmMarcaModelo(obj.clv_firmware);


                        obj.modelos.ForEach(x =>
                        {

                            try
                            {
                               SqlCommand command = new SqlCommand("AddfirmMarcaModelo @clv_firmware,@clv_marca,@clv_modelo", connection);

                                command.Parameters.AddWithValue("@clv_firmware", obj.clv_firmware);
                                command.Parameters.AddWithValue("@clv_marca", x.clv_marca);
                                command.Parameters.AddWithValue("@clv_modelo", x.clv_modelo);
                               
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection != null)
                                    connection.Close();
                                throw new Exception("Error insert_EstadoMunicipios " + ex.Message, ex);
                            }

                        });

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public List<Marcas> GetMarca()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<Marcas> lista = new List<Marcas>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("GetMarca", connection);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                Marcas tu = new Marcas();
                                tu.clv_marca = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Marca " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        //lista.ForEach(x =>
                        //{

                        //    x.modelos = GetMarcaModelo(x.clv_marca);
                        //});


                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<Marcas> GetFirmware()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<Marcas> lista = new List<Marcas>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("Getfirmware", connection);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                Marcas tu = new Marcas();
                                tu.clv_firmware = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Marca " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        //lista.ForEach(x =>
                        //{

                        //    x. = GetMarcaModelo(x.clv_modelo);
                        //});


                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



        public int? AddCiudad(municipios obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                int result = 0;
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        try
                        {

                            SqlCommand strSQL = new SqlCommand("insert_ciudades");
                            strSQL.Connection = connection;
                            strSQL.CommandType = CommandType.StoredProcedure;
                            strSQL.CommandTimeout = 0;

                            SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                            par1.Value = obj.nombre;
                            par1.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par1);

                            SqlParameter par2 = new SqlParameter("@clv_ciudad", SqlDbType.Int);
                            par2.Direction = ParameterDirection.Output;
                            strSQL.Parameters.Add(par2);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            strSQL.ExecuteNonQuery();

                            result = int.Parse(par2.SqlValue.ToString());

                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_municipios " + ex.Message, ex);
                        }
                        obj.estados.ForEach(x =>
                        {

                            try
                            {
                                SqlCommand command = new SqlCommand("insert_PaisCiudades @clv_pais,@clv_ciudad", connection);
                                command.Parameters.AddWithValue("@clv_estado", x.clv_estado);
                                command.Parameters.AddWithValue("@clv_ciudad", result);
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection != null)
                                    connection.Close();
                                throw new Exception("Error insert_EstadoMunicipios " + ex.Message, ex);
                            }

                        });

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? UpdateMunicipio(municipios obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                int result = 0;
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        try
                        {

                            SqlCommand strSQL = new SqlCommand("update_ciudad");
                            strSQL.Connection = connection;
                            strSQL.CommandType = CommandType.StoredProcedure;
                            strSQL.CommandTimeout = 0;

                            SqlParameter par1 = new SqlParameter("@nombre", SqlDbType.VarChar);
                            par1.Value = obj.nombre;
                            par1.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par1);

                            SqlParameter par2 = new SqlParameter("@clv_ciudad", SqlDbType.VarChar);
                            par2.Value = obj.clv_municipio;
                            par2.Direction = ParameterDirection.Input;
                            strSQL.Parameters.Add(par2);

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            strSQL.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (connection != null)
                                connection.Close();
                            throw new Exception("Error insert_municipios " + ex.Message, ex);
                        }

                        DeleteRelEstadosMunicipios(obj.clv_municipio);


                        obj.estados.ForEach(x =>
                        {

                            try
                            {
                                SqlCommand command = new SqlCommand("insert_PaisCiudades @clv_pais,@clv_ciudad", connection);
                                command.Parameters.AddWithValue("@clv_pais", x.clv_pais);
                                command.Parameters.AddWithValue("@clv_ciudad", obj.clv_municipio);
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection != null)
                                    connection.Close();
                                throw new Exception("Error insert_EstadoMunicipios " + ex.Message, ex);
                            }

                        });

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<municipios> GetMunicipiosByEstado(int? clv_estado)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<municipios> lista = new List<municipios>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("select_ciudadPais @clv_pais ", connection);
                            command.Parameters.AddWithValue("@clv_pais", clv_estado);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                municipios tu = new municipios();
                                tu.clv_ciudad = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_municipios " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<localidad> GetLocalidadesByMunicipio(int? clv_municipio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<localidad> lista = new List<localidad>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("select_localidadesMunicipio @clv_municipio ", connection);
                            command.Parameters.AddWithValue("@clv_municipio", @clv_municipio);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                localidad tu = new localidad();
                                tu.clv_localidad = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_localidadesMunicipio " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<ColoniaEntity> GetColoniasByLocalidad(int? clv_localidad, int? clv_municipio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<ColoniaEntity> lista = new List<ColoniaEntity>();

                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("select_coloniasLocalidad @clv_loc,@clv_municipio ", connection);
                            command.Parameters.AddWithValue("@clv_loc", clv_localidad);
                            command.Parameters.AddWithValue("@clv_municipio", clv_municipio);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ColoniaEntity tu = new ColoniaEntity();
                                tu.clv_colonia = Int32.Parse(rd[0].ToString());
                                tu.nombre = rd[1].ToString();
                                tu.cp = int.Parse(rd[5].ToString());
                                lista.Add(tu);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_coloniasLocalidad " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<int> GetPisosTorre(string torre, int? clv_proyecto)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<int> lista = new List<int>();
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("GetPisoTorre @clv_Proyecto,@torre", connection);
                            command.Parameters.AddWithValue("@clv_Proyecto", clv_proyecto);
                            command.Parameters.AddWithValue("@torre", torre);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                                lista.Add(int.Parse(rd[0].ToString()));
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error GetTorreProyecto " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<string> GetTorresProyecto(int? clv_proyecto )
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<string> lista = new List<string>();
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("GetTorreProyecto @clv_Proyecto ", connection);
                            command.Parameters.AddWithValue("@clv_Proyecto", clv_proyecto);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                lista.Add(rd[1].ToString());
                               
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error GetTorreProyecto " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }



        public List<EstatusVentaDepartamentoEntity> GetEstatusVentaDepartamento()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<EstatusVentaDepartamentoEntity> lista = new List<EstatusVentaDepartamentoEntity>();
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            SqlCommand command = new SqlCommand("GetEstatusVentaDepartamento", connection);
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                EstatusVentaDepartamentoEntity obj = new EstatusVentaDepartamentoEntity();
                                obj.Id = int.Parse(rd[0].ToString());
                                obj.Descripcion = rd[1].ToString();
                                lista.Add(obj);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error GetEstatusVentaDepartamento " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return lista;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }
        public List<ServiciosEntity> GetServicios()

        {
            List<ServiciosEntity> Vistalist = new List<ServiciosEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_servicio  ", connection);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ServiciosEntity i = new ServiciosEntity();
                                i.clv_servicio = Int32.Parse(rd[0].ToString());
                                i.nombre_Servicio = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Servicio " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? AddServicio(string nombre_Servicio, string descripcion, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("insert_servicios", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@nombre_Servicio", nombre_Servicio);
                        command.Parameters.AddWithValue("@descripcion", descripcion);
                        command.Parameters.AddWithValue("@activo", activo);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<ServiciosEntity> GetServicioC(int? clv_servicio)

        {
            List<ServiciosEntity> V1list = new List<ServiciosEntity>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_servicio_clv ", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@clv_servicio", clv_servicio);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ServiciosEntity i = new ServiciosEntity();
                                i.clv_servicio = Int32.Parse(rd[0].ToString());
                                i.nombre_Servicio = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                V1list.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error" + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return V1list;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? UpdateServicio(int? clv_servicio, string nombre_Servicio, string descripcion, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_servicios ", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@clv_servicio", clv_servicio);
                        command.Parameters.AddWithValue("@nombre_Servicio", nombre_Servicio);
                        command.Parameters.AddWithValue("@descripcion", descripcion);
                        command.Parameters.AddWithValue("@activo", activo);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error  " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }




        

        public List<CotizacionEntity> GetCotizacion(int? clv_cotizacion, string fechaCotizacion, string fechaExpiracion, string cliente, int? op, long  ? clv_usuario)
        {
            List<CotizacionEntity> Vlist = new List<CotizacionEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_cotizacion", connection);

                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@clv_cotizacion", clv_cotizacion);
                        command.Parameters.AddWithValue("@fechaCotizacion", fechaCotizacion);
                        command.Parameters.AddWithValue("@fechaExpiracion", "1900/01/01");
                        command.Parameters.AddWithValue("@cliente", cliente);
                        command.Parameters.AddWithValue("@op", op);
                        command.Parameters.AddWithValue("@clv_usuario", clv_usuario);

                        
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                CotizacionEntity j = new CotizacionEntity();
                                j.Id = Int32.Parse(rd[0].ToString());
                                j.Fecha = DateTime.Parse(rd[1].ToString());
                                j.FechaExpiracion = DateTime.Parse(rd[2].ToString());
                                j.Fecha2 = j.Fecha.ToShortDateString();
                                j.FechaExpiracion2 = j.FechaExpiracion.ToShortDateString();
                                
                               // j.Vendedor = rd[4].ToString();
                                j.Estatus = rd[5].ToString();
                                j.Clv_cliente = int.Parse(rd[6].ToString());
                                SoftvWCFService soft = new SoftvWCFService();
                                j.cliente = soft.GetClienteById(j.Clv_cliente);
                                Vlist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Estatus Departamento " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        

        public List<ProspectoEntity> AddProspecto(string Nombre, string Direccion, string Correo, string Telefono)
        {

            List<ProspectoEntity> Vistalist = new List<ProspectoEntity>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("insert_prospecto", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Nombre", Nombre);
                        command.Parameters.AddWithValue("@Direccion", Direccion);
                        command.Parameters.AddWithValue("@Correo", Correo);
                        command.Parameters.AddWithValue("@Telefono", Telefono);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ProspectoEntity i = new ProspectoEntity();
                                i.Clv_prospecto = Int32.Parse(rd[0].ToString());
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public int? UpdateProspecto(int? Clv_prospecto, string Nombre, string Direccion, string Correo, string Telefono)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("update_prospecto", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Clv_prospecto", Clv_prospecto);
                        command.Parameters.AddWithValue("@Nombre", Nombre);
                        command.Parameters.AddWithValue("@Direccion", Direccion);
                        command.Parameters.AddWithValue("@Correo", Correo);
                        command.Parameters.AddWithValue("@Telefono", Telefono);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<ProspectoEntity> GetProspecto()

        {
            List<ProspectoEntity> Vistalist = new List<ProspectoEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_prospecto", connection);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ProspectoEntity i = new ProspectoEntity();
                                i.Clv_prospecto = Int32.Parse(rd[0].ToString());
                                i.Nombre = rd[1].ToString();
                                i.Direccion = rd[2].ToString();
                                i.Correo = rd[3].ToString();
                                i.Telefono = rd[4].ToString();
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Servicio " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<ProspectoEntity> GetProspectoById(int? Clv_Prospecto)

        {
            List<ProspectoEntity> Vistalist = new List<ProspectoEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetProspectoById", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Clv_Prospecto", Clv_Prospecto);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ProspectoEntity i = new ProspectoEntity();
                                i.Clv_prospecto = Int32.Parse(rd[0].ToString());
                                i.Nombre = rd[1].ToString();
                                i.Direccion = rd[2].ToString();
                                i.Correo = rd[3].ToString();
                                i.Telefono = rd[4].ToString();
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Servicio " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);

                }
            }

        }
        public int? AddConfiguracionSistema(
            float Cetes, int? MultiplicadorDescuentos, 
            int? MultiplicadorFinanciamiento,
            int? Expiracion, string Folio, 
            string Serie, float IVA, 
            float InteresAtraso, float Escrituracion,
            float Reservacion,
            float DescuentoEnganche)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("AddConfiguracion", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Cetes", Cetes);
                        command.Parameters.AddWithValue("@MultiplicadorDescuentos", MultiplicadorDescuentos);
                        command.Parameters.AddWithValue("@MultiplicadorFinanciamiento", MultiplicadorFinanciamiento);
                        command.Parameters.AddWithValue("@Expiracion", Expiracion);
                        command.Parameters.AddWithValue("@Folio", Folio);
                        command.Parameters.AddWithValue("@Serie", Serie);
                        command.Parameters.AddWithValue("@IVA", IVA);
                        command.Parameters.AddWithValue("@InteresAtraso", InteresAtraso);
                        command.Parameters.AddWithValue("@Escrituracion", Escrituracion);
                        command.Parameters.AddWithValue("@Reservacion", Reservacion);
                        command.Parameters.AddWithValue("@DescuentoEnganche", DescuentoEnganche);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }


        public List<ConfiguracionEntity> GetConfiguracionSistema()

        {
            List<ConfiguracionEntity> Vistalist = new List<ConfiguracionEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetConfiguracion", connection);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ConfiguracionEntity i = new ConfiguracionEntity();
                                i.id = Int32.Parse(rd[0].ToString());
                                i.Cetes = float.Parse(rd[1].ToString());
                                i.MultiplicadorDescuentos = Int32.Parse(rd[2].ToString());
                                i.MultiplicadorFinanciamiento = Int32.Parse(rd[3].ToString());
                                i.Expiracion = Int32.Parse(rd[4].ToString());
                                i.Folio = rd[5].ToString();
                                i.Serie = rd[6].ToString();
                                i.IVA = float.Parse(rd[7].ToString());
                                i.InteresAtraso = float.Parse(rd[8].ToString());
                                i.Escrituracion = float.Parse(rd[9].ToString());
                                i.Reservacion = float.Parse(rd[10].ToString());
                                i.DescuentoEnganche = float.Parse(rd[11].ToString());
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error  " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public List<ConfiguracionEntity> GetConfiguracionSistemaById(int? @id)

        {
            List<ConfiguracionEntity> Vistalist = new List<ConfiguracionEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetConfiguracionByid", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@id", id);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                ConfiguracionEntity i = new ConfiguracionEntity();
                                i.id = Int32.Parse(rd[0].ToString());
                                i.Cetes = float.Parse(rd[1].ToString());
                                i.MultiplicadorDescuentos = Int32.Parse(rd[2].ToString());
                                i.MultiplicadorFinanciamiento = Int32.Parse(rd[3].ToString());
                                i.Expiracion = Int32.Parse(rd[4].ToString());
                                i.Folio = rd[5].ToString();
                                i.Serie = rd[6].ToString();
                                i.IVA = float.Parse(rd[7].ToString());
                                i.InteresAtraso = float.Parse(rd[8].ToString());
                                i.Escrituracion = float.Parse(rd[9].ToString());
                                i.Reservacion = float.Parse(rd[10].ToString());
                                i.DescuentoEnganche = float.Parse(rd[11].ToString());
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error  " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? UpdateConfiguracionSistema(int? id, float? Cetes, int? MultiplicadorDescuentos, int? MultiplicadorFinanciamiento, 
            int? Expiracion, string Folio, string Serie, float IVA, float InteresAtraso, float Escrituracion, float Reservacion, float DescuentoEnganche)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("update_configuracion", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@id", id);
                        command.Parameters.AddWithValue("@Cetes", Cetes);
                        command.Parameters.AddWithValue("@MultiplicadorDescuentos", MultiplicadorDescuentos);
                        command.Parameters.AddWithValue("@MultiplicadorFinanciamiento", MultiplicadorFinanciamiento);
                        command.Parameters.AddWithValue("@Expiracion", Expiracion);
                        command.Parameters.AddWithValue("@Folio", Folio);
                        command.Parameters.AddWithValue("@Serie", Serie);
                        command.Parameters.AddWithValue("@IVA", IVA);
                        command.Parameters.AddWithValue("@InteresAtraso", InteresAtraso);
                        command.Parameters.AddWithValue("@Escrituracion", Escrituracion);
                        command.Parameters.AddWithValue("@Reservacion", Reservacion);
                        command.Parameters.AddWithValue("@DescuentoEnganche", DescuentoEnganche);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }







        public int? AddPlanPago(float? engancheInicial, float? engancheDiferido, float? escrituras)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int id_plan_pago = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("insert_plan_de_pago", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@engancheInicial", engancheInicial);
                        command.Parameters.AddWithValue("@engancheDiferido", engancheDiferido);
                        command.Parameters.AddWithValue("@escrituras", escrituras);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                id_plan_pago = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return id_plan_pago;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }

        public List<PlanpagoEntity> GetPlanPago()

        {
            List<PlanpagoEntity> Vistalist = new List<PlanpagoEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_plan_de_pago", connection);
                       

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                PlanpagoEntity i = new PlanpagoEntity();
                                i.id_plan_pago = Int32.Parse(rd[0].ToString());
                                i.enganche_inicial = float.Parse(rd[1].ToString());
                                i.enganche_diferido = float.Parse(rd[2].ToString());
                                i.escrituras = float.Parse(rd[3].ToString());

                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error  " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<PlanpagoEntity> GetPlanPagoById(int? id )

        {
            List<PlanpagoEntity> Vistalist = new List<PlanpagoEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_byId_plan_de_pago", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@id",id);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                PlanpagoEntity i = new PlanpagoEntity();
                                i.id_plan_pago = Int32.Parse(rd[0].ToString());
                                i.enganche_inicial = float.Parse(rd[1].ToString());
                                i.enganche_diferido = float.Parse(rd[2].ToString());
                                i.escrituras = float.Parse(rd[3].ToString());

                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error  " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? UpdatePlanPago(int? id, float? engancheInicial, float? engancheDiferido, float? escrituras)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("update_plan_de_pago", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@id", id);
                        command.Parameters.AddWithValue("@engancheInicial", engancheInicial);
                        command.Parameters.AddWithValue("@engancheDiferido", engancheDiferido);
                        command.Parameters.AddWithValue("@escrituras", escrituras);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }

      //  public  string ReporteCotizacion( CotizacionEntity ObjReporte)
      //{
      //      string result = "";


      //      string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
      //      using (SqlConnection connection = new SqlConnection(ConnectionString))
      //      {
      //          string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
      //          string ruta = apppath + "/ReportesSistema1/";
      //          string name = "";

      //          if (ObjReporte.Opexportacion == 1)
      //          {
      //              name = Guid.NewGuid().ToString() + ".pdf";
      //          }
      //          else if (ObjReporte.Opexportacion == 2)
      //          {
      //              name = Guid.NewGuid().ToString() + ".xls";
      //          }

      //          string fileName = apppath + "/Reportes/" + name;


      //          ReportDocument reportDocument = new ReportDocument();
      //          try
      //          {

      //              if (connection.State == ConnectionState.Closed)
      //                  connection.Open();
      //              {
      //                 string sql = "";
      //                 sql = "ListadoCotizacion";

      //                  SqlCommand command = new SqlCommand (sql, connection);
      //                  command.Connection = connection;
      //                  command.CommandType = CommandType.StoredProcedure;
      //                  command.CommandTimeout = 0;


      //                  SqlParameter par1 = new SqlParameter("@op", SqlDbType.Int);
      //                  par1.Value = ObjReporte.op;
      //                  par1.Direction = ParameterDirection.Input;
      //                  command.Parameters.Add(par1);


      //                  SqlParameter par2 = new SqlParameter("@id_cotizacion", SqlDbType.Int);
      //                  par2.Value = ObjReporte.id_cotizacion;
      //                  par2.Direction = ParameterDirection.Input;
      //                  command.Parameters.Add(par2);

      //                  SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
      //                  DataSet dataSet = new DataSet();
      //                  dataAdapter.Fill(dataSet);

      //                  dataSet.Tables[0].TableName = "listadoCotizacion";


      //                  if (ObjReporte.op == 2)
      //                  {

      //                      reportDocument.Load(ruta + "listadoCotizacion3.rpt");
      //                      reportDocument.SetDataSource(dataSet);

      //                  }
      //                  else
      //                  {
      //                      reportDocument.Load(ruta + "listadoCotizacion2.rpt");
      //                      reportDocument.SetDataSource(dataSet);

      //                  }

                     

      //                  //string Amenitie = "amenities: ";
      //                  //ObjReporte.descripcion.ForEach(u =>
      //                  //{
      //                  //    Amenitie = (Amenitie == "amenities: ") ? Amenitie += u.descripcion : Amenitie += ", " + u.descripcion;
      //                  //});

      //               if (ObjReporte.Opexportacion == 1)
      //                  {
      //                      reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
      //                  }
      //                  else if (ObjReporte.Opexportacion == 2)
      //                  {
      //                      reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
      //                  }

      //              }
      //              result = name;
      //          }
      //          catch (Exception ex)
      //          {
      //             throw new Exception("Error  " + ex.Message, ex);
      //         }
      //         finally
      //         {
      //             if (connection != null)
      //                connection.Close();
      //         }

      //     }
      //     return result;
      //  }

        public int? AddEnganche(decimal? Precio_inicial, decimal? Monto_enganche_Inicial_sobre_precio,decimal? Monto_enganche_diferido, decimal? Monto_enganche_Total, 
            decimal? Monto_Financiamiento_sobre_engachediferido, decimal? Monto_enganche_diferido_mas_financiomiento, decimal? Monto_liq_a_la_escrituracion,
            decimal? Precio_total_del_departamento, decimal? Precio_por_M2, int? id_cotizacion)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("AddEnganche", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Precio_inicial", Precio_inicial);
                        command.Parameters.AddWithValue("@Monto_enganche_Inicial_sobre_precio", Monto_enganche_Inicial_sobre_precio);
                        command.Parameters.AddWithValue("@Monto_enganche_diferido", Monto_enganche_diferido);
                        command.Parameters.AddWithValue("@Monto_enganche_Total", Monto_enganche_Total);
                        command.Parameters.AddWithValue("@Monto_Financiamiento_sobre_engachediferido", Monto_Financiamiento_sobre_engachediferido);
                        command.Parameters.AddWithValue("@Monto_enganche_diferido_mas_financiomiento", Monto_enganche_diferido_mas_financiomiento);
                        command.Parameters.AddWithValue("@Monto_liq_a_la_escrituracion", Monto_liq_a_la_escrituracion);
                        command.Parameters.AddWithValue("@Precio_total_del_departamento", Precio_total_del_departamento);
                        command.Parameters.AddWithValue("@Precio_por_M2", Precio_por_M2);
                        command.Parameters.AddWithValue("@id_cotizacion", id_cotizacion);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }


        public int? UpdateEnganche(int? Id_Enganche_Diferido, decimal? Precio_inicial, decimal? Monto_enganche_Inicial_sobre_precio, decimal? Monto_enganche_diferido, decimal? Monto_enganche_Total,
    decimal? Monto_Financiamiento_sobre_engachediferido, decimal? Monto_enganche_diferido_mas_financiomiento, decimal? Monto_liq_a_la_escrituracion,
    decimal? Precio_total_del_departamento, decimal? Precio_por_M2, int? id_cotizacion)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("UpdateEnganche", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Id_Enganche_Diferido", Id_Enganche_Diferido);
                        command.Parameters.AddWithValue("@Precio_inicial", Precio_inicial);
                        command.Parameters.AddWithValue("@Monto_enganche_Inicial_sobre_precio", Monto_enganche_Inicial_sobre_precio);
                        command.Parameters.AddWithValue("@Monto_enganche_diferido", Monto_enganche_diferido);
                        command.Parameters.AddWithValue("@Monto_enganche_Total", Monto_enganche_Total);
                        command.Parameters.AddWithValue("@Monto_Financiamiento_sobre_engachediferido", Monto_Financiamiento_sobre_engachediferido);
                        command.Parameters.AddWithValue("@Monto_enganche_diferido_mas_financiomiento", Monto_enganche_diferido_mas_financiomiento);
                        command.Parameters.AddWithValue("@Monto_liq_a_la_escrituracion", Monto_liq_a_la_escrituracion);
                        command.Parameters.AddWithValue("@Precio_total_del_departamento", Precio_total_del_departamento);
                        command.Parameters.AddWithValue("@Precio_por_M2", Precio_por_M2);
                        command.Parameters.AddWithValue("@id_cotizacion", id_cotizacion);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }
        

        TipoVendedorController tipoVendedor = new TipoVendedorController();

        public List<TipoVendedorEntity> GetTipoVendedor()
        {            
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;     
                try
                {
                   return tipoVendedor.GetTipoVendedor();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
        }

        public int? UpdateTipoVendedor(int? Id, string Descripcion, bool? Activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tipoVendedor.UpdateTipoVendedor(Id, Descripcion, Activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public int? AddTipoVendedor(string Descripcion, bool? Activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tipoVendedor.AddTipoVendedor(Descripcion, Activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public int? DeleteRelDepartamentoCliente (int ? clv_comprador, int? Clv_departamento)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                int result = 0;
                IDataReader rd = null;
                try
                {


                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand strSQL = new SqlCommand("EliminaDepartamentoCliente");
                    strSQL.Connection = connection;
                    strSQL.CommandType = CommandType.StoredProcedure;
                    strSQL.CommandTimeout = 0;


                    SqlParameter par1 = new SqlParameter("@Clv_departamento", SqlDbType.Int);
                    par1.Value = Clv_departamento;
                    par1.Direction = ParameterDirection.Input;
                    strSQL.Parameters.Add(par1);

                    SqlParameter par2 = new SqlParameter("@clv_comprador", SqlDbType.Int);
                    par2.Value = clv_comprador;
                    par2.Direction = ParameterDirection.Input;
                    strSQL.Parameters.Add(par2);

                  

                    strSQL.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    if (connection != null)
                        connection.Close();
                   
                }

                return result;
            }
        }

        public List<EstatusDepartamentoEntity> GetrelDepClienteById(int? clv_comprador)

        {
           List<EstatusDepartamentoEntity> Vistalist = new List<EstatusDepartamentoEntity>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("GetrelDepartamentoCliente", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@clv_comprador", clv_comprador);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                EstatusDepartamentoEntity i = new EstatusDepartamentoEntity();
                                i.IdDetalle = Int32.Parse(rd[0].ToString());
                                i.departamento=  rd[1].ToString();
                                i.NumDepartamento = rd[2].ToString();


                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error  " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        MediosComunicacionController medios  = new MediosComunicacionController();
        public List<Medios_de_comunicacionEntity> GetMediosDifusion()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return medios.GetMediosDifusion();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public Medios_de_comunicacionEntity GetMediosDifusionById(int? clv_medio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return medios.GetMediosDifusionById(clv_medio);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? AddMedio(string nombre, bool estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return medios.AddMedio(nombre, estatus);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? UpdateMedio(int? clv_medio, string nombre, bool estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return medios.UpdateMedio(clv_medio, nombre, estatus);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public List<MedioContacto> GetMedioContacto()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return medios.GetMedioContacto();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public MedioContacto GetMedioContactoById(int? clv_medio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return medios.GetMedioContactoById(clv_medio);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? AddMedioContacto(string nombre, bool estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return medios.AddMedioContacto(nombre, estatus);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? UpdateMedioContacto(int? clv_medioContacto, string nombre, bool estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return medios.UpdateMedioContacto(clv_medioContacto, nombre, estatus);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        ClienteController ClientesEntity = new ClienteController();
        public List<ClientesEntity> GetClientes(int? clv_usuario, int? clv_estatus, string nombre, string celular, string pais)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return ClientesEntity.GetClientes(clv_usuario, clv_estatus,nombre,celular, pais);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

         public List<ClientesEntity> Searchclientes(int? Op, string nombre, string otro_nombre, string primer_apellido, string segundo_apellido)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return ClientesEntity.Searchclientes(Op, nombre, otro_nombre, primer_apellido, segundo_apellido);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public int? AddMotivosCancelacion(Motivos_de_cancelacionEntity motivos)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return MotivosCancelacionController.AddMotivosCancelacion(motivos);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }
         relUsuarioDepartamentoController relud = new relUsuarioDepartamentoController();


        public int ? AddRelacionUsuarioUD(List<int> departamentos, int? IdUsuario)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return relud.AddRelacion(departamentos, IdUsuario);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? DeleteRelacionUD(int? Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return relud.DeleteRelacion(Id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }
        public int? EditMotivosCancelacion(Motivos_de_cancelacionEntity motivos)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return MotivosCancelacionController.EditMotivosCancelacion(motivos);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
            
        }

        public List<relUsuarioDepartamentoEntity> GetRelacionUD(int? IdUsuario)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return relud.GetRelacion(IdUsuario);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        MotivosCancelacionController Motivos_de_cancelacionEntity = new MotivosCancelacionController();
        public List<Motivos_de_cancelacionEntity> GetMotivosCancelacion()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return Motivos_de_cancelacionEntity.GetMotivosCancelacion();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }




        public Motivos_de_cancelacionEntity GetMotivosCancelacionById(int? Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return MotivosCancelacionController.GetMotivosCancelacionById(Id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        cuentasBancariasController cuentaBancariaEntity = new cuentasBancariasController();
        public List<cuentaBancariaEntity> GetCuentasBancarias()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cuentaBancariaEntity.GetCuentasBancarias();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public int? AddCuentaBancaria(int banco, string cuenta, string CLABE, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cuentaBancariaEntity.AddCuentaBancaria(banco, cuenta, CLABE, activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public cuentaBancariaEntity getCuentaBancariaById(int? id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cuentaBancariaEntity.getCuentaBancariaById(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? updateCuentaBancaria(int? id, int banco, string cuenta, string CLABE, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cuentaBancariaEntity.updateCuentaBancaria(id, banco, cuenta, CLABE, activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        bancosController banco = new bancosController();
        public List<bancoEntity> GetBancos()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return banco.GetBancos();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? addBanco(string nombre, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return banco.addBanco(nombre, activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? updateBanco(int? id, string nombre, bool activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return banco.updateBanco(id, nombre, activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public bancoEntity GetBancoById(int? id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return banco.GetBancoById(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        conceptosDePagoController conceptoController = new conceptosDePagoController();

        public int? addConceptoDePago(string Clv_txt, string Descripcion, bool Activo, bool CobroAutomatico, Decimal Monto)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return conceptoController.addConceptoDePago(Clv_txt, Descripcion, Activo, CobroAutomatico, Monto);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? updateConceptoDePago(int Id, string Clv_txt, string Descripcion, bool Activo, bool CobroAutomatico, Decimal Monto)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return conceptoController.updateConceptoDePago(Id, Clv_txt, Descripcion, Activo, CobroAutomatico, Monto);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<conceptosDePagoEntity> getConceptosDePago()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return conceptoController.getConceptosDePago();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public conceptosDePagoEntity getConceptosDePagoById(int id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return conceptoController.getConceptosDePagoById(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        CotizacionController cotcon = new CotizacionController();


        public CotizacionEntity GetCotizacionDescuento(
            decimal? EngancheInicial,
            decimal? EngancheDiferido,
            decimal? EngancheAdicional,
            decimal ? LiquidacionEscrituracion,
            Decimal? PrecioMetro,
            Decimal? MetrosDepartamento,
            String FechaEntrega,
            int? Op



            )
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cotcon.GetCotizacionDescuento(EngancheInicial, EngancheDiferido, EngancheAdicional, LiquidacionEscrituracion, PrecioMetro, MetrosDepartamento, FechaEntrega, Op);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public CotizacionEntity GetCotizacionDiferido(
            decimal? EngancheInicial,
            decimal? EngancheDiferido,
            decimal? LiquidacionEscrituracion,
            String FechaEntrega,
            decimal? PrecioMetro,
            decimal? MetrosDepartamento
            )
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cotcon.GetCotizacionDiferido( EngancheInicial, EngancheDiferido, LiquidacionEscrituracion, FechaEntrega, PrecioMetro, MetrosDepartamento);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public CotizacionEntity GetCotizacionById(int? clv_cotizacion)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cotcon.GetCotizacionById(clv_cotizacion);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public int ? AddCotizacion(
             int? Clv_cliente,
            int? Clv_vendedor,
             decimal? Cetes,
             int? Multiplicador,
             decimal? Reservacion,
             decimal? Escrituracion,
             int? IdDetalle,
            Double? EngancheInicial,
            Double? EngancheAdicional,
            Double? EngancheDiferido,
            Double? Escrituras,
            int? MesesDiferidos,
            int? Op
            
            )

        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    
                    return cotcon.AddCotizacion(
                         Clv_cliente,
            Clv_vendedor,
              Cetes,
              Multiplicador,
             Reservacion,
              Escrituracion,
              IdDetalle,
             EngancheInicial,
             EngancheAdicional,
             EngancheDiferido,
             Escrituras,
             MesesDiferidos,
             Op
                        );
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? UpdateCotizacion(
             int? clv_cotizacion,
            decimal? Reservacion,
            int? Escrituracion,
            int? EngancheInicial,
            int? EngancheDiferido,
            int? Escrituras,
            int? MesesDiferidos
            )
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cotcon.UpdateCotizacion(clv_cotizacion, Reservacion, Escrituracion, EngancheInicial, EngancheDiferido, Escrituras, MesesDiferidos);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        ContratoController contratocon = new ContratoController();
        public int? AddContrato(int Clv_cotizacion, string bodega, string cajones, string FechaReservacion, string FechaEscrituracion, decimal? MontoContrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return contratocon.AddContrato(Clv_cotizacion, bodega, cajones, FechaReservacion, FechaEscrituracion, MontoContrato);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        ReporteController reportes = new ReporteController();

        public  string ReporteContrato(int ? Opexportacion,  int ? contrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.ReporteContrato(Opexportacion, contrato);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public string ReporteAceptacionDeCompra(int? Opexportacion, int? cotizacion, int? Op1)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return reportes.ReporteAceptacionDeCompra(Opexportacion, cotizacion, Op1);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }


        public string GetReporteCotizacion(int? Op, int? id, string url)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteCotizacion(Op, id,url);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public List<ContratoEntity> GetContrato(long? contrato, string nombre, string segundoNombre, string ApellidoPaterno, string ApellidoMaterno, int ? clv_usuario, string Estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return contratocon.GetContrato(contrato, nombre, segundoNombre, ApellidoPaterno, ApellidoMaterno, clv_usuario, Estatus);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public ContratoEntity GetContratoById(long? contrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return contratocon.GetContratoById(contrato);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        



        public int? cancelarCotizacion(int id, string Estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cotcon.cancelarCotizacion(id, Estatus);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<EstatusDepartamentoEntity> GetEstatusDepartamentoListByIdVendedor(int? Clv_vendedor, int? clv_proyecto, string torre, int? piso, int? estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<EstatusDepartamentoEntity> lista = new List<EstatusDepartamentoEntity>();

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand command = new SqlCommand("select_estatusDepartamento_ById_Vendedor", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@clv_vendedor", Clv_vendedor);
                            command.Parameters.AddWithValue("@clv_proyecto", clv_proyecto);
                            command.Parameters.AddWithValue("@torre", torre);
                            command.Parameters.AddWithValue("@piso", piso);
                            command.Parameters.AddWithValue("@estatus", estatus);
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                                EstatusDepartamentoEntity ed = new EstatusDepartamentoEntity();

                                ed.IdDetalle = int.Parse(rd[0].ToString());
                                ed.NumDepartamento = rd[1].ToString();
                                ed.FechaEntrega = "";
                                try
                                {
                                    ed.FechaEntrega = DateTime.Parse(rd[2].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.Estatus = int.Parse(rd[3].ToString());

                                ed.Clv_vendedor = 0;
                                try
                                {
                                    ed.Clv_vendedor = int.Parse(rd[4].ToString());
                                }
                                catch { }

                                ed.Clv_comprador = 0;
                                try
                                {
                                    ed.Clv_comprador = int.Parse(rd[5].ToString());
                                }
                                catch { }



                                ed.PrecioLista = 0;
                                try
                                {
                                    ed.PrecioLista = decimal.Parse(rd[6].ToString());
                                }
                                catch
                                { }

                                ed.PrecioActual = 0;
                                try
                                {
                                    ed.PrecioActual = decimal.Parse(rd[7].ToString());
                                }
                                catch
                                { }

                                ed.PrecioVendio = 0;
                                try
                                {
                                    ed.PrecioVendio = decimal.Parse(rd[8].ToString());
                                }
                                catch
                                { }

                                ed.FechaApartado = "";
                                try
                                {
                                    ed.FechaApartado = DateTime.Parse(rd[9].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.ImporteApartado = 0;
                                try
                                {
                                    ed.ImporteApartado = decimal.Parse(rd[10].ToString());
                                }
                                catch { }

                                ed.FechaVenta = "";
                                try
                                {
                                    ed.FechaVenta = DateTime.Parse(rd[11].ToString()).ToShortDateString();
                                }
                                catch { }


                                ed.FechaEscrituracion = "";
                                try
                                {
                                    ed.FechaEscrituracion = DateTime.Parse(rd[12].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.Notario = rd[13].ToString();

                                ed.Clv_Etapa = int.Parse(rd[14].ToString());

                                ed.Lado = rd[15].ToString();

                                //ed.Nivel = "";
                                //try
                                //{
                                //    ed.Nivel = rd[16].ToString();



                                //}
                                //catch { }




                                ed.IdDetalle = int.Parse(rd[16].ToString());


                                


                                ed.ImporteEnganche = 0;
                                try
                                {
                                    ed.ImporteEnganche = decimal.Parse(rd[17].ToString());
                                }
                                catch { }


                                ed.PorcentajeEncganche = 0;
                                try
                                {
                                    ed.PorcentajeEncganche = int.Parse(rd[18].ToString());
                                }
                                catch { }


                                ed.NumeroPagosEnganche = 0;
                                try
                                {
                                    ed.NumeroPagosEnganche = int.Parse(rd[19].ToString());
                                }
                                catch { }



                                ed.clv_proyecto = int.Parse(rd[20].ToString());

                                ed.Torre = rd[21].ToString();

                                ed.Piso = int.Parse(rd[22].ToString());

                                ed.clv_Departamento = int.Parse(rd[23].ToString());

                                ed.proyecto = rd[24].ToString();

                                ed.departamento = rd[25].ToString();




                                ed.vistas = new List<InfoMobiliti>();

                                ed.vistas = GetVistasEstatusDepartamento(ed.IdDetalle);

                                lista.Add(ed);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_estatusDepartamento_clave " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return lista;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public List<EstatusDepartamentoEntity> GetEstatusDepartamentoListaDisponible(int? clv_proyecto, string torre, int? piso, int? estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    List<EstatusDepartamentoEntity> lista = new List<EstatusDepartamentoEntity>();

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand command = new SqlCommand("select_estatusDepartamento_Disponibles", connection);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@clv_proyecto", clv_proyecto);
                            command.Parameters.AddWithValue("@torre", torre);
                            command.Parameters.AddWithValue("@piso", piso);
                            command.Parameters.AddWithValue("@estatus", estatus);
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                                EstatusDepartamentoEntity ed = new EstatusDepartamentoEntity();

                                ed.IdDetalle = int.Parse(rd[0].ToString());
                                ed.NumDepartamento = rd[1].ToString();
                                ed.FechaEntrega = "";
                                try
                                {
                                    ed.FechaEntrega = DateTime.Parse(rd[2].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.Estatus = int.Parse(rd[3].ToString());

                                ed.Clv_vendedor = 0;
                                try
                                {
                                    ed.Clv_vendedor = int.Parse(rd[4].ToString());
                                }
                                catch { }

                                ed.Clv_comprador = 0;
                                try
                                {
                                    ed.Clv_comprador = int.Parse(rd[5].ToString());
                                }
                                catch { }



                                ed.PrecioLista = 0;
                                try
                                {
                                    ed.PrecioLista = decimal.Parse(rd[6].ToString());
                                }
                                catch
                                { }

                                ed.PrecioActual = 0;
                                try
                                {
                                    ed.PrecioActual = decimal.Parse(rd[7].ToString());
                                }
                                catch
                                { }

                                ed.PrecioVendio = 0;
                                try
                                {
                                    ed.PrecioVendio = decimal.Parse(rd[8].ToString());
                                }
                                catch
                                { }

                                ed.FechaApartado = "";
                                try
                                {
                                    ed.FechaApartado = DateTime.Parse(rd[9].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.ImporteApartado = 0;
                                try
                                {
                                    ed.ImporteApartado = decimal.Parse(rd[10].ToString());
                                }
                                catch { }

                                ed.FechaVenta = "";
                                try
                                {
                                    ed.FechaVenta = DateTime.Parse(rd[11].ToString()).ToShortDateString();
                                }
                                catch { }


                                ed.FechaEscrituracion = "";
                                try
                                {
                                    ed.FechaEscrituracion = DateTime.Parse(rd[12].ToString()).ToShortDateString();
                                }
                                catch { }

                                ed.Notario = rd[13].ToString();

                                ed.Clv_Etapa = int.Parse(rd[14].ToString());

                                ed.Lado = rd[15].ToString();

                                ed.IdDetalle = int.Parse(rd[16].ToString());



                                ed.ImporteEnganche = 0;
                                try
                                {
                                    ed.ImporteEnganche = decimal.Parse(rd[17].ToString());
                                }
                                catch { }


                                ed.PorcentajeEncganche = 0;
                                try
                                {
                                    ed.PorcentajeEncganche = int.Parse(rd[18].ToString());
                                }
                                catch { }


                                ed.NumeroPagosEnganche = 0;
                                try
                                {
                                    ed.NumeroPagosEnganche = int.Parse(rd[19].ToString());
                                }
                                catch { }



                                ed.clv_proyecto = int.Parse(rd[20].ToString());

                                ed.Torre = rd[21].ToString();

                                ed.Piso = int.Parse(rd[22].ToString());

                                ed.clv_Departamento = int.Parse(rd[23].ToString());

                                ed.proyecto = rd[24].ToString();

                                ed.departamento = rd[25].ToString();

                                ed.vistas = new List<InfoMobiliti>();

                                ed.vistas = GetVistasEstatusDepartamento(ed.IdDetalle);

                                lista.Add(ed);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_estatusDepartamento_clave " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return lista;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<ClientesEntity> GetClientesWEB()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return ClientesEntity.GetClientesWEB();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public int? AddClientesWEB(int id, int clv_vendedor, int clv_usuario)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return ClientesEntity.AddClientesWEB(id, clv_vendedor, clv_usuario);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        AgendaController agendaController = new AgendaController();
        public int? AddAgenda(
            string Tipo, string Fecha, int? Clv_vendedor, string Observaciones, int? Clv_cliente, int? accion, string ObservacionesConCliente
            )
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    
                    return agendaController.AddAgenda(Tipo, Fecha, Clv_vendedor, Observaciones, Clv_cliente, accion, ObservacionesConCliente);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public int? AddMotivoClienteDescartado(string IdMotivo, string Observaciones, string Fecha, int? IdVendedor, int? IdCliente)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    AgendaController agendaController = new AgendaController();
                    return agendaController.AddMotivoClienteDescartado(IdMotivo, Observaciones, Fecha, IdVendedor, IdCliente);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public int? AddBodegaCajon(List<BodegaCajonEntity> obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    BodegaCajonController BodegaCajonController = new BodegaCajonController();
                    return BodegaCajonController.AddBodegaCajon(obj);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }


        public int? AddCajon(List<BodegaCajonEntity> obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    BodegaCajonController BodegaCajonController = new BodegaCajonController();
                    return BodegaCajonController.AddCajon(obj);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }



        public List<BodegaCajonEntity> GetBodegasCajonesByIdDepartamento(int? IdDetalle, string Nivel, int Tipo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    BodegaCajonController BodegaCajonController = new BodegaCajonController();
                    return BodegaCajonController.GetBodegasCajonesByIdDepartamento(IdDetalle, Nivel, Tipo);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public List<BodegaCajonEntity> GetCajonesByIdDepartamento(int? IdDetalle, string Nivel, int Tipo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    BodegaCajonController BodegaCajonController = new BodegaCajonController();
                    return BodegaCajonController.GetCajonesByIdDepartamento(IdDetalle, Nivel, Tipo);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }



        public  List<PlanosBodega> GetPalnosTipo(int? clv_proyecto, string Nivel, int Tipo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    BodegaCajonController BodegaCajonController = new BodegaCajonController();
                    return BodegaCajonController.GetPalnosTipo(clv_proyecto, Nivel, Tipo);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }



        public int? AsignacionAutomatica()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    BodegaCajonController BodegaCajonController = new BodegaCajonController();
                    return ClientesEntity.AsignacionAutomatica();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }



        relUsuarioProyectoController rel = new relUsuarioProyectoController();
        public int? AddRelUsuarioProyecto(int idUsuario, int idProyecto)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return rel.AddRelUsuarioProyecto(idUsuario, idProyecto);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<relUsuarioProyectoEntity> GetRelUsuarioProyectoDisponibles(int idUsuario)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return rel.GetRelUsuarioProyectoDisponibles(idUsuario);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<relUsuarioProyectoEntity> GetRelUsuarioProyectoById(int? id)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return rel.GetRelUsuarioProyectoById(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? DeleteRelUsuarioProyecto(int id)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return rel.DeleteRelUsuarioProyecto(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<relUsuarioProyectoEntity> GetAllProyectos()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return rel.GetAllProyectos();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<AgendaEntity> GetAgenda(int idUsuario)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return agendaController.GetAgenda(idUsuario);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public AgendaEntity GetAgendaById(int id)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return agendaController.GetAgendaById(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public int? UpdateAgenda(int id, string Tipo, string Fecha, int? Clv_vendedor, string Observaciones, int? Clv_cliente, int accion, string ObservacionesConCliente)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return agendaController.UpdateAgenda(id, Tipo, Fecha, Clv_vendedor, Observaciones, Clv_cliente, accion, ObservacionesConCliente);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? CancelarAgenda(int id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return agendaController.CancelarAgenda(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        TipoImagenController tipoimagen = new TipoImagenController();

        public List<TipoImagen> GetTipoImagen()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tipoimagen.GetTipoImagen();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public List<AgendaEntity> GetAgendasByUsuario(int ? clv_usuario, String Fecha1, String Fecha2, int ? folio,string tipo,string status )
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return agendaController.GetAgendasByUsuario(clv_usuario, Fecha1, Fecha2, folio, tipo, status);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        ImagenController ImagenCotroller = new ImagenController();
        public List<logo> GetImagenesDepartamento()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                int clv_departamento = Int32.Parse(request["id"].ToString());
                return ImagenCotroller.GetImagenesDepartamento(clv_departamento);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }
        public List<AgendaEntity> GetHistorialObs(int clv_usuario)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return agendaController.GetHistorialObs(clv_usuario);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public string ReporteCartaAgradecimientoNocompra(int? clv_cliente)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                ReporteController reporteController = new ReporteController();
                return reporteController.ReporteCartaAgradecimientoNocompra(clv_cliente);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public string GetReporteEstatusDepartamento(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteEstatusDepartamento(Opexportacion, clv_proyecto, torres, clv_estatus);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

            }

        public string GetReporteResumenEstatusDepartamento(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteResumenEstatusDepartamento(Opexportacion, clv_proyecto, torres, clv_estatus);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public string GetReporteDepartamentosApartadosVendidos(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus, string fechaInicio, string fechaFin)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteDepartamentosApartadosVendidos( Opexportacion,clv_proyecto,  torres,  clv_estatus, fechaInicio,  fechaFin);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public string GetReporteVendedorDepApartadosVendidos(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus, string fechaInicio, string fechaFin, List<UsuarioEntity> usuarios)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteVendedorDepApartadosVendidos(Opexportacion, clv_proyecto, torres,  clv_estatus,  fechaInicio,  fechaFin, usuarios);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }


        public string GetReporteListadoClientes(int? Opexportacion, List<UsuarioEntity> usuarios, string fecha_inicio, string @fecha_fin, int? clv_estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteListadoClientes(Opexportacion, usuarios, fecha_inicio, fecha_fin, clv_estatus);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }





        public string GetReporteListadoClientesResumen(int? Opexportacion, List<UsuarioEntity> usuarios, int? clv_estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteListadoClientesResumen(Opexportacion, usuarios,clv_estatus);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<AgendaEntity> GetAgendasByCliente(int clv_cliente, String Fecha1, String Fecha2)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return agendaController.GetAgendasByCliente(clv_cliente, Fecha1, Fecha2);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }
        public  List<Estatus_Cliente> GetEstatusCliente()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return ClientesEntity.GetEstatusCliente();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public string GetReporteLlamadasVisitasPendientes(int? Opexportacion, string fechaInicio, string fechaFin, List<UsuarioEntity> usuarios, int tipoAgenda)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteLlamadasVisitasPendientes(Opexportacion, fechaInicio, fechaFin, usuarios, tipoAgenda);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }




        public string GetReporteLlamadasVisitasEjecutadas(int? Opexportacion, string fechaInicio, string fechaFin, List<UsuarioEntity> usuarios, int tipoAgenda)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteLlamadasVisitasEjecutadas(Opexportacion, fechaInicio, fechaFin, usuarios, tipoAgenda);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }







        public int? UpdateFechaExpiracion(int id, string fecha)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cotcon.UpdateFechaExpiracion(id, fecha);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<CotizacionEntity> GetCotizacionByCliente(int? Clv_cliente)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cotcon.GetCotizacionByCliente(Clv_cliente);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        RelacionesProyectoController relaciones = new RelacionesProyectoController();
        public List<RelAmenitieEntity> GetrelProyectoAmenitie(int clv_proyecto)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return relaciones.GetrelProyectoAmenitie(clv_proyecto);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public int? DeleteAmenitieProyecto(int id)

        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return relaciones.DeleteAmenitieProyecto(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<RelServicioEntity> GetrelProyectoServicio(int clv_proyecto)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return relaciones.GetrelProyectoServicio(clv_proyecto);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public int? DeleteServicioProyecto(int id)

        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return relaciones.DeleteServicioProyecto(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



        public int? DeleteTorreProyecto(int clv_proyecto, string Torre)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return relaciones.DeleteTorreProyecto( clv_proyecto, Torre);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<RelTorresEntity> GetTorresProyecto1(int clv_proyecto, string torre)
        { 
             if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return relaciones.GetTorresProyecto1(clv_proyecto,torre);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



        MotivoCancelacionPagoController cancelacion = new MotivoCancelacionPagoController();

        public List<MotivosCancelacionFacturas> GetMotivoCacelacionPago()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cancelacion.GetMotivoCacelacionPago();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public MotivosCancelacionFacturas GetMotivoCacelacionPagoById(int Id)

        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cancelacion.GetMotivoCacelacionPagoById(Id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public int? AddMotivoCancelacionPago(string Descripcion, bool? Activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cancelacion.AddMotivoCancelacionPago(Descripcion, Activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        recepcionDePagosController recepcionDePago = new recepcionDePagosController();
        public List<recepcionDePagosEntity> GetRecepcionDePagos()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return recepcionDePago.GetRecepcionDePagos();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? addRecepcionDePago(int clv_cliente, int clv_usuario, string serie, int folio, bool pagoDolares, decimal total)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return recepcionDePago.addRecepcionDePago(clv_cliente, clv_usuario, serie, folio, pagoDolares, total);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? addRecepcionDePagoImpuestos(int idDetalle, float IVA, float IEPS, float subTotal, float total)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return recepcionDePago.addRecepcionDePagoImpuestos(idDetalle, IVA, IEPS, subTotal, total);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? addRecepcionDePagoDet(int clv_factura, int clv_concepto, decimal precio_unitario, string descripcion, int contrato, String Periodo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return recepcionDePago.addRecepcionDePagoDet(clv_factura, clv_concepto, precio_unitario, descripcion, contrato, Periodo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

        public int? updateRecepcionDePago(int? id, string banco, string cuenta, string concepto, DateTime fecha, float monto, string formaDePago, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return recepcionDePago.updateRecepcionDePago(id, banco, cuenta, concepto, fecha, monto, formaDePago, activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }


        public recepcionDePagosEntity GetRecepcionDePagoById(int id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return recepcionDePago.GetRecepcionDePagoById(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }

       NivelesController  Nivel = new NivelesController();

        public List<NivelesEntity> GetNivel()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return Nivel.GetNivel();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<relNivelesDepartamentoEntity> GetNivelbyDepartamento(long id_Departamento)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return Nivel.GetNivelbyDepartamento(id_Departamento);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }








        public int? AceptaPropuesta(int? clv_cotizacion)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cotcon.AceptaPropuesta(clv_cotizacion);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }
        
        
        public int? UpdateMotivoCancelacionPago(int? Id, string Descripcion, bool? Activo)

        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cancelacion.UpdateMotivoCancelacionPago(Id, Descripcion, Activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
        }



        public List<UsuarioEntity> GetVendedoresByUsuario( int ? clv_usuario)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return UsuarioController.GetVendedoresByUsuario(clv_usuario);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }


        }


        NivelesController bodega = new NivelesController();
        public PlanosBodegaEntity GetPlanosBodega(string Nivel, int clv_proyecto)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return bodega.GetPlanosBodega(Nivel, clv_proyecto);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        

            public List<cuentaBancariaEntity> getCuentasBancariasByBanco(int? idBanco)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return cuentaBancariaEntity.getCuentasBancariasByBanco(idBanco);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? selectPagoEnganche(int? clv_cotizacion)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return recepcionDePago.selectPagoEnganche(clv_cotizacion);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }
             
            
       
        public int? selectPagoApartado(int? clv_cotizacion)
        {
           
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return recepcionDePago.selectPagoApartado(clv_cotizacion);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<pagoEntity> GetPagos(int? Factura, int? Cotizacion, string FechaInicio, string FechaFin, int Contrato)

        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return recepcionDePago.GetPagos( Factura, Cotizacion, FechaInicio, FechaFin, Contrato);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? CancelarPago(int id)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {         
                    return recepcionDePago.CancelarPago(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? AddRelCancelacionFactura(int? clv_Factura, int? id_Usuario, int? Motivo)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {



                    return recepcionDePago.AddRelCancelacionFactura(clv_Factura, id_Usuario, Motivo);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<cotizacionPago> cotizacionPago()
        { 
            
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {         
                    return cotcon.cotizacionPago();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        relPagosContratoController relPagosContratoController = new relPagosContratoController();
        public List<cotizacionPago> SearchCotizacionByCliente(int Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return cotcon.SearchCotizacionByCliente(Id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? addRelacionPagosContrato(int clv_concepto, string descripcion, string fechaPago, decimal monto, int contrato, int clv_factura)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return relPagosContratoController.addRelacionPagosContrato(clv_concepto, descripcion, fechaPago, monto, contrato, clv_factura);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }
                

        public List<relPagosContratoEntity> GetRelacionPagosContratoById(int contrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return relPagosContratoController.GetRelacionPagosContratoById(contrato);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }
        public List<contratoByclienteEntity> GetContratoByCliente(int? Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                      return contratocon.GetContratoByCliente(Id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public int? GetContratoByCotizacion(int clv_cotizacion)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return contratocon.GetContratoByCotizacion(clv_cotizacion);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public fechaReservacionEntity GetFechaReservacion(int clv_cotizacion)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return recepcionDePago.GetFechaReservacion(clv_cotizacion);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public  string GetReporteIngresos(int? Opexportacion, string fechaInicio, string fechaFin, int usuarios)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteIngresos(Opexportacion, fechaInicio, fechaFin, usuarios);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public string GetReporteEstadoCuentaEnviado(int? Opexportacion, string fechaInicio, string fechaFin)

        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteEstadoCuentaEnviado(Opexportacion, fechaInicio, fechaFin);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public string GetReporteClientesVencidos(int? Opexportacion, string fechaInicio, string fechaFin)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteClientesVencidos(Opexportacion, fechaInicio, fechaFin);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }


        public string GetReporteClientesPorVencer(int? Opexportacion, string fechaInicio, string fechaFin)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteClientesPorVencer(Opexportacion, fechaInicio, fechaFin);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public int? addMetodoPago(int Clv_factura, int Clv_tipoPago, decimal Monto, int banco, long cuenta, int numeroCheque, string cuentaTrans)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return recepcionDePago.addMetodoPago(Clv_factura, Clv_tipoPago, Monto, banco, cuenta, numeroCheque, cuentaTrans);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

      

        public int? ValidarCotizacion(int? clv_cotizacion)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return recepcionDePago.ValidarCotizacion(clv_cotizacion);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }


        public fechaEngancheApartado getFechasEA(int? clv_cotizacion)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return recepcionDePago.getFechasEA(clv_cotizacion);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public int? ValidaContrato(int? contrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return recepcionDePago.ValidaContrato(contrato);


                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }


        public string GetReporteIngresosPorCobrar(int? Opexportacion, string fechaInicio, string fechaFin, List<conceptosDePagoEntity> concepto)

                    {
                        if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
                        {
                            return null;
                        }
                        else
                        {
                            try
                            {

                                return reportes.GetReporteIngresosPorCobrar(Opexportacion, fechaInicio, fechaFin, concepto);

                            }
                            catch (Exception ex)
                            {
                                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                            }
                        }


                    }

        public string GetReporteGeneralIngresos(int? Opexportacion, string fechaInicio, string fechaFin)

        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteGeneralIngresos(Opexportacion, fechaInicio, fechaFin);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }



        public string GetTicket(int? Opexportacion, int? IdDetalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetTicket(Opexportacion, IdDetalle);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }


       public string GetQrPago(int? Opexportacion, String CadenaQr)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetQrPago(Opexportacion, CadenaQr);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public int? ActivarCotizacion(int? Id, string FechaInicio)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return cotcon.ActivarCotizacion(Id, FechaInicio);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        VistaController vistaCon = new VistaController();

        public List<VistaEntity> GetVistaByProyecto(int? clv_vendedor)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    return vistaCon.GetVistaByProyecto(clv_vendedor);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public string GetReporteTarea(int? Opexportacion,int? clv_comprador, string fechaInicio, string fechaFin)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetReporteTarea(Opexportacion, clv_comprador, fechaInicio, fechaFin);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

       





        public int? UpdateDatosCliente(int clv_comprador, string estado, string municipio,
        string localidad, string colonia, string calle, string numero, string numeroInterior,
        int? codigoPostal, string rfc, string curp, string folio
            )
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return contratocon.UpdateDatosCliente( clv_comprador,  estado,  municipio,localidad,  colonia,  calle,  numero,  numeroInterior,codigoPostal,  rfc,  curp,  folio);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        DatosDepCotizacionController datosDepCoti = new DatosDepCotizacionController();
        public DatosDepCotizacionEntity getDatosDepCotizacion(int? IdDetalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return datosDepCoti.getDatosDepCotizacion(IdDetalle);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }


        public string GetAcepataCompra(int? Op, int? cotizacion)


        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return reportes.GetAcepataCompra(Op, cotizacion);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }


        }

        public List<SerieFacturaEntity> GetFaturaSerie()
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetFaturaSerie();

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



        public int? addSerie(SerieFacturaEntity serie)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.addSerie(serie);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<MensajeEntity> GetMensaje()
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetMensaje();

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? addMensaje(MensajeEntity mensaje)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.addMensaje(mensaje);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


      public  List<FirmwareModeloEntity> GetFirmwareModelo(int? clv_modelo)
          {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetFirmwareModelo(clv_modelo);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public int? GenraFacturaAlegra(Pagoentity Pago)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GenraFacturaAlegra(Pago);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



        public int? addOLT(OLTEntity olt)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.addOLT(olt);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? EnviarCorreoMasivo(CorreoMensajeEntity correo)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.EnviarCorreoMasivo(correo);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }
      

        public int? addUsuarioAlegra(UsuarioAlegraEntity Usuario)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.addUsuarioAlegra(Usuario);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<UsuarioAlegraEntity> GetUsuarioAlegra()
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetUsuarioAlegra();

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<EpaycoEntity> GetEpayco()
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetEpayco();

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<EstadosCuentaEntity> GetEstadosDecuentaEnviar()

        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetEstadosDecuentaEnviar();

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int?  EnviaSms(String Celular1)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.EnviaSms(Celular1);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<GneralCorreoEntity> ConGeneralCorreo()
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.ConGeneralCorreo();

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


   


        public int? addEpayco(EpaycoEntity Epayco)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.addEpayco(Epayco);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }




        public int? addGeneralcorreo(GneralCorreoEntity correo)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.addGeneralcorreo(correo);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }






        public int? addMonto(MontoEntity monto)
        {

            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.addMonto(monto);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public  OLTEntity GetOLTById(int? id)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetOLTById(id);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public List<OLTEntity> GetOLTByIdCliente(int? id)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetOLTByIdCliente(id);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



        public List<OLTEntity> GetOLTByIdClienteRecepcionPagos(int? id)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetOLTByIdClienteRecepcionPagos(id);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


       

        public List<OLTEntity> GetOLTList(int? clienteId)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetOLTList(clienteId);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<MontoEntity> GetMonto()

        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.GetMonto();

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? updateOLT(OLTEntity olt)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.updateOLT(olt);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public int? DeleteOLT(OLTEntity olt)
        {
            OltController olt_ctrl = new OltController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    return olt_ctrl.DeleteOLT(olt);

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }



    }
}


