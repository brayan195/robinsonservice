﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softv.Entities
{
    public class TorreEntity
    {
        public string Descripcion { get; set; }
        public string id { get; set; }

        public int Estatus { get; set; }

        public List<PisoEntity> Pisos { get; set; }

        public List<DepartamentoEntity> Lado { get; set; }
    }

    public class PisoEntity
    {
        public int Piso { get; set; }

        public string FechaEntrega { get; set; }

        public int Etapa { get; set; }

        public List<DepartamentoEntity> Departamentos { get; set; }

    }

    public class ProyectoDetalle
    {
        public int clv_proyecto { get; set; }

        public string Torre { get; set; }

        public int Piso { get; set; }

        public int clv_Departamento { get; set; }

        public long Id { get; set; }

    }

    public class MessageEntity
    {
        public int error { get; set; }

        public string mensaje { get; set; }



    }


}