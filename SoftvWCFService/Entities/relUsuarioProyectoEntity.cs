﻿using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class relUsuarioProyectoEntity
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int idProyecto { get; set; }
        [DataMember]
        public int idUsuario { get; set; }

        [DataMember]
        public int clv_proyecto { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string numeroExterior { get; set; }
        [DataMember]
        public string latitud { get; set; }
        [DataMember]
        public string longuitud { get; set; }
        [DataMember]
        public string direccion { get; set; }
    }
}