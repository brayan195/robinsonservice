﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftvWCFService.Entities
{
    public class TipoVendedorEntity
    {
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public bool Activo { get; set; }
    }
}