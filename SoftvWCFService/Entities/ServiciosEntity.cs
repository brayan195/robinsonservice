﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ServiciosEntity
    {
        [DataMember]
        public int ? clv_servicio { get; set; }

        [DataMember]
        public string nombre_Servicio { get; set; }

        [DataMember]
        public string descripcion { get; set; }

        [DataMember]
        public  bool? activo { get; set; }
   
       
    }
}