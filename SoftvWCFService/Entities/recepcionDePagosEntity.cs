﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class recepcionDePagosEntity
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string banco { get; set; }
        [DataMember]
        public string cuenta { get; set; }
        [DataMember]
        public string concepto { get; set; }
        [DataMember]
        public DateTime fecha { get; set; }
        [DataMember]
        public float monto { get; set; }
        [DataMember]
        public string formaDePago { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public string Fecha2 { get; set; }
    }
    public class pagoEntity {
        public long Clv_factura { get; set; }

        public string Estatus { get; set; }

        public long Clv_cliente { get; set; }

        public DateTime Fecha { get; set; }
        public string Fecha2 { get; set; }

        public long Cajero { get; set; }

        public string Serie { get; set; }

        public long Folio { get; set; }

        public bool PagoDolares { get; set; }

        public decimal Total { get; set; }

        public long IdDetalle { get; set; }

        public long Clv_Concepto { get; set; }

        public decimal PrecioUnitario { get; set; }

        public string Descripcion { get; set; }

        public long Contrato { get; set; }

        public long Clv_Cotizacion { get; set; }

        public long Clv_Departamento { get; set; }

        public long Clv_facturaImpuestos { get; set; }

        public decimal Iva { get; set; }

        public decimal Ieps { get; set; }

        public decimal SubTotal { get; set; }

        public long Clv_tipoPago { get; set; }

        public decimal Monto { get; set; }

        public string Nombre_cliente { get; set; }

        public string nombre { get; set; }
    }

    [DataContract]
    [Serializable]
    public class fechaReservacionEntity
    {
        [DataMember]
        public DateTime Fecha { get; set; }
        [DataMember]
        public string Fecha2 { get; set; }
    }

    [DataContract]
    [Serializable]
    public class fechaEngancheApartado
    {
        [DataMember]
        public DateTime PagoEnganche { get; set; }
        [DataMember]
        public string FechaEnganche2 { get; set; }
        [DataMember]
        public DateTime PagoApartado { get; set; }
        [DataMember]
        public string FechaApartado2 { get; set; }
    }
}