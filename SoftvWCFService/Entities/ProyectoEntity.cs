﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ProyectoEntity
    {
        [DataMember]
        public int? clv_proyecto { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string numeroExterior { get; set; }
        [DataMember]
        public decimal? costoMetroCuadradoInicial { get; set; }
        [DataMember]
        public decimal? precioVentaActual { get; set; }
        [DataMember]
        public int? clv_estado { get; set; }
        [DataMember]
        public int? clv_municipio { get; set; }
        [DataMember]
        public int? clv_localidad { get; set; }
        [DataMember]
        public int? clv_colonia { get; set; }
        [DataMember]
        public bool? activo { get; set; }
        [DataMember]
        public string latitud { get; set; }
        [DataMember]
        public string longuitud { get; set; }
        [DataMember]
        public int? factorIncremento { get; set; }
        [DataMember]
        public int? cambioEtapaPorVenta { get; set; }
        [DataMember]
        public string direccion { get; set; }
        [DataMember]
        public int? numeroEtapas { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public string Municipio { get; set; }

        [DataMember]
        public string Localidad { get; set; }

        [DataMember]
        public string Colonia { get; set; }

        [DataMember]
        public string frasePublicitaria { get; set; }

        [DataMember]
        public List<TorreEntity> Torres { get; set; }       
        [DataMember]
       public List<InfoMobiliti> amenities { get; set; }
        [DataMember]
        public List<ServiciosEntity> servicio { get; set; }

        [DataMember]
        List<string> imagenes { get; set; }

    }
}