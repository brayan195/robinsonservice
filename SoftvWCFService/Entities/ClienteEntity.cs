﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ClienteEntity
    {
        [DataMember]
        public int? clv_comprador { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public int? id { get; set; }

        [DataMember]
        public int? tipocliente { get; set; }





        [DataMember]
        public string primer_apellido { get; set; }

        [DataMember]
        public string segundo_apellido { get; set; }

        [DataMember]
        public string primerA { get; set; }

        [DataMember]
        public string segundoA { get; set; }

        [DataMember]
        public string otronombre { get; set; }

        [DataMember]
        public int? clv_estado { get; set; }
        [DataMember]
        public int? clv_ciudad { get; set; }

        [DataMember]
        public int? clv_calle { get; set; }

        [DataMember]
        public int? clv_pais { get; set; }

        [DataMember]
        public int? clv_municipio { get; set; }

        [DataMember]
        public int? clv_localidad { get; set; }

        [DataMember]
        public int? clv_colonia { get; set; }

        [DataMember]
        public String colonia { get; set; }

        [DataMember]
        public String calle { get; set; }
        [DataMember]
        public string numero { get; set; }
        [DataMember]
        public  int? codigoPostal { get; set; }
        [DataMember]
        public string telefono { get; set; }

        [DataMember]
        public string nombreR { get; set; }


        [DataMember]
        public string nombrecontrato { get; set; }

        


        [DataMember]
        public string rut { get; set; }

        [DataMember]
        public string Dominio { get; set; }

        [DataMember]
        public string password { get; set; }

        [DataMember]
        public string password1 { get; set; }


        [DataMember]
        public string nit { get; set; }
        [DataMember]
        public string celular { get; set; }
        [DataMember]
        public string correo { get; set; }

        [DataMember]
        public string correo1 { get; set; }
        [DataMember]
        public string cedulaR { get; set; }

        
        [DataMember]
        public  bool activo { get; set; }
        [DataMember]
        public string estado { get; set; }
        [DataMember]
        public string municipio { get; set; }
        [DataMember]
        public string localidad { get; set; }
        //[DataMember]
        //public string colonia { get; set; }
        //[DataMember]
        //public string calle { get; set; }
        [DataMember]
        public string numeroInterior { get; set; }
        [DataMember]
        public string telefono_casa { get; set; }
        [DataMember]
        public string Fechacreacion { get; set; }


        [DataMember]
        public string usuario { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public string Registro { get; set; }

        [DataMember]
        public string rfc { get; set; }

        [DataMember]
        public string curp { get; set; }

        [DataMember]
        public string folio { get; set; }

        [DataMember]
        public int Medios { get; set; }
        [DataMember]
        public string asignador { get; set; }

        [DataMember]
        public int medioContacto { get; set; }

        [DataMember]
        public string telefonoCA { get; set; }

        [DataMember]
        public string CorreoCadmin { get; set; }






    }
    public class ClientesEntity
    {

        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string Nombre_pais { get; set; }


        [DataMember]
        public int? clv_comprador { get; set; }

        [DataMember]
        public string nombre_cliente { get; set; }

        [DataMember]
        public int tipocliente { get; set; }

        [DataMember]
        public string primer_apellido { get; set; }

        [DataMember]
        public string segundo_apellido { get; set; }

        [DataMember]
        public string primerA { get; set; }

        [DataMember]
        public string segundoA { get; set; }

        [DataMember]
        public string otronombre { get; set; }

        [DataMember]
        public int? clv_estado { get; set; }
        [DataMember]
        public int? clv_ciudad { get; set; }

        [DataMember]
        public int? clv_calle { get; set; }

        [DataMember]
        public int? clv_pais { get; set; }

        [DataMember]
        public int? clv_municipio { get; set; }

        [DataMember]
        public int? clv_localidad { get; set; }

        [DataMember]
        public int? clv_colonia { get; set; }
        [DataMember]
        public string numero { get; set; }
        [DataMember]
        public int? codigoPostal { get; set; }
        [DataMember]
        public string telefono { get; set; }

        [DataMember]
        public string nombreR { get; set; }

        [DataMember]
        public string rut { get; set; }

        [DataMember]
        public string Dominio { get; set; }

        [DataMember]
        public string password { get; set; }


        [DataMember]
        public string nit { get; set; }
        [DataMember]
        public string celular { get; set; }
        [DataMember]
        public string correo { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public string estado { get; set; }
        [DataMember]
        public string municipio { get; set; }
        [DataMember]
        public string localidad { get; set; }
        [DataMember]
        public string colonia { get; set; }
        [DataMember]
        public string calle { get; set; }
        [DataMember]
        public string numeroInterior { get; set; }
        [DataMember]
        public string telefono_casa { get; set; }
        [DataMember]
        public string Fechacreacion { get; set; }


        [DataMember]
        public string usuario { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public string Registro { get; set; }

        [DataMember]
        public string rfc { get; set; }

        [DataMember]
        public string curp { get; set; }

        [DataMember]
        public string folio { get; set; }

        [DataMember]
        public int Medios { get; set; }
        [DataMember]
        public string asignador { get; set; }

        [DataMember]
        public int medioContacto { get; set; }


        [DataMember]
        public string CorreoContatoAdministrativo { get; set; }


    }
    public class Estatus_Cliente
    {
        public int Id { get; set; }

        public string Estatus { get; set; }

    }






    }