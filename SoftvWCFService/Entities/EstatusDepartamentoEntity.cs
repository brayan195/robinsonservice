﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class EstatusDepartamentoEntity
    {
        [DataMember]
        public long IdDetalle { get; set; }
        [DataMember]
        public string NumDepartamento { get; set; }
        [DataMember]
        public string FechaEntrega { get; set; }
        [DataMember]
        public int Estatus { get; set; }
        [DataMember]
        public int? Clv_vendedor { get; set; }
        [DataMember]
        public int? Clv_comprador { get; set; }
        [DataMember]
        public decimal? PrecioLista { get; set; }
        [DataMember]
        public int? preciolista { get; set; }

        [DataMember]
        public decimal? precioActual { get; set; }
        [DataMember]
        public decimal? PrecioActual { get; set; }
        [DataMember]
        public decimal? PrecioVendio { get; set; }
        [DataMember]
        public string FechaApartado { get; set; }
        [DataMember]
        public decimal? ImporteApartado { get; set; }
        [DataMember]
        public string FechaVenta { get; set; }
        [DataMember]
        public string FechaEscrituracion { get; set; }
        [DataMember]
        public string Notario { get; set; }
        [DataMember]
        public string Lado { get; set; }
        [DataMember]
        public int Clv_Etapa { get; set; }
        [DataMember]
        public decimal? ImporteEnganche { get; set; }
        [DataMember]
        public Decimal PorcentajeEncganche { get; set; }
        [DataMember]
        public int? NumeroPagosEnganche { get; set; }

        [DataMember]
        public int ? clv_proyecto { get; set; }
        [DataMember]
        public string proyecto { get; set; }

        [DataMember]
        public string Torre { get; set; }
        [DataMember]
        public int? Piso { get; set; }
        [DataMember]
        public int? clv_Departamento { get; set; }
        [DataMember]
        public string departamento { get; set; }

        [DataMember]
        public int mesesDiferencia { get; set; }


        //[DataMember]
        //public string Nivel { get; set; }

        [DataMember]
        public List<InfoMobiliti> vistas { get; set; }

        [DataMember]
        public List<DepartamentoEnganchePagoEntity> pagos { get; set; }

        [DataMember]
        public List<relNivelesDepartamentoEntity>nivel;

    }


    [DataContract]
    [Serializable]
    public class EstatusVentaDepartamentoEntity
    {
        [DataMember]
        public int ?  Id { get; set; }

        [DataMember]
        public string Descripcion { get; set; }


    }

    [DataContract]
    [Serializable]
    public class DepartamentoEnganchePagoEntity
    {
        [DataMember]
        public int ?IdDetalle { get; set; }
        [DataMember]
        public int ? NoPago { get; set; }
        [DataMember]
        public string Fecha { get; set; }
        [DataMember]
        public decimal ? Importe { get; set; }

    }

}