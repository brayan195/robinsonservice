﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class relPagosContratoEntity
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int clv_concepto { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public DateTime FechaPago { get; set; }
        [DataMember]
        public decimal Monto { get; set; }
        [DataMember]
        public int contrato { get; set; }
        [DataMember]
        public bool estatus { get; set; }
        [DataMember]
        public string FechaPago2 { get; set; }

    }
}