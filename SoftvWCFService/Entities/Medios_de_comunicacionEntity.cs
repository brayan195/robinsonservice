﻿




using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftvWCFService.Entities
{
    public class Medios_de_comunicacionEntity
    {
        public int clv_medio { get; set; }
        

        public string nombre { get; set; }

        public bool estatus { get; set; }

    }
    public class MedioContacto
    {

        public int clv_medioContacto { get; set; }

        public string nombre { get; set; }

        public bool estatus { get; set; }

    }


}