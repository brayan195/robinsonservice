﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ConfiguracionEntity
    {
        [DataMember]
        public int? id { get; set; }
        [DataMember]
        public int? MultiplicadorDescuentos { get; set; }
        [DataMember]
        public int? MultiplicadorFinanciamiento { get; set; }
        [DataMember]
        public float Cetes { get; set; }
        [DataMember]
        public int? Expiracion { get; set; }
        [DataMember]
        public string Folio { get; set; }
        [DataMember]
        public string Serie { get; set; }
        [DataMember]
        public float IVA { get; set; }
        [DataMember]
        public float InteresAtraso { get; set; }
        [DataMember]
        public float Escrituracion { get; set; }
        [DataMember]
        public float Reservacion { get; set; }
        [DataMember]
        public float DescuentoEnganche { get; set; }

    }
    public class PlanpagoEntity
    {
        [DataMember]
        public int? id_plan_pago { get; set; }
        [DataMember]
        public float? enganche_inicial { get; set; }
        [DataMember]
        public float? enganche_diferido { get; set; }
        [DataMember]
        public float? escrituras { get; set; }
    }
}