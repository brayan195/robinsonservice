﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ColoniaEntity
    {
        [DataMember]
        public int? clv_colonia { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public int ? clv_estado { get; set; }
        [DataMember]
        public int ? clv_localidad { get; set; }

        [DataMember]
        public int? clv_pais { get; set; }

        [DataMember]
        public int? clv_municipio { get; set; }



        

        [DataMember]
        public int? clv_ciudad { get; set; }


        [DataMember]
        public int? clv_calle { get; set; }

        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public int ? cp { get; set; }

        [DataMember]
        public string Estado { get; set; }

    }
}