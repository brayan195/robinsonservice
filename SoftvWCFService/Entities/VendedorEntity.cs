﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class VendedorEntity
    {
        [DataMember]
        public int clv_usuario { get; set; }
        [DataMember]
        public string nombreVendedor { get; set; }
        [DataMember]
        public string rfc { get; set; }
        [DataMember]
        public string curp { get; set; }
        [DataMember]
        public string celular { get; set; }
    }
}