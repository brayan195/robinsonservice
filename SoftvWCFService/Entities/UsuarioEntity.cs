﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftvWCFService.Entities
{
    public class UsuarioEntity
    {
        public int clv_usuario { get; set; }

        public string nombre { get; set; }

        public string correo { get; set; }

        public string contrasenia { get; set; }

        public bool activo { get; set; }

        public int tipoUsuario { get; set; }

        public int clv_tipoVendedor { get; set; }

        public string tipoVendedor { get; set; }

        public VendedorEntity detalleVendedor { get; set; }
    }
    public class relUsuarioDepartamentosEntity
    {
        public int id { get; set; }

        public int? idDepartamento { get; set; }

        public int? idUsuario { get; set; }

    }
}