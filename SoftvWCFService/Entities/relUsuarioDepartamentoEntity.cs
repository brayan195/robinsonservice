﻿using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class relUsuarioDepartamentoEntity
    {
        [DataMember]
        public int  id { get; set; }
        [DataMember]
        public int  idDepartamento { get; set; }
        [DataMember]
        public int idUsuario { get; set; }
        [DataMember]
        public EstatusDepartamentoEntity departamento { get; set; }

    }
}