﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftvWCFService.Entities
{
    public class OLTEntity
    {
        public long id { get; set; }

        public long cliente_id { get; set; }

        public string nombre_olt { get; set; }

        public string olt_ip { get; set; }

        public string NombreMarca { get; set; }

        public string nombre_modelo { get; set; }

        
        public string telnet_port { get; set; }

        public string telnet_username { get; set; }

        public string telnet_password { get; set; }

        public string snmp_readonly { get; set; }

        public string snmp_readwrite { get; set; }

        public string snmp_udp_port { get; set; }

        public string iptv_module { get; set; }

        public string hardware_version { get; set; }

        public string software_version { get; set; }

        public string supported_pon_types { get; set; }

        public string pago_inicial { get; set; }

        public string periodicity { get; set; }

        public string prox_pago { get; set; }

        public string olt_picture { get; set; }

        public long status { get; set; }

        public string continente { get; set; }

        public string latitud { get; set; }

        public string longitud { get; set; }

        public string moneda { get; set; }

        public string timezone { get; set; }

        public string usuario { get; set; }

        public string clave { get; set; }

        public string contrato_estado { get; set; }

        public int? notificacion { get; set; }

        public string fecha_alta { get; set; }

        public string fecha_baja { get; set; }

        public string fecha_suspendido { get; set; }

        public string fecha_ultimopago { get; set; }

        public int ultimo_mes { get; set; }

        public string ultimo_anio { get; set; }

        public string contacto_tecnico { get; set; }

        public string contacto_correo { get; set; }

        public string contacto_telefono { get; set; }

        public string contacto_celular { get; set; }

        public int clv_marca { get; set; }

        public int clv_modelo { get; set; }

        public int clv_firmware { get; set; }

        public bool check { get; set; }



    }


    public class MontoEntity
    {
        public long id { get; set; }

      
        public decimal Monto { get; set; }
        public decimal IVA { get; set; }



    }

    public class SerieFacturaEntity
    {
        public long id { get; set; }


        public int Folio { get; set; }

        public String Serie { get; set; }


    }

    public class MensajeEntity
    {
        public long id { get; set; }

        public String Mensaje { get; set; }


    }


    public class CorreoMensajeEntity
    {
        public long id { get; set; }

        public String Mensaje { get; set; }

        public bool Act { get; set; }

        public bool Sus { get; set; }

        public bool Baj { get; set; }

        public bool Dem { get; set; }


    }

    public class FirmwareModeloEntity
    {
        public long clv_firmware { get; set; }

        public String Firmware { get; set; }


    }

    public class Pagoentity
    {

        public int Clv_factura { get; set; }



    }


    public class UsuarioAlegraEntity
    {
        public long id { get; set; }

        public String Usuario { get; set; }

        public String Token { get; set; }




    }


    public class EpaycoEntity
    {


        public String Public_Key { get; set; }



    }



    public class GneralCorreoEntity
    {


        public String Cuenta { get; set; }
        public String Password { get; set; }
        public String Host { get; set; }

        public int Port { get; set; }





    }


    public class EstadosCuentaEntity
    {


        public int IdEstadoCuenta { get; set; }
        public String nombre_cliente { get; set; }
        public String nombre_olt { get; set; }

        public int id { get; set; }

        public String correo { get; set; }

        public String celular { get; set; }

        public String FECHA { get; set; }

        public Decimal Total { get; set; }

        




    }



}