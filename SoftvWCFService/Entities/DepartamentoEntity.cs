﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class DepartamentoEntity
    {
        [DataMember]
        public int? Clv_departamento { get; set; }

        [DataMember]
        public string NumDepartamento { get; set; }


        [DataMember]
        public string TipoDepartamento { get; set; }

        [DataMember]
        public int ? NumeroNiveles { get; set; }

        [DataMember]
        public float? MetrosTotales { get; set; }

        [DataMember]
        public decimal ? CostoDepartamento { get; set; }

        [DataMember]
        public int ? cantidad { get; set; }

        [DataMember]
        public bool? Activo { get; set; }

        [DataMember]
        public List<DepartamentoEspacioEntity> espacios { get; set; }

        [DataMember]
        public string Lado { get; set; }
        [DataMember]
        public int Estatus { get; set; }
        [DataMember]
        public string imagen { get; set; }

    }


    [DataContract]
    [Serializable]
    public class DepartamentoEspacioEntity
    {
        [DataMember]
        public int? clv_espacio_departamento { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public float? metros { get; set; }

        [DataMember]
        public bool suma { get; set; }

        [DataMember]
        public string descripcion { get; set; }       
    }

    public class TipoImagen
    {
        public int id { get; set; }

        public string nombre { get; set; }

    }
    public class logo
    {
        public int Idtipo { get; set; }

        public string Nombre { get; set; }

        public string Valor { get; set; }

    }
    public class NivelesEntity
    {
        public int Clv_nivel { get; set; }

        public string Nivel { get; set; }



    }
    public class relNivelesDepartamentoEntity
    {
        public int Id { get; set; }

        public string Nivel { get; set; }

        public int id_Departamento { get; set; }

    }

    public class PlanosBodegaEntity
    {
        public int Id_plano { get; set; }

        public string Nombre_Imagen { get; set; }
        public string Nivel { get; set; }

    }


}