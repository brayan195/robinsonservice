﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class CotizacionEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime Fecha { get; set; }

        [DataMember]
        public DateTime PagoApartado { get; set; }



        [DataMember]
        public DateTime FechaExpiracion { get; set; }
        [DataMember]
        public int Clv_cliente { get; set; }
        [DataMember]
        public int Clv_vendedor { get; set; }
        [DataMember]
        public string Estatus { get; set; }
      
        [DataMember]
        public decimal Cetes { get; set; }
        [DataMember]
        public decimal Multiplicador { get; set; }
        [DataMember]
        public decimal Reservacion { get; set; }
        [DataMember]
        public decimal Escrituracion { get; set; }
        [DataMember]
        public int IdDetalle { get; set; }       
        [DataMember]
        public decimal PrecioMetro { get; set; }
        [DataMember]
        public decimal MetrosDepartamento { get; set; }
        [DataMember]
        public int MesesDiferidos { get; set; }

        [DataMember]
       public string  Fecha2 { get; set; }

        [DataMember]
        public string FechaExpiracion2 { get; set; }

        [DataMember]
        public string PagoApartado2 { get; set; }

        [DataMember]
        public decimal EngancheDiferido { get; set; }

        [DataMember]
        public decimal EngancheInicial { get; set; }

        [DataMember]
        public decimal EngancheAdicional { get; set; }

        [DataMember]
        public decimal Escrituras { get; set; }


        [DataMember]
        public DateTime FechaEntrega { get; set; }

        [DataMember]
        public string FechaEntrega2 { get; set; }

        [DataMember]
        public int Op { get; set; }


        [DataMember]
        public InfoClientes cliente { get; set; }

        [DataMember]
        public EstatusDepartamentoEntity departamento { get; set; }

        [DataMember]
        public  cotizacionDiferidoEntity precios { get; set; }

        [DataMember]
        public List<PagosDiferidosEntity> pagos { get; set; }

        [DataMember]
        public List<PagosDescuentoEntity> pagosdescuento { get; set; }
    }


    



    public class PagosDiferidosEntity
    {
        [DataMember]
        public int  Id { get; set; }
        [DataMember]
        public decimal  Monto { get; set; }
        [DataMember]
        public DateTime FechaPago { get; set; }
        [DataMember]
        public string  FechaPago2 { get; set; }
    }

    public class PagosDescuentoEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public decimal Monto { get; set; }
        [DataMember]
        public DateTime FechaPago { get; set; }
        [DataMember]
        public string FechaPago2 { get; set; }
    }

    public class cotizacionDiferidoEntity
    {
        [DataMember]
        public long  Clv_cotizacion { get; set; }
        [DataMember]
        public decimal PrecioInicial { get; set; }
        [DataMember]
        public decimal EngancheInicial { get; set; }
        [DataMember]
        public decimal EngancheAdicional { get; set; }
        [DataMember]
        public decimal EngancheDiferido { get; set; }
        [DataMember]
        public decimal EngancheTotal { get; set; }
        [DataMember]
        public decimal EngancheTotalDiferido { get; set; }
        [DataMember]
        public decimal EngancheTotalNoDiferido { get; set; }
        [DataMember]
        public decimal EngancheTotalNoDiferidoMenosDescuento { get; set; }
        [DataMember]
        public decimal DescuentoEngancheDiferido { get; set; }
        [DataMember]
        public decimal DescuentoEngancheAdicional { get; set; }
        [DataMember]
        public decimal FinanciamientoEngancheDiferido { get; set; }
        [DataMember]
        public decimal EngancheDiferidoMenosDescuento { get; set; }
        [DataMember]
        public decimal EngancheDiferidoMasFinanciamiento { get; set; }
        [DataMember]
        public decimal LiquidacionEscrituracion { get; set; }
        [DataMember]
        public decimal PrecioTotal { get; set; }
        [DataMember]
        public decimal PrecioMetroCuadrado { get; set; }


        [DataMember]
        public decimal DescuentoEnganche { get; set; }


    }











    public class ProspectoEntity
    {
        [DataMember]
        public int? Clv_prospecto { get; set; }
   

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Correo { get; set; }

        [DataMember]
        public string Telefono { get; set; }

      


    }
    public class Motivos_de_cancelacionEntity
    {
        public int Id { get; set; }

        public string Motivos { get; set; }

        public bool activo { get; set; }

    }

    public class MotivosCancelacionFacturas
    {
        public long Id { get; set; }

        public string Descripcion { get; set; }

        public bool Activo { get; set; }

    }
    public class cotizacionPago
    {
        public int Id { get; set; }

        public DateTime Fecha { get; set; }

        public DateTime FechaExpiracion { get; set; }

        public int Clv_cliente { get; set; }

        public int Clv_vendedor { get; set; }

        public string Estatus { get; set; }

        public decimal Cetes { get; set; }
        public string Fecha2 { get; set; }
        public string FechaExpiracion2 { get; set; }


        public decimal Multiplicador { get; set; }

        public decimal Reservacion { get; set; }

        public decimal Escrituracion { get; set; }

        public int IdDetalle { get; set; }

        public decimal PrecioMetro { get; set; }

        public decimal MetrosDepartamento { get; set; }

        public DateTime FechaEntrega { get; set; }

        public int MesesDiferidos { get; set; }

        public string Nombre_cliente { get; set; }

    }



}