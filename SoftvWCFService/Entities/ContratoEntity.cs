﻿using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class ContratoEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int Clv_cotizacion { get; set; }
        [DataMember]
        public int IdDetalle { get; set; }
        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public int Escrituracion { get; set; }
        [DataMember]
        public decimal Reservacion { get; set; }
        [DataMember]
        public DateTime FechaEntrega { get; set; }
        [DataMember]
        public DateTime FechaEscrituracion { get; set; }
        [DataMember]
        public DateTime FechaReservacion { get; set; }
        [DataMember]
        public decimal MontoContrato { get; set; }
        [DataMember]
        public decimal PrecioMetroCuadrado { get; set; }
        [DataMember]
        public int MetrosTotales { get; set; }
        [DataMember]
        public DateTime FechaContrato { get; set; }
        [DataMember]
        public int Cetes { get; set; }
        [DataMember]
        public int Multiplicador { get; set; }
        [DataMember]
        public string Bodega { get; set; }
        [DataMember]
        public string Estacionamiento { get; set; }
        [DataMember]
        public InfoClientes cliente { get; set; }
        [DataMember]
        public EstatusDepartamentoEntity departamento { get; set; }

        [DataMember]
        public cotizacionDiferidoEntity precios { get; set; }

        [DataMember]
        public string Estatus { get; set; }


    }


    [DataContract]
    [Serializable]
    public class ContratoPrecioDetalle
    {
        [DataMember]
        public long Contrato { get; set; }
        [DataMember]
        public decimal PrecioInicial { get; set; }
        [DataMember]
        public decimal EngancheInicial { get; set; }
        [DataMember]
        public decimal EngancheDiferido { get; set; }
        [DataMember]
        public decimal EngancheTotal { get; set; }
        [DataMember]
        public decimal FinanciamientoEngancheDiferido { get; set; }
        [DataMember]
        public decimal EngancheDiferidoMasFinanciamiento { get; set; }
        [DataMember]
        public decimal LiquidacionEscrituracion { get; set; }
        [DataMember]
        public decimal PrecioTotal { get; set; }
        [DataMember]
        public decimal PrecioMetroCuadrado { get; set; }

    }
    public class contratoByclienteEntity
    {
        public long Id { get; set; }

        public int Clv_cotizacion { get; set; }

        public int IdDetalle { get; set; }

        public int IdCliente { get; set; }

        public int Escrituracion { get; set; }

        public decimal Reservacion { get; set; }

        public DateTime FechaEntrega { get; set; }

        public DateTime FechaEscrituracion { get; set; }

        public DateTime FechaReservacion { get; set; }

        public decimal MontoContrato { get; set; }

        public decimal PrecioMetroCuadrado { get; set; }

        public int MetrosTotales { get; set; }

        public DateTime FechaContrato { get; set; }

        public int Cetes { get; set; }

        public int Multiplicador { get; set; }

        public string Bodega { get; set; }

        public string Estacionamiento { get; set; }

        public int Clv_vendedor { get; set; }

        public string Nombre_cliente { get; set; }

        public string correo { get; set; }

        public string NumDepartamento { get; set; }

        public char Estatus { get; set; }

    }


}