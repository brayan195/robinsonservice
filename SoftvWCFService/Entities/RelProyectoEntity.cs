﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class RelAmenitieEntity

    {
        [DataMember]
        public int id { get; set; }
       
        [DataMember]
        public int clv_proyecto { get; set; }
        [DataMember]
        public int clv_amenitie { get; set; }
        [DataMember]
        public string observacion { get; set; }
        [DataMember]
        public string descripcion { get; set; }


    }
    public class RelServicioEntity

    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public int clv_proyecto { get; set; }
        [DataMember]
        public int clv_servicio { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public string nombre_Servicio { get; set; }


    }

    public class RelTorresEntity

    {
       

        [DataMember]
        public int clv_proyecto { get; set; }
        [DataMember]
        public string Torre { get; set; }

        [DataMember]
        public int Piso { get; set; }
        [DataMember]
        public int clv_Departamento { get; set; }
        
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string TipoDepartamento { get; set; }


        [DataMember]
        public string Estatus { get; set; }



    }











}