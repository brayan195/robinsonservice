﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class VistaEntity
    {
        [DataMember]
        public int clv_vista { get; set; }
        [DataMember]
        public string descripcion { get; set; }
    }
}