﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class AgendaEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public DateTime Fecha { get; set; }
        [DataMember]
        public string Fecha2 { get; set; }

        [DataMember]
        public DateTime FechaEjecucion { get; set; }
        [DataMember]
        public string FechaEjecucion2 { get; set; }
        [DataMember]
        public int Clv_cliente { get; set; }
        [DataMember]
        public int Clv_Vendedor { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public string Estatus { get; set; }
        [DataMember]
        public int IdClasificacionAgenda { get; set; }
        [DataMember]
        public string Observaciones { get; set; }

        [DataMember]
        public string nombre_cliente { get; set; }

        [DataMember]
        public string nombre_usuario { get; set; }
        [DataMember]
        public int accion { get; set; }

        [DataMember]
        public string ObservacionesConCliente { get; set; }

        [DataMember]
        public DateTime FechaAgendada { get; set; }

        [DataMember]
        public string FechaAgendada2 { get; set; }

        [DataMember]
        public string telefono_casa { get; set; }

        [DataMember]
        public string celular { get; set; }

        


    }
}