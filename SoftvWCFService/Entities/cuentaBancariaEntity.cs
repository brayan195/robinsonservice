﻿using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class cuentaBancariaEntity
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int banco { get; set; }
        [DataMember]
        public string cuenta { get; set; }
        [DataMember]
        public string CLABE { get; set; }
        [DataMember]
        public bool activo { get; set; }

        [DataMember]
        public string nombre { get; set; }
        
    }
}