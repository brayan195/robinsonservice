﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftvWCFService.Entities
{
    public class BodegaCajonEntity
    {
        public int Id { get; set; }

        public int IdDetalle { get; set; }

        public string Referencia { get; set; }

        public decimal x { get; set; }

        public decimal y { get; set; }

        public int IdLayout { get; set; }

        public int Tipo { get; set; }

        public string Nivel { get; set; }
    }
    public class PlanosBodega
    {
        public int Id_plano { get; set; }

        public string Nombre_Imagen { get; set; }

        public string Nivel { get; set; }

        public int clv_proyecto { get; set; }

        public int Tipo { get; set; }

    }

}