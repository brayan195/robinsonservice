﻿using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class conceptosDePagoEntity
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Clv_txt { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public bool Activo { get; set; }
        [DataMember]
        public bool CobroAutomatico { get; set; }
        [DataMember]
        public decimal Monto { get; set; }

        [DataMember]
        public int Id_real { get; set; }
        

    }
}