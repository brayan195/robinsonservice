﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;


namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class InfoCliente
    {
        [DataMember]
        public long? Contrato { get; set; }


        [DataMember]
        public string ContratoCompuesto { get; set; }


        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string password { get; set; }

        [DataMember]
        public string Serie { get; set; }

        [DataMember]
        public string token { get; set; }

        [DataMember]
        public bool  ? valid { get; set; }

        [DataMember]
        public string message { get; set; }

        [DataMember]
        public string nombre { get; set; }


        [DataMember]
        public int? clv_vista { get; set; }

        [DataMember]
        public string descripcion { get; set; }

        [DataMember]
        public
        bool? activo { get; set; }

        [DataMember]
        public bool? Resultado { get; set; }







    }

    public class InfoMobiliti
    {
       
        [DataMember]
        public string nombre { get; set; }


        [DataMember]
        public int? clv_vista { get; set; }

        [DataMember]
        public int? clv_vendedor { get; set; }

        [DataMember]
        public int? clv_usuario { get; set; }

        [DataMember]
        public int? clv_amenitie { get; set; }

        [DataMember]
        public int? clv_servicio { get; set; }


        [DataMember]
        public string descripcion { get; set; }

        [DataMember]
        public
        bool? activo { get; set; }

        [DataMember]
        public string domicilio { get; set; }
        [DataMember]
        public string observacion { get; set; }

    }

    public class InfoUser
    {

        [DataMember]
        public string nombre { get; set; }


        [DataMember]
        public int? clv_usuario { get; set; }   

        [DataMember]
        public string correo { get; set; }

        [DataMember]
        public  bool? activo  { get; set; }

        [DataMember]
        public string contrasenia { get; set; }

        [DataMember]
        public int ? tipoUsuario { get; set; }
        [DataMember]
        public int? tipo_vendedor { get; set; }

        [DataMember]
        public string token { get; set; }

        [DataMember]
        public string Public_Key { get; set; }

        [DataMember]
        public int? Cliente_Id { get; set; }




    }

    public class InfoDepto
    {

        [DataMember]
        public string nombre { get; set; }


        [DataMember]
        public int? clv_espacio_departamento { get; set; }

        [DataMember]
        public float? metros_cuadrados { get; set; }

        [DataMember]
        public  bool? activo { get; set; }

        [DataMember]
        public bool? suma { get; set; }

    }
    public class InfoDeptos
    {

        [DataMember]
        public string proyecto { get; set; }


        [DataMember]
        public int? clv_departamento { get; set; }

        [DataMember]
        public float metros_cuadrados_total { get; set; }

        [DataMember]
        public int? niveles { get; set; }

        [DataMember]
        public string vista { get; set; }

        [DataMember]
        public string espacio_departamento { get; set; }


        [DataMember]
        public int?  numero_recamaras { get; set; }

        [DataMember]
        public int? cajones_estacionamiento { get; set; }

        [DataMember]
        public float metros_cajones_est { get; set; }

        [DataMember]
        public float metros_bodega { get; set; }
        [DataMember]
        public float costo_departamento { get; set; }

        [DataMember]
        public bool? bodega_activa { get; set; }
        [DataMember]
        public string tipo_departamento { get; set; }

    }

    public class torre
    {
        [DataMember]
        public int? clv_torre { get; set; }      
        [DataMember]
        public int ? clv_proyecto { get; set; }
        [DataMember]
        public int ? pisos { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public bool activo { get; set; }
    }



    public class InfoComprador
    {
        [DataMember]
        public int? clv_comprador { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public string domicilio { get; set; }
    }

    public class Infomodelo
    {
        [DataMember]
        public int? clv_marca { get; set; }

        [DataMember]
        public int? clv_modelo { get; set; }


        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public int? Id { get; set; }



    }

    public class InfoPais
    {
      [DataMember]
        public int? clv_estado { get; set; }

        [DataMember]
        public int? clv_pais { get; set; }


        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public int? clv_colonia { get; set; }


        [DataMember]
        public int? clv_calle { get; set; }
        [DataMember]
        public int? clv_localidad { get; set; }
        [DataMember]
        public int? clv_municipio { get; set; }
        
        public String codigoArea { get; set; }
        public int? codigoPostal { get; set; }


    }
    public class InfoProyecto
    {
        [DataMember]
        public int? clv_proyecto { get; set; }
       
        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public int? numero_ext { get; set; }
        [DataMember]
        public float? costoMetroCuadrado { get; set; }
        [DataMember]
        public float? precioVentaActual { get; set; }
        [DataMember]
        public int? cantidad_torres { get; set; }
        [DataMember]
        public float? latitud { get; set; }
        [DataMember]
        public float? longitud { get; set; }
        [DataMember]
        public float? factorIncremento { get; set; }
        [DataMember]
        public float? cambioEtapaPorVenta { get; set; }
        [DataMember]
        public string direccion { get; set; }
        [DataMember]
        public int? numeroEtapas { get; set; }
        [DataMember]
        public int? clv_estado { get; set; }
        [DataMember]
        public int? clv_municipio { get; set; }
        [DataMember]
        public int? clv_localidad { get; set; }
        [DataMember]
        public int? clv_colonia { get; set; }
        [DataMember]
        public int? clv_amenitie { get; set; }
        [DataMember]
        public bool? activo { get; set; }

        [DataMember]
        public string estado { get; set; }


        [DataMember]
        public string municipio { get; set; }

        [DataMember]
        public string localidad { get; set; }


        [DataMember]
        public string amenitie { get; set; }

  
    }


    public class tipoUsuario
    {
        public int Id { get; set; }

        public int clv_modelo { get; set; }


        public int clv_marca { get; set; }

        public string Nombre { get; set; }

        public string nombre { get; set; }

        public bool? Activo { get; set; }
        public string MSG { get; set; }


        [DataMember]
        public List<Marcas> Marca { get; set; }
      

    }

    public class permisos
    {
        [DataMember]
        public int ? IdModulo { get; set; }

        
    }
    

    public class Modulo
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string componente { get; set; }
        [DataMember]
        public bool activo { get; set; }

        [DataMember]
        public int ? Orden { get; set; }

        [DataMember]
        public string Icono { get; set; }
        [DataMember]
        public bool Principal { get; set; }
        [DataMember]
        public int ? ModuloPadre { get; set; }
        [DataMember]
        public List<Modulo> SubModulos { get; set; }

        [DataMember]
        public bool? Add { get; set; }

        [DataMember]
        public bool? Edit { get; set; }

        [DataMember]
        public bool? Delete { get; set; }

        [DataMember]
        public bool? Access { get; set; }

        [DataMember]
        public int? Tipo { get; set; }


    }

    public class InfoD
    {

    public class municipios
    {
        [DataMember]
        public int ? clv_municipio { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public List<InfoPais> estados { get; set;}
    }

        [DataMember]
        public int? clv_estatusDepartamento
        { get; set; }

        [DataMember]
        public int? piso { get; set; }
        [DataMember]
        public string tipo_departamento { get; set; }
        [DataMember]
        public int? numero_departamento { get; set; }
        [DataMember]
        public string fecha_entrega { get; set; }
        [DataMember]
        public string estatus { get; set; }
        [DataMember]
        public string clv_vendedor { get; set; }
        [DataMember]
        public string clv_comprador { get; set; }

        [DataMember]
        public float? precio_lista_venta { get; set; }
        [DataMember]
        public float? precio_actual { get; set; }
        [DataMember]
        public float? precio_vendido { get; set; }

        [DataMember]
        public string fecha_apartado { get; set; }

        [DataMember]
        public float? importe_apartado { get; set; }

        [DataMember]
        public string fecha_venta { get; set; }
        [DataMember]
        public float? importe_enganche { get; set; }

        [DataMember]
        public float? porcentaje { get; set; }

        [DataMember]
        public int? numero_de_pago_enganche { get; set; }

        [DataMember]
        public string fecha_escrituracion { get; set; }


        [DataMember]
        public string notario { get; set; }


        [DataMember]
        public bool? activo { get; set; }

        [DataMember]
        public string clv_torre { get; set; }




    }



    public class municipios
    {
        [DataMember]
        public int? clv_municipio { get; set; }
        [DataMember]
        public int? clv_ciudad { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public List<InfoPais> estados { get; set; }
    }


    public class Marcas
    {
        [DataMember]
        public int clv_marca { get; set; }
        [DataMember]
        public int? clv_modelo { get; set; }
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public List<Infomodelo> modelos { get; set; }
        [DataMember]
        public int? clv_firmware { get; set; }

    }

    public class InfoClientes
    {
        [DataMember]
        public int id { get; set; }


        [DataMember]
        public int? clv_comprador { get; set; }

        [DataMember]
        public string nombre_cliente { get; set; }



        [DataMember]
        public string primer_apellido { get; set; }

        [DataMember]
        public string segundo_apellido { get; set; }

        [DataMember]
        public string primerA { get; set; }

        [DataMember]
        public string segundoA { get; set; }

        [DataMember]
        public string otronombre { get; set; }

        [DataMember]
        public int? clv_estado { get; set; }

        [DataMember]
        public bool tipocliente { get; set; }
        [DataMember]
        public int? clv_ciudad { get; set; }

        [DataMember]
        public int? clv_calle { get; set; }

        [DataMember]
        public int? clv_pais { get; set; }

        [DataMember]
        public int? clv_municipio { get; set; }

        [DataMember]
        public int? clv_localidad { get; set; }

        [DataMember]
        public int? clv_colonia { get; set; }
        [DataMember]
        public string numero { get; set; }
        [DataMember]
        public int? codigoPostal { get; set; }
        [DataMember]
        public string telefono { get; set; }

        [DataMember]
        public string nombreR { get; set; }

        [DataMember]
        public string rut { get; set; }

        [DataMember]
        public string Dominio { get; set; }

        [DataMember]
        public string password { get; set; }


        [DataMember]
        public string password1 { get; set; }
        [DataMember]
        public DateTime Fecha { get; set; }
        [DataMember]
        public string Fecha2 { get; set; }



        [DataMember]
        public string nit { get; set; }
        [DataMember]
        public string celular { get; set; }
        [DataMember]
        public string correo { get; set; }

        [DataMember]
        public string correo1 { get; set; }

        [DataMember]
        public bool? activo { get; set; }
        [DataMember]
        public string estado { get; set; }
        [DataMember]
        public string municipio { get; set; }
        [DataMember]
        public string localidad { get; set; }
        [DataMember]
        public string colonia { get; set; }
        [DataMember]
        public string calle { get; set; }
        [DataMember]
        public string numeroInterior { get; set; }
        [DataMember]
        public string telefono_casa { get; set; }
        [DataMember]
        public string Fechacreacion { get; set; }


        [DataMember]
        public string usuario { get; set; }


        [DataMember]
        public string NombreR { get; set; }
        [DataMember]
        public string nombrecontrato { get; set; }

        


        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public string Registro { get; set; }

        [DataMember]
        public string rfc { get; set; }

        [DataMember]
        public string curp { get; set; }

        [DataMember]
        public string folio { get; set; }

        [DataMember]
        public int Medios { get; set; }
        [DataMember]
        public string asignador { get; set; }

        [DataMember]
        public int medioContacto { get; set; }



        [DataMember]
        public string CorreoContactoAdministrativo { get; set; }

        [DataMember]
        public string telefonoCA { get; set; }



    }



    public class localidad
    {
        [DataMember]
        public int? clv_localidad { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public List<municipios> municipios { get; set; }
    }




}
