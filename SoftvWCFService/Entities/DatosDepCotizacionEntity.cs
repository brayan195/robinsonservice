﻿using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SoftvWCFService.Entities
{
    [DataContract]
    [Serializable]
    public class DatosDepCotizacionEntity
    {
        [DataMember]
        public string NumDepartamento { get; set; }
        [DataMember]
        public string Vista { get; set; }
        [DataMember]
        public string MetrosDepartamento { get; set; }
        [DataMember]
        public float Cajon { get; set; }
        [DataMember]
        public float Bodega { get; set; }
        [DataMember]
        public float SuperficieTotal { get; set; }
    }
}