﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class RelacionesProyectoController
    {
        
        public List<RelAmenitieEntity> GetrelProyectoAmenitie(int clv_proyecto)
        {
            dbHelper db = new dbHelper();
            List<RelAmenitieEntity> amenities = new List<RelAmenitieEntity>();
            try
            {
                db.agregarParametro("@clv_proyecto", SqlDbType.Int, clv_proyecto);
                SqlDataReader reader = db.consultaReader("GetrelProyectoAmenitie");
                amenities = db.MapDataToEntityCollection<RelAmenitieEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return amenities;
        }



        public int? DeleteAmenitieProyecto(int id)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
               
                db.agregarParametro("@id", SqlDbType.Int, id);
                db.consultaSinRetorno("EliminaAmenitie");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public List<RelServicioEntity> GetrelProyectoServicio(int clv_proyecto)
        {
            dbHelper db = new dbHelper();
            List<RelServicioEntity> amenities = new List<RelServicioEntity>();
            try
            {
                db.agregarParametro("@clv_proyecto", SqlDbType.Int, clv_proyecto);
                SqlDataReader reader = db.consultaReader("GetrelProyectoServicio");
                amenities = db.MapDataToEntityCollection<RelServicioEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return amenities;
        }

        public int? DeleteServicioProyecto(int id)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {

                db.agregarParametro("@id", SqlDbType.Int, id);
                db.consultaSinRetorno("EliminaServicio");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? DeleteTorreProyecto(int  clv_proyecto, string Torre)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {

                db.agregarParametro("@clv_proyecto", SqlDbType.Int, clv_proyecto);
                db.agregarParametro("@Torre", SqlDbType.VarChar, Torre);
                db.consultaSinRetorno("EliminaTorre");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public List<RelTorresEntity> GetTorresProyecto1(int clv_proyecto, string torre)
         {
            dbHelper db = new dbHelper();
            List<RelTorresEntity> torres = new List<RelTorresEntity>();
            try
            {
                db.agregarParametro("@clv_proyecto", SqlDbType.Int, clv_proyecto);
                db.agregarParametro("@torre", SqlDbType.VarChar, torre);
                SqlDataReader reader = db.consultaReader("GetTorresProyecto");
                torres = db.MapDataToEntityCollection<RelTorresEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return torres;
        }


    }




}