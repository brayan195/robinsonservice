﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class NivelesController
    {
        public List<NivelesEntity> GetNivel()
        {
            dbHelper db = new dbHelper();
            List<NivelesEntity> niveles = new List<NivelesEntity>();
            try
            {

                SqlDataReader reader = db.consultaReader("GetNivel");
                niveles = db.MapDataToEntityCollection<NivelesEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return niveles;
        }




        public List<relNivelesDepartamentoEntity> GetNivelbyDepartamento(long id_Departamento)
        {
            dbHelper db = new dbHelper();

            List<relNivelesDepartamentoEntity> n = new List<relNivelesDepartamentoEntity>();
            try
            {
                db.agregarParametro("@id_Departamento", SqlDbType.Int, id_Departamento );
                SqlDataReader reader = db.consultaReader("GetNivelByDepartamento");
                n = db.MapDataToEntityCollection<relNivelesDepartamentoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return n;
        }


        public PlanosBodegaEntity GetPlanosBodega(string Nivel, int clv_proyecto)
        {
            dbHelper db = new dbHelper();
            PlanosBodegaEntity Plano = new  PlanosBodegaEntity();
            try
            {
                db.agregarParametro("@Nivel", SqlDbType.VarChar, Nivel);
                db.agregarParametro("@clv_proyecto", SqlDbType.Int, clv_proyecto);
                SqlDataReader reader = db.consultaReader("GetPlanosBodega");
                Plano = db.MapDataToEntityCollection<PlanosBodegaEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return Plano;
        }



        public int? AddPlanosBodega (string Nombre_Imagen, string Nivel)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Nombre_Imagen", SqlDbType.VarChar, Nombre_Imagen);
                db.agregarParametro("@Nivel", SqlDbType.VarChar, Nivel);
                db.consultaSinRetorno("AddPlanosBodega");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }














    }

}