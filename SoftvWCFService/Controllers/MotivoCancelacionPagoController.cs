﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class MotivoCancelacionPagoController
    {
       
        public List<MotivosCancelacionFacturas> GetMotivoCacelacionPago()
        {
            dbHelper db = new dbHelper();
            List<MotivosCancelacionFacturas> tipos = new List<MotivosCancelacionFacturas>();
            try
            {
                     
                SqlDataReader reader = db.consultaReader("GetMotivoCancelacionPago");
                tipos = db.MapDataToEntityCollection<MotivosCancelacionFacturas>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return tipos;
        }


        public MotivosCancelacionFacturas GetMotivoCacelacionPagoById(int Id)
        {
            dbHelper db = new dbHelper();
            MotivosCancelacionFacturas tipos = new MotivosCancelacionFacturas();
            try
            {
                
                db.agregarParametro("@Id", SqlDbType.Int, Id);
                SqlDataReader reader = db.consultaReader("GetMotivoCancelacionPagoById");
                tipos = db.MapDataToEntityCollection<MotivosCancelacionFacturas>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return tipos;
        }







        public int? AddMotivoCancelacionPago(string Descripcion, bool? Activo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Descripcion", SqlDbType.VarChar, Descripcion);
                db.agregarParametro("@Activo", SqlDbType.Bit, Activo);
                db.consultaSinRetorno("AddMotivoCancelacionPago");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? UpdateMotivoCancelacionPago(int ? Id,string Descripcion, bool? Activo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Descripcion", SqlDbType.VarChar, Descripcion);
                db.agregarParametro("@Activo", SqlDbType.Bit, Activo);
                db.agregarParametro("@Id", SqlDbType.Int, Id);
                db.consultaSinRetorno("UpdateMotivoCancelacionPago");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

    }
}

