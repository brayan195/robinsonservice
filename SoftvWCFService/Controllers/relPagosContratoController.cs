﻿using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;
namespace SoftvWCFService.Controllers
{
    public class relPagosContratoController
    {

        public List<relPagosContratoEntity> GetRelacionPagosContratoById(int contrato)
        {
            dbHelper db = new dbHelper();
            List<relPagosContratoEntity> recepcion = new List<relPagosContratoEntity>();
            try
            {
                db.agregarParametro("@contrato", SqlDbType.Int, contrato);
                SqlDataReader reader = db.consultaReader("selectRelPagosContratoById");
                recepcion = db.MapDataToEntityCollection<relPagosContratoEntity>(reader).ToList();
                recepcion.ForEach(x => {
                    x.FechaPago2 = x.FechaPago.ToShortDateString();
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return recepcion;
        }

        public int? addRelacionPagosContrato(int clv_concepto, string descripcion, string fechaPago, decimal monto, int contrato, int clv_factura)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@clv_concepto", SqlDbType.Int, clv_concepto);
                db.agregarParametro("@descripcion", SqlDbType.VarChar, descripcion);
                db.agregarParametro("@fechaPago", SqlDbType.DateTime, fechaPago);
                db.agregarParametro("@monto", SqlDbType.Decimal, monto);
                db.agregarParametro("@contrato", SqlDbType.Int, contrato);
                db.agregarParametro("@clv_factura", SqlDbType.Int, clv_factura);
                db.consultaSinRetorno("insertRelPagosContrato");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

    }
}