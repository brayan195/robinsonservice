﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class CotizacionController
    {

        public CotizacionEntity GetCotizacionDiferido(
            decimal? EngancheInicial,
            decimal? EngancheDiferido,
            decimal? LiquidacionEscrituracion,
            String FechaEntrega,
            decimal? PrecioMetro,
            decimal? MetrosDepartamento
            )
        {

            CotizacionEntity cot = new CotizacionEntity();
            cot.pagos = new List<PagosDiferidosEntity>();
            cot.precios = new cotizacionDiferidoEntity();
            cot.precios = CotizaDiferidoPrecios(EngancheInicial, EngancheDiferido, LiquidacionEscrituracion, FechaEntrega, PrecioMetro,MetrosDepartamento);
            cot.pagos = GetCotizacionDiferidoPagos(EngancheInicial, EngancheDiferido, LiquidacionEscrituracion, FechaEntrega, PrecioMetro, MetrosDepartamento);
            return cot;
        }

        public cotizacionDiferidoEntity CotizaDiferidoPrecios(          
            decimal? EngancheInicial,
            decimal? EngancheDiferido,
            decimal? LiquidacionEscrituracion,
            String FechaEntrega,          
            decimal? PrecioMetro,
            decimal? MetrosDepartamento
            )
        {

            dbHelper db = new dbHelper();
            long clv_session = 0;
            cotizacionDiferidoEntity precios = new cotizacionDiferidoEntity();
            try
            {

                db.agregarParametro("@EngancheInicial", SqlDbType.Decimal, EngancheInicial);
                db.agregarParametro("@EngancheDiferido", SqlDbType.Decimal, EngancheDiferido);
                db.agregarParametro("@LiquidacionEscrituracion", SqlDbType.Decimal, LiquidacionEscrituracion);
                db.agregarParametro("@FechaEntrega", SqlDbType.DateTime, FechaEntrega);
                db.agregarParametro("@PrecioMetro", SqlDbType.Decimal, PrecioMetro);
                db.agregarParametro("@MetrosDepartamento", SqlDbType.Decimal, MetrosDepartamento);
                db.agregarParametro("@Clv_session", SqlDbType.BigInt,ParameterDirection.Output);
                db.consultaOutput("cotizaDiferido");
                clv_session = long.Parse(db.diccionarioOutput["@Clv_session"].ToString());                
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
             db = new dbHelper();
            try
            {

                db.agregarParametro("@clv_session", SqlDbType.BigInt, clv_session);
                SqlDataReader reader = db.consultaReader("ObtenDetalleCotizacion");              
                precios = db.MapDataToEntityCollection<cotizacionDiferidoEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }

            return precios;

        }

        public List<PagosDiferidosEntity> GetCotizacionDiferidoPagos(
        decimal? EngancheInicial,
        decimal? EngancheDiferido,
        decimal? LiquidacionEscrituracion,
        String FechaEntrega,
        decimal? PrecioMetro,
        decimal? MetrosDepartamento
        )
        {
            dbHelper db = new dbHelper();
            List<PagosDiferidosEntity> pagos = new List<PagosDiferidosEntity>();
            try
            {
                db.agregarParametro("@EngancheInicial", SqlDbType.Decimal, EngancheInicial);
                db.agregarParametro("@EngancheDiferido", SqlDbType.Decimal, EngancheDiferido);
                db.agregarParametro("@LiquidacionEscrituracion", SqlDbType.Decimal, LiquidacionEscrituracion);
                db.agregarParametro("@FechaEntrega", SqlDbType.DateTime, FechaEntrega);
                db.agregarParametro("@precioMetro", SqlDbType.Decimal, PrecioMetro);
                db.agregarParametro("@MetrosDepartamento", SqlDbType.Decimal, MetrosDepartamento);
                SqlDataReader reader = db.consultaReader("cotizaDiferidoPagos");
                pagos = db.MapDataToEntityCollection<PagosDiferidosEntity>(reader).ToList();
                pagos.ForEach(x => {
                    x.FechaPago2 = x.FechaPago.ToShortDateString();
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return pagos;
        }

        public CotizacionEntity GetCotizacionDescuento(            
            decimal ? EngancheInicial,
            decimal? EngancheDiferido,
            decimal? EngancheAdicional,
            decimal? LiquidacionEscrituracion ,
            Decimal ? PrecioMetro,
            Decimal ? MetrosDepartamento,
            String FechaEntrega,
            int? Op
            )
        {

            CotizacionEntity cot = new CotizacionEntity();
            cot.pagos = new List<PagosDiferidosEntity>();
            cot.precios = new cotizacionDiferidoEntity();
            cot.precios = GetCotizacionDescuentoPrecios(EngancheInicial, EngancheDiferido, EngancheAdicional, LiquidacionEscrituracion, FechaEntrega, PrecioMetro, MetrosDepartamento);
            cot.pagos = GetPagosDiferidoDescuento( EngancheInicial, EngancheDiferido, EngancheAdicional, LiquidacionEscrituracion, FechaEntrega, PrecioMetro, MetrosDepartamento, Op );
            return cot;
        }

        public cotizacionDiferidoEntity GetCotizacionDescuentoPrecios(
          decimal? EngancheInicial,
          decimal? EngancheDiferido,
          decimal? EngancheAdicional,
          decimal? LiquidacionEscrituracion,
          String FechaEntrega,
          Decimal? PrecioMetro,
          Decimal? MetrosDepartamento         
          )
        {
            dbHelper db = new dbHelper();
            cotizacionDiferidoEntity precios = new cotizacionDiferidoEntity();
            long clv_session = 0;
            try
            {
                db.agregarParametro("@EngancheInicial", SqlDbType.Decimal, EngancheInicial);
                db.agregarParametro("@EngancheDiferido", SqlDbType.Decimal, EngancheDiferido);
                db.agregarParametro("@EngancheAdicional", SqlDbType.Decimal, EngancheAdicional);
                db.agregarParametro("@LiquidacionEscrituracion", SqlDbType.Decimal, LiquidacionEscrituracion);
                db.agregarParametro("@precioMetro", SqlDbType.Decimal, PrecioMetro);
                db.agregarParametro("@FechaEntrega", SqlDbType.Date, FechaEntrega);
                db.agregarParametro("@MetrosDepartamento", SqlDbType.Decimal, MetrosDepartamento);
                db.agregarParametro("@Clv_session", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("CotizaDescuento");
                clv_session = long.Parse(db.diccionarioOutput["@Clv_session"].ToString());
                //SqlDataReader reader = db.consultaReader("CotizaDescuento");
              //  precios = db.MapDataToEntityCollection<cotizacionDiferidoEntity>(reader).ToList()[0];
                //reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
             db = new dbHelper();
            try
            {

                db.agregarParametro("@clv_session", SqlDbType.BigInt, clv_session);
                SqlDataReader reader = db.consultaReader("ObtenDetalleCotizacion");
                precios = db.MapDataToEntityCollection<cotizacionDiferidoEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }

            return precios;
        }


        public List<PagosDiferidosEntity> GetPagosDiferidoDescuento(
            decimal? EngancheInicial,
          decimal? EngancheDiferido,
          decimal? EngancheAdicional,
          decimal? LiquidacionEscrituracion,
          String FechaEntrega,
          Decimal? PrecioMetro,
          Decimal? MetrosDepartamento,
          int ? Op

            )
        {
            dbHelper db = new dbHelper();
            List<PagosDiferidosEntity> pagos = new List<PagosDiferidosEntity>();
            try
            {
                db.agregarParametro("@EngancheInicial", SqlDbType.Decimal, EngancheInicial);
                db.agregarParametro("@EngancheDiferido", SqlDbType.Decimal, EngancheDiferido);
                db.agregarParametro("@EngancheAdicional", SqlDbType.Decimal, EngancheAdicional);
                db.agregarParametro("@LiquidacionEscrituracion", SqlDbType.Decimal, LiquidacionEscrituracion);
                db.agregarParametro("@precioMetro", SqlDbType.Decimal, PrecioMetro);
                db.agregarParametro("@FechaEntrega", SqlDbType.Date, FechaEntrega);
                db.agregarParametro("@MetrosDepartamento", SqlDbType.Decimal, MetrosDepartamento);
                db.agregarParametro("@Op", SqlDbType.Int, Op);


                SqlDataReader reader = db.consultaReader("cotizaDescuentoPagos");
                pagos = db.MapDataToEntityCollection<PagosDiferidosEntity>(reader).ToList();
                pagos.ForEach(x => {
                    x.FechaPago2 = x.FechaPago.ToShortDateString();
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }

            return pagos;
        }

        public CotizacionEntity GetCotizacionById(int ? clv_cotizacion)
        {

            dbHelper db = new dbHelper();
            CotizacionEntity cotizacion = new CotizacionEntity();
            try
            {
                SoftvWCFService soft = new SoftvWCFService();
                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, clv_cotizacion);               
                SqlDataReader reader = db.consultaReader("select_cotizacion_clv");
                cotizacion = db.MapDataToEntityCollection<CotizacionEntity>(reader).ToList()[0];               
                cotizacion.FechaEntrega2 = cotizacion.FechaEntrega.ToShortDateString();
                cotizacion.Fecha2 = cotizacion.Fecha.ToShortDateString();
                cotizacion.cliente = soft.GetClienteById(cotizacion.Clv_cliente);
                cotizacion.departamento = soft.GetEstatusDepartamento(cotizacion.IdDetalle);
                cotizacion.FechaExpiracion2 = cotizacion.FechaExpiracion.ToShortDateString();
                
                if(cotizacion.EngancheInicial == 30 || cotizacion.EngancheAdicional > 0)
                {
                    cotizacion.precios = GetCotizacionDescuentoPrecios(cotizacion.EngancheInicial, cotizacion.EngancheDiferido, cotizacion.EngancheAdicional, cotizacion.Escrituras, cotizacion.FechaEntrega2 ,cotizacion.PrecioMetro, cotizacion.MetrosDepartamento);

                   if (cotizacion.EngancheDiferido > 0)
                   {
                        
                       cotizacion.pagos = GetPagosDiferidoDescuento(cotizacion.EngancheInicial, cotizacion.EngancheDiferido, cotizacion.EngancheAdicional, cotizacion.Escrituras, cotizacion.FechaEntrega2, cotizacion.PrecioMetro, cotizacion.MetrosDepartamento, cotizacion.Op);
                   }

                   
                }
                else
                {
                    cotizacion.precios = CotizaDiferidoPrecios(cotizacion.EngancheInicial, cotizacion.EngancheDiferido, cotizacion.Escrituras, cotizacion.FechaEntrega2, cotizacion.PrecioMetro, cotizacion.MetrosDepartamento);
                    if (cotizacion.EngancheDiferido > 0)
                    {
                        cotizacion.pagos = GetCotizacionDiferidoPagos(cotizacion.EngancheInicial, cotizacion.EngancheDiferido, cotizacion.Escrituras, cotizacion.FechaEntrega2, cotizacion.PrecioMetro, cotizacion.MetrosDepartamento);
                    }

                }

              

                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return cotizacion;

        }



        public int? AddCotizacion(
            int? Clv_cliente, 
            int? Clv_vendedor, 
             decimal? Cetes,
             int? Multiplicador, 
             decimal ? Reservacion, 
             decimal? Escrituracion, 
             int? IdDetalle,
            Double? EngancheInicial,
            Double? EngancheAdicional,
            Double? EngancheDiferido,
            Double? Escrituras,
            int ? MesesDiferidos,
            int ? Op
            )
        {


            dbHelper db = new dbHelper();
            int clv_cotizacion = 0;
            try
            {
                db.agregarParametro("@Clv_cliente", SqlDbType.Int, Clv_cliente);
                db.agregarParametro("@Clv_vendedor", SqlDbType.Int, Clv_vendedor);
                db.agregarParametro("@Cetes", SqlDbType.Decimal, Cetes);
                db.agregarParametro("@Multiplicador", SqlDbType.Int, Multiplicador);
                db.agregarParametro("@Reservacion", SqlDbType.Decimal, Reservacion);
                db.agregarParametro("@Escrituracion", SqlDbType.Decimal, Escrituracion);
                db.agregarParametro("@IdDetalle", SqlDbType.Int, IdDetalle);
                db.agregarParametro("@EngancheInicial", SqlDbType.Float, EngancheInicial);
                db.agregarParametro("@EngancheDiferido", SqlDbType.Float, EngancheDiferido);
                db.agregarParametro("@EngancheAdicional", SqlDbType.Float, EngancheAdicional);
                db.agregarParametro("@Escrituras", SqlDbType.Float, Escrituras);         
                db.agregarParametro("@Op", SqlDbType.Float, Op);

                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("AddCotizacion");

                clv_cotizacion = int.Parse(db.diccionarioOutput["@clv_cotizacion"].ToString());
               
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return clv_cotizacion;


        }



        public int? UpdateCotizacion(
            int ? clv_cotizacion, 
            decimal? Reservacion, 
            decimal? Escrituracion, 
            int? EngancheInicial,
            int? EngancheDiferido,
            decimal ? Escrituras,
            int ? MesesDiferidos
            )
        {


            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, clv_cotizacion);              
                db.agregarParametro("@Reservacion", SqlDbType.Decimal, Reservacion);
                db.agregarParametro("@Escrituracion", SqlDbType.Decimal, Escrituracion);
                db.agregarParametro("@EngancheInicial", SqlDbType.Int, EngancheInicial);
                db.agregarParametro("@EngancheDiferido", SqlDbType.Int, EngancheDiferido);
                db.agregarParametro("@Escrituras", SqlDbType.Decimal, Escrituras);
                db.agregarParametro("@MesesDiferidos", SqlDbType.Int, MesesDiferidos);                
                db.consultaSinRetorno("UpdateCotizacion");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return clv_cotizacion;


        }

        public int? cancelarCotizacion(int id, string Estatus)
        {


            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.Int, id);
                db.agregarParametro("@Estatus", SqlDbType.VarChar, Estatus);
                db.consultaSinRetorno("Cancelar_Cotizacion");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;


        }

        public int? UpdateFechaExpiracion(int id, string fecha)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.Int, id);
                db.agregarParametro("@fecha", SqlDbType.DateTime, fecha);
                db.consultaSinRetorno("updateFechaExpiracion");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public List<CotizacionEntity> GetCotizacionByCliente(int? Clv_cliente)
        {

            dbHelper db = new dbHelper();
            List<CotizacionEntity> cotizacion = new List<CotizacionEntity>();
            try
            {
                db.agregarParametro("@Clv_cliente", SqlDbType.Int, Clv_cliente);
                SqlDataReader reader = db.consultaReader("select_cotizacionByCliente");
                cotizacion = db.MapDataToEntityCollection<CotizacionEntity>(reader).ToList();
                cotizacion.ForEach(cotiza =>
                {
                    cotiza.FechaEntrega2 = cotiza.FechaEntrega.ToShortDateString();
                    cotiza.Fecha2 = cotiza.Fecha.ToShortDateString();
                    cotiza.FechaExpiracion2 = cotiza.FechaExpiracion.ToShortDateString();
                });
               
          

                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return cotizacion;

        }
 
        public int? AceptaPropuesta(int? clv_cotizacion)
        {
            dbHelper db = new dbHelper();
            try
            {
              
                db.agregarParametro("@clv_cotizacion", SqlDbType.Decimal, clv_cotizacion);
                SqlDataReader reader = db.consultaReader("aceptaProupuesta");
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }

            try
            {

                CotizacionEntity clientes = GetCotizacionById(clv_cotizacion);

                {
                    SoftvWCFService soft = new SoftvWCFService();
                    InfoClientes solicitudobjc = soft.GetClienteById(clientes.Clv_cliente);

                    try {
                        clientes.pagos.ForEach(s => {
                        s.Monto.ToString() ;
                    });
                    }
                  
                    catch { }
           


                     //   SendEmail(
                           
                     //    clientes.Clv_cliente,
                     //    //solicitudobjc.nombre.ToString(),
                     //    //solicitudobjc.otro_nombre.ToString(),
                     //    solicitudobjc.primer_apellido.ToString(),
                     //    solicitudobjc.segundo_apellido.ToString(),
                     //    clientes.precios.EngancheInicial.ToString(),
                     //    clientes.FechaEntrega2.ToString(),
                     //    clientes.precios.PrecioTotal.ToString()
                       
                          
                     //); 

                   
                }
        }
             
            catch (Exception ex)
            {

            }

            return 0;
        }


        public void SendEmail( int? Clv_cliente, string reemplazanombre, string remplazaotronombre, string remplazaprimerapellido, string remplazasegundoapellido,
            string remplazaImporteInicial, string remplazafecha , string remplazaimportetotal  )
        {
            SoftvWCFService us = new SoftvWCFService();
            InfoClientes user = us.GetClienteById((Clv_cliente));

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential("ohlive.noreply@gmail.com", "0601x-2L");

            string output = null;
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(user.correo));
            email.From = new MailAddress("ohlive.noreply@gmail.com");
            email.Subject = "Aceptacion de propuesta";

            email.Body = emailBody()
            .Replace("reemplazanombre", reemplazanombre)
            .Replace("remplazaotronombre", remplazaotronombre)
            .Replace("remplazaprimerapellido", remplazaprimerapellido)
            .Replace("remplazasegundoapellido", remplazasegundoapellido)
            .Replace("remplazaImporteInicial", remplazaImporteInicial)
            .Replace("remplazafecha", remplazafecha)
            .Replace("remplazaimportetotal", remplazaimportetotal);
            //.Replace("remplazaMonto", remplazaMonto);

            
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            try
            {
                smtp.Send(email);
                email.Dispose();
                output = "Correo electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
        }
        public static string emailBody()
        {
            string Body = "";
            try
            {

                Body = System.IO.File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplate/AceptaPropuesta.htm");
            }
            catch (Exception e)
            {

            }


            return Body;
        }


        public List<cotizacionPago> cotizacionPago()
        {
            dbHelper db = new dbHelper();
            List<cotizacionPago> cot = new List<cotizacionPago>();
            try
            {

                SqlDataReader reader = db.consultaReader("GetCotizacionPago");
                cot = db.MapDataToEntityCollection<cotizacionPago>(reader).ToList();
                cot.ForEach(x => {
                    x.Fecha2 = x.Fecha.ToShortDateString();
                    x.FechaExpiracion2 = x.FechaExpiracion.ToShortDateString();
                  
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return cot;
        }


        public List<cotizacionPago> SearchCotizacionByCliente(int Id)
        {
            dbHelper db = new dbHelper();
            List<cotizacionPago> cot = new List<cotizacionPago>();
            try
            {
                db.agregarParametro("@Id", SqlDbType.Int, Id);
                SqlDataReader reader = db.consultaReader("SearchCotizacioByCliente");
                cot = db.MapDataToEntityCollection<cotizacionPago>(reader).ToList();
                cot.ForEach(x => {
                    x.Fecha2 = x.Fecha.ToShortDateString();
                    x.FechaExpiracion2 = x.FechaExpiracion.ToShortDateString();

                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return cot;
        }

        //ActivarCotizacion


        public int? ActivarCotizacion(int? Id, string FechaInicio)
        {


            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Id", SqlDbType.Int, Id);
                db.agregarParametro("@FechaInicio", SqlDbType.Date, FechaInicio);
                db.consultaSinRetorno("ActivarCotizacion");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return 0;


        }





    }
}