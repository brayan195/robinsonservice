﻿using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;
namespace SoftvWCFService.Controllers
{
    public class relUsuarioProyectoController
    {

        public int? AddRelUsuarioProyecto(int idUsuario, int idProyecto)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@idUsuario", SqlDbType.Int, idUsuario);
                db.agregarParametro("@idProyecto", SqlDbType.Int, idProyecto);
                db.consultaSinRetorno("insert_relUsuarioProyecto");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public List<relUsuarioProyectoEntity> GetRelUsuarioProyectoDisponibles(int idUsuario)
        {
            dbHelper db = new dbHelper();
            List<relUsuarioProyectoEntity> rel = new List<relUsuarioProyectoEntity>();
            try
            {
                db.agregarParametro("@idUsuario", SqlDbType.Int, idUsuario);
                SqlDataReader reader = db.consultaReader("select_relUsuarioProyecto_disponible");
                rel = db.MapDataToEntityCollection<relUsuarioProyectoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return rel;
        }

        public List<relUsuarioProyectoEntity> GetRelUsuarioProyectoById(int? id)
        {
            dbHelper db = new dbHelper();
            List<relUsuarioProyectoEntity> rel = new List<relUsuarioProyectoEntity>();

            try
            {
                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("select_relUsuarioProyectoById");
                rel = db.MapDataToEntityCollection<relUsuarioProyectoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return rel;
        }

        public int? DeleteRelUsuarioProyecto(int id)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.Int, id);
                db.consultaSinRetorno("delete_relUsuarioProyecto");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public List<relUsuarioProyectoEntity> GetAllProyectos()
        {
            dbHelper db = new dbHelper();
            List<relUsuarioProyectoEntity> rel = new List<relUsuarioProyectoEntity>();
            try
            {
                SqlDataReader reader = db.consultaReader("select_AllProyectos");
                rel = db.MapDataToEntityCollection<relUsuarioProyectoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return rel;
        }
    }
}