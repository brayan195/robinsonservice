﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{

public class ClienteController
    {




            public List<ClientesEntity> Searchclientes(int? Op, string nombre, string otro_nombre, string primer_apellido, string segundo_apellido)
        {
            dbHelper db = new dbHelper();
            List<ClientesEntity> cliente = new List<ClientesEntity>();
            try
            {
                db.agregarParametro("@Op", SqlDbType.Int, Op);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@otro_nombre", SqlDbType.VarChar, otro_nombre);
                db.agregarParametro("@primer_apellido", SqlDbType.VarChar, primer_apellido);
                db.agregarParametro("@segundo_apellido", SqlDbType.VarChar, segundo_apellido);
                SqlDataReader reader = db.consultaReader("Searchclientes");
                cliente = db.MapDataToEntityCollection<ClientesEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return cliente;
        }










        public List<ClientesEntity> GetClientes(int ? clv_usuario, int ?  clv_estatus, string nombre, string celular, string pais)
    {
        dbHelper db = new dbHelper();
        List<ClientesEntity> cliente = new List<ClientesEntity>();
        try
        {
                //db.agregarParametro("@clv_usuario", SqlDbType.Int, clv_usuario);
                //db.agregarParametro("@estatus", SqlDbType.Int, clv_estatus);
                db.agregarParametro("@celular", SqlDbType.VarChar, celular);
                db.agregarParametro("@Nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@Pais", SqlDbType.VarChar, pais);

                SqlDataReader reader = db.consultaReader("GetClientes");
                cliente = db.MapDataToEntityCollection <ClientesEntity>(reader).ToList();
            reader.Close();
            db.conexion.Close();
        }
        catch (Exception ex)
        {
            db.conexion.Close();
            db.conexion.Dispose();
        }

        return cliente;
    }

    public List<ClientesEntity> GetClientesWEB()
    {
        dbHelper db = new dbHelper();
        List<ClientesEntity> cliente = new List<ClientesEntity>();
        try
        {
            SqlDataReader reader = db.consultaReader("selectCompradorWEB");
            cliente = db.MapDataToEntityCollection<ClientesEntity>(reader).ToList();
            reader.Close();
            db.conexion.Close();
        }
        catch (Exception ex)
        {
            db.conexion.Close();
            db.conexion.Dispose();
        }

        return cliente;
    }

    public int? AddClientesWEB(int? id, int clv_vendedor, int clv_usuario)
    {
        dbHelper db = new dbHelper();
        int result = 0;
        try
        {
            db.agregarParametro("@id", SqlDbType.Int, id);
            db.agregarParametro("@id_vendedor", SqlDbType.Int, clv_vendedor);
            db.agregarParametro("@clv_usuario", SqlDbType.Int, clv_usuario);
            db.consultaSinRetorno("addVendedorUsuarioWEB");
            db.conexion.Close();
        }
        catch (Exception ex)
        {
            db.conexion.Close();
            db.conexion.Dispose();
            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
        }
        return result;
    }

    public int? AsignacionAutomatica()
    {
        dbHelper db = new dbHelper();
        int result = 0;
        try
        {
            db.consultaSinRetorno("AsignacionDeClientesWEB");
            db.conexion.Close();
        }
        catch (Exception ex)
        {
            db.conexion.Close();
            db.conexion.Dispose();
            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
        }
        return result;
    }


        public List<Estatus_Cliente> GetEstatusCliente()
        {
            dbHelper db = new dbHelper();
            List<Estatus_Cliente> cliente = new List<Estatus_Cliente>();
            try
            {
                SqlDataReader reader = db.consultaReader("GetEstatusCliente");
                cliente = db.MapDataToEntityCollection<Estatus_Cliente>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return cliente;
        }









        





    }
}