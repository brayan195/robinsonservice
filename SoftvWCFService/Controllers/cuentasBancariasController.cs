﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class cuentasBancariasController
    {
        public List<cuentaBancariaEntity> GetCuentasBancarias()
        {
            dbHelper db = new dbHelper();
            List<cuentaBancariaEntity> cuentas = new List<cuentaBancariaEntity>();
            try
            {

                SqlDataReader reader = db.consultaReader("select_cuentasBancarias");
                cuentas = db.MapDataToEntityCollection<cuentaBancariaEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return cuentas;
        }

        public int? AddCuentaBancaria(int banco, string cuenta, string CLABE, bool? activo)
        {
            int result = 0;
            dbHelper db = new dbHelper();
          
                try
                {                    
                    db.agregarParametro("@banco", SqlDbType.VarChar, banco);
                    db.agregarParametro("@cuenta", SqlDbType.VarChar, cuenta);
                    db.agregarParametro("@CLABE", SqlDbType.VarChar, CLABE);
                    db.agregarParametro("@activo", SqlDbType.Bit, activo);
                    db.consultaSinRetorno("insert_cuentaBancaria");
                    result = 1;
                    db.conexion.Close();
                }
                catch (Exception ex)
                {
                    db.conexion.Close();
                    db.conexion.Dispose();                
                }           

            return result;
        }

        public cuentaBancariaEntity getCuentaBancariaById(int? Id)
        {
            dbHelper db = new dbHelper();
            cuentaBancariaEntity cuenta = new cuentaBancariaEntity();

            try
            {                
                db.agregarParametro("@Id", SqlDbType.Int, Id);
                SqlDataReader reader = db.consultaReader("select_cuentaBancaria_ById");
                cuenta = db.MapDataToEntityCollection<cuentaBancariaEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();                
            }
            return cuenta;
        }

        public int? updateCuentaBancaria(int? id, int banco, string cuenta, string CLABE, bool? activo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.VarChar, id);
                db.agregarParametro("@banco", SqlDbType.Int, banco);
                db.agregarParametro("@cuenta", SqlDbType.VarChar, cuenta);
                db.agregarParametro("@CLABE", SqlDbType.VarChar, CLABE);
                db.agregarParametro("@activo", SqlDbType.Bit, activo);
                db.consultaSinRetorno("update_cuentaBancaria");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            return result;
        }


        public List<cuentaBancariaEntity> getCuentasBancariasByBanco(int? idBanco)
        {
            dbHelper db = new dbHelper();
            List<cuentaBancariaEntity> cuenta = new List<cuentaBancariaEntity>();

            try
            {
                db.agregarParametro("@idBanco", SqlDbType.Int, idBanco);
                SqlDataReader reader = db.consultaReader("select_cuentasBancarias_ByBanco");
                cuenta = db.MapDataToEntityCollection<cuentaBancariaEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            return cuenta;
        }

    }
}