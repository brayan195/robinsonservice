﻿using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class relUsuarioDepartamentoController
    {

        public int ? AddRelacion(List<int> departamentos, int ? IdUsuario)
        {
            int result = 0;
            dbHelper db = new dbHelper();
            departamentos.ForEach(x=> {
                try
                {
                    db.agregarParametro("@IdDepartamento", SqlDbType.Int, x);
                    db.agregarParametro("@IdUsuario", SqlDbType.Int, IdUsuario);
                    db.consultaSinRetorno("insert_relUsuarioVendedor");
                    result = 1;
                    db.conexion.Close();
                }
                catch (Exception ex)
                {
                    db.conexion.Close();
                    db.conexion.Dispose();
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            });
            
            return result;
        }


        public List<relUsuarioDepartamentoEntity> GetRelacion(int? IdUsuario)
        {

            int result = 0;
            dbHelper db = new dbHelper();
            List<relUsuarioDepartamentoEntity> relaciones = new List<relUsuarioDepartamentoEntity>();
            try
            {
                db.agregarParametro("@IdUsuario", SqlDbType.Int, IdUsuario);
                SqlDataReader reader = db.consultaReader("select_relUsuarioVendedor");
                relaciones = db.MapDataToEntityCollection<relUsuarioDepartamentoEntity>(reader).ToList();
                SoftvWCFService sc = new SoftvWCFService();
                relaciones.ForEach(s=> {
                    s.departamento = sc.GetEstatusDepartamento(s.idDepartamento);

                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }


            return relaciones;
        }


        public int? DeleteRelacion(int? Id)
        {
            int result = 0;
            dbHelper db = new dbHelper();
           
                try
                {
                    db.agregarParametro("@Id", SqlDbType.Int, Id);                   
                    db.consultaSinRetorno("delete_relUsuarioVendedor");
                    result = 1;
                    db.conexion.Close();
                }
                catch (Exception ex)
                {
                    db.conexion.Close();
                    db.conexion.Dispose();
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
           

            return result;
        }

    }
}