﻿using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class TipoVendedorController
    {
       
        public List<TipoVendedorEntity> GetTipoVendedor()
        {
            dbHelper db = new dbHelper();
            List<TipoVendedorEntity> tipos = new List<TipoVendedorEntity>();
            try
            {
                     
                SqlDataReader reader = db.consultaReader("select_tipoVendedor");
                tipos = db.MapDataToEntityCollection<TipoVendedorEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return tipos;
        }


        public int? AddTipoVendedor(string Descripcion, bool? Activo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Descripcion", SqlDbType.VarChar, Descripcion);
                db.agregarParametro("@Activo", SqlDbType.Bit, Activo);
                db.consultaSinRetorno("AddTipoVendedor");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? UpdateTipoVendedor(int ? Id,string Descripcion, bool? Activo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Descripcion", SqlDbType.VarChar, Descripcion);
                db.agregarParametro("@Activo", SqlDbType.Bit, Activo);
                db.agregarParametro("@Id", SqlDbType.Int, Id);
                db.consultaSinRetorno("UpdateTipoVendedor");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

    }
}

