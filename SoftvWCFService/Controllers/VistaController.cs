﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class VistaController
    {

        public List<VistaEntity> GetVistaByProyecto(int? clv_vendedor)
        {
            List<VistaEntity> vistas = new List<VistaEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@clv_vendedor", SqlDbType.BigInt, clv_vendedor);              
                SqlDataReader reader = db.consultaReader("vistasByProyecto");
                vistas = db.MapDataToEntityCollection<VistaEntity>(reader).ToList();                           
                reader.Close();

                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return vistas;


        }

    }
}