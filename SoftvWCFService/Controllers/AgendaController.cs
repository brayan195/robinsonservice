﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{

    
    public class AgendaController
    {
        
     
        public List<AgendaEntity> GetAgenda(int idUsuario)
        {
            dbHelper db = new dbHelper();
            List<AgendaEntity> agendas = new List<AgendaEntity>();
            try
            {
                db.agregarParametro("@idUsuario", SqlDbType.Int, idUsuario);
                SqlDataReader reader = db.consultaReader("select_agenda");
                agendas = db.MapDataToEntityCollection<AgendaEntity>(reader).ToList();
                agendas.ForEach(x => {
                    x.Fecha2 = x.Fecha.ToShortDateString();
                    x.FechaEjecucion2 = x.FechaEjecucion.ToShortDateString();
                    x.FechaAgendada2 = x.FechaAgendada.ToShortDateString();
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
          

            return agendas;
        }

        public int? AddAgenda(string Tipo, string Fecha, int? Clv_vendedor, string Observaciones, int? Clv_cliente, int? accion, string ObservacionesConCliente)
        {
            
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Clv_cliente", SqlDbType.Int, Clv_cliente);
                db.agregarParametro("@Clv_Vendedor", SqlDbType.Int, Clv_vendedor);
                db.agregarParametro("@Tipo", SqlDbType.VarChar, Tipo);
                db.agregarParametro("@Observaciones", SqlDbType.VarChar, Observaciones);
                db.agregarParametro("@Fecha", SqlDbType.Date, Fecha);
                db.agregarParametro("@accion", SqlDbType.Int, accion);
                db.agregarParametro("@ObservacionesConCliente", SqlDbType.VarChar, ObservacionesConCliente);
                db.consultaSinRetorno("AddAgenda");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            try
            {
                SoftvWCFService soft = new SoftvWCFService();

                InfoClientes solicitudobjc = soft.GetClienteById(Clv_cliente);
                  //  SendEmail(
                   
                  //   solicitudobjc.nombre_cliente.ToString(),
                  //   //solicitudobjc.otro_nombre.ToString(),
                  //   solicitudobjc.primer_apellido.ToString(),
                  //   solicitudobjc.segundo_apellido.ToString(),
                  //   solicitudobjc.telefono.ToString(),
                  //   solicitudobjc.telefono_casa.ToString(),
                  //   solicitudobjc.celular.ToString(),
                  //   solicitudobjc.correo.ToString(),
                  //   Clv_vendedor
                  //);

            }
            catch (Exception ex)
           {

            }

            return Clv_vendedor;
        }
        public void SendEmail(string reemplazanombre, string remplazaotronombre, string remplazaprimerapellido, string remplazasegundoapellido,
         string reemplazatelefonooficina, string reemplazatelefonocasa, string reemplazacelular, string reemplazacorreo, int? Clv_vendedor)
        {
            SoftvWCFService soft = new SoftvWCFService();
            UsuarioEntity user = soft.GetUsuariosC((int.Parse(Clv_vendedor.ToString())));

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential("ohlive.noreply@gmail.com", "0601x-2L");

            string output = null;
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(user.correo));
            email.From = new MailAddress("ohlive.noreply@gmail.com");
            email.Subject = "Nuevo Cliente Agendado";

            email.Body = emailBody()
            .Replace("reemplazanombre", reemplazanombre)
            .Replace("remplazaotronombre", remplazaotronombre)
            .Replace("remplazaprimerapellido", remplazaprimerapellido)
            .Replace("remplazasegundoapellido", remplazasegundoapellido)
            .Replace("reemplazatelefonooficina", reemplazatelefonooficina)
            .Replace("reemplazatelefonocasa", reemplazatelefonocasa)
            .Replace("reemplazacelular", reemplazacelular)
            .Replace("reemplazacorreo", reemplazacorreo);

            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            try
            {
                smtp.Send(email);
                email.Dispose();
                output = "Correo electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
        }
        public static string emailBody()
        {
            string Body = "";
            try
            {

                Body = System.IO.File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplate/emailAgenda.htm");
            }
            catch (Exception e)
            {

            }


            return Body;
        }


        public int? AddMotivoClienteDescartado(string IdMotivo, string Observaciones, string Fecha, int? IdVendedor, int ? IdCliente)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@IdMotivo", SqlDbType.Int, IdMotivo);
                db.agregarParametro("@Observaciones", SqlDbType.VarChar, Observaciones);                
                db.agregarParametro("@IdVendedor", SqlDbType.Int, IdVendedor);
                db.agregarParametro("@IdCliente", SqlDbType.Int, IdCliente);
                db.consultaSinRetorno("AddMotivoClienteDescartado");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? UpdateAgenda(int id, string Tipo, string Fecha, int? Clv_vendedor, string Observaciones, int? Clv_cliente, int accion, string ObservacionesConCliente)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.Int, id);
                db.agregarParametro("@Clv_cliente", SqlDbType.Int, Clv_cliente);
                db.agregarParametro("@Clv_Vendedor", SqlDbType.Int, Clv_vendedor);
                db.agregarParametro("@Tipo", SqlDbType.VarChar, Tipo);
                db.agregarParametro("@Observaciones", SqlDbType.VarChar, Observaciones);
                db.agregarParametro("@Fecha", SqlDbType.Date, Fecha);
                db.agregarParametro("@accion", SqlDbType.Int, accion);
                db.agregarParametro("@ObservacionesConCliente", SqlDbType.VarChar, ObservacionesConCliente);
                db.consultaSinRetorno("updateAgenda");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }        

        public AgendaEntity GetAgendaById(int id)
        {
            dbHelper db = new dbHelper();
            AgendaEntity agenda = new AgendaEntity();
            try
            {

                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("select_agendaById");
                agenda = db.MapDataToEntityCollection<AgendaEntity>(reader).ToList()[0];
                agenda.Fecha2 = agenda.Fecha.ToShortDateString();
                agenda.FechaEjecucion2 = agenda.FechaEjecucion.ToShortDateString();
                agenda.FechaAgendada2 = agenda.FechaAgendada.ToShortDateString();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return agenda;
        }

        public int? CancelarAgenda(int id)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {

                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("cancelar_agenda");                
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;
        }       


        public List<AgendaEntity> GetAgendasByUsuario(int? clv_usuario, String Fecha1, String Fecha2, int? folio, string tipo, string status)
        {
            dbHelper db = new dbHelper();
            List<AgendaEntity> agendas = new List<AgendaEntity>();
            try
            {

                db.agregarParametro("@clv_usuario", SqlDbType.Int, clv_usuario);
                db.agregarParametro("@Fecha1", SqlDbType.VarChar, Fecha1);
                db.agregarParametro("@Fecha2", SqlDbType.VarChar, Fecha2);
                db.agregarParametro("@folio", SqlDbType.Int, folio);
                db.agregarParametro("@tipo", SqlDbType.VarChar, tipo);
                db.agregarParametro("@status", SqlDbType.VarChar, status);
                SqlDataReader reader = db.consultaReader("select_agendaByUsuario");
                agendas = db.MapDataToEntityCollection<AgendaEntity>(reader).ToList();
                agendas.ForEach(x =>
                {
                    x.Fecha2 = x.Fecha.ToShortDateString();
                    x.FechaEjecucion2 = x.FechaEjecucion.ToShortDateString();
                    x.FechaAgendada2 = x.FechaAgendada.ToShortDateString(); 
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return agendas;
        }

        public List<AgendaEntity> GetHistorialObs(int clv_usuario)
        {
            dbHelper db = new dbHelper();
            List<AgendaEntity> agendas = new List<AgendaEntity>();
            try
            {

                db.agregarParametro("@clv_cliente", SqlDbType.Int, clv_usuario);
                SqlDataReader reader = db.consultaReader("select_historialObs");
                agendas = db.MapDataToEntityCollection<AgendaEntity>(reader).ToList();
                agendas.ForEach(x =>
                {
                    x.Fecha2 = x.Fecha.ToShortDateString();
                    x.FechaEjecucion2 = x.FechaEjecucion.ToShortDateString();
                    x.FechaAgendada2 = x.FechaAgendada.ToShortDateString();
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return agendas;
        }


        public List<AgendaEntity> GetAgendasByCliente(int clv_cliente, String Fecha1, String Fecha2)
        {
            dbHelper db = new dbHelper();
            List<AgendaEntity> agendas = new List<AgendaEntity>();
            try
            {

                db.agregarParametro("@clv_cliente", SqlDbType.Int, clv_cliente);
                db.agregarParametro("@Fecha1", SqlDbType.VarChar, Fecha1);
                db.agregarParametro("@Fecha2", SqlDbType.VarChar, Fecha2);
                SqlDataReader reader = db.consultaReader("select_agendaByCliente");
                agendas = db.MapDataToEntityCollection<AgendaEntity>(reader).ToList();
                agendas.ForEach(x =>
                {
                    x.Fecha2 = x.Fecha.ToShortDateString();
                    x.FechaEjecucion2 = x.FechaEjecucion.ToShortDateString();
                    x.FechaAgendada2 = x.FechaAgendada.ToShortDateString();
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return agendas;
        }


    }
}