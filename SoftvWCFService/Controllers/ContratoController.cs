﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class ContratoController
    {

        public List<ContratoEntity> GetContrato(long? contrato, string nombre, string segundoNombre, string ApellidoPaterno, string ApellidoMaterno, int ? clv_usuario, string Estatus)
        {
            long? clv_contrato = 0;
            dbHelper db = new dbHelper();
            List<ContratoEntity> contratos = new List<ContratoEntity>();

            try
            {
                db.agregarParametro("@contrato", SqlDbType.BigInt, contrato);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@segundoNombre", SqlDbType.VarChar, segundoNombre);
                db.agregarParametro("@ApellidoPaterno", SqlDbType.VarChar, ApellidoPaterno);
                db.agregarParametro("@ApellidoMaterno", SqlDbType.VarChar, ApellidoMaterno);
                db.agregarParametro("@Estatus", SqlDbType.VarChar, Estatus);
                db.agregarParametro("@clv_usuario", SqlDbType.Int, clv_usuario);
                SqlDataReader reader = db.consultaReader("ListaContratos");
                contratos = db.MapDataToEntityCollection<ContratoEntity>(reader).ToList();

                SoftvWCFService soft = new SoftvWCFService();

                //ClienteController clienteController = new ClienteController();

                contratos.ForEach(x=> {

                    x.departamento = soft.GetEstatusDepartamento(x.IdDetalle) ;
                    x.cliente = soft.GetClienteById(x.IdCliente);
                });
                reader.Close();

                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return contratos;


        }



        public int ?  AddContrato(int Clv_cotizacion, string bodega, string cajones, string FechaReservacion, string FechaEscrituracion, decimal? MontoContrato)
        {
            int clv_contrato = 0;
            dbHelper db = new dbHelper();
        
            try
            {
                db.agregarParametro("@Clv_cotizacion", SqlDbType.Int, Clv_cotizacion);
                db.agregarParametro("@bodega", SqlDbType.VarChar, bodega);
                db.agregarParametro("@cajones", SqlDbType.VarChar, cajones);
                db.agregarParametro("@fechaReservacion", SqlDbType.DateTime, FechaReservacion);
                db.agregarParametro("@FechaEscrituracion", SqlDbType.DateTime, FechaEscrituracion);
                db.agregarParametro("@montoContrato", SqlDbType.Decimal, MontoContrato);
                db.agregarParametro("@clv_contrato", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("AddContrato");
                clv_contrato  = int.Parse(db.diccionarioOutput["@clv_contrato"].ToString());                
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return clv_contrato;


        }


        public cotizacionDiferidoEntity GetContratoPrecios(long? contrato)
        {

            cotizacionDiferidoEntity obj = new cotizacionDiferidoEntity();
            dbHelper db = new dbHelper();
            try
            {
               
                db.agregarParametro("@contrato", SqlDbType.BigInt, contrato);
                SqlDataReader reader = db.consultaReader("select_contrato_precios");              
                while (reader.Read())
                {
                    
                    obj.PrecioInicial = decimal.Parse(reader[1].ToString());
                    //obj.MontoEngancheInicial = decimal.Parse(reader[2].ToString());
                    //obj.MontoEngancheDiferido = decimal.Parse(reader[3].ToString());
                    //obj.MontoEngancheTotal = decimal.Parse(reader[1].ToString());
                    //obj.MontoFinanciamientoEngancheDiferido = decimal.Parse(reader[1].ToString());
                    //obj.MontoEngancheMasFinanciamiento = decimal.Parse(reader[1].ToString());
                    //obj.MontoLiquidacionEscrituracion = decimal.Parse(reader[1].ToString());
                    obj.PrecioTotal = decimal.Parse(reader[1].ToString());
                    obj.PrecioMetroCuadrado = decimal.Parse(reader[1].ToString());
                    
                   // precios.Add(obj);
                }
                //precios = db.MapDataToEntityCollection<cotizacionDiferidoEntity>(reader).ToList();                ;
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return obj;


        }



        public ContratoEntity GetContratoById(long? contrato)
        {
            ContratoEntity contratoObj = new ContratoEntity();
            dbHelper db = new dbHelper();

            try
            {               
                db.agregarParametro("@contrato", SqlDbType.BigInt, contrato);
                SqlDataReader reader = db.consultaReader("select_contrato_clv");
                contratoObj = db.MapDataToEntityCollection<ContratoEntity>(reader).ToList()[0];

                SoftvWCFService soft = new SoftvWCFService();
                contratoObj.departamento = soft.GetEstatusDepartamento(contratoObj.IdDetalle);
                contratoObj.cliente = soft.GetClienteById(contratoObj.IdCliente);
                contratoObj.precios = GetContratoPrecios(contratoObj.Id);
                reader.Close();
                db.conexion.Close();               
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return contratoObj;
        }

        
             public List<contratoByclienteEntity> GetContratoByCliente(int? Id)
        {

            dbHelper db = new dbHelper();
            List<contratoByclienteEntity> contratoObj = new List<contratoByclienteEntity>();
         
     
            try
            {
                db.agregarParametro("@Id", SqlDbType.Int, Id);
                SqlDataReader reader = db.consultaReader("SearchContratoByCliente");
                contratoObj = db.MapDataToEntityCollection<contratoByclienteEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return contratoObj;
        }

















        public int? GetContratoByCotizacion(int clv_cotizacion)
        {
            int resultado = 0;
            dbHelper db = new dbHelper();

            try
            {                
                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, clv_cotizacion);
                db.agregarParametro("@resultado", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("selectContratoByCotizacion");
                resultado = int.Parse(db.diccionarioOutput["@resultado"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return resultado;


        }

        public int? UpdateDatosCliente(int clv_comprador, string estado, string municipio, 
        string localidad, string colonia,string calle, string numero, string numeroInterior,
        int? codigoPostal, string rfc, string curp, string folio
            )
        {
            int clv_contrato = 0;
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@clv_comprador", SqlDbType.Int, clv_comprador);
                db.agregarParametro("@estado", SqlDbType.VarChar, estado);
                db.agregarParametro("@municipio", SqlDbType.VarChar, municipio);
                db.agregarParametro("@localidad", SqlDbType.VarChar, localidad);
                db.agregarParametro("@colonia", SqlDbType.VarChar, colonia);
                db.agregarParametro("@calle", SqlDbType.VarChar, calle);
                db.agregarParametro("@numero", SqlDbType.VarChar, numero);
                db.agregarParametro("@numeroInterior", SqlDbType.VarChar, numeroInterior);
                db.agregarParametro("@codigoPostal", SqlDbType.Int, codigoPostal);
                db.agregarParametro("@rfc", SqlDbType.VarChar, rfc);
                db.agregarParametro("@curp", SqlDbType.VarChar, curp);
                db.agregarParametro("@folio", SqlDbType.VarChar, folio);
                db.consultaOutput("update_DatosCliente");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return clv_contrato;


        }










    }
}