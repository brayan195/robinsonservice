﻿
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

namespace SoftvWCFService.Controllers
{
    public class OltController
    {

        public int? addOLT(OLTEntity olt)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@cliente_id", SqlDbType.BigInt, olt.cliente_id);
                db.agregarParametro("@nombre_olt", SqlDbType.VarChar, olt.nombre_olt);
                db.agregarParametro("@olt_ip", SqlDbType.VarChar, olt.olt_ip);
                db.agregarParametro("@telnet_port", SqlDbType.VarChar, olt.telnet_port);
                db.agregarParametro("@telnet_username", SqlDbType.VarChar, olt.telnet_username);
                db.agregarParametro("@telnet_password", SqlDbType.VarChar, olt.telnet_password);
                db.agregarParametro("@snmp_readonly", SqlDbType.VarChar, olt.snmp_readonly);
                db.agregarParametro("@snmp_readwrite", SqlDbType.VarChar, olt.snmp_readwrite);
                db.agregarParametro("@snmp_udp_port", SqlDbType.VarChar, olt.snmp_udp_port);
                db.agregarParametro("@iptv_module", SqlDbType.VarChar, olt.iptv_module);
                db.agregarParametro("@hardware_version", SqlDbType.VarChar, olt.hardware_version);
                db.agregarParametro("@software_version", SqlDbType.VarChar, olt.software_version);
                db.agregarParametro("@supported_pon_types", SqlDbType.VarChar, olt.supported_pon_types);
                db.agregarParametro("@pago_inicial", SqlDbType.VarChar, olt.pago_inicial);
                db.agregarParametro("@periodicity", SqlDbType.VarChar, olt.periodicity);
                db.agregarParametro("@prox_pago", SqlDbType.VarChar, olt.prox_pago);
                db.agregarParametro("@olt_picture", SqlDbType.VarChar, olt.olt_picture);
                db.agregarParametro("@status", SqlDbType.Int, olt.status);
                db.agregarParametro("@continente", SqlDbType.VarChar, olt.continente);
                db.agregarParametro("@latitud", SqlDbType.VarChar, olt.latitud);
                db.agregarParametro("@longitud", SqlDbType.VarChar, olt.longitud);
                //db.agregarParametro("@latitud", SqlDbType.VarChar, olt.latitud);
                db.agregarParametro("@moneda", SqlDbType.VarChar, olt.moneda);
                db.agregarParametro("@timezone", SqlDbType.VarChar, olt.timezone);
                db.agregarParametro("@usuario", SqlDbType.VarChar, olt.usuario);
                db.agregarParametro("@clave", SqlDbType.VarChar, olt.clave);
                db.agregarParametro("@contrato_estado", SqlDbType.VarChar, olt.contrato_estado);
                db.agregarParametro("@notificacion", SqlDbType.VarChar, olt.contrato_estado);
                db.agregarParametro("@fecha_alta", SqlDbType.VarChar, olt.fecha_alta);
                db.agregarParametro("@fecha_baja", SqlDbType.VarChar, olt.fecha_baja);
                db.agregarParametro("@fecha_suspendido", SqlDbType.VarChar, olt.fecha_suspendido);
                db.agregarParametro("@fecha_ultimopago", SqlDbType.VarChar, olt.fecha_ultimopago);

                db.agregarParametro("@ultimo_mes", SqlDbType.Int, olt.ultimo_mes);
                db.agregarParametro("@Ultimo_anio", SqlDbType.Int, olt.ultimo_anio);
                db.agregarParametro("@contacto_tecnico", SqlDbType.VarChar, olt.contacto_tecnico);
                db.agregarParametro("@contacto_correo", SqlDbType.VarChar, olt.contacto_correo);
                db.agregarParametro("@contacto_telefono", SqlDbType.VarChar, olt.contacto_telefono);
                db.agregarParametro("@contacto_celular", SqlDbType.VarChar, olt.contacto_celular);
                db.agregarParametro("@marca", SqlDbType.Int, olt.clv_marca);
                db.agregarParametro("@modelo", SqlDbType.Int, olt.clv_modelo);
                db.agregarParametro("@clv_firmware", SqlDbType.Int, olt.clv_firmware);
                


                db.consultaSinRetorno("addOLT");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? updateOLT(OLTEntity olt)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, olt.id);
                db.agregarParametro("@cliente_id", SqlDbType.BigInt, olt.cliente_id);
                db.agregarParametro("@nombre_olt", SqlDbType.VarChar, olt.nombre_olt);
                db.agregarParametro("@olt_ip", SqlDbType.VarChar, olt.olt_ip);
                db.agregarParametro("@telnet_port", SqlDbType.VarChar, olt.telnet_port);
                db.agregarParametro("@telnet_username", SqlDbType.VarChar, olt.telnet_username);
                db.agregarParametro("@telnet_password", SqlDbType.VarChar, olt.telnet_password);
                db.agregarParametro("@snmp_readonly", SqlDbType.VarChar, olt.snmp_readonly);
                db.agregarParametro("@snmp_readwrite", SqlDbType.VarChar, olt.snmp_readwrite);
                db.agregarParametro("@snmp_udp_port", SqlDbType.VarChar, olt.snmp_udp_port);
                db.agregarParametro("@iptv_module", SqlDbType.VarChar, olt.iptv_module);
                db.agregarParametro("@hardware_version", SqlDbType.VarChar, olt.hardware_version);
                db.agregarParametro("@software_version", SqlDbType.VarChar, olt.software_version);
                db.agregarParametro("@supported_pon_types", SqlDbType.VarChar, olt.supported_pon_types);
                db.agregarParametro("@pago_inicial", SqlDbType.VarChar, olt.pago_inicial);
                db.agregarParametro("@periodicity", SqlDbType.VarChar, olt.periodicity);
                db.agregarParametro("@prox_pago", SqlDbType.Date, olt.prox_pago);
                db.agregarParametro("@olt_picture", SqlDbType.VarChar, olt.olt_picture);
                db.agregarParametro("@status", SqlDbType.Int, olt.status);
                db.agregarParametro("@continente", SqlDbType.VarChar, olt.continente);
                db.agregarParametro("@latitud", SqlDbType.VarChar, olt.latitud);
                db.agregarParametro("@longitud", SqlDbType.VarChar, olt.longitud);
                //db.agregarParametro("@latitud", SqlDbType.VarChar, olt.latitud);
                db.agregarParametro("@moneda", SqlDbType.VarChar, olt.moneda);
                db.agregarParametro("@timezone", SqlDbType.VarChar, olt.timezone);
                db.agregarParametro("@usuario", SqlDbType.VarChar, olt.usuario);
                db.agregarParametro("@clave", SqlDbType.VarChar, olt.clave);
                db.agregarParametro("@contrato_estado", SqlDbType.VarChar, olt.contrato_estado);
                db.agregarParametro("@notificacion", SqlDbType.VarChar, olt.contrato_estado);
                db.agregarParametro("@fecha_alta", SqlDbType.Date, olt.fecha_alta);
                db.agregarParametro("@fecha_baja", SqlDbType.Date, olt.fecha_baja);
                db.agregarParametro("@fecha_suspendido", SqlDbType.Date, olt.fecha_suspendido);
                db.agregarParametro("@fecha_ultimopago", SqlDbType.Date, olt.fecha_ultimopago);

                db.agregarParametro("@ultimo_mes", SqlDbType.Int, olt.ultimo_mes);
                //db.agregarParametro("@Ultimo_anio_p", SqlDbType.Date, olt.ultimo_anio);
                db.agregarParametro("@contacto_tecnico", SqlDbType.VarChar, olt.contacto_tecnico);
                db.agregarParametro("@contacto_correo", SqlDbType.VarChar, olt.contacto_correo);
                db.agregarParametro("@contacto_telefono", SqlDbType.VarChar, olt.contacto_telefono);
                db.agregarParametro("@contacto_celular", SqlDbType.VarChar, olt.contacto_celular);
                db.agregarParametro("@marca", SqlDbType.Int, olt.clv_marca);
                db.agregarParametro("@modelo", SqlDbType.Int, olt.clv_modelo);
                db.agregarParametro("@clv_firmware", SqlDbType.Int, olt.clv_firmware);

                db.consultaSinRetorno("updateOLT");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? DeleteOLT(OLTEntity olt)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, olt.id);
     
                db.consultaSinRetorno("DeleteOlt");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }



        public int? EnviarCorreoMasivo (CorreoMensajeEntity correo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Act", SqlDbType.Bit, correo.Act);
                db.agregarParametro("@Sus", SqlDbType.Bit, correo.Sus);
                db.agregarParametro("@Baj", SqlDbType.Bit, correo.Baj);
                db.agregarParametro("@Dem", SqlDbType.Bit, correo.Dem);
                db.agregarParametro("@Mensaje", SqlDbType.VarChar, correo.Mensaje);

                db.consultaSinRetorno("MandarCorreoMasivo");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }



        public OLTEntity GetOLTById(int? id)
        {
            dbHelper db = new dbHelper();
            OLTEntity OLTEntity = new OLTEntity();

            try
            {

                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("GetOlt");
                OLTEntity = db.MapDataToEntityCollection<OLTEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return OLTEntity;
        }



        public List<OLTEntity> GetOLTByIdCliente(int? id)
        {
            dbHelper db = new dbHelper();
            List<OLTEntity> OLTEntity = new List<OLTEntity>();

            try
            {

                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("GetOltByIdClient");
                OLTEntity = db.MapDataToEntityCollection<OLTEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return OLTEntity;
        }


        public List<OLTEntity> GetOLTByIdClienteRecepcionPagos(int? id)
        {
            dbHelper db = new dbHelper();
            List<OLTEntity> OLTEntity = new List<OLTEntity>();

            try
            {

                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("GetOltByIdClientRP");
                OLTEntity = db.MapDataToEntityCollection<OLTEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return OLTEntity;
        }




        public List<OLTEntity> GetOLTList(int? clienteId)
        {
            dbHelper db = new dbHelper();
            List<OLTEntity> bancos = new List<OLTEntity>();
            try
            {
                db.agregarParametro("@clienteId", SqlDbType.Int, clienteId);
                SqlDataReader reader = db.consultaReader("GetOltList");
                bancos = db.MapDataToEntityCollection<OLTEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }

        public List<MontoEntity> GetMonto()
        {
            dbHelper db = new dbHelper();
            List<MontoEntity> bancos = new List<MontoEntity>();
            try
            {
                //db.agregarParametro("@clienteId", SqlDbType.Int, clienteId);
                SqlDataReader reader = db.consultaReader("GetMontoOlt");
                bancos = db.MapDataToEntityCollection<MontoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }



        public int? addMonto(MontoEntity monto)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Monto", SqlDbType.BigInt,monto.Monto );
                db.agregarParametro("@IVA", SqlDbType.BigInt, monto.IVA);


                


                db.consultaSinRetorno("AddUpdateMonto");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public List<SerieFacturaEntity> GetFaturaSerie()
        {
            dbHelper db = new dbHelper();
            List<SerieFacturaEntity> bancos = new List<SerieFacturaEntity>();
            try
            {
                //db.agregarParametro("@clienteId", SqlDbType.Int, clienteId);
                SqlDataReader reader = db.consultaReader("GetSerie");
                bancos = db.MapDataToEntityCollection<SerieFacturaEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }


        public int? addSerie(SerieFacturaEntity serie)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Folio", SqlDbType.BigInt, serie.Folio);
                db.agregarParametro("@Serie", SqlDbType.VarChar, serie.Serie);

                db.consultaSinRetorno("AddUpdateSerie");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }



        public List<MensajeEntity> GetMensaje()
        {
            dbHelper db = new dbHelper();
            List<MensajeEntity> bancos = new List<MensajeEntity>();
            try
            {
                //db.agregarParametro("@clienteId", SqlDbType.Int, clienteId);
                SqlDataReader reader = db.consultaReader("GetMensaje");
                bancos = db.MapDataToEntityCollection<MensajeEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }


        public int? addMensaje(MensajeEntity mensaje)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
             
                db.agregarParametro("@Mensaje", SqlDbType.VarChar, mensaje.Mensaje);

                db.consultaSinRetorno("AddUpdateMensaje");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public List<FirmwareModeloEntity> GetFirmwareModelo(int? clv_modelo)
        {
            dbHelper db = new dbHelper();
            List<FirmwareModeloEntity> OLTEntity = new List<FirmwareModeloEntity>();

            try
            {

                db.agregarParametro("@clv_modelo", SqlDbType.Int, clv_modelo);
                SqlDataReader reader = db.consultaReader("GetFirmwareModelo");
                OLTEntity = db.MapDataToEntityCollection<FirmwareModeloEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return OLTEntity;
        }



        public int? GenraFacturaAlegra(Pagoentity Pago)
        {

            dbHelper db = new dbHelper();
            int result = 0;
            string UsuarioAlegra = "brayanhuy19@gmail.com";
            string ContraseñaAlegra = "6010c9495cd42a0ff9c8";
            string URLFacturasAlegra = "https://api.alegra.com/api/v1/invoices";
            try
            {

                GeneraFactura(Pago.Clv_factura);

                //db.agregarParametro("@clv_firmware", SqlDbType.Int, olt.clv_firmware);



                //db.consultaSinRetorno("addOLT");
                //db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public static void GeneraFactura(long Clv_Factura)
        {

            SoftvWCFService soft = new SoftvWCFService();
            List<UsuarioAlegraEntity> solicitudobjc = soft.GetUsuarioAlegra();

            string UsuarioAlegra = solicitudobjc[0].Usuario;
            string ContraseñaAlegra = solicitudobjc[0].Token;
            string URLFacturasAlegra = "https://api.alegra.com/api/v1/invoices";
            try
            {
               
                dbHelper db = new dbHelper();
                db.agregarParametro("@Clv_factura", SqlDbType.Int, Clv_Factura);
                SqlDataReader reader = db.consultaReader("ObtieneDatosParaGenerarFactura");
                if (reader.Read())
                {
                    string jsonRequest = "{" +
                        "\"date\" : \"" + reader.GetValue(1).ToString() + "\"," +
                        "\"dueDate\" : \"" + reader.GetValue(2).ToString() + "\"," +
                        "\"status\" : \"open\"," +
                        "\"client\" :{" +"\"id\": "  + reader.GetValue(0).ToString() +
                        "}," +
                        "\"numberTemplate\" : {" +
                                        "\"id\" : 1," +
                                        "\"prefix\" : \"null\"" +
                        "}," +
                        "\"type\" : \"NATIONAL\"," +
                        "\"paymentForm\" : \"CREDIT\"," +
                        
                        "\"items\" : [";

                    reader.NextResult();
                    int count = 0;
                    while (reader.Read())
                    {
                        if (count == 0)
                        {
                            jsonRequest = jsonRequest + "{" +
                              "\"id\" : " + reader.GetValue(0).ToString() + "," +
                              "\"description\" : \"" + reader.GetValue(1).ToString() + "\"," +
                              "\"reference\" : \"" + reader.GetValue(2).ToString() + "\"," +
                              "\"discount\" : 0," +
                              "\"price\" : " + reader.GetValue(3).ToString() + "," +
                              "\"quantity\" : 1" +
                              "}";
                        }
                        else
                        {
                            jsonRequest = jsonRequest + ",{" +
                             "\"id\" : " + reader.GetValue(0).ToString() + "," +
                             "\"description\" : \"" + reader.GetValue(1).ToString() + "\"," +
                             "\"reference\" : \"" + reader.GetValue(2).ToString() + "\"," +
                             "\"discount\" : 0," +
                             "\"price\" : " + reader.GetValue(3).ToString() + "," +
                             "\"quantity\" : 1" +
                             "}";
                        }
                        count++;

                    }

                    jsonRequest = jsonRequest + "]" +
                           "}";

                    //Formamos la petición al proveedor
                    var httpClient = new HttpClient();
                    var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

          

                    byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(UsuarioAlegra + ":" + ContraseñaAlegra);
                    string auth = Convert.ToBase64String(toEncodeAsBytes.ToArray());

                    //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };//Produccion
                    httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);
                    httpClient.Timeout = TimeSpan.FromMilliseconds(600000000);

                    DateTime time1 = DateTime.Now;

                    Task<HttpResponseMessage> response = httpClient.PostAsync(URLFacturasAlegra, httpContent);
                    var responseAux = response.Result.Content.ReadAsStringAsync();
                    string jsonResponse = responseAux.Result.ToString();

                    if (jsonResponse.Contains("message") && !jsonResponse.Contains("id"))
                    {
                        //dbHelper db = new dbHelper();

                   
                        db.agregarParametro("@Clv_Factura", SqlDbType.VarChar, "ERROR");
                        db.agregarParametro("@FacturaJson", SqlDbType.VarChar, jsonResponse);
                        db.consultaSinRetorno("GuardaDatosFactura");

                        Console.WriteLine("Error al generar la factura : " + Clv_Factura.ToString());
                    }
                    else
                    {
                        JObject facturaPrincipal = JObject.Parse(jsonResponse);
                        string IdFactura = "";
                        foreach (JProperty nodo in facturaPrincipal.Properties())
                        {
                            try
                            {
                                if (nodo.Name == "id")
                                {
                                    IdFactura = nodo.Value.ToString();
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                        }

                        //DBFuncion dbGuardaDatosFactura = new DBFuncion();
                        dbHelper db1 = new dbHelper();

                        db1.agregarParametro("@Clv_Factura", SqlDbType.VarChar, IdFactura);
                        db1.agregarParametro("@FacturaJson", SqlDbType.VarChar, jsonResponse);
                        db1.consultaSinRetorno("GuardaDatosFactura");
                        db1.conexion.Close();

                        Console.WriteLine("Generación de factura correcta: " + Clv_Factura.ToString());
                    }
                }
                reader.Close();
                //dbObtieneDatosFactura.conexion.Close();
                //dbObtieneDatosFactura.conexion.Dispose();
            }
            catch (Exception ex)
            {
                dbHelper db = new dbHelper();
                //DBFuncion dbGuardaDatosFacturaEx = new DBFuncion();
           
                db.agregarParametro("@Clv_Factura", SqlDbType.VarChar, "EX");
                db.agregarParametro("@FacturaJson", SqlDbType.VarChar, ex.Message);
                db.consultaSinRetorno("GuardaDatosFactura");
            }
        }


        public List<EpaycoEntity> GetEpayco()
        {
            dbHelper db = new dbHelper();
            List<EpaycoEntity> bancos = new List<EpaycoEntity>();
            try
            {
                //db.agregarParametro("@clienteId", SqlDbType.Int, clienteId);
                SqlDataReader reader = db.consultaReader("GetEpayco");
                bancos = db.MapDataToEntityCollection<EpaycoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }

        public List<GneralCorreoEntity> ConGeneralCorreo()
        {
            dbHelper db = new dbHelper();
            List<GneralCorreoEntity> bancos = new List<GneralCorreoEntity>();
            try
            {
                //db.agregarParametro("@clienteId", SqlDbType.Int, clienteId);
                SqlDataReader reader = db.consultaReader("ConGeneralCorreo");
                bancos = db.MapDataToEntityCollection<GneralCorreoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }

        

        public List<UsuarioAlegraEntity> GetUsuarioAlegra()
        {
            dbHelper db = new dbHelper();
            List<UsuarioAlegraEntity> bancos = new List<UsuarioAlegraEntity>();
            try
            {
                //db.agregarParametro("@clienteId", SqlDbType.Int, clienteId);
                SqlDataReader reader = db.consultaReader("GetUsuarioALegra");
                bancos = db.MapDataToEntityCollection<UsuarioAlegraEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }




        public int? addUsuarioAlegra(UsuarioAlegraEntity Usuario)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Usuario", SqlDbType.VarChar, Usuario.Usuario);
                db.agregarParametro("@Token", SqlDbType.VarChar, Usuario.Token);

                db.consultaSinRetorno("AddUsuarioAlegra");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? addGeneralcorreo(GneralCorreoEntity correo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Cuenta", SqlDbType.VarChar, correo.Cuenta);
                db.agregarParametro("@Password", SqlDbType.VarChar, correo.Password);
                db.agregarParametro("@Host", SqlDbType.VarChar, correo.Host);
                db.agregarParametro("@Port", SqlDbType.VarChar, correo.Port);

                db.consultaSinRetorno("AddUpdateGeneralCorreo");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }



        public int? addEpayco(EpaycoEntity Epayco)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Public_Key", SqlDbType.VarChar, Epayco.Public_Key);
             

                db.consultaSinRetorno("AddUpdateEpayco");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public int? EnviaSms(String Celular1)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                List<MensajeEntity> mensaje = GetMensaje();

                string jsonRequest = "{" +

                                      "\"Numero\" : \"" + Celular1 + "\"," +
                                       "\"Mensaje\" : \"" + mensaje[0].Mensaje + "\"";




                var httpClient = new HttpClient();
                var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                Task<HttpResponseMessage> response = httpClient.PostAsync("http://915009d10274.sn.mynetname.net:10101/SoftvWCFService.svc/Mensajes/MensajesEspeciales", httpContent);
                var responseAux = response.Result.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {

            }
           
            return result;
        }


        
        public List<EstadosCuentaEntity> GetEstadosDecuentaEnviar()
        {
            dbHelper db = new dbHelper();
            List<EstadosCuentaEntity> bancos = new List<EstadosCuentaEntity>();
            try
            {
                //db.agregarParametro("@clienteId", SqlDbType.Int, clienteId);
                SqlDataReader reader = db.consultaReader("MandarEstadoCuentaCorreoSMSWhats");
                bancos = db.MapDataToEntityCollection<EstadosCuentaEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }


    }
}