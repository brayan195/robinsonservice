﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class DatosDepCotizacionController
    {
        public DatosDepCotizacionEntity getDatosDepCotizacion(int? IdDetalle)
        {
            dbHelper db = new dbHelper();
            DatosDepCotizacionEntity cuenta = new DatosDepCotizacionEntity();

            try
            {
                db.agregarParametro("@idDetalle", SqlDbType.Int, IdDetalle);
                SqlDataReader reader = db.consultaReader("datosDepCotizacion");
                cuenta = db.MapDataToEntityCollection<DatosDepCotizacionEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            return cuenta;
        }
    }
}