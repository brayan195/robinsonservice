﻿using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class BodegaCajonController
    {

        public int? AddBodegaCajon(List<BodegaCajonEntity> obj)
        {
            int result = 0;

            DeleteBodegaCajon(obj[0].IdDetalle);
            obj.ForEach(x=> {
                dbHelper db = new dbHelper();
                try
                {
                   
                    db.agregarParametro("@IdDetalle", SqlDbType.Int, x.IdDetalle);
                    db.agregarParametro("@Referencia", SqlDbType.VarChar, x.Referencia);
                    db.agregarParametro("@x", SqlDbType.BigInt, x.x);
                    db.agregarParametro("@y", SqlDbType.BigInt, x.y);
                    db.agregarParametro("@IdLayout", SqlDbType.BigInt, x.IdLayout);
                    db.agregarParametro("@Tipo", SqlDbType.BigInt, x.Tipo);
                    db.agregarParametro("@Nivel", SqlDbType.VarChar, x.Nivel);
                    db.consultaSinRetorno("AddBodegaCajon");
                    db.conexion.Close();
                }
                catch (Exception ex)
                {
                    db.conexion.Close();
                    db.conexion.Dispose();
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }



            });
            
          
            
            return result;
        }


        public int? AddCajon(List<BodegaCajonEntity> obj)
        {
            int result = 0;

            DeleteCajon(obj[0].IdDetalle);
            obj.ForEach(x => {
                dbHelper db = new dbHelper();
                try
                {

                    db.agregarParametro("@IdDetalle", SqlDbType.Int, x.IdDetalle);
                    db.agregarParametro("@Referencia", SqlDbType.VarChar, x.Referencia);
                    db.agregarParametro("@x", SqlDbType.BigInt, x.x);
                    db.agregarParametro("@y", SqlDbType.BigInt, x.y);
                    db.agregarParametro("@IdLayout", SqlDbType.BigInt, x.IdLayout);
                    db.agregarParametro("@Tipo", SqlDbType.BigInt, x.Tipo);
                    db.agregarParametro("@Nivel", SqlDbType.VarChar, x.Nivel);
                    db.consultaSinRetorno("AddCajon");
                    db.conexion.Close();
                }
                catch (Exception ex)
                {
                    db.conexion.Close();
                    db.conexion.Dispose();
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }



            });



            return result;
        }






        public int? DeleteBodegaCajon(int ? IdDetalle)
        {
            int result = 0;          
                dbHelper db = new dbHelper();
                try
                {

                    db.agregarParametro("@IdDetalle", SqlDbType.Int, IdDetalle);              
                    db.consultaSinRetorno("deleteBodegaCajon");
                    db.conexion.Close();
                }
                catch (Exception ex)
                {
                    db.conexion.Close();
                    db.conexion.Dispose();
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            return result;
        }

        public int? DeleteCajon(int? IdDetalle)
        {
            int result = 0;
            dbHelper db = new dbHelper();
            try
            {

                db.agregarParametro("@IdDetalle", SqlDbType.Int, IdDetalle);
                db.consultaSinRetorno("deleteCajon");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }

            return result;
        }

        public List<BodegaCajonEntity> GetBodegasCajonesByIdDepartamento(int? IdDetalle,string Nivel, int Tipo)
        {

            dbHelper db = new dbHelper();
            List<BodegaCajonEntity> lista = new List<BodegaCajonEntity>();
            try
            {
                db.agregarParametro("@IdDetalle", SqlDbType.Int, IdDetalle);
                db.agregarParametro("@Nivel", SqlDbType.VarChar, Nivel);
                db.agregarParametro("@Tipo", SqlDbType.Int, Tipo);
                SqlDataReader reader = db.consultaReader("GetBodegasCajonesByIdDepartamento");
                lista = db.MapDataToEntityCollection<BodegaCajonEntity>(reader).ToList();
               
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return lista;

        }
        public List<BodegaCajonEntity> GetCajonesByIdDepartamento(int? IdDetalle, string Nivel, int Tipo)
        {

            dbHelper db = new dbHelper();
            List<BodegaCajonEntity> lista = new List<BodegaCajonEntity>();
            try
            {
                db.agregarParametro("@IdDetalle", SqlDbType.Int, IdDetalle);
                db.agregarParametro("@Nivel", SqlDbType.VarChar, Nivel);
                db.agregarParametro("@Tipo", SqlDbType.Int, Tipo);
                SqlDataReader reader = db.consultaReader("GetCajonesByIdDepartamento");
                lista = db.MapDataToEntityCollection<BodegaCajonEntity>(reader).ToList();

                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return lista;

        }


        public List<PlanosBodega> GetPalnosTipo(int? clv_proyecto, string Nivel, int Tipo)
        {

            dbHelper db = new dbHelper();
            List<PlanosBodega> lista = new List<PlanosBodega>();
            try
            {
                db.agregarParametro("@clv_proyecto", SqlDbType.Int, clv_proyecto);
                db.agregarParametro("@Nivel", SqlDbType.VarChar, Nivel);
                db.agregarParametro("@Tipo", SqlDbType.Int, Tipo);
                SqlDataReader reader = db.consultaReader("obtenPlanosTipo");
                lista = db.MapDataToEntityCollection<PlanosBodega>(reader).ToList();

                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return lista;

        }


    }
}