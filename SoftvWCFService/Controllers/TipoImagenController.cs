﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class TipoImagenController
    {
        public List<TipoImagen> GetTipoImagen()
        {
            dbHelper db = new dbHelper();
            List<TipoImagen> tipos = new List<TipoImagen>();
            try
            {

                SqlDataReader reader = db.consultaReader("GetTipoImagen");
                tipos = db.MapDataToEntityCollection<TipoImagen>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return tipos;
        }
    }
       
}