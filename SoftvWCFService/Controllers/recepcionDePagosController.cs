using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class recepcionDePagosController
    {
        public List<recepcionDePagosEntity> GetRecepcionDePagos()
        {
            dbHelper db = new dbHelper();
            List<recepcionDePagosEntity> recepcion = new List<recepcionDePagosEntity>();
            try
            {

                SqlDataReader reader = db.consultaReader("select_recepcionDePago");
                recepcion = db.MapDataToEntityCollection<recepcionDePagosEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return recepcion;
        }

        public int? addRecepcionDePago(int clv_cliente, int clv_usuario, string serie, int folio, bool pagoDolares, decimal total)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Clv_factura", SqlDbType.Int, ParameterDirection.Output);
                db.agregarParametro("@Clv_cliente", SqlDbType.Int, clv_cliente);
                db.agregarParametro("@Clv_usuario", SqlDbType.Int, clv_usuario);
                db.agregarParametro("@Serie", SqlDbType.VarChar, serie);
                db.agregarParametro("@Folio", SqlDbType.BigInt, folio);
                db.agregarParametro("@pagoDolares", SqlDbType.Bit, pagoDolares);
                db.agregarParametro("@Total", SqlDbType.Decimal, total);
                db.consultaOutput("registrarPagoFactura");
                result = int.Parse(db.diccionarioOutput["@Clv_factura"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public int? addRecepcionDePagoDet(int clv_factura, int clv_concepto, decimal precio_unitario, string descripcion, int contrato,
            String Periodo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@idDetalle", SqlDbType.Int, ParameterDirection.Output);
                db.agregarParametro("@Clv_factura", SqlDbType.Int, clv_factura);
                db.agregarParametro("@Clv_Concepto", SqlDbType.Int, clv_concepto);
                db.agregarParametro("@PrecioUnitario", SqlDbType.Decimal, precio_unitario);
                db.agregarParametro("@Descripcion", SqlDbType.VarChar, descripcion);
                db.agregarParametro("@Contrato", SqlDbType.Int, contrato);
                db.agregarParametro("@Periodo", SqlDbType.VarChar, Periodo);
              
                db.consultaOutput("registrarPagoFacturaDet");
                result = int.Parse(db.diccionarioOutput["@idDetalle"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public int? addRecepcionDePagoImpuestos(int idDetalle, float IVA, float IEPS, float subTotal, float total)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@idDetalle", SqlDbType.Int, idDetalle);
                db.agregarParametro("@IVA", SqlDbType.Decimal, IVA);
                db.agregarParametro("@IEPS", SqlDbType.Decimal, IEPS);
                db.agregarParametro("@subTotal", SqlDbType.Decimal, subTotal);
                db.agregarParametro("@Total", SqlDbType.Int, total);
                db.consultaOutput("registrarPagoFacturaImpuestos");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? updateRecepcionDePago(int? id, string banco, string cuenta, string concepto,
            DateTime fecha, float monto, string formaDePago, bool? activo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.Int, id);
                db.agregarParametro("@banco", SqlDbType.VarChar, banco);
                db.agregarParametro("@cuenta", SqlDbType.VarChar, cuenta);
                db.agregarParametro("@concepto", SqlDbType.VarChar, concepto);
                db.agregarParametro("@fecha", SqlDbType.DateTime, fecha);
                db.agregarParametro("@monto", SqlDbType.Float, monto);
                db.agregarParametro("@forma_de_pago", SqlDbType.VarChar, formaDePago);
                db.agregarParametro("@activo", SqlDbType.Bit, activo);
                db.consultaSinRetorno("update_recepcionDePago");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public recepcionDePagosEntity GetRecepcionDePagoById(int? id)
        {
            dbHelper db = new dbHelper();
            recepcionDePagosEntity recepcionDePago = new recepcionDePagosEntity();

            try
            {
                TipoVendedorController tv = new TipoVendedorController();
                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("select_recepcionDePagoById");
                recepcionDePago = db.MapDataToEntityCollection<recepcionDePagosEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return recepcionDePago;
        }

     
     
        public List<pagoEntity> GetPagos(int? Factura, int? Cotizacion, string FechaInicio, string FechaFin, int Contrato)
        {
            dbHelper db = new dbHelper();
            List<pagoEntity> recepcion = new List<pagoEntity>();
            try
            {
             
                db.agregarParametro("@Factura", SqlDbType.Int, Factura);
                db.agregarParametro("@Cotizacion", SqlDbType.Int, Cotizacion);
                db.agregarParametro("@FechaInicio", SqlDbType.VarChar, FechaInicio);
                db.agregarParametro("@FechaFin", SqlDbType.VarChar, FechaFin);
                db.agregarParametro("@Contrato", SqlDbType.VarChar, Contrato);
                SqlDataReader reader = db.consultaReader("GetPagoPrueba");
                recepcion = db.MapDataToEntityCollection<pagoEntity>(reader).ToList();
                recepcion.ForEach(x => {
                    x.Fecha2 = x.Fecha.ToShortDateString();

                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return recepcion;
        }

        public int? CancelarPago (int id)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {

                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("cancelar_pago");
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;
        }



        public int? AddRelCancelacionFactura(int? clv_Factura, int?id_Usuario, int? Motivo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {

                db.agregarParametro("@clv_Factura", SqlDbType.Int, clv_Factura);
                db.agregarParametro("@id_Usuario", SqlDbType.Int, id_Usuario);
                db.agregarParametro("@Motivo", SqlDbType.Int, Motivo);
                SqlDataReader reader = db.consultaReader("AddRelCancelacionFactura");

                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;
        }


        public int? selectPagoApartado(int? clv_cotizacion)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, clv_cotizacion);
                db.agregarParametro("@resultado", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("conceptosCobroApartado");
                result = int.Parse(db.diccionarioOutput["@resultado"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public int? selectPagoEnganche(int? clv_cotizacion)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, clv_cotizacion);
                db.agregarParametro("@resultado", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("conceptosCobroEnganche");
                result = int.Parse(db.diccionarioOutput["@resultado"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public fechaReservacionEntity GetFechaReservacion(int clv_cotizacion)
        {
            dbHelper db = new dbHelper();
            fechaReservacionEntity recepcion = new fechaReservacionEntity();
            try
            {

                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, clv_cotizacion);
                SqlDataReader reader = db.consultaReader("fechaReservacion");
                recepcion = db.MapDataToEntityCollection<fechaReservacionEntity>(reader).ToList()[0];
                recepcion.Fecha2 = recepcion.Fecha.ToShortDateString();                
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return recepcion;
        }

        public int? addMetodoPago(int Clv_factura, int Clv_tipoPago, decimal Monto, int banco, long cuenta, int numeroCheque, string cuentaTrans)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {

                db.agregarParametro("@Clv_factura", SqlDbType.Int, Clv_factura);
                db.agregarParametro("@Clv_tipoPago", SqlDbType.Int, Clv_tipoPago);
                db.agregarParametro("@Monto", SqlDbType.Decimal, Monto);
                db.agregarParametro("@banco", SqlDbType.Decimal, banco);
                db.agregarParametro("@cuenta", SqlDbType.Int, cuenta);
                db.agregarParametro("@numeroCheque", SqlDbType.Decimal, numeroCheque);
                db.agregarParametro("@cuentaTrans", SqlDbType.VarChar, cuentaTrans);
                SqlDataReader reader = db.consultaReader("insertTipoPago");               
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;
        }

        public int? ValidarCotizacion(int? clv_cotizacion)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, clv_cotizacion);
                db.agregarParametro("@resultado", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("ValidarCotizacion");
                result = int.Parse(db.diccionarioOutput["@resultado"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public fechaEngancheApartado  getFechasEA(int? clv_cotizacion)
        {
            dbHelper db = new dbHelper();
            fechaEngancheApartado result = new fechaEngancheApartado();
            try
            {
                db.agregarParametro("@clv_cotizacion", SqlDbType.Int, clv_cotizacion);
                SqlDataReader reader = db.consultaReader("getFechasEngancheApartado");
                result = db.MapDataToEntityCollection<fechaEngancheApartado>(reader).ToList()[0];
                result.FechaEnganche2 = result.PagoEnganche.ToShortDateString();
                result.FechaApartado2 = result.PagoApartado.ToShortDateString();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public int? ValidaContrato(int? contrato)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@contrato", SqlDbType.Int, contrato);
                db.agregarParametro("@resultado", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("ValidaContrato");
                result = int.Parse(db.diccionarioOutput["@resultado"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }



    }
}


