﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class conceptosDePagoController
    {

        public int? addConceptoDePago(string Clv_txt, string Descripcion, bool Activo, bool CobroAutomatico, Decimal Monto)
        {
            int id = 0;
            dbHelper db = new dbHelper();
        
            try
            {
                db.agregarParametro("@Clv_txt", SqlDbType.VarChar, Clv_txt);
                db.agregarParametro("@Descripcion", SqlDbType.VarChar, Descripcion);
                db.agregarParametro("@Activo", SqlDbType.Bit, Activo);
                db.agregarParametro("@CobroAutomatico", SqlDbType.Bit, CobroAutomatico);
                db.agregarParametro("@Monto", SqlDbType.Decimal, Monto);
                db.agregarParametro("@id", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("insert_conceptoDePago");
                id = int.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return id;
        }

        public int? updateConceptoDePago(int Id, string Clv_txt, string Descripcion, bool Activo, bool CobroAutomatico, Decimal Monto)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.Int, Id);
                db.agregarParametro("@Clv_txt", SqlDbType.VarChar, Clv_txt);
                db.agregarParametro("@Descripcion", SqlDbType.VarChar, Descripcion);
                db.agregarParametro("@Activo", SqlDbType.Bit, Activo);
                db.agregarParametro("@CobroAutomatico", SqlDbType.Bit, CobroAutomatico);
                db.agregarParametro("@Monto", SqlDbType.Decimal, Monto);
                db.consultaSinRetorno("update_conceptoDePago");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public List<conceptosDePagoEntity> getConceptosDePago()
        {
            dbHelper db = new dbHelper();
            List<conceptosDePagoEntity> concepto = new List<conceptosDePagoEntity>();
            try
            {

                SqlDataReader reader = db.consultaReader("select_conceptosDePago");
                concepto = db.MapDataToEntityCollection<conceptosDePagoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return concepto;
        }

        public conceptosDePagoEntity getConceptosDePagoById(int id)
        {
            dbHelper db = new dbHelper();
            conceptosDePagoEntity usuario = new conceptosDePagoEntity();

            try
            {
                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("select_conceptoDePagoById");
                usuario = db.MapDataToEntityCollection<conceptosDePagoEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return usuario;
        }
    }
}