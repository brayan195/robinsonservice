﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class MotivosCancelacionController
    {

        public List<Motivos_de_cancelacionEntity> GetMotivosCancelacion()
        {
            dbHelper db = new dbHelper();
            List<Motivos_de_cancelacionEntity> motivos = new List<Motivos_de_cancelacionEntity>();
            try
            {

                SqlDataReader reader = db.consultaReader("GetMotivosCancelacion");
                motivos = db.MapDataToEntityCollection<Motivos_de_cancelacionEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return motivos;
        }


    public int? AddMotivosCancelacion(Motivos_de_cancelacionEntity motivos)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Motivos", SqlDbType.VarChar, motivos.Motivos);
                db.agregarParametro("@activo", SqlDbType.VarChar, motivos.activo);
                db.consultaSinRetorno("AddMotivosCancelacion");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? EditMotivosCancelacion(Motivos_de_cancelacionEntity motivos)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@Id", SqlDbType.VarChar, motivos.Id);
                db.agregarParametro("@Motivos", SqlDbType.VarChar, motivos.Motivos);
                db.agregarParametro("@activo", SqlDbType.Int, motivos.activo);
                db.consultaSinRetorno("UpdateMotivosCancelacion");              
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        //public List<Motivos_de_cancelacionEntity> GetMotivosCancelacionById(Motivos_de_cancelacionEntity motivos)
        //{
        //    dbHelper db = new dbHelper();

        //    try
        //    {
        //        db.agregarParametro("@Id", SqlDbType.VarChar, motivos.Id);
        //        SqlDataReader reader = db.consultaReader("GetMotivosCancelacionById");
        //        motivos = db.MapDataToEntityCollection<Motivos_de_cancelacionEntity>(reader).ToList();
        //        reader.Close();
        //        db.conexion.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        db.conexion.Close();
        //        db.conexion.Dispose();
        //    }

        //    return motivos;
        //}



        public Motivos_de_cancelacionEntity GetMotivosCancelacionById(int ? Id)
        {
            dbHelper db = new dbHelper();
           Motivos_de_cancelacionEntity usuario = new Motivos_de_cancelacionEntity();
            
            try
            {
                TipoVendedorController tv = new TipoVendedorController();
               db.agregarParametro("@Id", SqlDbType.Int, Id);
               SqlDataReader reader = db.consultaReader("GetMotivosCancelacionById");
                usuario = db.MapDataToEntityCollection<Motivos_de_cancelacionEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return usuario;
        }



    }
}