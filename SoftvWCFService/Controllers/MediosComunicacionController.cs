﻿using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SoftvWCFService.Controllers
{

public class MediosComunicacionController
    {

    public List<Medios_de_comunicacionEntity> GetMediosDifusion()
    {
        dbHelper db = new dbHelper();
        List<Medios_de_comunicacionEntity> medios = new List<Medios_de_comunicacionEntity>();
        try
        {

            SqlDataReader reader = db.consultaReader("getMediosDifusion");
                medios = db.MapDataToEntityCollection <Medios_de_comunicacionEntity>(reader).ToList();
            reader.Close();
            db.conexion.Close();
        }
        catch (Exception ex)
        {
            db.conexion.Close();
            db.conexion.Dispose();
        }

        return medios;
    }

        public Medios_de_comunicacionEntity GetMediosDifusionById(int? clv_medio)
        {
            dbHelper db = new dbHelper();
            Medios_de_comunicacionEntity medios = new Medios_de_comunicacionEntity();
            try
            {

                db.agregarParametro("@clv_medio", SqlDbType.Int, clv_medio);
                SqlDataReader reader = db.consultaReader("getMediosDifusionById");
                medios = db.MapDataToEntityCollection<Medios_de_comunicacionEntity>(reader).ToList()[0];
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return medios;
        }

        public int? AddMedio(string nombre, bool estatus)
        {
            dbHelper db = new dbHelper();
            int medios = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@estatus", SqlDbType.Bit, estatus);
                db.consultaSinRetorno("insertMediosDifusion");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return medios;
        }

        public int? UpdateMedio(int? clv_medio, string nombre, bool estatus)
        {
            dbHelper db = new dbHelper();
            int medios = 0;
            try
            {
                db.agregarParametro("@clv_medio", SqlDbType.Int, clv_medio);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@estatus", SqlDbType.Bit, estatus);
                db.consultaReader("updateMEdiosDifusion");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return medios;
        }





        public List<MedioContacto> GetMedioContacto()
        {
            dbHelper db = new dbHelper();
            List<MedioContacto> medios = new List<MedioContacto>();
            try
            {

                SqlDataReader reader = db.consultaReader("selectMedioContacto");
                medios = db.MapDataToEntityCollection<MedioContacto>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return medios;
        }

        public MedioContacto GetMedioContactoById(int? clv_medioContacto)
        {
            dbHelper db = new dbHelper();
            MedioContacto medios = new MedioContacto();
            try
            {

                db.agregarParametro("@clv_medioContacto", SqlDbType.Int, clv_medioContacto);
                SqlDataReader reader = db.consultaReader("selectMedioContactoById");
                medios = db.MapDataToEntityCollection<MedioContacto>(reader).ToList()[0];
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return medios;
        }

        public int? AddMedioContacto(string nombre, bool estatus)
        {
            dbHelper db = new dbHelper();
            int medios = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@estatus", SqlDbType.Bit, estatus);
                db.consultaSinRetorno("insertMedioContacto");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return medios;
        }

        public int? UpdateMedioContacto(int? clv_medioContacto, string nombre, bool estatus)
        {
            dbHelper db = new dbHelper();
            int medios = 0;
            try
            {
                db.agregarParametro("@clv_medioContacto", SqlDbType.Int, clv_medioContacto);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@estatus", SqlDbType.Bit, estatus);
                db.consultaReader("updateMedioContacto");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return medios;
        }

    }
}