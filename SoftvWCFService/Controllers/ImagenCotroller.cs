﻿using Softv.Entities;
using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class ImagenController
    {

        public List<logo> GetImagenesDepartamento(int? clv_departamento)
        {
            List<logo> lista = new List<logo>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@clv_departamento", SqlDbType.BigInt, clv_departamento);
                SqlDataReader rd = db.consultaReader("obtenImagenesDepartamento");
                while (rd.Read())
                {
                    logo l = new logo();
                    l.Idtipo = int.Parse(rd[3].ToString());
                    l.Valor = rd[2].ToString();
                    lista.Add(l);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }



      





    }
}