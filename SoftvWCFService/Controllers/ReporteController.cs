using CrystalDecisions.CrystalReports.Engine;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Softv.Entities;
using SoftvWCFService.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Image = iTextSharp.text.Image;

namespace SoftvWCFService.Controllers
{
    public class ReporteController
    {

        public PdfPCell CreateblankCell(bool border)
        {
            PdfPCell blank = new PdfPCell(new Phrase(" "));
            blank.BorderWidth = (border) ? 1 : 0;
            return blank;
        }

        public PdfPCell CreateCell(bool border, bool borderBottom, bool borderTop, bool borderLeft, bool borderRight, string value, iTextSharp.text.BaseColor color, iTextSharp.text.Font font, bool center)
        {
            PdfPCell cell = new PdfPCell(new Phrase(value, font));
            cell.BorderWidth = (border) ? 1 : 0;
            cell.BorderWidthBottom = (borderBottom) ? 1 : 0;
            cell.BorderWidthTop = (borderTop) ? 1 : 0;
            cell.BorderWidthLeft = (borderLeft) ? 1 : 0;
            cell.BorderWidthRight = (borderRight) ? 1 : 0;
            cell.BackgroundColor = color;
            if (center)
            {
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            }

            return cell;
        }



        public PdfPCell CreateCell2(bool border, string value, iTextSharp.text.BaseColor color, iTextSharp.text.Font font, bool center, int colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(value, font));
            cell.BorderWidth = (border) ? 1 : 0;
            cell.BackgroundColor = color;
            cell.Padding = 5f;
            if (center)
            {
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            }
            cell.Colspan = colspan;

            return cell;
        }




        public PdfPCell CreateCell3(bool border, bool borderBottom, bool borderTop, bool borderLeft, bool borderRight, string value, iTextSharp.text.BaseColor color, iTextSharp.text.Font font, bool center)
        {
            PdfPCell cell = new PdfPCell(new Phrase(value, font));
            cell.BorderWidth = (border) ? 1 : 0;
            cell.BorderWidthBottom = (borderBottom) ? 1 : 0;
            cell.BorderWidthTop = (borderTop) ? 1 : 0;
            cell.BorderWidthLeft = (borderLeft) ? 1 : 0;
            cell.BorderWidthRight = (borderRight) ? 1 : 0;
            cell.BackgroundColor = color;
            if (center)
            {
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            }

            return cell;
        }

        public PdfPCell CreateCell4(bool border, bool borderBottom, bool borderTop, bool borderLeft, bool borderRight, string value, iTextSharp.text.BaseColor color, iTextSharp.text.Font font, bool center)
        {
            PdfPCell cell = new PdfPCell(new Phrase(value, font));
            cell.BorderWidth = (border) ? 1 : 0;
            cell.BorderWidthBottom = (borderBottom) ? 1 : 0;
            cell.BorderWidthTop = (borderTop) ? 1 : 0;
            cell.BorderWidthLeft = (borderLeft) ? 1 : 0;
            cell.BorderWidthRight = (borderRight) ? 1 : 0;
            cell.BackgroundColor = color;
            if (center)
            {
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
            }

            return cell;
        }



        public string ReporteCartaAgradecimientoNocompra(int? clv_cliente)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                SoftvWCFService softv = new SoftvWCFService();
                InfoClientes cliente = softv.GetClienteById(clv_cliente);

                name = Guid.NewGuid().ToString() + ".pdf";
                string fileName = apppath + "/Reportes/" + name;
                ReportDocument reportDocument = new ReportDocument();
                reportDocument.Load(ruta + "CartaAgradecimientoNocompra.rpt");
                //string ncliente = cliente.nombre + " " + cliente.otro_nombre + " " + cliente.primer_apellido + " " + cliente.segundo_apellido;
                //reportDocument.DataDefinition.FormulaFields["nombre"].Text = "'" + ncliente + "'";
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                result = name;

            }
            return result;
        }


        public string ReporteContrato(int? Opexportacion, int? contrato)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }
                else if (Opexportacion == 3)
                {
                    name = Guid.NewGuid().ToString() + ".doc";
                }

                string fileName = apppath + "/Reportes/" + name;


                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "selects_contrato";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;


                        SqlParameter par1 = new SqlParameter("@contrato", SqlDbType.Int);
                        par1.Value = contrato;
                        par1.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par1);

                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "contrato";
                        dataSet.Tables[1].TableName = "cajones";
                        dataSet.Tables[2].TableName = "Nivel";
                        dataSet.Tables[3].TableName = "No.Efectivo";
                        dataSet.Tables[4].TableName = "PagosEfectivo";
                        dataSet.Tables[5].TableName = "No.Cheque";
                        dataSet.Tables[6].TableName = "PagosCheque";
                        dataSet.Tables[7].TableName = "No.Transferencia";
                        dataSet.Tables[8].TableName = "PagosTransferencia";

                        reportDocument.Load(ruta + "ReporteContrato.rpt");
                        reportDocument.SetDataSource(dataSet);




                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }
                        else if (Opexportacion == 3)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.WordForWindows, fileName);
                        }

                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }


        public string ReporteAceptacionDeCompra(int? Opexportacion, int? cotizacion, int? Op1)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }
                else if (Opexportacion == 3)
                {
                    name = Guid.NewGuid().ToString() + ".doc";
                }

                string fileName = apppath + "/Reportes/" + name;


                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "reporteAceptacionDeCompra";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;


                        SqlParameter par1 = new SqlParameter("@cotizacion", SqlDbType.Int);
                        par1.Value = cotizacion;
                        par1.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par1);


                        SqlParameter par2 = new SqlParameter("@Op1", SqlDbType.Int);
                        par2.Value = Op1;
                        par2.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par2);






                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "datosCotizacion";
                        dataSet.Tables[1].TableName = "pagos";
                        dataSet.Tables[2].TableName = "cajones";
                        dataSet.Tables[3].TableName = "bodegas";


                        reportDocument.Load(ruta + "ReporteAceptacionDeVenta1.rpt");
                        reportDocument.SetDataSource(dataSet);




                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }
                        else if (Opexportacion == 3)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.WordForWindows, fileName);
                        }

                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }

        public string GetReporteCotizacion(int? Op, int? id, string url)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string name = "";
            name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            string ruta = apppath + "/Logos/oh!-live.jpg";
            string ruta2 = apppath + "/Logos/Hi.png";

            iTextSharp.text.Font _FontHead1A = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead1 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead5 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead3 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead4 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead41 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead42 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLDITALIC, BaseColor.BLACK);
            //cell = new PdfPCell() { Border = 0, BorderWidthBottom = 1, VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = Element.ALIGN_CENTER };


            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
            doc.Open();

            PdfContentByte cb = writer.DirectContent;

            //Linea Divisora                      
            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(2.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 3)));

            CotizacionController cotizacionCtrl = new CotizacionController();
            UsuarioController usuarioController = new UsuarioController();

            CotizacionEntity cotizacion_ = cotizacionCtrl.GetCotizacionById(id);
            SoftvWCFService soft = new SoftvWCFService();
            InfoClientes P = soft.GetClienteById(cotizacion_.Clv_cliente);
            EstatusDepartamentoEntity E = soft.GetEstatusDepartamento(cotizacion_.IdDetalle);
            ProyectoEntity O = soft.GetProyectoById(E.clv_proyecto);
            List<ConfiguracionEntity> W = soft.GetConfiguracionSistema();
            DatosDepCotizacionEntity datosDep = soft.getDatosDepCotizacion(cotizacion_.IdDetalle);
            List<DepartamentoEntity> De = soft.GetDepartamentoC(E.clv_Departamento);

            //CotizacionEntity c = soft.GetCotizacionById(id);
            //EstatusDepartamentoEntity E = soft.GetEstatusDepartamento(c.IdDetalle);
            //cotizacionDiferidoEntity D = new cotizacionDiferidoEntity(); //soft.GetCotizacionDiferido( c.enganche_inicial, c.enganche_diferido, c.escrituras,c.MesesDiferidos,c.PrecioMetro,c.MetrosDepartamento).precios;
            //List<PagosDiferidosEntity> Q = new List<PagosDiferidosEntity>(); //soft.GetCotizacionDiferido(c.enganche_inicial, c.enganche_diferido, c.escrituras, c.MesesDiferidos, c.PrecioMetro, c.MetrosDepartamento).pagos;

            //if (c.enganche_diferido > 0)
            //{
            //    Op = 2;
            //}else{Op = 1; }
            //List<ConfiguracionEntity> W = soft.GetConfiguracionSistema();
            //InfoClientes P = soft.GetClienteById(c.Clv_cliente);
            //List<DepartamentoEntity> H = soft.GetDepartamentoC(E.clv_Departamento);
            //ProyectoEntity O =  soft.GetProyectoById(E.clv_proyecto);




            PdfPCell logo1 = new PdfPCell();
            logo1.BorderWidth = 0;
            logo1.BorderWidthBottom = 0;
            logo1.BorderWidthTop = 0;
            logo1.BorderWidthLeft = 0;
            logo1.BorderWidthRight = 0;
            logo1.Border = 0;
            Image img2 = Image.GetInstance(ruta2);
            //img2.ScaleAbsolute(100f, 100f);
            // img2.ScaleToFit(50f, 50f);
            logo1.HorizontalAlignment = Element.ALIGN_LEFT;


            PdfPCell logo2 = new PdfPCell();
            logo2.BorderWidth = 0;
            logo2.BorderWidthBottom = 0;
            logo2.BorderWidthTop = 0;
            logo2.BorderWidthLeft = 0;
            logo2.BorderWidthRight = 0;
            logo2.Border = 0;
            Image img = Image.GetInstance(ruta);
            logo2.HorizontalAlignment = Element.ALIGN_RIGHT;


            iTextSharp.text.Image startv = iTextSharp.text.Image.GetInstance(ruta2);
            startv.ScaleToFit(50f, 50f);
            startv.SetAbsolutePosition(38f, 700f);
            doc.Add(startv);

            iTextSharp.text.Image s = iTextSharp.text.Image.GetInstance(ruta);
            s.ScaleToFit(100f, 100f);
            s.SetAbsolutePosition(480f, 700f);
            doc.Add(s);

            Paragraph saltoDeLinea1 = new Paragraph("                                                                                                                                                                                                                                                                                                                                                                                   ");
            doc.Add(saltoDeLinea1);
            Paragraph saltoDeLinea2 = new Paragraph("                                                                                                                                                                                                                                                                                                                                                                                   ");
            doc.Add(saltoDeLinea2);
            //PdfPTable TableHeader = new PdfPTable(1);
            //TableHeader.WidthPercentage = 100;
            //TableHeader.AddCell(CreateCell(false, false, false, false, false, "Cotización", BaseColor.WHITE, _FontHead1, true));
            //TableHeader.AddCell(CreateCell3(false, false, false, false, false, " " , BaseColor.WHITE, _FontHead2, true));
            //TableHeader.AddCell(CreateCell3(false, false, false, false, false, " " , BaseColor.WHITE, _FontHead2, true));
            //TableHeader.AddCell(CreateblankCell(false));

            //doc.Add(TableHeader);

            //int reusltadodif = (DateTime.Parse(E.FechaEntrega).Month - c.Fecha.Month) + 12 * (DateTime.Parse(E.FechaEntrega).Year - c.Fecha.Year);

            PdfPTable TableHeader1 = new PdfPTable(2);
            TableHeader1.SpacingAfter = 10;
            TableHeader1.WidthPercentage = 100;
            TableHeader1.AddCell(CreateCell4(false, false, false, false, false, "PROPUESTA DE VENTA", BaseColor.WHITE, _FontHead4, false));
            TableHeader1.AddCell(CreateblankCell(false));
            doc.Add(TableHeader1);

            PdfPTable TableHeader1ab = new PdfPTable(2);
            TableHeader1ab.WidthPercentage = 100;
            TableHeader1ab.AddCell(CreateCell4(false, false, false, false, false, "A quien corresponda", BaseColor.WHITE, _FontHead42, false));
            TableHeader1ab.AddCell(CreateblankCell(false));
            doc.Add(TableHeader1ab);

            PdfPTable TableHeader1ac = new PdfPTable(2);
            TableHeader1ac.WidthPercentage = 100;
            TableHeader1ac.SpacingAfter = 15;
            TableHeader1ac.AddCell(CreateCell4(false, false, false, false, false, "P r e s e n t e", BaseColor.WHITE, _FontHead4, false));
            TableHeader1ac.AddCell(CreateblankCell(false));
            doc.Add(TableHeader1ac);

            PdfPTable Tablebody1A = new PdfPTable(2);
            Tablebody1A.WidthPercentage = 100;
            Tablebody1A.SpacingAfter = 10;
            Tablebody1A.AddCell(CreateCell4(false, false, false, false, false, "Departamento ", BaseColor.WHITE, _FontHead4, true));
            Tablebody1A.AddCell(CreateCell3(false, false, false, false, false, " " + datosDep.NumDepartamento, BaseColor.WHITE, _FontHead1, true));
            Tablebody1A.AddCell(CreateCell4(false, false, false, false, false, "Ubicación", BaseColor.WHITE, _FontHead41, true));
            Tablebody1A.AddCell(CreateCell3(false, false, false, false, false, " " + "N" + E.Piso + ", " + E.Torre, BaseColor.WHITE, _FontHead41, true));
            Tablebody1A.AddCell(CreateCell4(false, false, false, false, false, "Vista ", BaseColor.WHITE, _FontHead41, true));
            Tablebody1A.AddCell(CreateCell3(false, false, false, false, false, " " + datosDep.Vista, BaseColor.WHITE, _FontHead41, true));
            doc.Add(Tablebody1A);

            PdfPTable Tablebody2A = new PdfPTable(2);
            Tablebody2A.WidthPercentage = 100; Tablebody1A.SpacingAfter = 10;
            Tablebody2A.AddCell(CreateCell4(false, false, false, false, false, "Superficie Total (M2) ", BaseColor.WHITE, _FontHead4, true));
            Tablebody2A.AddCell(CreateCell3(false, false, false, false, false, " " + datosDep.SuperficieTotal, BaseColor.WHITE, _FontHead1, true));

            Tablebody2A.AddCell(CreateCell4(false, false, false, false, false, "    Integrada por: ", BaseColor.WHITE, _FontHead42, true));
            Tablebody2A.AddCell(CreateCell4(false, false, false, false, false, " ", BaseColor.WHITE, _FontHead42, true));
            Tablebody2A.AddCell(CreateCell4(false, false, false, false, false, "    Superficie departamento (M2) ", BaseColor.WHITE, _FontHead41, true));
            Tablebody2A.AddCell(CreateCell3(false, false, false, false, false, " " + datosDep.MetrosDepartamento, BaseColor.WHITE, _FontHead41, true));
            Tablebody2A.AddCell(CreateCell4(false, false, false, false, false, "    Superficie estacionamientos (M2) ", BaseColor.WHITE, _FontHead41, true));
            Tablebody2A.AddCell(CreateCell3(false, false, false, false, false, " " + datosDep.Cajon, BaseColor.WHITE, _FontHead41, true));
            Tablebody2A.AddCell(CreateCell4(false, false, false, false, false, "    Superficie bodega (M2) ", BaseColor.WHITE, _FontHead41, true));
            Tablebody2A.AddCell(CreateCell3(false, false, false, false, false, " " + datosDep.Bodega, BaseColor.WHITE, _FontHead41, true));

            doc.Add(Tablebody2A);
            doc.Add(p);



            PdfPTable TableHeader1C = new PdfPTable(2);
            TableHeader1C.WidthPercentage = 100;
            TableHeader1C.SpacingAfter = 10;
            TableHeader1C.SpacingBefore = 10;
            TableHeader1C.AddCell(CreateCell4(false, false, false, false, false, "PRECIO DEL DEPARTAMENTO:", BaseColor.WHITE, _FontHead1, true));
            TableHeader1C.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.MetrosDepartamento * cotizacion_.PrecioMetro).ToString("C"), BaseColor.WHITE, _FontHead1, true));
            doc.Add(TableHeader1C);

            /////////////////////////////////////////////////////////////////////////////////
            var aplica = false;
            if ((cotizacion_.EngancheDiferido > 0 || cotizacion_.EngancheAdicional > 0) && cotizacion_.EngancheInicial == 30)
            {
                PdfPTable Tablebody1Ca = new PdfPTable(3);
                Tablebody1Ca.WidthPercentage = 100;
                Tablebody1Ca.AddCell(CreateCell4(false, false, false, false, false, "Enganche ", BaseColor.WHITE, _FontHead2, true));
                Tablebody1Ca.AddCell(CreateCell(false, false, false, false, false, Decimal.Round(cotizacion_.EngancheInicial, 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                Tablebody1Ca.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.EngancheInicial).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                doc.Add(Tablebody1Ca);

                if (cotizacion_.EngancheAdicional > 0)
                {
                    PdfPTable Tablebody1CA = new PdfPTable(3);
                    Tablebody1CA.WidthPercentage = 100;
                    Tablebody1CA.AddCell(CreateCell4(false, false, false, false, false, "Enganche Adicional ", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1CA.AddCell(CreateCell(false, false, false, false, false, Decimal.Round(cotizacion_.EngancheAdicional, 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));

                    Tablebody1CA.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.EngancheAdicional).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                    doc.Add(Tablebody1CA);
                }

                if (cotizacion_.EngancheDiferido > 0)
                {
                    PdfPTable Tablebody1CB = new PdfPTable(3);
                    Tablebody1CB.WidthPercentage = 100;
                    Tablebody1CB.AddCell(CreateCell4(false, false, false, false, false, "Enganche Diferido", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1CB.AddCell(CreateCell(false, false, false, false, false, Decimal.Round(cotizacion_.EngancheDiferido, 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1CB.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.EngancheDiferido).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                    doc.Add(Tablebody1CB);
                }


                PdfPTable Tablebody1CC = new PdfPTable(3);
                Tablebody1CC.WidthPercentage = 100;
                Tablebody1CC.AddCell(CreateCell4(false, false, false, false, false, "Enganche Total", BaseColor.WHITE, _FontHead2, true));
                Tablebody1CC.AddCell(CreateCell(false, false, false, false, false, Decimal.Round((cotizacion_.EngancheInicial + cotizacion_.EngancheDiferido + cotizacion_.EngancheAdicional), 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                Tablebody1CC.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.EngancheAdicional + cotizacion_.precios.EngancheDiferido + cotizacion_.precios.EngancheInicial).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                doc.Add(Tablebody1CC);

                //PdfPTable TableHeader1CD = new PdfPTable(2);
                //TableHeader1CD.WidthPercentage = 100;
                //TableHeader1CD.AddCell(CreateCell4(false, false, false, false, false, "Descuento por enganche adicional y diferido", BaseColor.WHITE, _FontHead1A, true));
                //TableHeader1CD.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.DescuentoEngancheAdicional + cotizacion_.precios.DescuentoEngancheDiferido).ToString("C"), BaseColor.WHITE, _FontHead1A, true));
                //doc.Add(TableHeader1CD);



                PdfPTable TableHeader1CE = new PdfPTable(2);
                TableHeader1CE.WidthPercentage = 100;
                TableHeader1CE.SpacingBefore = 5;
                TableHeader1CE.AddCell(CreateCell4(false, false, false, false, false, "PECIO A PAGAR:", BaseColor.WHITE, _FontHead1, true));
                TableHeader1CE.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.PrecioTotal).ToString("C"), BaseColor.WHITE, _FontHead1, true));
                doc.Add(TableHeader1CE);

            }
            else
            {
                aplica = true;
            }
            doc.Add(p);
            ///////////////////////////////////////////////////////////////////////////////////
            ///
            PdfPTable TableHeader1EA = new PdfPTable(2);
            TableHeader1EA.WidthPercentage = 100;
            TableHeader1EA.SpacingAfter = 10;
            TableHeader1EA.SpacingBefore = 10;
            TableHeader1EA.AddCell(CreateCell4(false, false, false, false, false, "FORMA DE PAGO:", BaseColor.WHITE, _FontHead1, true));
            TableHeader1EA.AddCell(CreateCell3(false, false, false, false, false, "", BaseColor.WHITE, _FontHead1, true));
            doc.Add(TableHeader1EA);




            if (Op == 1)
            {

                PdfPTable Tablebody1EB = new PdfPTable(3);
                Tablebody1EB.WidthPercentage = 100;
                Tablebody1EB.AddCell(CreateCell4(false, false, false, false, false, "Enganche ", BaseColor.WHITE, _FontHead2, true));
                Tablebody1EB.AddCell(CreateCell(false, false, false, false, false, Decimal.Round((cotizacion_.EngancheAdicional + cotizacion_.EngancheInicial), 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                Tablebody1EB.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.EngancheInicial + cotizacion_.precios.EngancheAdicional).ToString("C"), BaseColor.WHITE, _FontHead2, true));

                Tablebody1EB.AddCell(CreateCell4(false, false, false, false, false, "Descueto por enganche: ", BaseColor.WHITE, _FontHead2, true));


                W.ForEach(rr => {

                    Tablebody1EB.AddCell(CreateCell(false, false, false, false, false, (rr.DescuentoEnganche).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1EB.AddCell(CreateCell3(false, false, false, false, false, Decimal.Round((cotizacion_.precios.DescuentoEnganche), 0).ToString("C"), BaseColor.WHITE, _FontHead2, true));


                });



                doc.Add(Tablebody1EB);
            }



            if (Op == 0)
            {
                PdfPTable Tablebody1EB2 = new PdfPTable(3);
                Tablebody1EB2.WidthPercentage = 100;
                Tablebody1EB2.AddCell(CreateCell4(false, false, false, false, false, "Enganche ", BaseColor.WHITE, _FontHead2, true));
                Tablebody1EB2.AddCell(CreateCell(false, false, false, false, false, Decimal.Round((cotizacion_.EngancheInicial + cotizacion_.EngancheAdicional), 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                //Tablebody1EB.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.EngancheAdicional + cotizacion_.precios.EngancheDiferido + cotizacion_.precios.EngancheInicial).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                Tablebody1EB2.AddCell(CreateCell3(false, false, false, false, false, (((cotizacion_.EngancheInicial + cotizacion_.EngancheAdicional) / 100) * cotizacion_.precios.PrecioInicial).ToString("C"), BaseColor.WHITE, _FontHead2, true));



                Tablebody1EB2.AddCell(CreateCell4(false, false, false, false, false, "Descueto por enganche: ", BaseColor.WHITE, _FontHead2, true));


                W.ForEach(rr => {

                    Tablebody1EB2.AddCell(CreateCell(false, false, false, false, false, (rr.DescuentoEnganche).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1EB2.AddCell(CreateCell3(false, false, false, false, false, Decimal.Round((cotizacion_.precios.DescuentoEnganche), 0).ToString("C"), BaseColor.WHITE, _FontHead2, true));


                });

                doc.Add(Tablebody1EB2);
            }





            if (Op == 1)
            {
                if (cotizacion_.EngancheDiferido > 0)
                {
                    PdfPTable Tablebody1O = new PdfPTable(3);
                    Tablebody1O.WidthPercentage = 100;
                    Tablebody1O.AddCell(CreateCell4(false, false, false, false, false, " ", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1O.AddCell(CreateCell(false, false, false, false, false, cotizacion_.pagos.Count + " pagos de: ", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1O.AddCell(CreateCell(false, false, false, false, false, Decimal.Round((((cotizacion_.EngancheDiferido / 100) * cotizacion_.precios.PrecioInicial) / cotizacion_.pagos.Count), 0).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                    Tablebody1O.AddCell(CreateCell(false, false, false, false, false, Decimal.Round(cotizacion_.EngancheDiferido, 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1O.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.EngancheDiferido).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                    doc.Add(Tablebody1O);
                }
            }



            if (Op == 0)
            {
                if (cotizacion_.EngancheDiferido > 0)
                {

                    PdfPTable Tablebody1O = new PdfPTable(3);
                    Tablebody1O.WidthPercentage = 100;
                    Tablebody1O.AddCell(CreateCell4(false, false, false, false, false, "Enganche Diferido", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1O.AddCell(CreateCell(false, false, false, false, false, Decimal.Round((cotizacion_.EngancheDiferido), 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1O.AddCell(CreateCell3(false, false, false, false, false, ((cotizacion_.EngancheDiferido / 100) * cotizacion_.precios.PrecioInicial).ToString("C"), BaseColor.WHITE, _FontHead2, true));

                    Tablebody1O.AddCell(CreateCell4(false, false, false, false, false, " ", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1O.AddCell(CreateCell(false, false, false, false, false, cotizacion_.pagos.Count + " pagos de: ", BaseColor.WHITE, _FontHead2, true));
                    Tablebody1O.AddCell(CreateCell(false, false, false, false, false, Decimal.Round((((cotizacion_.EngancheDiferido / 100) * cotizacion_.precios.PrecioInicial) / cotizacion_.pagos.Count), 2).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                    doc.Add(Tablebody1O);
                }
            }




            if (Op == 1)
            {
                PdfPTable Tablebody11 = new PdfPTable(3);
                Tablebody11.WidthPercentage = 100;
                Tablebody11.AddCell(CreateCell4(false, false, false, false, false, "Saldo a la escrituración ", BaseColor.WHITE, _FontHead2, true));
                Tablebody11.AddCell(CreateCell(false, false, false, false, false, "                              ", BaseColor.WHITE, _FontHead2, true));
                Tablebody11.AddCell(CreateCell3(false, false, false, false, false, Decimal.Round((cotizacion_.precios.PrecioTotal - (cotizacion_.precios.EngancheDiferido + cotizacion_.precios.EngancheInicial + cotizacion_.precios.EngancheAdicional)), 0).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                doc.Add(Tablebody11);
            }


            if (Op == 0)
            {
                PdfPTable Tablebody11 = new PdfPTable(3);
                Tablebody11.WidthPercentage = 100;
                Tablebody11.AddCell(CreateCell4(false, false, false, false, false, "Saldo a la escrituración ", BaseColor.WHITE, _FontHead2, true));
                Tablebody11.AddCell(CreateCell(false, false, false, false, false, Decimal.Round((cotizacion_.Escrituras), 0).ToString() + "%", BaseColor.WHITE, _FontHead2, true));
                Tablebody11.AddCell(CreateCell3(false, false, false, false, false, Decimal.Round((((cotizacion_.Escrituras) / 100) * cotizacion_.precios.PrecioInicial - cotizacion_.precios.DescuentoEnganche), 0).ToString("C"), BaseColor.WHITE, _FontHead2, true));
                doc.Add(Tablebody11);
            }

            if (aplica == true)
            {

                PdfPTable TableHeader1CE = new PdfPTable(2);
                TableHeader1CE.WidthPercentage = 100;
                TableHeader1CE.SpacingBefore = 5;
                TableHeader1CE.SpacingAfter = 5;
                TableHeader1CE.AddCell(CreateCell4(false, false, false, false, false, "PECIO A PAGAR:", BaseColor.WHITE, _FontHead1, true));
                TableHeader1CE.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.PrecioTotal).ToString("C"), BaseColor.WHITE, _FontHead1, true));
                doc.Add(TableHeader1CE);
            }
            PdfPTable Tablebody1E = new PdfPTable(3);
            Tablebody1E.WidthPercentage = 100;
            Tablebody1E.AddCell(CreateCell4(false, false, false, false, false, "Fecha estimada de entrega:  ", BaseColor.WHITE, _FontHead2, true));
            Tablebody1E.AddCell(CreateCell(false, false, false, false, false, cotizacion_.FechaEntrega2, BaseColor.WHITE, _FontHead2, true));
            Tablebody1E.AddCell(CreateblankCell(false));
            doc.Add(Tablebody1E);



            //PdfPTable TableHeader1CE1 = new PdfPTable(2);
            //TableHeader1CE1.WidthPercentage = 100;
            //TableHeader1CE1.SpacingBefore = 5;
            //TableHeader1CE1.AddCell(CreateCell4(false, false, false, false, false, "Total:", BaseColor.WHITE, _FontHead1, true));
            //TableHeader1CE1.AddCell(CreateCell3(false, false, false, false, false, (cotizacion_.precios.PrecioTotal).ToString("C"), BaseColor.WHITE, _FontHead1, true));
            //doc.Add(TableHeader1CE1);
            doc.Add(p);





            PdfPTable Tablebody1F = new PdfPTable(3);
            Tablebody1F.WidthPercentage = 100;
            //Tablebody1F.SpacingAfter = 5;
            Tablebody1F.SpacingBefore = 5;
            Tablebody1F.AddCell(CreateCell4(false, false, false, false, false, "Vendedor (a)", BaseColor.WHITE, _FontHead2, true));
            //UsuarioEntity usuario = usuarioController.GetUsuarioById(cotizacion_.Clv_vendedor);
            VendedorEntity vendedor = usuarioController.GetDetalleVendedor(cotizacion_.Clv_vendedor);
            Tablebody1F.AddCell(CreateCell(false, false, false, false, false, vendedor.nombreVendedor, BaseColor.WHITE, _FontHead2, true));
            Tablebody1F.AddCell(CreateblankCell(false));
            doc.Add(Tablebody1F);

            PdfPTable Tablebody1G = new PdfPTable(3);
            Tablebody1G.SpacingAfter = 5;
            //Tablebody1G.SpacingBefore = 5;
            Tablebody1G.WidthPercentage = 100;
            Tablebody1G.AddCell(CreateCell4(false, false, false, false, false, "Costo estimado de escrituración", BaseColor.WHITE, _FontHead2, true));
            Tablebody1G.AddCell(CreateCell(false, false, false, false, false, (cotizacion_.PrecioMetro * cotizacion_.MetrosDepartamento * cotizacion_.Escrituracion / 100).ToString("C"), BaseColor.WHITE, _FontHead2, true));
            Tablebody1G.AddCell(CreateblankCell(false));
            doc.Add(Tablebody1G);


            PdfPTable Tablebody1H = new PdfPTable(3);
            Tablebody1H.WidthPercentage = 100;
            Tablebody1H.AddCell(CreateCell4(false, false, false, false, false, "Fecha de la propuesta:", BaseColor.WHITE, _FontHead5, true));
            Tablebody1H.AddCell(CreateCell(false, false, false, false, false, cotizacion_.Fecha2, BaseColor.WHITE, _FontHead5, true));
            Tablebody1H.AddCell(CreateblankCell(false));
            //Tablebody1H.AddCell(CreateCell2(false, "Folio:" + cotizacion_.Id, BaseColor.WHITE, _FontHead5, false, 2));
            //Tablebody1H.AddCell(CreateblankCell(false));
            W.ForEach(v =>
            {
                Tablebody1H.AddCell(CreateCell2(false, "Nota: La vigencia de la propuesta es de " + v.Expiracion + " " + "días naturales", BaseColor.WHITE, _FontHead5, false, 2));
            });
            Tablebody1H.AddCell(CreateblankCell(false));
            doc.Add(Tablebody1H);

            ///////////////////////////////////////////////////////////////////////////////////









            doc.NewPage();

            PdfPCell logo3 = new PdfPCell();
            logo1.BorderWidth = 0;
            logo1.BorderWidthBottom = 0;
            logo1.BorderWidthTop = 0;
            logo1.BorderWidthLeft = 0;
            logo1.BorderWidthRight = 0;
            logo1.Border = 0;
            Image img3 = Image.GetInstance(ruta2);
            //img2.ScaleAbsolute(100f, 100f);
            // img2.ScaleToFit(50f, 50f);
            logo1.HorizontalAlignment = Element.ALIGN_LEFT;


            PdfPCell logo4 = new PdfPCell();
            logo2.BorderWidth = 0;
            logo2.BorderWidthBottom = 0;
            logo2.BorderWidthTop = 0;
            logo2.BorderWidthLeft = 0;
            logo2.BorderWidthRight = 0;
            logo2.Border = 0;
            Image img4 = Image.GetInstance(ruta);
            logo2.HorizontalAlignment = Element.ALIGN_RIGHT;


            iTextSharp.text.Image st = iTextSharp.text.Image.GetInstance(ruta2);
            st.ScaleToFit(50f, 50f);
            st.SetAbsolutePosition(38f, 700f);
            doc.Add(st);

            iTextSharp.text.Image sr = iTextSharp.text.Image.GetInstance(ruta);
            sr.ScaleToFit(100f, 100f);
            sr.SetAbsolutePosition(480f, 700f);
            doc.Add(sr);



            Paragraph saltoDeLinea28 = new Paragraph("                                                                                                                                                                                                                                                                                                                                                                                   ");
            doc.Add(saltoDeLinea28);
            Paragraph saltoDeLinea29 = new Paragraph("                                                                                                                                                                                                                                                                                                                                                                                   ");
            doc.Add(saltoDeLinea29);


            PdfPTable TableHeader22 = new PdfPTable(1);
            TableHeader22.WidthPercentage = 100;
            //TableHeader22.AddCell(CreateCell4(false, false, false, false, false, (P.nombre + " " + P.otro_nombre + " " + P.primer_apellido + " " + P.segundo_apellido).ToUpper(), BaseColor.WHITE, _FontHead1, true));
            TableHeader22.AddCell(CreateCell4(false, false, false, false, false, "", BaseColor.WHITE, _FontHead1, true));
            TableHeader22.AddCell(CreateblankCell(false));
            doc.Add(TableHeader22);


            PdfPTable Tablebody1A1 = new PdfPTable(2);
            Tablebody1A1.WidthPercentage = 100;
            Tablebody1A1.SpacingAfter = 10;
            Tablebody1A1.AddCell(CreateCell4(false, false, false, false, false, "DEPARTAMENTO ", BaseColor.WHITE, _FontHead1, true));
            Tablebody1A1.AddCell(CreateCell3(false, false, false, false, false, " " + datosDep.NumDepartamento, BaseColor.WHITE, _FontHead41, true));
            doc.Add(Tablebody1A1);


            PdfPTable Tablebody1A2 = new PdfPTable(1);
            Tablebody1A2.WidthPercentage = 100;
            Tablebody1A2.SpacingAfter = 10;
            Tablebody1A2.AddCell(CreateCell4(false, false, false, false, false, " Espacios departamento: ", BaseColor.WHITE, _FontHead1, true));
            De.ForEach(r => {
                ;
                Tablebody1A2.AddCell(CreateCell4(false, false, false, false, false, " Número de plantas" + "                   " + r.NumeroNiveles, BaseColor.WHITE, _FontHead41, true));

            });
            var espacios = soft.GetEspaciosDepartamento(E.clv_Departamento).GroupBy(i => i.nombre)
                .Select(group => new {
                    Nombre = group.Key,
                    Cantidad = group.Count()
                }).ToList();



            espacios.ForEach(a =>
            {


                if (a.Cantidad == 1)
                {
                    Tablebody1A2.AddCell(CreateCell4(false, false, false, false, false, " " + a.Nombre, BaseColor.WHITE, _FontHead41, true));

                }
                if (a.Cantidad > 1)
                {


                    Tablebody1A2.AddCell(CreateCell4(false, false, false, false, false, " " + a.Nombre + "s" + "                                         " + a.Cantidad, BaseColor.WHITE, _FontHead1, true));

                }

            });
            doc.Add(Tablebody1A2);
            PdfPTable TableHeaderA7 = new PdfPTable(1);
            TableHeaderA7.WidthPercentage = 100;
            TableHeaderA7.AddCell(CreateCell4(false, false, false, false, false, O.frasePublicitaria, BaseColor.WHITE, _FontHead1, true));
            TableHeaderA7.AddCell(CreateblankCell(false));
            doc.Add(TableHeaderA7);




            PdfPTable TableHeader7 = new PdfPTable(1);
            TableHeader7.WidthPercentage = 100;

            TableHeader7.AddCell(CreateCell4(false, false, false, false, false, "AMENITIES:  ", BaseColor.WHITE, _FontHead1, true));
            O.amenities.ForEach(k => {

                TableHeader7.AddCell(CreateCell4(false, false, false, false, false, " " + k.descripcion + " " + k.observacion, BaseColor.WHITE, _FontHead41, true));
            });


            TableHeader7.AddCell(CreateblankCell(false));
            doc.Add(TableHeader7);


            PdfPTable TableHeader5 = new PdfPTable(1);
            TableHeader5.WidthPercentage = 100;
            TableHeader5.AddCell(CreateCell4(false, false, false, false, false, "SERVICIOS:  ", BaseColor.WHITE, _FontHead1, true));
            O.servicio.ForEach(i => {
                TableHeader5.AddCell(CreateCell4(false, false, false, false, false, i.nombre_Servicio + " " + i.descripcion, BaseColor.WHITE, _FontHead41, true));

            });

            TableHeader5.AddCell(CreateblankCell(false));
            doc.Add(TableHeader5);



            PdfPTable TableHeader8 = new PdfPTable(1);
            TableHeader8.WidthPercentage = 100;
            TableHeader8.AddCell(CreateCell4(false, false, false, false, false, " ", BaseColor.WHITE, _FontHead3, true));
            TableHeader8.AddCell(CreateCell4(false, false, false, false, false, " ", BaseColor.WHITE, _FontHead3, true));
            TableHeader8.AddCell(CreateCell4(false, false, false, false, false, "Fecha de entrega:  " + cotizacion_.FechaEntrega.ToShortDateString(), BaseColor.WHITE, _FontHead3, true));
            TableHeader8.AddCell(CreateblankCell(false));
            doc.Add(TableHeader8);





            //Plano del departamento
            ImagenController ic = new ImagenController();
            List<logo> planos = ic.GetImagenesDepartamento(E.clv_Departamento).Where(x => x.Idtipo == 1).ToList();
            planos.ForEach(plano => {
                try
                {
                    doc.NewPage();

                    string rutaplano = url + "/ImagenesDepartamentos/" + plano.Valor;
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(rutaplano);

                    //Resize image depend upon your need
                    jpg.SetAbsolutePosition(0, 0); // set the position to bottom left corner of pdf
                    jpg.ScaleAbsolute(iTextSharp.text.PageSize.LETTER.Width, iTextSharp.text.PageSize.LETTER.Height);


                    doc.Add(jpg);
                }
                catch
                {

                }

            });


            doc.Close();
            writer.Close();


            return name;
        }


        public  string GetReporteVendedorDepApartadosVendidos(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus, string fechaInicio, string fechaFin, List<UsuarioEntity> usuarios)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                sb.Append("<torres>");
                torres.ForEach(x => {
                    sb.Append("<torre><clv>" + x + "</clv></torre> ");

                });

                sb.Append("</torres>");

                StringBuilder sb2 = new StringBuilder();
                sb2.Append("<usuarios>");
                usuarios.ForEach(x => {
                    sb2.Append("<usuario><clv>" + x.clv_usuario + "</clv></usuario> ");

                });

                sb2.Append("</usuarios>");



                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteVendedorDepApartadosVendidos";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;


                        SqlParameter par1 = new SqlParameter("@clv_proyecto", SqlDbType.Int);
                        par1.Value = clv_proyecto;
                        par1.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par1);

                        SqlParameter par2 = new SqlParameter("@xmltorre", SqlDbType.Xml);
                        par2.Value = sb.ToString();
                        par2.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par2);

                        SqlParameter par3 = new SqlParameter("@clv_estatus", SqlDbType.Int);
                        par3.Value = clv_estatus;
                        par3.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par3);

                        SqlParameter par4 = new SqlParameter("@fechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@fechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);


                        SqlParameter par6 = new SqlParameter("@usuarios", SqlDbType.Xml);
                        par6.Value = sb2.ToString();
                        par6.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par6);

                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "DETALLE";


                        reportDocument.Load(ruta + "ReporteVendedorDepApartadosVendidos.rpt");
                        reportDocument.SetDataSource(dataSet);

                        DateTime fecha_Inicio = DateTime.Parse(fechaInicio);
                        DateTime fecha_fin = DateTime.Parse(fechaFin);
                        string periodo = "Periodo desde " + fecha_Inicio.ToShortDateString() + " Hasta " + fecha_fin.ToShortDateString();

                        reportDocument.DataDefinition.FormulaFields["Periodo"].Text = "'" + periodo + "'";


                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;

        }

        




        public string GetReporteEstatusDepartamento(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                sb.Append("<torres>");
                torres.ForEach(x => {
                    sb.Append("<torre><clv>" + x + "</clv></torre> ");

                });

                sb.Append("</torres>");



                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteEstatusDepartamento";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;


                        SqlParameter par1 = new SqlParameter("@clv_proyecto", SqlDbType.Int);
                        par1.Value = clv_proyecto;
                        par1.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par1);

                        SqlParameter par2 = new SqlParameter("@xmltorre", SqlDbType.Xml);
                        par2.Value = sb.ToString();
                        par2.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par2);

                        SqlParameter par3 = new SqlParameter("@clv_estatus", SqlDbType.Int);
                        par3.Value = clv_estatus;
                        par3.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par3);






                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "DETALLE";


                        reportDocument.Load(ruta + "ReporteEstatusDepartammento.rpt");
                        reportDocument.SetDataSource(dataSet);




                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }



        public string GetReporteResumenEstatusDepartamento(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                sb.Append("<torres>");
                torres.ForEach(x => {
                    sb.Append("<torre><clv>" + x + "</clv></torre> ");

                });

                sb.Append("</torres>");



                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteEstatusDepartamento";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;


                        SqlParameter par1 = new SqlParameter("@clv_proyecto", SqlDbType.Int);
                        par1.Value = clv_proyecto;
                        par1.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par1);

                        SqlParameter par2 = new SqlParameter("@xmltorre", SqlDbType.Xml);
                        par2.Value = sb.ToString();
                        par2.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par2);

                        SqlParameter par3 = new SqlParameter("@clv_estatus", SqlDbType.Int);
                        par3.Value = clv_estatus;
                        par3.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par3);






                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "DETALLE";


                        reportDocument.Load(ruta + "ReporteResumenDepartamento.rpt");
                        reportDocument.SetDataSource(dataSet);




                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }


        public string GetReporteDepartamentosApartadosVendidos(int? Opexportacion, int? clv_proyecto, List<string> torres, int? clv_estatus, string fechaInicio, string fechaFin)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                sb.Append("<torres>");
                torres.ForEach(x => {
                    sb.Append("<torre><clv>" + x + "</clv></torre> ");

                });

                sb.Append("</torres>");



                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteDepartamentosApartadosVendidos";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;


                        SqlParameter par1 = new SqlParameter("@clv_proyecto", SqlDbType.Int);
                        par1.Value = clv_proyecto;
                        par1.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par1);

                        SqlParameter par2 = new SqlParameter("@xmltorre", SqlDbType.Xml);
                        par2.Value = sb.ToString();
                        par2.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par2);

                        SqlParameter par3 = new SqlParameter("@clv_estatus", SqlDbType.Int);
                        par3.Value = clv_estatus;
                        par3.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par3);

                        SqlParameter par4 = new SqlParameter("@fechaInicio", SqlDbType.DateTime);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@fechaFin", SqlDbType.DateTime);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);









                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "listaReporteDepartamentosApartadosVendidos";


                        reportDocument.Load(ruta + "ReporteEstatusDepartammentoVendiosFecha.rpt");
                        reportDocument.SetDataSource(dataSet);




                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }


        public string GetReporteListadoClientes(int? Opexportacion,List<UsuarioEntity> usuarios, string fecha_inicio, string fecha_fin, int? clv_estatus)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                sb.Append("<usuarios>");
                usuarios.ForEach(x => {
                    sb.Append("<usuario><clv>" + x.clv_usuario + "</clv></usuario> ");

                });

                sb.Append("</usuarios>");



                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteListadoClientes";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par2 = new SqlParameter("@xmlusuarios", SqlDbType.Xml);
                        par2.Value = sb.ToString();
                        par2.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par2);

                        SqlParameter par3 = new SqlParameter("@fecha_inicio", SqlDbType.Date);
                        par3.Value = fecha_inicio;
                        par3.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par3);

                        SqlParameter par4 = new SqlParameter("@fecha_fin", SqlDbType.Date);
                        par4.Value = fecha_fin;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@clv_estatus", SqlDbType.Int);
                        par5.Value = clv_estatus;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);


                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);
                        SoftvWCFService softv = new SoftvWCFService();
                        string titulo = "";
                        if (clv_estatus == 0)
                        {
                            titulo = "Todos los estatus";
                        }
                        else
                        {
                            titulo= softv.GetEstatusCliente().Where(x => x.Id == clv_estatus).Select(x=> x.Estatus).First();
                        }
                        DateTime fechainicio = DateTime.Parse(fecha_inicio);
                        DateTime fechafin = DateTime.Parse(fecha_fin);
                        string periodo = "Periodo desde " + fechainicio.ToShortDateString() + " Hasta " + fechafin.ToShortDateString();
                  


                        dataSet.Tables[0].TableName = "DETALLE";


                        reportDocument.Load(ruta + "ReporteClientes.rpt");
                        reportDocument.SetDataSource(dataSet);

                        reportDocument.DataDefinition.FormulaFields["Titulo"].Text = "'" + titulo + "'";
                        reportDocument.DataDefinition.FormulaFields["Periodo"].Text = "'" + periodo + "'";


                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }


        public string GetReporteListadoClientesResumen(int? Opexportacion, List<UsuarioEntity> usuarios, int? clv_estatus)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                sb.Append("<usuarios>");
                usuarios.ForEach(x => {
                    sb.Append("<usuario><clv>" + x.clv_usuario + "</clv></usuario> ");

                });

                sb.Append("</usuarios>");



                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteResumenListadoClientes";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par2 = new SqlParameter("@xmlusuarios", SqlDbType.Xml);
                        par2.Value = sb.ToString();
                        par2.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par2);

                        SqlParameter par3 = new SqlParameter("@clv_estatus", SqlDbType.Int);
                        par3.Value = clv_estatus;
                        par3.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par3);


                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "lista ReporteResumenListadoClientes";


                        reportDocument.Load(ruta + "ReporteClientesResumen.rpt");
                        reportDocument.SetDataSource(dataSet);




                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }

        public string GetReporteLlamadasVisitasPendientes(int? Opexportacion,string fechaInicio, string fechaFin,List<UsuarioEntity> usuarios, int tipoAgenda)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();

                sb.Append("<usuarios>");
                usuarios.ForEach(x => {
                    sb.Append("<usuario><clv>" + x.clv_usuario + "</clv></usuario>");
                });
                sb.Append("</usuarios>");


                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteClientesAgendados";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@fechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@fechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);

                        SqlParameter par6 = new SqlParameter("@usuarios", SqlDbType.Xml);
                        par6.Value = sb.ToString();
                        par6.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par6);

                        SqlParameter par7 = new SqlParameter("@tipoAgenda", SqlDbType.Int);
                        par7.Value = tipoAgenda;
                        par7.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par7);


                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "DETALLE";
                        


                        reportDocument.Load(ruta + "ReporteLllamadasVisitasPendientes.rpt");
                        reportDocument.SetDataSource(dataSet);
                        reportDocument.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape;

                        DateTime fecha_Inicio = DateTime.Parse(fechaInicio);
                        DateTime fecha_fin = DateTime.Parse(fechaFin);
                        string periodo = "Periodo desde " + fecha_Inicio.ToShortDateString() + " Hasta " + fecha_fin.ToShortDateString();

                        reportDocument.DataDefinition.FormulaFields["Periodo"].Text = "'" + periodo + "'";
                        if (Opexportacion == 1)
                        {
                            
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }



        public string GetReporteLlamadasVisitasEjecutadas(int? Opexportacion, string fechaInicio, string fechaFin, List<UsuarioEntity> usuarios, int tipoAgenda)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                sb.Append("<usuarios>");
                usuarios.ForEach(x => {
                    sb.Append("<usuario><clv>" + x.clv_usuario + "</clv></usuario>");
                });
                sb.Append("</usuarios>");


                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteClientesAgendadasEjecutadas";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@fechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@fechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);

                        SqlParameter par6 = new SqlParameter("@usuarios", SqlDbType.Xml);
                        par6.Value = sb.ToString();
                        par6.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par6);

                        SqlParameter par7 = new SqlParameter("@tipoAgenda", SqlDbType.Int);
                        par7.Value = tipoAgenda;
                        par7.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par7);


                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "DETALLE";


                        
                        reportDocument.Load(ruta + "ReporteLllamadasVisitasEjecutadas.rpt");
                        reportDocument.SetDataSource(dataSet);
                        reportDocument.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape;

                        DateTime fecha_Inicio = DateTime.Parse(fechaInicio);
                        DateTime fecha_fin = DateTime.Parse(fechaFin);
                        string periodo = "Periodo desde " + fecha_Inicio.ToShortDateString() + " Hasta " + fecha_fin.ToShortDateString();

                        reportDocument.DataDefinition.FormulaFields["Periodo"].Text = "'" + periodo + "'";



                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }


        public string GetReporteIngresos(int? Opexportacion, string fechaInicio, string fechaFin, int usuarios)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                //sb.Append("<usuarios>");
                //usuarios.ForEach(x => {
                //    sb.Append("<usuario><clv>" + x.clv_usuario + "</clv></usuario> ");

                //});

                //sb.Append("</usuarios>");


                
                          
                            
                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteIngresos";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@FechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@FechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);

                        SqlParameter par6 = new SqlParameter("@usuarios", SqlDbType.Int);
                        par6.Value = usuarios;
                        par6.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par6);



                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "listaReporteIngresos";
                        dataSet.Tables[1].TableName = "Pago_Efectivo";
                        dataSet.Tables[2].TableName = "Pago_targeta";
                        dataSet.Tables[3].TableName = "Pago_Cheque";
                        dataSet.Tables[4].TableName = "Total";

                        reportDocument.Load(ruta + "ReporteIngresos.rpt");
                        reportDocument.SetDataSource(dataSet);

                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }



        public string GetReporteClientesVencidos(int? Opexportacion, string fechaInicio, string fechaFin)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "DameClientesVencidos";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@FechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@FechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);

                      


                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "ClientesVencidos";
                      

                        reportDocument.Load(ruta + "ReporteClientesVencidos.rpt");
                        reportDocument.SetDataSource(dataSet);

                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }


        public string GetReporteClientesPorVencer(int? Opexportacion, string fechaInicio, string fechaFin)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "DameProximoClientesVencidosReporte";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@FechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@FechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);




                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "ClientesPorVencer";


                        reportDocument.Load(ruta + "ReporteClientesPorVencer.rpt");
                        reportDocument.SetDataSource(dataSet);

                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }


        public string GetReporteEstadoCuentaEnviado(int? Opexportacion, string fechaInicio, string fechaFin)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "DameEstadoDecuentaEnviado";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@fechainicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@fechafin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);




                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "EstadoCuentaenviados";


                        reportDocument.Load(ruta + "EstadoCuentaEnviado.rpt");
                        reportDocument.SetDataSource(dataSet);

                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }



        public string GetReporteIngresosPorCobrar(int? Opexportacion, string fechaInicio, string fechaFin, List<conceptosDePagoEntity> concepto)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                sb.Append("<conceptos>");
                concepto.ForEach(x => {
                    sb.Append("<concepto><clv>" + x.Id + "</clv></concepto> ");

                });

                sb.Append("</conceptos>");





                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteIngresosPorCobrar";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@FechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@FechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);

                        SqlParameter par6 = new SqlParameter("@concepto", SqlDbType.Xml);
                        par6.Value = sb.ToString();
                        par6.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par6);



                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);
                        dataSet.Tables[0].TableName = "listaReporteIngresosPorCobrar";
                        dataSet.Tables[1].TableName = "Total_Mensualidad";
                        dataSet.Tables[2].TableName = "Total_Escrituracion";

                        reportDocument.Load(ruta + "ReporteIngresosPorPagar.rpt");
                        reportDocument.SetDataSource(dataSet);

                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }


        public string GetReporteGeneralIngresos(int? Opexportacion, string fechaInicio, string fechaFin)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteGeneralIngresos";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@FechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@FechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);

                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);
              
                        dataSet.Tables[0].TableName = "listaReporteIngresosGeneral";
                        dataSet.Tables[1].TableName = "Total_PagoEfectivo";
                        dataSet.Tables[2].TableName = "Total_PagoTargeta";
                        dataSet.Tables[3].TableName = "Total_PagoCheque";
                        dataSet.Tables[4].TableName = "Total_PagoTotal";

                        reportDocument.Load(ruta + "ReporteGeneralDeIngresos.rpt");
                        reportDocument.SetDataSource(dataSet);

                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }




        public Byte[] GeneraQr(String data)
        {
            {

                Byte[] imgQr = null;
                try
                {
                    QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
                    QrCode qrCode = new QrCode();
                    qrEncoder.TryEncode(data, out qrCode);

                    GraphicsRenderer renderer = new GraphicsRenderer(new FixedCodeSize(300, QuietZoneModules.Zero), Brushes.Black, Brushes.White);

                    MemoryStream ms = new MemoryStream();
                    renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, ms);
                    var imageTemporal = new Bitmap(ms);
                    var imagen = new Bitmap(imageTemporal, new Size(new Point(200, 200)));
                    //imagen.Save("imagen.png", ImageFormat.Png);
                    Byte[] imgbites;
                    imgbites = ImageToByte2(imagen);
                    imgQr = imgbites;
                    return imgQr;
                }
                catch (Exception ex)
                {

                }
                //return imgBarCode;
                return imgQr;


            }
        }
        public Byte[] ImageToByte2(System.Drawing.Image img)
        {
            Byte[] byteArray;
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                stream.Close();
                byteArray = stream.ToArray();
            }
            return byteArray;
        }



        public string GetQrPago(int? Opexportacion, String CadenaQr)
        {
            string result = "";
            Byte[] imgQr = GeneraQr(CadenaQr);

            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "GetQr";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@imgQr", SqlDbType.Image);
                        par4.Value = imgQr;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);



                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "Qr";
       


                        reportDocument.Load(ruta + "ReporteQr.rpt");
                        reportDocument.SetDataSource(dataSet);

                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }



        public string GetTicket(int? Opexportacion, int? IdDetalle)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "TicketPago";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par4 = new SqlParameter("@IdDetalle", SqlDbType.Int);
                        par4.Value = IdDetalle;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                       

                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "listaTicket";
                        dataSet.Tables[1].TableName = "Total_PagoEfectivo";
                        dataSet.Tables[2].TableName = "Total_PagoTargeta";
                        dataSet.Tables[3].TableName = "Total_PagoCheque";
                        dataSet.Tables[4].TableName = "Total_PagoTotal";

                       

                        reportDocument.Load(ruta + "Ticket.rpt");
                        reportDocument.SetDataSource(dataSet);

                        if (Opexportacion == 1)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }

        public string GetReporteTarea(int? Opexportacion, int? clv_comprador, string fechaInicio, string fechaFin)
        {
            string result = "";


            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema1/";
                string name = "";

                if (Opexportacion == 1)
                {
                    name = Guid.NewGuid().ToString() + ".pdf";
                }
                else if (Opexportacion == 2)
                {
                    name = Guid.NewGuid().ToString() + ".xls";
                }


                string fileName = apppath + "/Reportes/" + name;

                StringBuilder sb = new StringBuilder();
                ReportDocument reportDocument = new ReportDocument();
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    {
                        string sql = "";
                        sql = "ReporteTareas";

                        SqlCommand command = new SqlCommand(sql, connection);
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        SqlParameter par3 = new SqlParameter("@clv_comprador", SqlDbType.Int);
                        par3.Value = clv_comprador;
                        par3.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par3);

                        SqlParameter par4 = new SqlParameter("@fechaInicio", SqlDbType.Date);
                        par4.Value = fechaInicio;
                        par4.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par4);

                        SqlParameter par5 = new SqlParameter("@fechaFin", SqlDbType.Date);
                        par5.Value = fechaFin;
                        par5.Direction = ParameterDirection.Input;
                        command.Parameters.Add(par5);

                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        DataSet dataSet = new DataSet();
                        dataAdapter.Fill(dataSet);

                        dataSet.Tables[0].TableName = "Tareas";

                        reportDocument.Load(ruta + "ReporteTareas.rpt");
                        reportDocument.SetDataSource(dataSet);
                        reportDocument.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape;

                        DateTime fecha_Inicio = DateTime.Parse(fechaInicio);
                        DateTime fecha_fin = DateTime.Parse(fechaFin);
                        string periodo = "Periodo desde " + fecha_Inicio.ToShortDateString() + " Hasta " + fecha_fin.ToShortDateString();

                        reportDocument.DataDefinition.FormulaFields["Periodo"].Text = "'" + periodo + "'";
                        if (Opexportacion == 1)
                        {

                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                        }
                        else if (Opexportacion == 2)
                        {
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                        }


                    }
                    result = name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }

            }
            return result;
        }




        public string GetAcepataCompra (int? Op, int? cotizacion)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string name = "";
            name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            string ruta = apppath + "/Logos/oh!-live.jpg";
            string ruta2 = apppath + "/Logos/Hi.png";

            iTextSharp.text.Font _FontHead1A = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead1 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead5 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead3 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead4 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font _FontHead6 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            //cell = new PdfPCell() { Border = 0, BorderWidthBottom = 1, VerticalAlignment = Element.ALIGN_MIDDLE, HorizontalAlignment = Element.ALIGN_CENTER };


            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
            doc.Open();

            PdfContentByte cb = writer.DirectContent;

            //Linea Divisora                      
            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(2.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 3)));

            CotizacionController cotizacionCtrl = new CotizacionController();
            UsuarioController usuarioController = new UsuarioController();

            CotizacionEntity cotizacion_ = cotizacionCtrl.GetCotizacionById(cotizacion);
          
            SoftvWCFService soft = new SoftvWCFService();
            InfoClientes P = soft.GetClienteById(cotizacion_.Clv_cliente);
            EstatusDepartamentoEntity E = soft.GetEstatusDepartamento(cotizacion_.IdDetalle);
            ProyectoEntity O = soft.GetProyectoById(E.clv_proyecto);
            List<ConfiguracionEntity> W = soft.GetConfiguracionSistema();
            DatosDepCotizacionEntity datosDep = soft.getDatosDepCotizacion(cotizacion_.IdDetalle);

            PdfPCell logo1 = new PdfPCell();
            logo1.BorderWidth = 0;
            logo1.BorderWidthBottom = 0;
            logo1.BorderWidthTop = 0;
            logo1.BorderWidthLeft = 0;
            logo1.BorderWidthRight = 0;
            logo1.Border = 0;
            Image img2 = Image.GetInstance(ruta2);
            //img2.ScaleAbsolute(100f, 100f);
            // img2.ScaleToFit(50f, 50f);
            logo1.HorizontalAlignment = Element.ALIGN_LEFT;


            PdfPCell logo2 = new PdfPCell();
            logo2.BorderWidth = 0;
            logo2.BorderWidthBottom = 0;
            logo2.BorderWidthTop = 0;
            logo2.BorderWidthLeft = 0;
            logo2.BorderWidthRight = 0;
            logo2.Border = 0;
            Image img = Image.GetInstance(ruta);
            logo2.HorizontalAlignment = Element.ALIGN_RIGHT;


            iTextSharp.text.Image startv = iTextSharp.text.Image.GetInstance(ruta2);
            startv.ScaleToFit(50f, 50f);
            startv.SetAbsolutePosition(38f, 700f);
            doc.Add(startv);

            iTextSharp.text.Image s = iTextSharp.text.Image.GetInstance(ruta);
            s.ScaleToFit(100f, 100f);
            s.SetAbsolutePosition(480f, 700f);
            doc.Add(s);

            Paragraph saltoDeLinea1 = new Paragraph("                                                                                                                                                                                                                                                                                                                                                                                   ");
            doc.Add(saltoDeLinea1);
            Paragraph saltoDeLinea2 = new Paragraph("                                                                                                                                                                                                                                                                                                                                                                                   ");
            doc.Add(saltoDeLinea2);
           

            PdfPTable TableHeader1 = new PdfPTable(3);
            TableHeader1.SpacingAfter = 10;
            TableHeader1.WidthPercentage = 100;
            TableHeader1.AddCell(CreateCell2(false, "", BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader1.AddCell(CreateCell2(false, "", BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader1.AddCell(CreateCell3(false, false, false, false, false, "ACEPTACIÓN DE COMPRA VENTA", BaseColor.WHITE, _FontHead4, false));
            TableHeader1.AddCell(CreateblankCell(false));
            doc.Add(TableHeader1);

            PdfPTable TableHeader1ab = new PdfPTable(1);
            TableHeader1ab.WidthPercentage = 100;
            //TableHeader1ab.AddCell(CreateCell3(false, false, false, false, false, "NOMBRE DEL CLIENTE:  " + (P.nombre + " " + P.otro_nombre + " " + P.primer_apellido + " " + P.segundo_apellido).ToUpper(), BaseColor.WHITE, _FontHead3, false));
            TableHeader1ab.AddCell(CreateCell3(false, false, false, false, false, "DESARROLLO (UBICACIÓN): " + "Olivar de los Padres número Mil Diecisiete, Colonia Olivar de los Padres número Mil Diecisiete, Colonia Olivar de Los Padres, Delegación Álvaro Obregón, Código Postal 01780, Ciudad de México.", BaseColor.WHITE, _FontHead3, false));
            TableHeader1ab.AddCell(CreateCell3(false, false, false, false, false, "DEPARTAMENTO NÚMERO:  " + datosDep.NumDepartamento, BaseColor.WHITE, _FontHead3, false));
            //TableHeader1ab.AddCell(CreateCell4(false, false, false, false, false, "Precio Total:  " + (cotizacion_.precios.PrecioTotal).ToString("C"), BaseColor.WHITE, _FontHead4, true));
            TableHeader1ab.AddCell(CreateblankCell(false));
            doc.Add(TableHeader1ab);

            PdfPTable TableHeader1ad = new PdfPTable(1);
            TableHeader1ad.WidthPercentage = 100;
            TableHeader1ad.AddCell(CreateCell4(false, false, false, false, false, "Precio Total:  " + (cotizacion_.precios.PrecioTotal).ToString("C") + "                "+ "                  "+ "CRÉDITO ( )" + "     " +"CONTADO ( )", BaseColor.WHITE, _FontHead4, true));
            TableHeader1ad.AddCell(CreateblankCell(false));
            doc.Add(TableHeader1ad);

            PdfPTable TableHeader1ai = new PdfPTable(2);
            TableHeader1ai.WidthPercentage = 100;
            TableHeader1ai.AddCell(CreateCell4(false, false, false, false, false, " " , BaseColor.WHITE, _FontHead4, true));
            TableHeader1ai.AddCell(CreateCell2(false, " Banco ( )", BaseColor.WHITE, _FontHead6, false, 2));
            TableHeader1ai.AddCell(CreateCell4(false, false, false, false, false, " ", BaseColor.WHITE, _FontHead4, true));
            TableHeader1ai.AddCell(CreateCell2(false, " Sofol( ) ", BaseColor.WHITE, _FontHead6, false, 2));
            TableHeader1ai.AddCell(CreateCell4(false, false, false, false, false, " ", BaseColor.WHITE, _FontHead4, true));
            TableHeader1ai.AddCell(CreateCell2(false, "INFONAVIT ( ) ", BaseColor.WHITE, _FontHead6, false, 2));
            TableHeader1ad.AddCell(CreateblankCell(false));
            doc.Add(TableHeader1ai);

            PdfPTable TableHeader2 = new PdfPTable(1);
            TableHeader2.WidthPercentage = 100;
            TableHeader2.AddCell(CreateCell4(false, false, false, false, false, " EN CASO DE QUE EL CLIENTE CUENTE CON UN CRÉDITO:", BaseColor.WHITE, _FontHead4, true));
            TableHeader2.AddCell(CreateCell2(false, "NOMBRE DEL BANCO:", BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader2.AddCell(CreateCell2(false, "EJECUTIVO: ", BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader2.AddCell(CreateCell2(false, "QUIÉN LO ATENDERÁ: ", BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader2.AddCell(CreateCell2(false, "TELÉFONO: ", BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader2.AddCell(CreateCell2(false, "CORREO ELECTRÓNICO: ", BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader2.AddCell(CreateblankCell(false));
            doc.Add(TableHeader2);

            PdfPTable TableHeader3 = new PdfPTable(2);
            TableHeader3.WidthPercentage = 100;
            TableHeader3.AddCell(CreateCell4(false, false, false, false, false, "FORMA DE PAGO:", BaseColor.WHITE, _FontHead4, true));
            TableHeader3.AddCell(CreateCell4(false, false, false, false, false, "", BaseColor.WHITE, _FontHead4, true));
            TableHeader3.AddCell(CreateCell4(false, false, false, false, false, "RESERVACIÓN:  " + cotizacion_.Reservacion.ToString("C"), BaseColor.WHITE, _FontHead5, true));
            TableHeader3.AddCell(CreateCell2(false, "FECHA DE RESERVACIÓN:  " + cotizacion_.PagoApartado, BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader3.AddCell(CreateCell4(false, false, false, false, false, "CONTRATO :$  ", BaseColor.WHITE, _FontHead5, true));
            TableHeader3.AddCell(CreateCell2(false, "FECHA DE CONTRATO  " + DateTime.Now, BaseColor.WHITE, _FontHead5, false, 2));
            TableHeader3.AddCell(CreateCell4(false, false, false, false, false, "ENGANCHE :  " + ((cotizacion_.EngancheDiferido / 100) * cotizacion_.precios.PrecioTotal).ToString("C"), BaseColor.WHITE, _FontHead5, true));
            TableHeader3.AddCell(CreateblankCell(false));
            doc.Add(TableHeader3);

            PdfPTable TableHeader1C = new PdfPTable(1);
            TableHeader1C.WidthPercentage = 100;
            TableHeader1C.SpacingAfter = 10;
            TableHeader1C.SpacingBefore = 10;
            TableHeader1C.AddCell(CreateCell4(false, false, false, false, false, "RESTO DEL ENGANCHE: ", BaseColor.WHITE, _FontHead1, true));
            doc.Add(TableHeader1C);

           





           




           


            doc.Close();
            writer.Close();


            return name;
        }








































    }
}