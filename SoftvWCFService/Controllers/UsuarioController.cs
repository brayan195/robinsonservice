﻿using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class UsuarioController
    {

        public List<UsuarioEntity> GetUsuarios()
        {
            dbHelper db = new dbHelper();
            List<UsuarioEntity> usuarios = new List<UsuarioEntity>();
            TipoVendedorController tv = new TipoVendedorController();
            try
            {
                SqlDataReader reader = db.consultaReader("select_usuario");              
                usuarios = db.MapDataToEntityCollection<UsuarioEntity>(reader).ToList();
                usuarios.ForEach(usuario => {
                    usuario.detalleVendedor = GetDetalleVendedor(usuario.clv_usuario);
                    try
                    {
                        //usuario.tipoVendedor = tv.GetTipoVendedor().Where(x => x.Id == usuario.clv_tipoVendedor).First();
                    }
                    catch
                    {                      
                    }
                    
                });
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return usuarios;
        }

        public VendedorEntity GetDetalleVendedor(int ? clv_usuario)
        {
            dbHelper db = new dbHelper();
            VendedorEntity vendedor = new VendedorEntity();
            try
            {
                db.agregarParametro("@clv_usuario", SqlDbType.Int, clv_usuario);
                SqlDataReader reader = db.consultaReader("select_vendedor");
                vendedor = db.MapDataToEntityCollection<VendedorEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
               // throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return vendedor;

        }

        public UsuarioEntity GetUsuarioById(int ? clv_usuario)
        {
            dbHelper db = new dbHelper();
            UsuarioEntity usuario = new UsuarioEntity();
            try
            {
                TipoVendedorController tv = new TipoVendedorController();
                db.agregarParametro("@clv_usuario", SqlDbType.Int, clv_usuario);
                SqlDataReader reader = db.consultaReader("select_usuario_clv");
                usuario = db.MapDataToEntityCollection<UsuarioEntity>(reader).ToList()[0];
                usuario.detalleVendedor = GetDetalleVendedor(usuario.clv_usuario);
                try
                {
                    //usuario.tipoVendedor = tv.GetTipoVendedor().Where(x => x.Id == usuario.clv_tipoVendedor).First();
                }
                catch { }
                
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return usuario;
        }

        public int ?  AddUsuario(UsuarioEntity usuario,VendedorEntity vendedor)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, usuario.nombre);
                db.agregarParametro("@correo", SqlDbType.VarChar, usuario.correo);
                db.agregarParametro("@contrasenia", SqlDbType.VarChar, usuario.contrasenia);
                db.agregarParametro("@activo", SqlDbType.Bit, usuario.activo);
                db.agregarParametro("@tipoUsuario", SqlDbType.Int, usuario.tipoUsuario);
                db.agregarParametro("@clv_tipoVendedor", SqlDbType.Int, usuario.clv_tipoVendedor);
                db.agregarParametro("@nombreVendedor", SqlDbType.VarChar,vendedor.nombreVendedor);
                db.agregarParametro("@rfc", SqlDbType.VarChar, vendedor.rfc);
                db.agregarParametro("@curp", SqlDbType.VarChar, vendedor.curp);
                db.agregarParametro("@celular", SqlDbType.VarChar, vendedor.celular);
                db.agregarParametro("@clv_usuario", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("insert_usuarios");
                result = int.Parse(db.diccionarioOutput["@clv_usuario"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public List<UsuarioEntity> GetVendedoresByUsuario(int ? clv_usuario)
        {

            dbHelper db = new dbHelper();
            List<UsuarioEntity> vendedores = new List<UsuarioEntity>();
            try
            {
                db.agregarParametro("@clv_usuario", SqlDbType.Int, clv_usuario);
                SqlDataReader reader = db.consultaReader("ObtieneVendedorPorUsuario");
                vendedores = db.MapDataToEntityCollection<UsuarioEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                // throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return vendedores;

        }


        public int? EditUsuario(UsuarioEntity usuario, VendedorEntity vendedor)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, usuario.nombre);
                db.agregarParametro("@correo", SqlDbType.VarChar, usuario.correo);
                db.agregarParametro("@contrasenia", SqlDbType.VarChar, usuario.contrasenia);
                db.agregarParametro("@activo", SqlDbType.Bit, usuario.activo);
                db.agregarParametro("@tipoUsuario", SqlDbType.Int, usuario.tipoUsuario);
                db.agregarParametro("@clv_tipoVendedor", SqlDbType.Int, usuario.clv_tipoVendedor);
                db.agregarParametro("@nombreVendedor", SqlDbType.VarChar, vendedor.nombreVendedor);
                db.agregarParametro("@rfc", SqlDbType.VarChar, vendedor.rfc);
                db.agregarParametro("@curp", SqlDbType.VarChar, vendedor.curp);
                db.agregarParametro("@celular", SqlDbType.VarChar, vendedor.celular);
                db.agregarParametro("@clv_usuario", SqlDbType.Int, usuario.clv_usuario);
                db.agregarParametro("@clv", SqlDbType.Int, ParameterDirection.Output);
                db.consultaOutput("update_usuarios");
                result = int.Parse(db.diccionarioOutput["@clv"].ToString());
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }




    }
}