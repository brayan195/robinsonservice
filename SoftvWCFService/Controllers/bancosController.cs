﻿using SoftvWCFService.Entities;
using SoftvWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Controllers
{
    public class bancosController
    {
        public List<bancoEntity> GetBancos()
        {
            dbHelper db = new dbHelper();
            List<bancoEntity> bancos = new List<bancoEntity>();
            try
            {

                SqlDataReader reader = db.consultaReader("select_bancos");
                bancos = db.MapDataToEntityCollection<bancoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return bancos;
        }

        public int? addBanco(string nombre, bool? activo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, activo);
                db.consultaSinRetorno("insert_bancos");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }


        public int? updateBanco(int? id, string nombre, bool activo)
        {
            dbHelper db = new dbHelper();
            int result = 0;
            try
            {
                db.agregarParametro("@id", SqlDbType.Int, id);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, activo);
                db.consultaSinRetorno("update_banco");
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return result;
        }

        public bancoEntity GetBancoById(int? id)
        {
            dbHelper db = new dbHelper();
            bancoEntity usuario = new bancoEntity();

            try
            {
                TipoVendedorController tv = new TipoVendedorController();
                db.agregarParametro("@id", SqlDbType.Int, id);
                SqlDataReader reader = db.consultaReader("select_bancoById");
                usuario = db.MapDataToEntityCollection<bancoEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }
            return usuario;
        }

    }
}